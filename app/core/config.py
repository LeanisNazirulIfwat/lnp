from decimal import Decimal
from functools import lru_cache
from typing import Any, Dict, Optional
import os
from pydantic import BaseSettings, PostgresDsn, validator


class Settings(BaseSettings):
    #############################################
    # MAM variables
    #############################################
    PROJECT_NAME: str = "FastAPI LNP SYSTEM"
    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = "W9SHTQxU6HMfK4TvrtHRJfUksGTkswLiV445qvM0UEXSb3CgZBQ9pEpOUF50PgF"
    API_SECRET_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
    SWITCH_SECRET_KEY = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
    ADMIN_API_KEY = "fe7ebc72-23bb-4bd5-8366-670957ce244d"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30
    USERS_OPEN_REGISTRATION: str = 'user@user.com'
    SECURE_PAY_TOKEN: str = "00aciWhz4I6q/b/zt1d0qByCgyzt8pLqzxSzYw7u9iPdqSHje2xjJG9pb1uSxeiOsHXhJrEXhOKcgsC6SdFrx84YrBekcPvMC7sO154BHouzVQT1TKxjp9m4lda3Rk7U8JPHyXOSLU19UrGRlT9Zg6Zg1fUgrtHceTa14Vtl/Hkp2zg=U2FsdGVkX1/FA7yZ71+736Te3w+SsKbJcMot9bzrOU90R+nhDx3YqNonRTeeGs8n"
    ONBOARD_AUTH_KEY: str = "MM-1D11AFAD55-LNP|67a7cbfd-e556-455f-8ab5-67f889e17b88|2559740a3ea079577b388ccb9d5c3c2d4db8d33e460f31e25b3d793de2f97ab3ed3ea756429bcf74a73096dae6e001f311bbbf4e832b3a63774f3461e6736907"
    ADMIN_COLLECTION_UUID: str = "LEANX-CL"
    LEAN_X_TAX: float = 0.06
    PREFUND_POOL_NAME: str = "ADMIN_DEFAULT_ACCOUNT"
    PREFUND_FEE_POOL_NAME: str = "ADMIN_FEE_DEFAULT_ACCOUNT"
    PREFUND_TOPUP_CHARGES: Decimal = 0.50

    ENVIRONMENT: Optional[str]

    FIRST_SUPER_ADMIN_EMAIL: str = 'admin@admin.com.my'
    ADMIN_LEANX_EMAIL: str = 'admin@leanis.com.my'
    FIRST_SUPER_ADMIN_PASSWORD: str = 'password'
    FIRST_SUPER_ADMIN_ACCOUNT_NAME: str = 'admin'
    FIRST_SUPER_ADMIN_PHONE_NUMBER: str = '01110686400'

    DB_HOST: str = 'localhost:3306'
    DB_USER: str = 'root'
    DB_PASSWORD: str = ''
    DB_NAME: str = 'leanpay'
    # DB_NAME: str = 'leanpay_uat'

    #############################################
    # twilio variables
    #############################################

    TWILIO_ACCOUNT_SID: str = "AC9ae59364f31beacc128adc7a5c3319f1"
    TWILIO_LEAN_ACCOUNT_SID: str = "ACa2b8237ab0e1100c7d637f50538bb01f"
    TWILIO_AUTH_TOKEN: str = "a34a45ee61e25b4323b1258808611947"
    TWILIO_LEAN_AUTH_TOKEN: str = "170f82132379f5c0d23506bdc263874b"
    TWILIO_MESSAGING_SERVICE_ID: str = "MGbb04076148bf5ba3435d96e5b610db1d"
    TWILIO_LEAN_MESSAGING_SERVICE_ID: str = "MG91d7e9879e2f2772f77283a7bdd53765"
    TWILIO_WHATSAPP_NUMBER: str = "+14155238886"
    TWILIO_TESTER_WHATSAPP_NUMBER: str = "+60182954574"
    TWILIO_SENDGRID_API: str = "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"
    TWILIO_LEAN_SENDGRID_API: str = "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"

    #############################################
    # Parcel Asia variables
    #############################################

    PARCEL_ASIA_DEMO_API_KEY: str = "kea8e829a817bbe655eaa74599b930e9b"
    PARCEL_ASIA_DEMO_SECRET_KEY: str = "s1e54d8183dee7af971740adfcc621253"
    PARCEL_ASIA_DEMO_URI: str = "https://demo.myparcelasia.com/apiv2/"
    PARCEL_ASIA_URI: str = "https://app.myparcelasia.com/apiv2/"

    #############################################
    # sql alchemy variables
    #############################################
    # SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://root:root@10.10.8.30:3306/{DB_NAME}'
    # SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://leanpay:root@192.168.0.105:3306/{DB_NAME}'
    SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://leanpay:root@10.10.8.51:3306/{DB_NAME}'
    # SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://leanpay:Le#n1s13a@10.10.8.19:3306/{DB_NAME}'
    # SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://leanpay:QYzndWXXKB@10.10.18.30:3306/{DB_NAME}'
    # SQLALCHEMY_DATABASE_URI: str = f'mysql+pymysql://root@localhost:3306/{DB_NAME}'

    #   FOR SQL LITE
    FILE_PATH = os.path.abspath(os.getcwd()) + "\lnp.db"
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///./lnp.db'
    SQLALCHEMY_DATABASE_URI_LINUX = 'sqlite:///' + FILE_PATH

    #############################################
    # status record variables
    #############################################
    RECORD_ACTIVE = 1
    RECORD_NOT_ACTIVE = 2
    RECORD_PENDING = 3
    RECORD_DELETE = 4

    # transaction status
    TRANSACTION_PENDING = 1
    TRANSACTION_SUCCESS = 2
    TRANSACTION_FAILED = 3

    # SYSTEM IDENTITY
    SUPER_ADMIN_SYSTEM = 1
    MERCHANT_SYSTEM = 2
    WHITE_LABEL_SYSTEM = 3
    ADMIN_SYSTEM = 4
    MASTER_MERCHANT = 5

    #############################################
    # payment status variables
    #############################################
    PENDING_PAYMENT = 1
    PAID_PAYMENT = 2
    FAILED_PAYMENT = 3
    DUE_PAYMENT = 4

    #############################################
    # subscription plan variables
    #############################################
    BASIC = 1
    STANDARD = 2
    ENTERPRISE = 3

    #############################################
    # portal variables
    #############################################
    PORTAL_URL = "https://stag-onboard.leanpay.my/api/v1"
    # PORTAL_URL = "http://dev.onboard.leanpay.my/api"
    PORTAL_SIGNATURE = "9f73371d-f8d3-442d-b5fb-93898a9f4dc9"
    REGISTER_PORTAL_URL = f"{PORTAL_URL}/portalRetriggerMerchantRegistration"
    SUCCESS_MERCHANT_PORTAL_REGISTER_CALLBACK_URL = f"{PORTAL_URL}/portalSuccessMerchantRegistrationCallback"
    PORTAL_FORGOT_PASSWORD = f"{PORTAL_URL}/portalForgotPassword"
    PORTAL_UPDATE_PASSWORD = f"{PORTAL_URL}/portalUpdatePassword"
    PORTAL_CHANGE_PASSWORD_TOKEN = f"{PORTAL_URL}/portalChangePassword"
    PORTAL_GET_PENDING_FORM = f"{PORTAL_URL}/getPendingForm"
    PORTAL_GET_FORM_LIST = f"{PORTAL_URL}/formList"
    PORTAL_GET_CONSTANT = f"{PORTAL_URL}/systemconstants"
    PORTAL_APPROVE_FORM = f"{PORTAL_URL}/approveForm"
    PORTAL_REJECT_FORM = f"{PORTAL_URL}/rejectForm"
    PORTAL_DOWNLOAD_FILE = f"{PORTAL_URL}/adminDownloadFile"
    ONBOARDING_CALLBACK = f"{PORTAL_URL}/onboardingCallback"

    #############################################
    # switch variables
    #############################################
    # SWITCH_URL
    SWITCH_UAT_URL: str = "https://stag.switch.leanpay.my/"
    API_UAT_URL: str = "https://stag-api.leanpay.my/"
    # SWITCH_UAT_URL: str = "https://stag.switch.leanpay.my/"
    # SWITCH_UAT_URL: str = "switch.leanx.dev"
    # SWITCH_UAT_URL: str = "switch.leanx.io"
    # SWITCH_UAT_URL: str = "http://10.10.9.10/"
    SWITCH_DEV_URL: str = "https://stag.switch.leanpay.my/"

    #############################################
    # portal url
    #############################################
    PORTAL_UPLOAD_URL: str = "https://stag-portal.leanpay.my/pay/invoice/"
    PORTAL_BILL_URL: str = "https://stag-portal.leanpay.my/"
    CALLBACK_URL_LEANPAY: str = "https://stag-api.leanpay.my/api/v1/callback-url/call-encrypt"
    PUBLIC_CALLBACK_URL_LEANPAY: str = "https://stag-api.leanpay.my/api/v1/callback-url/callback-redirect"
    REDIRECT_URL_LEANPAY: str = "https://stag-portal.leanpay.my/pay/thankyou?"

    #############################################
    # Onboarding Variable
    #############################################


    #############################################
    # digital ocean S3 variables
    #############################################
    S3_ACCESS_ID = 'DO00D9MUBJAH3RLL3EEF'
    S3_SECRET_KEY = 'kGXcmSWQ9o+XKss8XJfOI7ct7w4nwlqkrIsyb7GxvPw'

    class Config:
        case_sensitive = True
        env_file = ".env"


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()
