import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text
from sqlalchemy.orm import relationship

class PaymentSetting(Base):
    """
    Database Model for an payment setting
    """
    __tablename__ = "payment_settings"

    id = Column(Integer, primary_key=True, index=True)
    api_interface = Column(String(255))
    fpx_bank_selection = Column(String(255))
    account_id = Column(Integer)

    payment_setting_collection = relationship("Collection", back_populates="collection_payment_setting")
    payment_setting_store = relationship("Store", back_populates="store_payment_setting")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
