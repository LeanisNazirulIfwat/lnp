import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Account(Base):
    """
    Database model for an account
    """

    id = Column(
        Integer,
        primary_key=True, index=True
    )
    name = Column(String(255), index=True)
    description = Column(String(255))
    is_active = Column(Boolean(), default=True)
    account_name = Column(String(255))
    merchant_id = Column(String(255))
    account_type = Column(String(255))
    url = Column(String(255))
    business_location = Column(String(255))
    business_proof = Column(String(255))
    parent_key = Column(String(255))
    bank_type = Column(Integer)
    enable_customer_pay = Column(Boolean(), default=False)
    subscription_plan_id = Column(Integer, ForeignKey('subscriptions.id'))
    record_status = Column(Integer, default=1)
    current_subscription_ends = Column(DateTime, default=datetime.datetime.today())
    current_subscription_time_period = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
    user_id = Column(
        ForeignKey("users.id")
    )
    system_preference_id = Column(Integer, ForeignKey('system_preference.id'))
    # business_owner_details_id = Column(Integer, ForeignKey('business_owner_details.id'))
    company_details_id = Column(Integer, ForeignKey('company_details.id'))

    # FK relationship
    users = relationship("User", back_populates="account")
    subscription_plan = relationship("Subscription", back_populates="account_subscription")
    account_collection = relationship("Collection", back_populates="collection_account")
    account_domain = relationship("Domain", back_populates="domain_account")
    account_bank = relationship("Bank", back_populates="bank_account")
    account_system = relationship("SystemPreference", back_populates="system_account")
    account_sub_payment = relationship("SubscriptionPayment", back_populates="sub_payment_account")
    account_company_details = relationship("CompanyDetail", back_populates="company_details_account")
    # account_business_owner_details = relationship("BusinessOwnerDetail",
    #                                               back_populates="business_owner_details_account")
