import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class Pool(Base):
    """
    Database model for a Pool Management
    """

    id = Column(Integer, primary_key=True, index=True)
    pool_name = Column(String(255))
    value = Column(Numeric(precision=10, scale=2))
    account_id = Column(Integer)
    api_key = Column(Text(4294000000))
    pool_type = Column(Integer)

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
