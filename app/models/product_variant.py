import datetime
from decimal import Decimal

from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, BigInteger, Text
from sqlalchemy.orm import relationship


class ProductVariant(Base):

    __tablename__ = "product_variants"

    """
    Database model for a tier pricing
    """

    id = Column(Integer, primary_key=True, index=True)
    options = Column(String(255))
    code = Column(String(255))
    quantity = Column(BigInteger)
    min_quantity = Column(BigInteger)
    max_quantity = Column(BigInteger)
    price = Column(Numeric(precision=10, scale=2))
    custom_pricing_enable = Column(Boolean)
    custom_pricing_price = Column(Numeric(precision=10, scale=2))
    product_variant_image = Column(Text(4294000000))
    product_id = Column(Integer, ForeignKey('products.id'))


    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
