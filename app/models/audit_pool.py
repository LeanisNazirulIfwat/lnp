import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import DATETIME


class AuditPool(Base):
    """
    Database model for a Pool Management
    """
    __tablename__ = "audit_pools"

    id = Column(Integer, primary_key=True, index=True)
    previous_value = Column(Numeric(precision=10, scale=2))
    current_value = Column(Numeric(precision=10, scale=2))
    charges = Column(Numeric(precision=10, scale=2))
    account_id = Column(Integer)
    api_key = Column(Text(4294000000))
    pool_id = Column(Integer)
    invoice_no = Column(Text(4294000000))
    invoice_status = Column(String(255))
    data = Column(Text(4294000000))
    pool_type = Column(Integer)
    transaction_type = Column(String(255))
    description = Column(Text(4294000000))


    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    precision_created_at = Column(DATETIME(fsp=6), default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
