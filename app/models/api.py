import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class Api(Base):
    """
    Database model for a shipment
    """

    id = Column(Integer, primary_key=True, index=True)
    interface_name = Column(String(255))
    callback_url = Column(String(255))
    redirect_url = Column(String(255))
    cancel_url = Column(String(255))
    time_out_url = Column(String(255))
    payment_mode = Column(String(255))
    fpx_bank_selection = Column(String(255))
    payment_model = Column(String(255))

    uuid = Column(String(255))
    hash_key = Column(Text(4294000000))
    account_id = Column(Integer)

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today)
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today,
        onupdate=datetime.datetime.today,
    )
