import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class Shipment(Base):
    """
    Database model for an shipment
    """
    __tablename__ = "shipments"

    id = Column(Integer, primary_key=True, index=True)
    shipment_status = Column(String(255))

    customer_id = Column(Integer, ForeignKey("customers.id"))
    account_id = Column(Integer)
    shipping_profile_id = Column(Integer, ForeignKey("shipping_profiles.id"))
    customer_bill_id = Column(Integer, ForeignKey("customer_bills.id"))
    shipping_infos = Column(Text(4294000000))
    parcel_shipment_key = Column(String(255))
    parcel_tracking_no = Column(String(255))

    shipment_customer_bill = relationship("CustomerBill", backref="shipments")
    shipment_customer = relationship("Customer", back_populates="customer_shipment")
    shipment_shipping_profile = relationship("ShippingProfile", back_populates="shipping_profile_shipment")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
