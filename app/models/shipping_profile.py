import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class ShippingProfile(Base):
    """
    Database model for a shipping profile
    """
    __tablename__ = "shipping_profiles"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    account_id = Column(Integer)

    shipping_profile_shipping_zone = relationship("ShippingZone", backref="shipping_profiles")

    shipping_profile_shipment = relationship("Shipment", back_populates="shipment_shipping_profile")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
