import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, String, Integer, Numeric, ForeignKey, Text
from sqlalchemy.orm import relationship


class Bank(Base):
    """
    Database Model for an bank
    """

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    code = Column(String(255))
    bank_branch = Column(String(255))
    description = Column(String(255))
    account_number = Column(String(255))
    bank_account_statement = Column(Text(4294000000))
    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )

    account_id = Column(Integer, ForeignKey("accounts.id"))
    bank_account = relationship("Account", back_populates="account_bank")
