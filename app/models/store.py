import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text, Boolean
from sqlalchemy.orm import relationship


class Store(Base):
    """
    Database Model for a store
    """

    id = Column(Integer, primary_key=True, index=True)
    account_id = Column(Integer, ForeignKey("accounts.id"))
    name = Column(String(255))
    base_url = Column(String(255))
    tax = Column(String(255))
    status = Column(String(255))
    banner = Column(Text(4294000000))
    short_url = Column(String(255))
    slug = Column(String(255))
    custom_field = Column(Text(4294000000))
    menu_items = Column(Text(4294000000))
    unique_id = Column(String(255))

    billing_address_enable = Column(Boolean)
    shipping_address_enable = Column(Boolean)
    shipping_rate_enable = Column(Boolean)
    discount_enable = Column(Boolean)

    product_list = Column(Text(4294000000))

    collection_id = Column(Integer, ForeignKey("collections.id"))
    domain_id = Column(Integer, ForeignKey("domains.id"))
    share_setting_id = Column(Integer, ForeignKey("share_settings.id"))
    support_detail_id = Column(Integer, ForeignKey("support_details.id"))
    payment_setting_id = Column(Integer, ForeignKey("payment_settings.id"))

    store_collection = relationship("Collection", backref="collections")
    store_domain = relationship("Domain", back_populates="domain_store")
    store_share_setting = relationship("ShareSetting", back_populates="share_setting_store")
    store_support_detail = relationship("SupportDetail", back_populates="support_detail_store")
    store_payment_setting = relationship("PaymentSetting", back_populates="payment_setting_store")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
