import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class Product(Base):
    """
    Database model for a product
    """

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    title = Column(String(255))
    code = Column(String(255))
    description = Column(Text(4294000000))
    min_purchase_quantity = Column(Integer)
    max_purchase_quantity = Column(Integer)
    price = Column(Numeric(precision=10, scale=2))
    quantity = Column(Integer)
    base_url = Column(String(255))
    short_url = Column(String(255))
    intro = Column(Text(4294000000))
    footer = Column(Text(4294000000))
    image = Column(Text(4294000000))
    weight = Column(Numeric(precision=10, scale=2))
    weight_condition = Column(Integer)
    value = Column(Integer)
    product_category_list = Column(Text(4294000000))

    limited_stock_enable = Column(Boolean)
    physical_product_enable = Column(Boolean)
    tier_pricing_enable = Column(Boolean)
    product_variant_enable = Column(Boolean)

    account_id = Column(Integer)
    product_category_id = Column(Integer, ForeignKey("product_categories.id"))

    product_product_variant = relationship('ProductVariant', backref='products')
    product_tier_pricing = relationship('TierPricing', backref='products')
    product_product_category = relationship("ProductCategory", back_populates="product_category_product")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
