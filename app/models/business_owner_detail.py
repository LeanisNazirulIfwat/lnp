import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class BusinessOwnerDetail(Base):
    """
    Database model for a shipment
    """
    __tablename__ = "business_owner_details"

    id = Column(Integer, primary_key=True, index=True)
    legal_name = Column(String(255))
    individual_nric = Column(String(255))
    address_line_one = Column(Text(4294000000))
    address_line_two = Column(Text(4294000000))
    address_postcode = Column(Text(4294000000))
    address_state = Column(Text(4294000000))
    address_city = Column(Text(4294000000))
    address_country = Column(Text(4294000000))
    bank_name = Column(String(255))
    business_bank_account_number = Column(String(255))
    bank_account_statement_header = Column(Text(4294000000))
    external_account = Column(String(255))

    business_owner_detail_company_detail = relationship("CompanyDetail",
                                                        back_populates="company_detail_business_owner_detail")
    # business_owner_details_account = relationship("Account", back_populates="account_business_owner_details")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
