import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class CustomerBillItem(Base):
    """
    Database model for a CustomerBill item
    """
    __tablename__ = "customer_bill_items"

    id = Column(Integer, primary_key=True, index=True)
    product_id = Column(Integer)
    customer_bill_id = Column(Integer)
    quantity = Column(Integer)
    amount = Column(Numeric(precision=10, scale=2))
    total_amount = Column(Numeric(precision=10, scale=2))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
