import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric
from sqlalchemy.orm import relationship


class Discount(Base):
    """
    Database model for a shipment
    """

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    account_usage_limit = Column(String(255))

    rule_id = Column(Integer, ForeignKey("rules.id"))
    account_id = Column(Integer)
    coupon_uuid = Column(String(255))

    discount_rule = relationship("Rule", back_populates="rule_discount")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today)
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today,
        onupdate=datetime.datetime.today,
    )
