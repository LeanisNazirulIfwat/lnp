import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text, Boolean
from sqlalchemy.orm import relationship


class PaymentCollectionChannel(Base):
    """
    Database Model for a Payment Collection Channel
    """

    __tablename__ = "payment_collection_channel"

    id = Column(Integer, primary_key=True, index=True)
    collection_id = Column(Integer, ForeignKey("collections.id"))
    name = Column(String(255))
    description = Column(Text(4294000000))
    data = Column(Text(4294000000))

    fpx_enable = Column(Boolean, default=True)
    credit_card_enable = Column(Boolean, default=False)
    ewallet_enable = Column(Boolean, default=False)
    bnpl_enable = Column(Boolean, default=False)
    paypal_enable = Column(Boolean, default=False)
    crypto_enable = Column(Boolean, default=False)

    channel_collection = relationship("Collection", backref="collections_channel")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
