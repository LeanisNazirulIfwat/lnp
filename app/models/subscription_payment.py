import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class SubscriptionPayment(Base):
    """
    Database model for a shipment
    """
    __tablename__ = "subscription_payments"

    id = Column(Integer, primary_key=True, index=True)
    account_id = Column(Integer, ForeignKey("accounts.id"))
    subscription_plan_id = Column(Integer, ForeignKey("subscriptions.id"))
    month = Column(Integer)
    year = Column(Integer)
    invoice_no = Column(String(255), unique=True)
    transaction_no = Column(String(255), unique=True)
    payment_status_id = Column(Integer)
    amount = Column(Numeric(precision=10, scale=2))

    sub_payment_account = relationship("Account", back_populates="account_sub_payment")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
