import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Boolean, Text
from sqlalchemy.orm import relationship

class Collection(Base):
    """
    Database Model for an collection
    """

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    uuid = Column(String(255))
    type = Column(String(255))
    base_url = Column(String(255))
    short_url = Column(String(255))
    min_amount = Column(Numeric(precision=10, scale=2))
    tax = Column(Numeric(precision=10, scale=2))
    discount = Column(Integer)
    collection_method = Column(Integer)
    custom_field = Column(Text(4294000000))
    description = Column(Text(4294000000))
    secondary_description = Column(Text(4294000000))
    footer_description = Column(Text(4294000000))
    bill_photo = Column(Integer)

    fixed_amount = Column(Boolean)
    allow_customers_to_enter_quantity = Column(Boolean)
    enable_quantity_limit = Column(Boolean)
    quantity_limit = Column(Numeric(precision=10, scale=2))
    enable_total_amount_tracking = Column(Boolean)
    enable_billing_address = Column(Boolean)
    total_target_amount = Column(Numeric(precision=10, scale=2))
    disable_payment_when_target_reached = Column(Boolean)
    show_seller_information_enable = Column(Boolean, default=False)
    cod_enable = Column(Boolean, default=False)
    sms_enable = Column(Boolean, default=False)
    whatsapp_enable = Column(Boolean, default=False)
    email_enable = Column(Boolean, default=False)

    account_id = Column(Integer, ForeignKey("accounts.id"))
    share_setting_id = Column(Integer, ForeignKey("share_settings.id"))
    success_field_id = Column(Integer, ForeignKey("success_fields.id"))
    payment_setting_id = Column(Integer, ForeignKey("payment_settings.id"))
    support_detail_id = Column(Integer, ForeignKey("support_details.id"))
    shipping_profile_id = Column(Integer)

    collection_catalog = relationship("Catalog", back_populates="catalog_collection")
    collection_account = relationship("Account", back_populates="account_collection")
    collection_share_setting = relationship("ShareSetting", back_populates="share_setting_collection")
    collection_payment_setting = relationship("PaymentSetting", back_populates="payment_setting_collection")
    collection_customer_bill = relationship("CustomerBill", back_populates="customer_bill_collection")
    collection_success_field = relationship("SuccessField", back_populates="success_field_collection")
    collection_support_detail = relationship("SupportDetail", back_populates="support_detail_collection")
    collection_payment_upload_files = relationship("PaymentUploadFile", back_populates="payment_upload_files_collection")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
