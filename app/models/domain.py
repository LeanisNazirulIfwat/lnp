import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric
from sqlalchemy.orm import relationship


class Domain(Base):
    """
    Database Model for a domain
    """

    id = Column(Integer, primary_key=True, index=True)
    domain = Column(String(255))
    used_link = Column(String(255))
    record_status = Column(Integer, default=1)

    account_id = Column(Integer, ForeignKey('accounts.id'))

    domain_account = relationship("Account", back_populates="account_domain")
    domain_store = relationship("Store", back_populates="store_domain")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
