import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, String, Integer, Boolean, ForeignKey, Numeric
from sqlalchemy.orm import relationship


class Subscription(Base):
    """
    Database Model for a subscription
    """

    id = Column(Integer, primary_key=True, index=True)
    plan_name = Column(String(255))
    plan_charges = Column(Numeric(precision=10, scale=2))
    fpx_charges = Column(Numeric(precision=10, scale=2))
    threshold_charges = Column(Numeric(precision=10, scale=2))
    credit_card_charges = Column(Numeric(precision=10, scale=2))
    payment_form = Column(Integer)
    bill_form = Column(Integer)
    domain = Column(Integer)
    short_url = Column(String(255))
    store = Column(Integer)
    catalog = Column(Integer)
    product = Column(Integer)
    virtual_account = Column(Integer)
    api = Column(Integer)
    description = Column(String(255))

    user_id = Column(
        ForeignKey("users.id")
    )

    credit_card_enable = Column(Boolean)
    fpx_enable = Column(Boolean)
    ewallet_enable = Column(Boolean)
    paypal_enable = Column(Boolean)
    crypto_enable = Column(Boolean, default=False)
    bnpl_enable = Column(Boolean)
    ewallet_charges = Column(Numeric(precision=10, scale=2))
    crypto_charges = Column(Numeric(precision=10, scale=2))
    bnpl_charges = Column(Numeric(precision=10, scale=2))
    payout_charges = Column(Numeric(precision=10, scale=2))
    paypal_charges = Column(Numeric(precision=10, scale=2))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )

    account_subscription = relationship("Account", back_populates="subscription_plan")
