import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text
from sqlalchemy.orm import relationship


class MerchantShippingAddress(Base):
    """
    Database Model for a merchant shipping address
    """
    __tablename__ = "merchant_shipping_address"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    phone_number = Column(String(255))
    company = Column(String(255))
    address_line_one = Column(Text(4294000000))
    address_line_two = Column(Text(4294000000))
    address_line_three = Column(Text(4294000000))
    address_line_four = Column(Text(4294000000))
    address_postcode = Column(Text(4294000000))
    address_state = Column(Text(4294000000))
    address_city = Column(Text(4294000000))
    address_country = Column(Text(4294000000))
    shipping_profile_id = Column(Integer, ForeignKey("shipping_profiles.id"))
    record_status = Column(Integer, default=1)

    shipping_address_shipping_profile = relationship("ShippingProfile", backref="merchant_shipping_address")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
