import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, Text
from sqlalchemy.orm import relationship


class User(Base):
    """
    Database Model for an application user
    """

    id = Column(
        Integer,
        primary_key=True, index=True
    )
    full_name = Column(String(255))
    email = Column(String(100), index=True)
    phone_number = Column(String(13), index=True)
    hashed_password = Column(String(255))
    is_active = Column(Boolean(), default=True)
    name = Column(String(255))
    first_name = Column(String(255))
    last_name = Column(String(255))
    email_verified = Column(Boolean, default=False)
    OTP = Column(String(255))
    mobile = Column(String(255))
    mobile_pin = Column(String(255))
    kyc_information = Column(String(255))
    secure_word = Column(String(255))
    api_key = Column(Text(255))
    system_id = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )

    record_status = Column(Integer, default=2)
    user_role = relationship("UserRole", back_populates="user", uselist=False)
    account = relationship("Account", back_populates="users")
