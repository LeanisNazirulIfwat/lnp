import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class ShippingRate(Base):
    """
    Database model for a shipping rate
    """
    __tablename__ = "shipping_rates"

    id = Column(Integer, primary_key=True, index=True)
    courier = Column(Integer)
    price = Column(Numeric(precision=10, scale=2))
    condition = Column(Integer)
    weight_condition = Column(Integer)
    min_weight = Column(Numeric(precision=10, scale=2))
    max_weight = Column(Numeric(precision=10, scale=2))

    shipping_zone_id = Column(Integer, ForeignKey("shipping_zones.id"))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
