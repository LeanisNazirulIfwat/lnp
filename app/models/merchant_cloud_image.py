import datetime
from sqlalchemy import Boolean, Column, DateTime, String, Integer, Text, BigInteger
from sqlalchemy.orm import relationship
from app.db.base_class import Base
from fastapi import FastAPI, Form, Depends
from pydantic.dataclasses import dataclass


class MerchantCloudImage(Base):
    """
    Database model for a Image Upload to CDN
    """
    __tablename__ = "merchant_cloud_images"

    id = Column(
        Integer,
        primary_key=True, index=True
    )
    file_name = Column(Text)
    description = Column(Text)
    image_link = Column(Text)
    image_direct_link = Column(Text)
    account_id = Column(Integer)
    upload_date = Column(DateTime)
    download_count = Column(BigInteger)

    is_active = Column(Boolean, default=True)

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
