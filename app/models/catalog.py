import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class Catalog(Base):
    """
    Database model for an catalog
    """

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    slug = Column(String(255))
    collection = Column(String(255))
    layout = Column(String(255))
    base_url = Column(String(255))
    short_url = Column(String(255))
    intro = Column(String(255))
    footer = Column(String(255))
    banner = Column(String(255))
    address = Column(String(255))

    share_setting_id = Column(Integer, ForeignKey("share_settings.id"))
    collection_id = Column(Integer, ForeignKey("collections.id"))
    page_id = Column(Integer, ForeignKey("pages.id"))
    account_id = Column(Integer)

    catalog_share_setting = relationship("ShareSetting", back_populates="share_setting_catalog")
    catalog_collection = relationship("Collection", back_populates="collection_catalog")
    catalog_page = relationship("Page", back_populates="page_catalog")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
