import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric
from sqlalchemy.orm import relationship


class MerchantCallback(Base):
    """
    Database Model for a merchant callback
    """
    __tablename__ = "merchant_callbacks"

    id = Column(Integer, primary_key=True, index=True)
    bill_invoice = Column(String(255))
    transaction_invoice = Column(String(255))
    callback_merchant_url = (Column(String(255)))
    redirect_merchant_url = (Column(String(255)))
    callback_user_url = (Column(String(255)))
    redirect_user_url = (Column(String(255)))
    record_status = Column(Integer, default=1)

    api_key = Column(String(255))

    # domain_account = relationship("Account", back_populates="account_domain")
    # domain_store = relationship("Store", back_populates="store_domain")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
