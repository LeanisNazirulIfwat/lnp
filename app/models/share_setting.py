import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text
from sqlalchemy.orm import relationship


class ShareSetting(Base):
    """
    Database Model for a share setting
    """
    __tablename__ = "share_settings"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    description = Column(String(255))
    image_of_media_social = Column(Text(4294000000))
    image_for_whatsapp = Column(Text(4294000000))
    record_status = Column(Integer, default=1)
    account_id = Column(Integer)

    share_setting_catalog = relationship("Catalog", back_populates="catalog_share_setting")
    share_setting_collection = relationship("Collection", back_populates="collection_share_setting")
    share_setting_store = relationship("Store", back_populates="store_share_setting")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
