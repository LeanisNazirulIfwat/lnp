import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric
from sqlalchemy.orm import relationship


class Recurring(Base):
    """
    Database Model for a recurring
    """

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    initiate_by = Column(Integer)
    start_at = Column(DateTime)
    frequency = Column(Integer)
    kind = Column(Integer)
    repetition = Column(Integer)
    api_interface = Column(String(255))
    amount = Column(Numeric(precision=10, scale=2))
    tax = Column(Numeric(precision=10, scale=2))
    recurrable_type = Column(String(255))
    recurrable = Column(String(255))
    customer_repetition_options = Column(String(255))

    customer_id = Column(Integer, ForeignKey("customers.id"))
    account_id = Column(Integer)
    # customer_bill_id = Column(Integer, ForeignKey("customer_bills.id"))

    recurring_customer = relationship("Customer", back_populates="customer_recurring")
    # recurring_customer_bill = relationship("CustomerBill", back_populates="customer_bill_recurring")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
