import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text
from sqlalchemy.orm import relationship

class SupportDetail(Base):
    """
    Database Model for an support details
    """
    __tablename__ = "support_details"

    id = Column(Integer, primary_key=True, index=True)
    support_contact_person = Column(String(255))
    support_email = Column(String(255))
    support_phone_number = Column(String(255))
    record_status = Column(Integer, default=1)
    account_id = Column(Integer)

    support_detail_company_detail = relationship("CompanyDetail", back_populates="company_detail_support_detail")
    support_detail_store = relationship("Store", back_populates="store_support_detail")
    support_detail_collection = relationship("Collection", back_populates="collection_support_detail")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
