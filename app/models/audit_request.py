import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, String, Integer, Numeric, ForeignKey, Text
from sqlalchemy.orm import relationship

class AuditRequest(Base):
    """
    Database Model for an Audit Request
    """

    __tablename__ = "audit_requests"

    id = Column(Integer, primary_key=True, index=True)
    method = Column(String(255))
    url = Column(String(255))
    email = Column(String(255))
    account_id = Column(String(255))
    description = Column(String(255))
    data = Column(Text(4294000000))
    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
