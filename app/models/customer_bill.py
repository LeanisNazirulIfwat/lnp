import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship
from app.core.config import settings


class CustomerBill(Base):
    """
    Database model for a shipment
    """
    __tablename__ = "customer_bills"

    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String(255))
    email = Column(String(255))
    phone_number = Column(String(255))
    status = Column(Boolean)
    total = Column(Numeric(precision=10, scale=2))
    total_amount_with_fee = Column(Numeric(precision=10, scale=2))
    transaction_fee = Column(Numeric(precision=10, scale=2))
    amount_fee = Column(Numeric(precision=10, scale=2))
    fee_type = Column(Integer, default=1)
    payment_provider = Column(String(255))
    quantity = Column(Integer)
    invoice_no = Column(Text(4294000000))
    transaction_invoice_no = Column(Text(4294000000))
    invoice_status_id = Column(Integer, default=1)
    bill_transaction_type_id = Column(Integer, default=1)
    shipping_address_id = Column(Integer, ForeignKey("shipping_address.id"))
    invoice_status = Column(String(255), default="PENDING")
    base_url = Column(String(255))
    checking_counter = Column(Integer, default=1)
    price_per_quantity = Column(Numeric(precision=10, scale=2))

    sms_enable = Column(Boolean, default=False)
    whatsapp_enable = Column(Boolean, default=False)
    email_enable = Column(Boolean, default=False)

    collection_id = Column(Integer, ForeignKey("collections.id"))
    payment_record_id = Column(Integer, ForeignKey("payment_records.id"))
    customer_id = Column(Integer, ForeignKey("customers.id"))
    account_id = Column(Integer, ForeignKey("accounts.id"))
    api_key = Column(Text)
    payment_date = Column(DateTime)

    customer_bill_customer = relationship("Customer", back_populates="customer_customer_bill")
    customer_bill_shipping_address = relationship("ShippingAddress", back_populates="shipping_address_customer_bill")
    customer_bill_collection = relationship("Collection", back_populates="collection_customer_bill")
    customer_bill_payment_record = relationship("PaymentRecord", back_populates="payment_record_customer_bill")

    # customer_bill_recurring = relationship("Recurring", back_populates="recurring_customer_bill")

    record_status = Column(Integer, default=settings.RECORD_ACTIVE)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
