import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric
from sqlalchemy.orm import relationship


class VirtualAccount(Base):
    """
    Database model for a payout lean-x
    """

    __tablename__ = "virtual_accounts"

    id = Column(Integer, primary_key=True, index=True)
    pool_name = Column(String(255))
    balance = Column(Numeric(precision=10, scale=2))
    switch_payout_pool_id = Column(String(255))
    merchant_payout_pool_id = Column(String(255))
    account_id = Column(Integer)

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
