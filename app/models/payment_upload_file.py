import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric, Text
from sqlalchemy.orm import relationship

class PaymentUploadFile(Base):
    """
    Database Model for an payment setting
    """
    __tablename__ = "payment_upload_files"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    status = Column(String(255))
    status_id = Column(Integer)
    collection_id = Column(Integer, ForeignKey("collections.id"))
    customer_bills = Column(Text(4294000000))

    payment_upload_files_collection = relationship("Collection", back_populates="collection_payment_upload_files")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
