import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, ForeignKey, String, Integer, Numeric
from sqlalchemy.orm import relationship


class SuccessField(Base):
    """
    Database Model for an success field
    """
    __tablename__ = "success_fields"

    id = Column(Integer, primary_key=True, index=True)
    additional_message = Column(String(255))
    redirect_url = Column(String(255))
    record_status = Column(Integer, default=1)
    account_id = Column(Integer)

    success_field_collection = relationship("Collection", back_populates="collection_success_field")

    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
