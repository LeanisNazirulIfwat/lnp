import datetime
from decimal import Decimal

from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class TierPricing(Base):

    __tablename__ = "tier_pricings"

    """
    Database model for a tier pricing
    """

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    quantity_from = Column(Integer)
    quantity_to = Column(Integer)
    price = Column(Numeric(precision=10, scale=2))
    product_id = Column(Integer, ForeignKey('products.id'))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
