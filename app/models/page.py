import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class Page(Base):
    """
    Database model for a page
    """

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    slug = Column(String(255))
    cover = Column(String(255))
    content = Column(String(255))
    published = Column(Boolean)
    account_id = Column(Integer)

    page_catalog = relationship("Catalog", back_populates="catalog_page")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
