import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class Rule(Base):
    """
    Database model for a shipping profile
    """

    id = Column(Integer, primary_key=True, index=True)
    discount_for = Column(String(255))
    discount_type = Column(String(255))
    discount_amount = Column(Numeric(precision=10, scale=2))
    condition = Column(String(255))
    group = Column(String(255))

    rule_discount = relationship("Discount", back_populates="discount_rule")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
