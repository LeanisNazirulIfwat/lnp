import datetime
from app.db.base_class import Base
from sqlalchemy import Column, DateTime, String, Integer, Numeric, ForeignKey, Text
from sqlalchemy.orm import relationship


class BankAccountVerification(Base):
    """
    Database Model for Bank account verification
    """
    __tablename__ = "bank_account_verifications"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(255))
    bank_name = Column(String(255))
    bank_verification_id = Column(Integer)
    account_number = Column(String(255))
    data = Column(Text(4294000000))
    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )

    api_key = Column(String(255))
    account_id = Column(Integer)
