import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class PayOutRecord(Base):
    """
    Database model for a Payout Record
    """
    __tablename__ = "payout_records"

    id = Column(Integer, primary_key=True, index=True)
    switch_invoice_id = Column(String(255))
    merchant_invoice_id = Column(String(255))
    transaction_invoice_no = Column(String(255))
    payout_invoice_no = Column(String(255))
    payout_account_number = Column(String(255))
    transaction_status = Column(String(255))
    value = Column(Numeric(precision=10, scale=2))
    account_id = Column(Integer)
    data = Column(Text(4294000000))
    api_key = Column(Text(255))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
