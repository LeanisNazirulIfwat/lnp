import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class SubscriptionHistory(Base):
    """
    Database model for a shipment
    """
    __tablename__ = "subscription_histories"

    id = Column(Integer, primary_key=True, index=True)
    subscribed_at = Column(DateTime)
    plan_name = Column(Integer)
    interval = Column(String(255))
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    current_plan_name = Column(Integer)
    bill = Column(String(255))
    bill_status = Column(String(255))
    activated_at = Column(DateTime)
    account_id = Column(Integer, ForeignKey("accounts.id"))
    upgrade_complete = Column(Boolean)

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
