import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class PaymentRecord(Base):
    """
    Database model for a shipment
    """
    __tablename__ = "payment_records"

    id = Column(Integer, primary_key=True, index=True)
    payment_date = Column(DateTime)
    reference_number = Column(String(255))
    payment_mode = Column(String(255))
    payment_method = Column(String(255))
    amount = Column(Numeric(precision=10, scale=2))
    transaction_fee = Column(Numeric(precision=10, scale=2))
    amount_fee = Column(Numeric(precision=10, scale=2))
    fee_type = Column(Integer, default=1)
    payment_provider = Column(String(255))
    switch_transaction_fee = Column(Numeric(precision=10, scale=2))
    bank_code = Column(String(255))
    kind = Column(String(255))
    net_amount = Column(Numeric(precision=10, scale=2))
    paid_at = Column(DateTime)
    total = Column(String(255))
    data = Column(Text(4294000000))

    customer_id = Column(Integer, ForeignKey("customers.id"))
    payment_record_customer = relationship("Customer", back_populates="customer_payment_record")
    payment_record_customer_bill = relationship("CustomerBill", back_populates="customer_bill_payment_record")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
