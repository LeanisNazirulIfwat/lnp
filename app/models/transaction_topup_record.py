import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class TransactionTopupRecord(Base):
    """
    Database model for a Prefund Record
    """
    __tablename__ = "transaction_topup_records"

    id = Column(Integer, primary_key=True, index=True)
    invoice_no = Column(String(255))
    transaction_invoice_no = Column(String(255))
    transaction_status = Column(String(255))
    amount = Column(Numeric(precision=10, scale=2))
    account_id = Column(Integer)
    payment_date = Column(DateTime)
    data = Column(Text(4294000000))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
