import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text
from sqlalchemy.orm import relationship


class Customer(Base):
    """
    Database model for a shipment
    """

    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String(255))
    email = Column(String(255))
    phone_number = Column(String(255))
    address_line_one = Column(Text(4294000000))
    address_line_two = Column(Text(4294000000))
    address_postcode = Column(Text(4294000000))
    address_city = Column(Text(4294000000))
    address_state = Column(Text(4294000000))
    address_country = Column(Text(4294000000))
    account_id = Column(Integer)

    customer_customer_bill = relationship("CustomerBill", back_populates="customer_bill_customer")
    customer_payment_record = relationship("PaymentRecord", back_populates="payment_record_customer")
    customer_recurring = relationship("Recurring", back_populates="recurring_customer")
    customer_shipment = relationship("Shipment", back_populates="shipment_customer")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
