import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric, Text
from sqlalchemy.orm import relationship


class SystemOption(Base):
    """
    Database model for a options
    """

    __tablename__ = "system_options"

    id = Column(Integer, primary_key=True, index=True)
    application_name = Column(String(255))
    main_site = Column(String(255))
    logo = Column(Text(4294000000))
    logo_alt = Column(Text(4294000000))
    qr_code_logo = Column(Text(4294000000))
    favicon = Column(Text(4294000000))
    meta_image = Column(Text(4294000000))
    meta_description = Column(String(255))
    background_color = Column(String(255))
    background_image = Column(Text(4294000000))
    primary_color = Column(String(255))
    secondary_color = Column(String(255))
    site_title = Column(String(255))
    site_support = Column(String(255))
    company_name = Column(String(255))
    platform_version = Column(String(255))
    mailer = Column(String(255))
    faq_url = Column(String(255))
    privacy_term_url = Column(String(255))
    contact_url = Column(String(255))
    facebook = Column(String(255))
    twitter = Column(String(255))
    instagram = Column(String(255))
    linkedin = Column(String(255))
    base_url_endpoint = Column(String(255))
    callback_url = Column(String(255))
    powered_by = Column(String(255))
    account_id = Column(Integer)
    api_parent_key = Column(Text(255))

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
