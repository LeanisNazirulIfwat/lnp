import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey ,Numeric
from sqlalchemy.orm import relationship


class ShippingZone(Base):
    """
    Database model for an shipping zone
    """
    __tablename__ = "shipping_zones"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    states = Column(String(255))
    shipping_profile_id = Column(Integer, ForeignKey("shipping_profiles.id"))

    shipping_zone_shipping_rate = relationship("ShippingRate", backref="shipping_zones")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
