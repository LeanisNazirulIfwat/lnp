import datetime
from app.db.base_class import Base
from sqlalchemy import Boolean, Column, DateTime, String, Integer, ForeignKey, Numeric, Text, BigInteger
from sqlalchemy.orm import relationship


class CompanyDetail(Base):
    """
    Database model for a CompanyDetail
    """
    __tablename__ = "company_details"

    id = Column(Integer, primary_key=True, index=True)
    account_name = Column(String(255))
    company_name = Column(String(255))
    logo = Column(Text(4294000000))
    company_number = Column(String(255))
    business_website_url = Column(String(255))
    nature_of_business = Column(String(255))
    fav_icon = Column(Text(4294000000))

    support_detail_id = Column(Integer, ForeignKey("support_details.id"))
    company_detail_support_detail = relationship("SupportDetail", back_populates="support_detail_company_detail")

    business_owner_detail_id = Column(Integer, ForeignKey("business_owner_details.id"))
    company_detail_business_owner_detail = relationship("BusinessOwnerDetail",
                                                        back_populates="business_owner_detail_company_detail")

    company_details_account = relationship("Account", back_populates="account_company_details")

    record_status = Column(Integer, default=1)
    created_at = Column(DateTime, default=datetime.datetime.today())
    updated_at = Column(
        DateTime,
        default=datetime.datetime.today(),
        onupdate=datetime.datetime.today(),
    )
