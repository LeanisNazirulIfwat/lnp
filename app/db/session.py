from app.core.config import settings
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

DB_POOL_SIZE = 600
WEB_CONCURRENCY = 9
POOL_SIZE = max(DB_POOL_SIZE // WEB_CONCURRENCY, 5)

engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True, pool_size=POOL_SIZE, max_overflow=400)

#   FOR SQL LITE
# engine = create_engine(settings.SQLALCHEMY_DATABASE_URI_LINUX, connect_args={
#     "check_same_thread": False})

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

test_engine = create_engine(
    f"{settings.SQLALCHEMY_DATABASE_URI}_test", pool_pre_ping=True
)
TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=test_engine
)
