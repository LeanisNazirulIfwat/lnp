from datetime import datetime
import decimal
import json
import uuid

from app import schemas, crud
from app.Utils.shared_utils import SharedUtils
from app.constants.role import Role
from app.core.config import settings
from sqlalchemy.orm import Session
from app.constants.system_constant import SystemConstant
import requests


def init_db(db: Session) -> None:
    # Create 1st Superuser
    user = crud.user.get_by_email(db, email=settings.FIRST_SUPER_ADMIN_EMAIL)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPER_ADMIN_EMAIL,
            password=settings.FIRST_SUPER_ADMIN_PASSWORD,
            phone_number=settings.FIRST_SUPER_ADMIN_PHONE_NUMBER,
            full_name=settings.FIRST_SUPER_ADMIN_EMAIL,
            system_id=settings.SUPER_ADMIN_SYSTEM,
            api_key=str(uuid.uuid4())
        )
        superuser = crud.user.create(db, obj_in=user_in)

    # Create Super Admin Account
    account = crud.account.get_by_name(
        db, name=settings.FIRST_SUPER_ADMIN_ACCOUNT_NAME
    )
    if not account:
        user = crud.user.get_by_email(db, email=settings.FIRST_SUPER_ADMIN_EMAIL)
        if user:
            admin_account = 0
            business_in = schemas.BusinessOwnerDetailCreate(
                legal_name="LEANIS SOLUTION SDN BHD.",
                individual_nric="960606-08-2154",
                bank_name="PUBLIC BANK",
                business_bank_account_number="526472756-46879710",
                address_line_one="Unit 8, Level 13A, Tower 1",
                address_line_two="8trium, Jalan Cempaka SD12/5",
                address_postcode=52200,
                address_city="Bandar Sri Damansara",
                address_state="Kuala Lumpur",
                address_country="Malaysia",
                created_at=datetime.today()
            )

            business_create = crud.business_owner_detail.create_self_date(db, obj_in=business_in)

            company_details_in = schemas.CompanyDetailCreate(
                company_name="LEANIS SOLUTION SDN BHD.",
                company_number="65-8692993",
                business_website_url="https://leanis.com.my",
                nature_of_business="FINTECH",
                created_at=datetime.today(),
                business_owner_detail_id=business_create.id,
                fav_icon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
                logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO")
            )

            company_details_create = crud.company_detail.create_self_date(db, obj_in=company_details_in)

            account_in = schemas.AccountCreate(
                name=settings.FIRST_SUPER_ADMIN_ACCOUNT_NAME,
                account_name=settings.FIRST_SUPER_ADMIN_ACCOUNT_NAME,
                bank_type=1,
                company_details_id=company_details_create.id,
                description="superadmin account",
                account_type="SUPER_ADMIN",
                user_id=user.id,
                parent_key=user.api_key
            )
            account_ = crud.account.create(db, obj_in=account_in)
            admin_account = account_.id
            # bind option to user account
            option_create = schemas.SystemOptionCreate(
                application_name="Lean",
                main_site="https://leanis.com.my/",
                logo=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("LOGO"),
                logo_alt=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("LOGO_ALT"),
                qr_code_logo=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("QR_LOGO"),
                favicon=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("FAV_ICON"),
                meta_image=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("META_LOGO"),
                meta_description="Leanis Solution Payment Gateway Admin",
                background_color="#ffffff",
                background_image=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("QR_LOGO"),
                primary_color="#263872",
                secondary_color="#6610f2",
                company_name="Leanis",
                platform_version="V1",
                api_parent_key=user.api_key,
                account_id=account_.id
            )
            option = crud.system_option.create_self_date(db, obj_in=option_create)

            # Initiate admin collection
            pre_collection_pool = crud.collection.get_by_name(db, title="ADMIN_COLLECTION_POOL")
            if not pre_collection_pool:
                # create success field template
                success_in = schemas.SuccessFieldCreate(
                    account_id=account_.id,
                    created_at=datetime.today()
                )

                share_in = schemas.ShareSettingCreate(
                    account_id=account_.id,
                    created_at=datetime.today()
                )

                payment_in = schemas.PaymentSettingCreate(
                    account_id=account_.id,
                    created_at=datetime.today()
                )

                support_in = schemas.SupportDetailCreate(
                    account_id=account_.id,
                    created_at=datetime.today()
                )

                collection_pool = schemas.PoolCreate(
                    pool_name="ADMIN_COLLECTION",
                    value=decimal.Decimal(0),
                    account_id=account_.id,
                    api_key=account_.parent_key,
                    pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
                )

                threshold_pool = schemas.PoolCreate(
                    pool_name="ADMIN_THRESHOLD",
                    value=decimal.Decimal(0),
                    account_id=account_.id,
                    api_key=account_.parent_key,
                    pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL"),
                )

                crud.pool.create_self_date(db, obj_in=collection_pool)
                crud.pool.create_self_date(db, obj_in=threshold_pool)
                success_create = crud.success_field.create_self_date(db, obj_in=success_in)
                support_create = crud.support_detail.create_self_date(db, obj_in=support_in)
                share_create = crud.share_setting.create_self_date(db, obj_in=share_in)
                payment_create = crud.payment_setting.create_self_date(db, obj_in=payment_in)

                pre_create_collection = schemas.CollectionCreate(
                    base_url="http://dev.portal.leanpay.my/pay/collections/",
                    title="Admin Collection Portal",
                    uuid="LEANX-CL",
                    disable_payment_when_target_reached=False,
                    fixed_amount=False,
                    allow_customers_to_enter_quantity=False,
                    enable_total_amount_tracking=False,
                    total_target_amount=0,
                    enable_billing_address=False,
                    collection_method=1,
                    record_status=1,
                    min_amount=10,
                    description="<p>Admin Collection Portal</p>",
                    secondary_description="<p>Admin Collection Portal</p>",
                    footer_description="<p>Admin Collection Portal</p>",
                    enable_quantity_limit=False,
                    quantity_limit=0,
                    share_setting_id=share_create.id,
                    success_field_id=success_create.id,
                    payment_setting_id=payment_create.id,
                    support_detail_id=support_create.id,
                    account_id=account_.id
                )

                query_create = crud.collection.create_self_date(db, obj_in=pre_create_collection)

            admin_default_va = crud.virtual_account.get_by_admin_pool_name(db, name=settings.PREFUND_POOL_NAME)
            if not admin_default_va:
                url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/superadmin/virtualpools/clientCreateVirtualPool"
                payload = {'client_identifier': 'LEANPAY',
                           'skip_encryption': 'true'}
                files = [

                ]
                headers = {}

                response = requests.request("POST", url, headers=headers, data=payload, files=files)

                if response.status_code == requests.codes.ok:
                    data = json.loads(response.text)
                    if data['response_code'] == 2100:
                        unique_name = f'VA-{SharedUtils.my_random_string(6)}-{datetime.now().strftime("%H%M%S%f")}-PAYOUT'
                        virtual_account = schemas.VirtualAccountCreate(
                            account_id=admin_account,
                            pool_name=settings.PREFUND_POOL_NAME,
                            balance=0,
                            switch_payout_pool_id=data['data']['virtual_pool_reference'],
                            merchant_payout_pool_id=unique_name
                        )

                        va_create = crud.virtual_account.create_self_date(db, obj_in=virtual_account)

        # new_user = ac.Account(
        #     name=settings.FIRST_SUPER_ADMIN_ACCOUNT_NAME,
        #     description="superadmin account"
        # )
        # db.add(new_user)
        # db.commit()
        # db.refresh(new_user)

    # Create Role If They Don't Exist
    guest_role = crud.role.get_by_name(db, name=Role.GUEST["name"])
    if not guest_role:
        guest_role_in = schemas.RoleCreate(
            name=Role.GUEST["name"], description=Role.GUEST["description"]
        )
        crud.role.create(db, obj_in=guest_role_in)

    account_admin_role = crud.role.get_by_name(
        db, name=Role.ACCOUNT_ADMIN["name"]
    )

    if not account_admin_role:
        account_admin_role_in = schemas.RoleCreate(
            name=Role.ACCOUNT_ADMIN["name"],
            description=Role.ACCOUNT_ADMIN["description"],
        )
        crud.role.create(db, obj_in=account_admin_role_in)

    account_manager_role = crud.role.get_by_name(
        db, name=Role.ACCOUNT_MANAGER["name"]
    )

    if not account_manager_role:
        account_manager_role_in = schemas.RoleCreate(
            name=Role.ACCOUNT_MANAGER["name"],
            description=Role.ACCOUNT_MANAGER["description"],
        )
        crud.role.create(db, obj_in=account_manager_role_in)

    admin_role = crud.role.get_by_name(db, name=Role.ADMIN["name"])
    if not admin_role:
        admin_role_in = schemas.RoleCreate(
            name=Role.ADMIN["name"], description=Role.ADMIN["description"]
        )
        crud.role.create(db, obj_in=admin_role_in)

    super_admin_role = crud.role.get_by_name(db, name=Role.SUPER_ADMIN["name"])
    if not super_admin_role:
        super_admin_role_in = schemas.RoleCreate(
            name=Role.SUPER_ADMIN["name"],
            description=Role.SUPER_ADMIN["description"],
        )
        crud.role.create(db, obj_in=super_admin_role_in)

    # create merchant role
    merchant_role = crud.role.get_by_name(
        db, name=Role.MERCHANT["name"]
    )

    if not merchant_role:
        merchant_role_in = schemas.RoleCreate(
            name=Role.MERCHANT["name"],
            description=Role.MERCHANT["description"],
        )
        crud.role.create(db, obj_in=merchant_role_in)

    # create master merchant role
    master_merchant_role = crud.role.get_by_name(
        db, name=Role.MASTER_MERCHANT["name"])

    if not master_merchant_role:
        master_merchant_role_in = schemas.RoleCreate(
            name=Role.MASTER_MERCHANT["name"],
            description=Role.MASTER_MERCHANT["description"],
        )
        crud.role.create(db, obj_in=master_merchant_role_in)

    # create white label role
    white_label_role = crud.role.get_by_name(
        db, name=Role.WHITE_LABEL["name"])

    if not white_label_role:
        white_label_role_in = schemas.RoleCreate(
            name=Role.WHITE_LABEL["name"],
            description=Role.WHITE_LABEL["description"],
        )
        crud.role.create(db, obj_in=white_label_role_in)

    # Assign super_admin role to user
    user_role = crud.user_role.get_by_user_id(db, user_id=user.id)
    if not user_role:
        role = crud.role.get_by_name(db, name=Role.SUPER_ADMIN["name"])
        user_role_in = schemas.UserRoleCreate(user_id=user.id, role_id=role.id)
        crud.user_role.create(db, obj_in=user_role_in)

    # Initiate system id
    list_system = [{"name": "ADMIN", "description": "Admin System"},
                   {"name": "MERCHANT", "description": "Merchant System"},
                   {"name": "STAFF", "description": "Staff Login"}]

    for sys in list_system:
        system_preference = crud.system_preference.get_by_name(db, name=sys['name'])
        if not system_preference:
            system_in = schemas.SystemPreferenceCreate(name=sys['name'], description=sys['description'],
                                                       record_status=1)
            crud.system_preference.create(db, obj_in=system_in)

    # Initiate basic subscription plan
    sub_plan = crud.subscription.get_by_plan_name(db, plan_name="Basic Plan")
    if not sub_plan:
        sub_in = schemas.SubscriptionCreate(
            plan_name='Basic Plan',
            api=2,
            bill_form=5,
            catalog=5,
            credit_card_charges=2.9,
            domain=5,
            fpx_charges=0.80,
            payout_charges=0.90,
            payment_form=5,
            plan_charges=0.00,
            product=5,
            store=5,
            short_url=5,
            fpx_enable=True,
            credit_card_enable=False,
            ewallet_enable=False,
            ewallet_charges=1.6,
            bnpl_charges=6.3,
            virtual_account=5,
            bnpl_enable=False,
            threshold_charges=1.50,
            paypal_enable=True,
            paypal_charges=2.99,
            crypto_enable=False,
            crypto_charges=2.0
        )
        crud.subscription.create(db, obj_in=sub_in)

    pre_plan = crud.subscription.get_by_plan_name(db, plan_name="Standard Plan")
    if not pre_plan:
        sub_pre_in = schemas.SubscriptionCreate(
            plan_name='Standard Plan',
            api=2,
            bill_form=5,
            catalog=5,
            credit_card_charges=2.7,
            domain=5,
            fpx_charges=0.60,
            payout_charges=0.70,
            payment_form=5,
            plan_charges=299,
            product=5,
            store=5,
            short_url=5,
            fpx_enable=True,
            credit_card_enable=True,
            ewallet_enable=False,
            virtual_account=10,
            bnpl_enable=False,
            ewallet_charges=1.5,
            bnpl_charges=6.1,
            threshold_charges=1.50,
            paypal_enable=True,
            paypal_charges=2.99,
            crypto_enable=True,
            crypto_charges=1.8

        )
        crud.subscription.create(db, obj_in=sub_pre_in)

        ent_plan = crud.subscription.get_by_plan_name(db, plan_name="Enterprise Plan")
        if not ent_plan:
            sub_ent_in = schemas.SubscriptionCreate(
                plan_name='Enterprise Plan',
                api=4,
                bill_form=15,
                catalog=15,
                credit_card_charges=2.2,
                domain=5,
                fpx_charges=0.50,
                payout_charges=0.50,
                payment_form=15,
                plan_charges=899,
                product=15,
                store=15,
                short_url=15,
                fpx_enable=True,
                credit_card_enable=True,
                ewallet_enable=False,
                virtual_account=15,
                bnpl_enable=False,
                ewallet_charges=1.4,
                bnpl_charges=5.9,
                threshold_charges=1.50,
                paypal_enable=True,
                paypal_charges=2.99,
                crypto_enable=True,
                crypto_charges=1.2
            )
            crud.subscription.create(db, obj_in=sub_ent_in)
