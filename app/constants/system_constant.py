class SystemConstant:
    """
    System constant for LNP Payment Gateway
    """

    SYSTEM_CONS = {
        "discount_for":
            [
                {"name": "Product",
                 "value": 1},
                {"name": "Bill",
                 "value": 2}
            ],

        "discount_type":
            [
                {"name": "Fixed",
                 "value": 1},
                {"name": "Bill",
                 "value": 2}
            ],

        "kind":
            [
                {"name": "Repetition",
                 "value": 1},
                {"name": "Date Range",
                 "value": 2}
            ],

        "initiate_by":
            [
                {"name": "Customer",
                 "value": 1},
                {"name": "Merchant",
                 "value": 2}
            ],

        "recurrable_type":
            [
                {"name": "Collection",
                 "value": 1},
                {"name": "Store",
                 "value": 2}
            ],

        "record_status":
            [
                {"name": "Active",
                 "value": 1},
                {"name": "Inactive",
                 "value": 2},
                {"name": "Delete",
                 "value": 4},
                {"name": "Pending ",
                 "value": 3},
                {"name": "Inactive ",
                 "value": 5}
            ],

        "invoice_status":
            [
                {"name": "PENDING",
                 "value": 1},
                {"name": "SUCCESS",
                 "value": 2},
                {"name": "FAILED ",
                 "value": 3},
                {"name": "INVALID ",
                 "value": 5}
            ],

        "launch_status":
            [
                {"name": "Collection",
                 "value": 1},
                {"name": "Store",
                 "value": 2}
            ],

        "api_interface":
            [
                {"name": "Default",
                 "value": 1},
                {"name": "Custom",
                 "value": 2}
            ],

        "fpx_bank_selection":
            [
                {"name": "Dropdown",
                 "value": 1},
                {"name": "Grid",
                 "value": 2}
            ],

        "frequency":
            [
                {"name": "Daily",
                 "value": 1},
                {"name": "Weekly",
                 "value": 2},
                {"name": "By Weekly",
                 "value": 3},
                {"name": "Monthly ",
                 "value": 4},
                {"name": "Yearly ",
                 "value": 5}
            ],

        "layout":
            [
                {"name": "Default",
                 "value": 1},
                {"name": "Simple",
                 "value": 2}
            ],

        "condition":
            [
                {"name": "Flat Rate",
                 "value": 1},
                {"name": "Weight",
                 "value": 2}
            ],

        "states": [
            {
                "id": "249666046",
                "name": "Pulau Pinang"
            },
            {
                "id": "591590403",
                "name": "Kedah"
            },
            {
                "id": "591590407",
                "name": "Perlis"
            },
            {
                "id": "1775640129",
                "name": "Sabah"
            },
            {
                "id": "2609539356",
                "name": "Johor"
            },
            {
                "id": "2609539371",
                "name": "Sarawak"
            },
            {
                "id": "3208142734",
                "name": "Perak"
            },
            {
                "id": "3222282161",
                "name": "Pahang"
            },
            {
                "id": "3665372243",
                "name": "Selangor"
            },
            {
                "id": "4165045379",
                "name": "Terengganu"
            },
            {
                "id": "4165056889",
                "name": "Kelantan"
            },
            {
                "id": "4165116689",
                "name": "Federal Territory of Kuala Lumpur"
            },
            {
                "id": "4165154141",
                "name": "Melaka"
            },
            {
                "id": "4416752667",
                "name": "Federal Territory of Labuan"
            },
            {
                "id": "4534491713",
                "name": "Federal Territory of Putrajaya"
            },
            {
                "id": "4556962889",
                "name": "Negeri Sembilan"
            }
        ],

        "payment_mode":
            [
                {"name": "Merchant",
                 "value": 1},
                {"name": "Customer",
                 "value": 2},
                {"name": "Merchant Credit",
                 "value": 3}
            ],

        "role":
            [
                {"name": "Admin",
                 "value": 1},
                {"name": "Staff",
                 "value": 2},
                {"name": "Developer",
                 "value": 3}
            ],

        "system_preference":
            {
                "SUPER_ADMIN": 1,
                "MASTER_MERCHANT": 2,
                "MERCHANT": 3,
                "WHITE_LABEL": 4,
            }
        ,

        "courier":
            [
                {"name": "POS_LAJU",
                 "value": 1},
                {"name": "SKY_NET",
                 "value": 2},
                {"name": "GDEX",
                 "value": 3},
                {"name": "CITY_LINK",
                 "value": 4},
                {"name": "AIRPAK_EXPRESS",
                 "value": 5},
                {"name": "TA_Q_BIN",
                 "value": 6},
                {"name": "NATION_WIDE_EXPRESS",
                 "value": 7},
                {"name": "JT_EXPRESS",
                 "value": 8},
                {"name": "EASY_PARCEL",
                 "value": 9},
                {"name": "DHL",
                 "value": 10},
                {"name": "PGEON",
                 "value": 11},
                {"name": "NATION_WIDE",
                 "value": 12},
                {"name": "LALAMOVE",
                 "value": 13},
                {"name": "ABX",
                 "value": 14},
                {"name": "NINJA_VAN",
                 "value": 15},
                {"name": "KANGAROO_EXPRESS",
                 "value": 16},
                {"name": "MAT_DESPATCH",
                 "value": 17},
                {"name": "DELYVA",
                 "value": 18},
                {"name": "ZEPTO_EXPRESS",
                 "value": 19},
                {"name": "ARAMEX",
                 "value": 20},
                {"name": "COD",
                 "value": 21},
                {"name": "RUNNER",
                 "value": 22},
                {"name": "STORE_PICKUP",
                 "value": 23}
            ],

        "image_provider_method":
            [
                {"name": "DISPLAY",
                 "value": "display"},
                {"name": "BYTE",
                 "value": "byte"}
            ],

        "collection_method":
            [
                {"name": "PAYMENT_FORM",
                 "value": 1},
                {"name": "BILL_FORM",
                 "value": 2}
            ],

        "Billing_Details":
            {
                "COMPANY_NAME": "Leanis Solution Sdn. Bhd.",
                "COMPANY_ADDRESS": "Unit 8, Level 13A, Tower 1, 8trium, Jalan Cempaka SD12/5, Bandar Sri Damansara,"
                                   "52200 Kuala Lumpur, Malaysia.",

            },

        "leanis_image_64":
            {
                "FAV_ICON": "iVBORw0KGgoAAAANSUhEUgAAADwAAAA9CAYAAADxoArXAAAACXBIWXMAAAsSAAALEgHS3X78AAAEK0lEQVRoge1bS0hUURj+8hGmxkhmZWIzLeQiFT6guoTQi7sbsKB9466dE1zGNtm0i8tA4yJw57gPMri7WTjuZte4HDAcpUWLEA3T1FHjxK/Y+Oi87nV8fBtBz+vj/I/z//fzzObmJk4SKk4U21PCJwAnjnDVYW186/GgXV1X92x1cTG3trQ8nM8mcn7s62uUNky7oaKq8hWAFxvF9UDJn4cAxPPZxLyXZ/CNMLvR4srq4EaxeP6AYQsAovlsIuXVOTwnfPPR6zCA4bWl5RaBaZNEPKP7PJ4Sbu8ZeLdRXB9QWOIzES/oOpOWKG2Ydsgw7RT7ufP317rvof7yJZWlewHkDNOOM/9XPqgqYXYIw7STAKYBPAfwD+Hqmlqz2ehCa/dd1ARKYxQ32MQ3RDyicl7IpiW6ySiACB3oQNTUN6C1w8TC9xnMzU6j+HtFZtsggBEiHZVNY0KEiWicblMYgStB1F+8ivlvU5ibnZVZguE+gC+GaUulMW6TJh/KyZLdQmVVNRpD7Qje6UFt4wWVpfoBZER9W8SHkzzmy4uzNXVouXEbTW3GKIAZyWU6AAjlbBHCSje7HxqaQ4V8NsFc5SU9PETRa5j2A945ZVM85LOJJEX5IYnpT3gHllW1xAJQPptg0b9L0Mw7eQeWZXlIKadTwMSPNmHQbQuYKncwLesGABUPEzrXFCEsE0F1QGvFJELYl46E1/sehZ6W1g7IadfyEMCdUo4L4d5wLK2luOdBuZh03K+NyoVwfziWVu5m8MBrwiIpZSQcS0c9PMtflBNhhvfhWDoTjqW5yz1ReErYdayUxAuNtXDGw7F0KhxLhzjGC8EPH5b1TdZwyDW1GVp923PCrmONAXgrOT1Qfa5Oa6fFlyjtOhZLO08PsQDZhm9piW5atoWjDb7mYdex5l3HYqnnuu46lxcihHkiJtcT0XWsgutYLPU8VGjRSkGEcJBjjFD+dB0r4zqWSotWGLpNmrtduhOuY221aEc1n2cXdBMOGqYt9Twk/45Qi9Yz/xYhPMk5jn3Lla5xXcfKkX/3eeHfIoR5Wy0B+sglZd5boGdp5+qvnx9U1imFCOExgbGM9CfDtDOlqgARMDP/8XXqo+z8veAV4S2wQmCaqQR0SRZUwU2YhCWyb2L2LbegQ7KwD7iDnFCUzmcTcYXUESDJQk7k8yYHFkh+wQXhtJTPJiJ007IPBfYRe9ww7TEV/6b92TlCInoPqTxMN92p+FDYliQJzttJVFjjoSxMI/OMU4CSxQwpc3YFRrKCaSLKXmRJFT2mNiUeBaQ455t7P7DgEylV3tHrLaVDeKpVekipJ0pCMln0HTlxKZlhkvyUF3verm54Ki4l/079x8wnSGCmXTm7F3zRS5MPxkukCZ5JhA+CbwLxHf7NUPDSTw/C6T9qHXecEj7WAPAHnf9nI5V/SpUAAAAASUVORK5CYII=",
                "LOGO": "iVBORw0KGgoAAAANSUhEUgAAAPoAAAA9CAYAAACEAXQxAAAACXBIWXMAAAsSAAALEgHS3X78AAAIuklEQVR4nO2dvY8bRRTA3yWHiBDCB0UUUmAnykIBBKeDpbARMjQ5xfkL4mtoz9FKJxp0vobCwsqlpIqvpcGRyyvia9zi65Bc5IxEUoCELRJxCJ2C5vImmXuZ2Q/fer3jfT/Jyoftt/O88+bNvPdmdunZs2fAMMxic4bvL8MsPmzoDJMB2NAZJgOwoTNMBmBDZ5gMwIbOMBmADZ1hMgAbOsNkADZ0hskAbOgMkwHY0BkmA7ChM0wGWF4UFT/68rvrS2eWfjg6Orrw3+FhDwDqw37rIAVNY5i5Y/3uteL1768e/Xv40+HTpx+QtyYAsD3stxpxXs9xPVXewbDfascpn2FmgdWGfvWrxjf/PPn7x4CPjdC7d+K4puN66g+2N+y3ynHIZZhZYtUa3XG9muN6B47rHRtXvvj5xfOXr8DZZd8VSB4AfnZcr+e4XjGxxjJMirBijS4MHAAaaLQneOfiFcidz8Mfv/0K40e/+4kpAcAvjuvdFbKG/dY4kcYzTApIvUcXnhgA7umMXHJ2+TW4cPljKBQ/hTfeygWJXAcA4d1XZtFehkkjqTZ0nGqXwn7+3Jsr8N7Vz8Rfb+La3MQnIlA3q3YzTNpIu0efyuuKwNuw3yoAwJbPx25N3yyGsYuFLpjB1NpaCprCMHNl4SvjMM99PwVNYZi5sTCVcQEMAODGvC7uuJ5YRlTxpcYc9gBA5Pc7Uar4MJAo5anLGyGjh/LGGOOoKu+31euQ4p+BrDVwXE/KLmB7J/gbDrAISdtW1LOG/xTtEAOtDHyKlGgR/3xxTXx1omRBUF4NZan6n5CHesiU6njYb20r35Xfi1z0pJFxLDuKjKTJiqGLTreZ9EWxQ9R9rl3C1x3H9XawsMe3wzuuV8dUoy69UMLYwz2UVyWf6+FgIDnRLsf17qPxUNk5pa3rIkU57LfqmusXFJmbKHMfg58m/QXCAGthippwcKr76A+K/jQOI43xxO/nuB5ENPY2cRx7aQ/uLuTUfXVjtzDvNqCRRxlgRKcc+BX1OK4nOtgdQyfXyQvzOZUbIb+zjm0Jg8nIVXJY1OQrE9/fjKD/K+BASgeU0EaK94fODlNfBr2oa3Sdt0manqGTj9AD7GveE7UCHV2OHzt5mjIFt3BqHBWT7lKmdm9CzPrTa+SwKCsMtG+NbNjvsKiGvr66sTs3Y8fOSo1cdPBLIu0n6uOH/ZbwDG8DwA75XJ52Jiz51XVy8d1rw35rSbxQ3lpADUEYtrCtUu41Q0AzrCcUhr2G8qTulwDgruazm3RWgwNKbPpjjIH+7oGbnzAGQdthxaYm29foHZ+p8Z3Vjd1qmBsYJ8q6XEW7+QWnkTX8jjodrJN26wxqjXoSlNd2XK+Dv03oYqMAuSLAVdV41bzo/AGBRJPu4jt1x/UGWPmosk2CdtPob5pRSRoaXWoB3pl6/YkthVdWe/RuszJAT2lCdPQH777/4bcJNosGwCDEYEMHhpzcuIPejXbYLb8OKQeQSK1+zk5ARzcF4PzwnVnh9WhhUwm9p9Sflj+fWv+oXt0wgEfKFsyTRYi6VzVR0BMsv37u6wTbo922Kg03opyeRl4oLyI6suN6exG9uq9cTFlRmbKdOiY4GwhzXTozK+N9pXGASZgzBsR1RTQ9gChefZoBPDVYv0bvNivjbrMibsIXMaxN40Dn4R6EeJmg8nqz8iIhjTIKoeShPnRmJvWmWQjToBKZiF6d/v+eTScY2Wror0Slu81Kr9usyPr2yXyaNRNoR49ijDbt0DMZMNUh7sGIGnCeRuBxNkaXD9Z4c7DY0I1pnW6z0kBvQEdq25AdmnrvKDUCYXLYacE0KJ1G/0AMXp2u76lRi5RabDOLJLB1jS7yrW3Tjy2m8+JmrW7sbuP6b5roc1yc9rgpWr67qEdXUb2kgSehP12ri2BgGct3C5r+Y5U3h7R79IBRs4PloEZEVL7brJSf/Pn4djItft4u8u/SKQ+5oFPVfJhClZQcm/Uieu6HIbMg771O/0BjD3NdScBanRr1xMYDQW2Yupt2nuWwRvwg6Mb/9fhR3Os6P3T12qFyrUIP2kGx/pvGHNohDDkN1YFgqvST4HvUcEYyMGjQvxPCkKPqTw26hGt1U728Vdhg6PWA4JoIkjzAwx+jrN/88u9TY/AOx0sNQ2mrKJgZ4OmyIvr+0HG9MdGFdq4cHof1Sq5YfM9xve0UlcsKTy0G44Y6OIm/44xsoPHm1Oh0+g8C9F+P0kjDfaOFPGBLJRwl9Wt0zAcX8Gb7dd4SGskWbqU0paBGeDjkLG9YXZN3vYXVZR1lB1nNcBZeVU3diLwxTtdVg8jhLq1tZXq7ktIAXA7z5Jshctv3NRVvSelP1+qUHVsfCmJF1F0Y7bDfqmGuPMgTbxpGe2HgN7HWfKajMg4yZc1MJIcdaRNfOiO/bYhNlA2bQdQtpDZF2XXs+1S0zVx/NGJd/b3E2nMGrUqvCQPACHbQxoU87nEuKt8rxPUQh5BtHWAOPMoS4bbpAANl8JjJkmNGRKlnON4Lb5qJJai/KaK+N4OCosSwMr0mPDJOgU2HOoQ6xGFK1Lps32kceogyBgtrhjLKEQbwjCe3KPLGKK+K8kxlv/vKWnIFX0VNTtrv8EwTbVLc4pcZGaDOZeWEmaLyG+zjZ4ypUhVFf/lb6vSfyFN2UF9xvULY/LuhzBdsPzXY+mevKev3FTS8RprXUdhe2ekGpx2MaMZhnoUc2Ba1nHfmj6wi+p/694SXzxJQDX2Epwpbi/WGzqSHeRh63OBA/JCINS6pbIGfj84wJ9HtObf+ibls6AyDGPactxfhOX1s6AzzkpomWLoQj+5iQ2eYl1Bvbm2BDIUNnWHMe86tX5tL2NAZ5jm6Az2t2nPuR1ae1MIkw0GUgqKUIQtsCtjutOz+iwXOozNMBuCpO8NkADZ0hskAbOgMkwHY0BkmA7ChM0wGYENnmAzAhs4wGYANnWEyABs6wyw6APA/10RwGGUEudYAAAAASUVORK5CYII=",
                "LOGO_ALT": "iVBORw0KGgoAAAANSUhEUgAAAPsAAAA9CAYAAABrwx8PAAAACXBIWXMAAAsSAAALEgHS3X78AAAOI0lEQVR4nO2dCXAVRRrHPxACIQGFICJHAMOtQpBgOCVyJAgKCAKWLCAoAsUSdVkWOUrQEhWlUOO6rOImAuJyqqxIQZD7CgRZOQyEEIFwyGmUJJAEWLf+Y/oxb6bnese8ebz+Vb1KZV6/mb6+7v6+/r6eck3jJ95FRLEkEAhuZ36tUCbom0QzCwS3NVvKi/YVCEIDIewCQYgghF0gCBGEsAsEIYIQdoEgRBDCLhCECI4W9jaNa0sfgUDgPRWcWIc94hrTC88lUd36Nam0pJS2bc2i1CWb6cT53xyQO4EgOHGUsGMWnzJxoCTktyhP8e2bSZ//fL2L0r7OoKsl1/3y/MZ1qlPyC33crqV88i0dO5vvl+cJBHbiGGGfOaEv9Uxqo5umb/8OlNAtltI+S6c1u7J9noeI8EqU1Mvdc/hfi77z+XMEgkAQEJ291l1VaNKontSiwd2ua4d+PEE52WcNf1utWji9mNyP5s8e5fZ7gUCgj60ze8N77qQ+Ca2kGRqs3vCD67vCqyW0ZPlWuq9BLUrqFUc1766me6/o6Jr0/tsjaXdGNv19wXq68OtVv+dfIAhmbBH2KpUq0vihCdSjp/4yHfx08gKlpqVTbOsY6tylJVW44w7d9Eyfnz9/La3YeMCHuRYIbi9sEfZXxvaWBNIsJaU3aHdmNmVlnaQ2sTHUNq6x4S9Hj+4l/RUCLxDwsUVntyLocgqKiiltyWZ66ZU0yvoxzzD9kCFdpVWEQCBQExQedIdPXqSX31hCH6SsoitXrmmmg/Hu/oa1bM2bQBAsBJW7LLbbJr26UFfgG9SNsjVPAkGwEHS+8fCiW7p0iwNyIhAEF450lzVizY7DLoOcE4kMr0gxdaKodYt6rtzlnrhAuWcu07n8Ip/kuHWMe8xA7tnLVHjNP56FtatH0D01qrrKg7Kc/6XA556F8GCEYxP5uDysPeTszz3nk3sHE0Ep7P5yl/UWCODgfh3oqcGdNO+0JyOHFn2xkdL35Fh+WqcHoql3YlvN+x/NPktr12TSum2HJEEcP6QLJb/cz+27j5Zuc/sN0rTv0JIebt9E+j/103Sa/Wm6JOADEx+iXr3bUdNmdTSf9+FH3xiWJTtjDl2+XEB7M3Op4EoRZR89RTv35UoDSOf2zalzlwe4z8BvVn21ixas3OHRIJn4cBMa9kw3V9mUoC0ydmVJ9XUuv5DenDxE8qBkeV29ZrdUNtT72Ocfc7vPhOSPPWpDKhvU3ntrpKvMqMfly7fSwtV7PbqfWYJS2AGs8y3vj3ZATv6YOaaMe1xXyBnoMPiggae//oWpGQb3Zx1RD3Seps36SQKODhsVVdWVGt8dP96AiNyFvVmLBm6deNTziZrCpwRpPkwZIwnNuCmpujMx8iLPvzJ/Wr9BfvCZ9cYS08IAYZrxt8GaQs5gbYH6QnuwMrO87t17RPp/f+7Pqnuh3H0GzLK8ukFbLpyfrGobrJb8jeN19qpVKnGvFxRoG+nsBB0rfcVUU4IuBw28bPFfpdlHD3Z/I0FXYiRIRnmzAgRh3lujpI7sr/xNm/40TX4+0TAd6gvCZCToSvTKjEEMKx4lyqApMyTFN1eVHQPNjkPGW8ve4nhhrxVVjSpX0veiCxS8UdoqmCGwTOThi/vbBYRrRN/2fn0aZng9gWeC7o/6giqhBAMw1B0rPDuypyr1Z2nrfZ5fHkGxjI9rWpcysk7TjZv/c0BuboGlO69jrVv7Ay1dsdVttEanSOzUgsaO66P6zbuzR1LiU2+qlsFYumt1XMw02zOOSEa/wuJSyQDVuP7dUmeyOjNrgaX24oUbafveHEndwODTOuZeGvLUI9yVBpbDK9P3WdKvVyzbQVu2H6KdB0+4yg+hHdi7nSTcSnAN5ebNhFi68+pLXg4Y/gDqq3NcExo6vJupwQFlQp0r8zRiYCfJxmEGrOKUbYO8rdt9xHR9eYNjZvb837Q7SGREOCW0aUS1q1exNU96wBjHW7qjQyTP/FzVGdFZoHMOH50iNbAcSUeMb+52DffnCRT0Y+iK6GB4Bu4LIYEwrtx8kJ4YMVfSb70FS0vkFQY9ZlfAc/BMlA8GKh4Y0MzAyjFt7leSoUs+0EEPRvkGD52jqisqGxyVKsPwx+O4S3c8Z8Cf5rjKgeew+sI1DLK8JToP3uwO4Tc7uz/eO151DYOQv3ZRlDhG2M9c1D+FpkKFChTbrD7Ft6hH1aqE2ZYvLWB1V4IZ3WiUR0f+57xvVdf79+/o9v9zw3qo0qDjwxBmZBTCoJLy3iqPy4bnvDwlTfc5EFCekMTFNeemVzLnw1WG5YBAjuMMKhgcOz7Y0O3aoEGPqNIxw6HeSgOChjZD2xnBZnclmN2NwGqFN3hjJWQXjhH205cKdGd3RvU7I6hjq0ZUo0Y1KleunF3ZU8Gb1bFVY4b0HYdVqTArsdkKf3kdY9LkNNOzgDedCLONGSvzyjWZqmuNGhm7K0MIze5zIx1PwOSzJASJp7q89s4y0/Vltu08nd2hliiBCuMrvwszOMpA9/3Rs5T382VTaSMjK1NEZGUKC7PfeKd0aGEUXS2RvjP6YH+ZB3P8gF6sxKrFFp3oqInDQHhAtzUDb0AwYy/I/9VaB9cyjjE6PhSj+h6CZGVb7DxHXeDhyeyOgYBnf1i2apfp/PkCRxnoYIDLOnmJzucXUuO6NaVZ3IiwSmFUMYyouLiUbt64aUs+YQjjkfppsk/uH8MJ5lnLmUWNiKrpmVXaincZZmmr21xWYQOXciDBjA6Bvrd2DdUd9+075rf8YPBRCi/+13L+4dkxrKxufEVAZ/ZGddSNBC5fKabdh0/ToWNnqbik1PA+WM2Hh4dReJVKdEd5/xcpIpK/9+9PCoqs+xXYsWVndZb2lOPH1U4nzLW2bj314Hjs1EVLTyq6VmI6rdbszhNqqGTYgVECL0q7CaywR+vrd9Djtx84SbmnLtKNGzcM7wehrxiAZb2vsNLhghlsSzoNNnCYhadaQKiVuwQwJCoHXRhAPXW19YaALuNxFt3ydd/rnh+HpX3OmV/o1KXfqGV0LcnJRsnvv5N0vvz16zepuNj/2xjnLvB3DrzZ8tp/+LSujslbqgYr8Ie36jEW106tl7PBEW6tSoMmgnb8uUzm7btDqAd0b+3m1jth/BOq3/J2Y+wg4Dr7lD/3pWnvrjQMbikuuUn7cn6mqPP51KLBrRVBSfEfQm4nWsacLzfs98meKQRfSb8nO5h23iAdI6IT8KQsPJWEDY5FheoVUc8ebf0eWMLT3TG7s36AfPMMlvg+ENiyjNc7UgrBLAtSxlLX2IaaaeRAn99+MI/y8i5QYcE12wWdygxYPGcP5d6vpzAvLzno7AMTHjR9x3sc7GKLsmi5CPPg+RzA2s7Ysf8n1fcwGloZ8OBNZxWe7s5md9LIN9Lb5USjxBZhX7/hv7rf4zipqZMH0+sv9guas+AReqlk5oxnLAWDaKEVeDFx0gDJAm0G3vLRScDl1gwY4Hg+B3CxZUDoeE4x81LGmKovtBncZj1BS3fXcqLhpbcLW4Qdx0mZOTASB1PiLHjEWHt6cORWP265yOE1GkZ1RH/pdTD4Ry96f4wU4y3/4JqZ+yPQQ2+Gx57urL886TP/eH8BQTCKYoML7Jtvj1Bdxzac0sDFe3MPqy+9VQTaCm3m6c6F1uz+7ZfTVGkxINnpRKPENp0devmgnrH0zNBHDdOy1zzh+CmzR0Pb/bIINBoMcgi9lIPlIxpaHqgSGR5G99WrKXl98UZ7qASTXvtcdX+4vLLDJxjoSBCAZ8sOo8g5fk6yIcAg1axpfcuhtoGExc4j6gtbZVBfzAT04BwAJVCtsLRXlh/1Bf8H7Guv/+57V9w4fBmg1/vCR4Cnu/MI9C6EbcIOA9yC1Zm0JfMojXo6wfB4aSztcfRUUlIczf3HN9IJszzwskcji76/gAEIvuA8AWaHLpgB/t+8ER+BGvJTZOSwgyqCHZSDN3trgQAcLSv7W/NWU3R0LW59sYMq/IFWRJwcu2LW9bB9nx0HRr76wSqaPnMx5eVdMkzPXvOEd8PJl/YZe47QmJc+lgQikK9+mjp7qemoKR56nRcgkMNMkEYoYHQUFGwdqC/M4naD2Z1ntGXg+K5AEzCnmszsMzR6cqr02ia9o6EZVau6h7fCDuCE97WzqCl0RCu+6EiLEE4j5wrcHyGlvghbdQoouxWBNFtXJBN4bwZgT8DszjPaUpmahnj9QBPwfXbo5DCqDUpq63rhoxzo4v/+eqfmMt6X4MRUpVDhmhnQEdP3zJUMcF07P0CPdm/FNfqwwxqselBBZUC0nJlDILdvOyTZCwqvlUr2AuZrXzUiXJUe0V7srDVP8OT3cH3FAAYrOLYrmzSqLZ2Fh4g5Vi6sZs6cvqB5UIUebABGVB6izfQOtNy04YDr8AzYC1h9oa60nKe0WLvpAHcpDyeaQG23ySnXNH5iAhFtCnhOiKRttxeGdZf23rHE/yR1nbQCCFbkRxhj0PClJZYd7yzHn8dJewN2G+RAkCHsdqI8Ttof3nWw+vOCobo+NiOgVvgytjgq6o295ul2gZ2I4g/QeRzQgYIGf7YFA27ASqBOOKWdgu6NMAKBE9GKWcfS3ikIYRcIfABsKUqgrjjpzTNC2AUCL9FytzV71JVdCGEXCLxE68UPgYhZ10MIu0DgJYF88YMVhLALBF7Ai1m388UPVhDCLhB4Ae/9AU5xolEihF0g8BAY5uApKQduwIE6icYIR3nQCQTBCGLi2YGVTvVidJwHnUAQjFh9R3ugEMt4gSBEEMIuEIQIQtgFghBBCLtAECIIYRcIQgQh7AJBKEBE/wfwOXLmF9GjoQAAAABJRU5ErkJggg==",
                "META_LOGO": "iVBORw0KGgoAAAANSUhEUgAAAMkAAADJCAYAAACJxhYFAAAACXBIWXMAAAsSAAALEgHS3X78AAAJ2UlEQVR4nO3dzW8bRRjH8UlIXyi0SSgIVEHriiIDUhW3B2SBRNKDxSVSw1+Q5C9oKlm51pU4WZbq3rjF3DhRI/mWQ5MDkm+kN3xAJGcOdUCCUlCDpn22mmy9fuz12t71fj9SJBo29ni1P8/L7sxMHR0dGQDBpjk3QHeEBFAQEkBBSAAFIQEUhARQEBJAQUgABSEBFIQEUBASQEFIAAUhARSEBFAQEkBBSAAFIQEUhARQEBJAQUgABSEBFIQEUBASQEFIAAUhARSEBFAQEkBBSAAFIQEUhARQEBJAQUgABSEBFIQEUBASQEFIAAUhARSEBFAQEkBBSAAFIQEUhARQEBJAQUgABSEBFIQEUBASQEFIAAUhARSEBFAQEkBBSAAFIQEUhARQEBJAQUgABSEBFIQEUMxwguIrmy8unZmf37AF/Ovx441Ws7Kf9nMyDlNHR0fp+9QxZ8MxffLkN8+ePv3CV9K7xphqq1lpp/0cjRIhiZFsvpiZef30D//9/eRal1IdGmNsrVJL3QkaE0ISA9l8ce7M/Hztr8ePb/ZRml1jTKnVrOxM7ImJCUIyZtl8MTc1Pd08evbsVMiSfCc1C02wIWF0a4Sy+eJaNl/ccN/xoy+/ap/PXD41PfNa2IKsGmP2s/liKUGnIlEIyQjYjng2X7QjU1vGmBXfO+bm379iMp8tmnMXLoQtzKwx5o59j2y+6H99DIgh4CGyNYdtChljFrq8y/Nm0mszJ8y7V66a2fc+ML//+ot5cngYpmCXjDEPsvmi7a+sMWQcDWqSIZBmlVdzdAvIK06/OWc+WMib9z69amZOh+2mmEVjzG80waJBSCImfY4t+VYP7ezbF8zF61+Yty5eNFPT0/+EfB3bBGOoeECEJEJ2KNcOy0b1irYJdj7ziblw9fq3MooVxio1ymAISbRy0omO1JnZ8+1Ws2L7NzeMMY9CvPaGBBghEJIEsTcOW82KDeK63Hnv1WyHUTX0iJAkkDySkumzVsmk/byFRUgSSu6wL/URlFzaz1lYhCTBJChrPX4C+iQhEZKEazUre8aYH9N+HoaJkEyGvR4+BQ9AhkRIJkMvIenlGHRASCYDtcQQEZJkWEr7CRgnQpIMDN+OESFJhtnlze1eh3oRMUKSHNXlzW3udYwBIUkO+/zVDkEZPUIyfv3MHrQTuPaXN7d5WHGECMmYNcqF/T4fVLQ1yoPlzW1bq9ChHwFCEg9hJkXZKbo/L29u106cef3NSTwpcUFIYqBRLtQHmXn4zocffz9RJyRmCElMNMoFO8R7P1RppqbemMBTEhuEJEYa5cLGAFN0MSSEJGYa5cJOo1wIM0UXQ0JIYqpRLnhTdO+m/VyMGyEZvZ6HbRvlQrtRLtiRr8uDTqw6fe7s5/E7FclASEZv1q4k38+72nspjXJhRforB2FK/OSPP0/G83TEHyEZj1CLxUl/xTbBbtNfGR1CMh43ZTHtUBrlQlX6K2HvraAPhCRa/UyR3fLvVdIP6a/YoF179u/Tn+J5OiYDO11FTFaT72ex7IG3SbD7nxhjHmrv02pWmOEYAjVJ9Ppdxd3bJqHKer3xREiiVw3Zqb4l27qFboIpWC0lJEISMWf50TBBsY/B38vmi3vShIrKYZRbQqQNIRkCWVWxn3V6/ezkqofZfLFu93YfoISHcsc+w+684dFxHzIZ6q0OsG/Jofx9NehC79Bx9/6mxr6Jg6MmGTJnm4Rwj8HLzrq2T9HDzrpuzVEiINGgJhkhaTrVZEQrrFeGjGVUbE1qDppVESMkYyDNo9oAm4/ebjUr1USfhAQhJGMkw72lPvorthbZkIEBjAghGTNpKtlaYbVLSWw4bB9jJxUnJWYISUzI4/NVX3/lQPofhGOMCEnMuDcRCUc8EBJAwX0SQEFIAAUhARQznKBjI0uGiUnwIyQvzA34qAgmGM0tQEFIAAUhART0SRTybJV9EHFFZgzaR0V25FmqV+ZryCQrd02tPTne/q7t/Z083Ghfsy6/X5HZjG3/6zsDCzuyfVzOt1xqTeat+Mvild0djPBeY03+uyavPdft4Um7UIW8Zz1tTyBzx903s6/VrEw5v8/JhRT0lO66d3HKBVnvYQDghn3cJJsv7ijH2glUS/ai7XHJoEdyfLvHsj9nP6+zDFLHZYck+Fv+z5wWNLe6qzsX2X1Zi3fdWY93y5mDXnIu+gOZIbje4yqLh87x3sLYs10Wb7Cv+bUcvyu/W/CGsYVb9l059nbAvHvvol8MWIDCqxkP0hYQQ3MrmHx7epOijk1ysquZ2P0K5Z9r2XyxJksCGf83ur0A5fh7AW/2ssZwjrcX+E358btrp+Y6v3OPX83miyVpXnll9x9f7VCLVaVZ5gXzZVAkNN6xqQuIoSbpyv1GzdiLz/uR/oNnydc/KHWYQtvt4qp36Ae8PL7DgnWdngx2Q7DklP3QF5COryHl9b4E/LWJ279K5WxIapJg7lI+t7oct+eGpNWs1Dsc022rhU6LNbgh8//tK8dLv8X7Z8Ypez8zGN3a5HmnXpqS3mSw79I6f56aJJh7Md4I+mk1KxvuxdjDiiaD6tSxdoO075S9531QfLXJqgTEXU0ytYvbEZJgbpMkZ0ek3B/vf8jF5H5jlzo0kaJ8HqzTlg1uM2jHKfus9Jf8gsrjLtFadd5rN83LE9HcCmBHcaT/cUmWHs1JXyEn37Bex/iaNHfuS7NsQdbI8i7OjDJ/XbPnqxEWpaNe8+5tyHta9+Virsl9mAWnVqjL8SvO8cfY2kQ+8z3foEGql0ilJuluxRnuXZV7FfecgKw7ne6SMxx7SRaUuzNgQExAP8BewA/k3oV3wT/yXcxrTq2wKOW+ExQQ5/2qvi3nDtI+jZia5IX9TrvcSg2RkwtuRWqFtny719yLx1so27nj7g2b7srxbfkmfzk07GsaqeURt+V1vCZTW0bIjjWrpOwZ5477ogRp32keBq0z7O6xkvqFtrnjngC+O+43hv3NLvd1FqQmSv1i2zS3cIzUPl6TLHCR7jQhJPA7djefs0NI4JCha/fmYepXpTd03BPD7cgP88Kdkwc5/TcSU42OO6CguQUoCAmgICSAgpAACkICKAgJoCAkgIKQAApCAigICaAgJICCkAAKQgIoCAmgICSAgpAACkICKAgJoCAkgIKQAApCAigICaAgJICCkAAKQgIoCAmgICSAgpAACkICKAgJoCAkgIKQAApCAigICaAgJICCkAAKQgIoCAmgICSAgpAACkICKAgJoCAkgIKQAApCAigICaAgJICCkAAKQgIoCAmgICSAgpAACkICKAgJoCAkgIKQAApCAigICaAgJICCkAAKQgIoCAmgICSAgpAACkICdGOM+R+ESdPucGsK5wAAAABJRU5ErkJggg==",
                "QR_LOGO": "iVBORw0KGgoAAAANSUhEUgAAAHkAAAB6CAYAAACbbvznAAAACXBIWXMAAAsSAAALEgHS3X78AAAFpElEQVR4nO2dTWwbRRiGvzhuWhogoaUUBaiNVHWbooq04mcRoITDHpCMUtRLb0nvSGnFykJcsA9cVpYwJ4SEVPfEkVTamw+kN98IR0sIHIRoERVJBUGK69howmzlRk3inV3bO/N+j5SL450d7+P5n/k8cubNj2eIaJIYU2mkiahMRLOs2FiKKfQngEC66zMW67VSAf2BmIJluytBDc0lGQCWDABLBoAlA8CSAWDJALBkAFgyACwZAJYMAEsGgCUDwJIBYMkAsGQAWDIALBkAlgwASwaAJQPAkgFgyQCwZABYMgAsGQCWDABLBoAlA8CSAWDJALBkANJoH9iy3cnRw2OF0XT6SqvZvNt+0LpWr5VWEpC1vgEjWcgdSae/GUmlPtjeao5tbzXFyyeJ6HvLdm8RkZDdGH5O4weiuj43+2l5JJX6o9NqXe6022OPecs8Ea1atlsQX4YhZLGvGC35wvufX5l+95O/t7eaS3vI7WaCiD6Tsi8NNqf9xVjJZ9/Of/Xv+vq37QetJ0NemiGi70TMDct2Z/qUvYGivWTLducs2122bDfb/XrmtXfo2KlTlEqPqiYtgqr8YNluRfcqXFvJQqoQIDpOsk19RPKhw09MH89O00sX36Kjx49FudWCCHgm2uvImR4S2vWuRcklokX58A9k7Mg4vfDK67S58Sfd+7lOzX82VW67015btivuu6jbkEsbybI6rqhGDxyfPEHjF0/Q+m8/0V+/Nqjd2lZJJtM15BKyN1QSGTRaVNdS8C9xhId85sXTlH1jlp6emoqSjGgeVnRpq3Vpk8txJjaaPkQnT5+n58+d/5qIbism8yoRLceZr36hi+T5fiT61LNTd+u1kmjjPySiNYUkZmUfIdHwAgUR1Wul5XqtJJqEIhHdD3n5Yp+yFRssuQsZQFZMgPwY4rJsD+8ZKix5F3KRYi6E6MTPirHkxyCHRtd6fPvEQDOnAEveAznhodrzThS6SB7WwzZiMwGX5P1hyYwesGQA0CUbsSngINAlz+fy1cRPZkQFXTLFvfiRRFjy/6U58fPPUTBZcpjhz41cvmpsiTZZ8mrI9y/l8tVGLl81ajsumSzZ95xlhTXine24uXx1JZevGtPzNr1N7nWRYTc723GfO3P2o/5lbXAYLVmW5quq16ePHL0cb46Gg/G9a99zxA7P9xS39xgBxBDK95wV33NUt/doD9Q42fecgtyuczMB2RkYJknuaQ+07zkbvueIyY8LpmwKOAhdJPeyqT7U1ljfc1Z9z5mTHTOj22uTSrLSJIbsmM2Y3F6bJDmjevJQVuHBdtxb8WdtuJi2xys4eaiE7zkN33MumTbkMrF3fcOyXdWZrh2CIdfmvTvX48vW8NBFctiDZV/IcBCRzind//1O2EWORKKL5IpCp2hWniWu7A41gYYWkuWJBtVwDgt9Dt+U+I6aNm1yvVYqR5ip6lf4prUIK10DQ6uOV71WWpRniVXHs3GFbxL3L4rjrjpE8dOudy3OEsv552KEZILwTeWQVfianCHLymOuWqDlEEq00fIhvxyxTVyS4Zv2qnKD3rUouVdlya3oEhAmQOtxsqgq67VS1MmLCTnkWt1dhUuZQcmtxJPrwWNElFx5zDQrS2RB8cxwVk5rPjI21llugFEzXrIHLmR9GfLSm7qX1v0wLt51ECVAhmQU0vdbphRyC6bGuQ4wNqh5vVYS1a4Irrooq/BM179vy4h6RssNMD5yvaiCRRTdrhOMG/ILAAPEzxPIKtzo35nYDz7wBgBLBoAlA8CSAWDJALBkAFgyACwZAJYMAEsGgCUDwJIBYMkAsGQAWDIALBkAlgwASwaAJQPAkgFgyQCwZABYMgAsGQCWDABLBoAlA8CSAWDJALBkALqPrmajxqJkEsXD0FXdkhfkH2MYXF0DMNLpdNCfgdkQ0X+GxXccx2MAaQAAAABJRU5ErkJggg=="
            },

        "pool_type":
            {
                "TRANSACTION_POOL": 1,
                "COLLECTION_POOL": 2,
                "LOYALTY_POINT": 3,
                "PAY_OUT_POOL": 4,
                "THRESHOLD_POOL": 5,
                "PREFUND_POOL": 6
            },

        "weight_condition":
            {
                "Kilogram": 1,
                "Gram": 2
            },

        "bank_type":
            {
                "B2C": 1,
                "B2B": 2
            },

        "shipment_status":
            {
                "incomplete": "INCOMPLETE",
                "complete": "COMPLETE",
                "pending_pickup": "PENDING_PICKUP",
                "pending_drop_off": "PENDING_DROP_OFF",
                "in_transit": "IN_TRANSIT",
                "delivered": "DELIVERED",
                "failed": "FAIL_DELIVERY",
                "received": "RECEIVED",
                "refund_requested": "CANCEL_AND_REFUND_REQUESTED",
                "returned": "RETURNED_TO_SENDER",
                "refund_denied": "CANCEL_AND_REFUND_REQUEST_DENIED",
                "cancelled_refunded": "REFUNDED",
                "cancelled": "CANCELLED",
                "pending_shipping": "PENDING_SHIPPING",
            },

        "bill_transaction_type":
            {
                "NORMAL": 1,
                "PREFUND": 2,
                "TRANSACTION": 3,
                "BILL_PAYMENT": 4
            },

        "leanis_image":
            {
                'DEFAULT_LOGO_ALT': 'https://lean.sgp1.digitaloceanspaces.com/F5BAF7_LP_LogoAlt.png',
                'DEFAULT_FAVICON': 'https://lean.sgp1.digitaloceanspaces.com/CD3CFF_LP_FavIcon.png',
                'DEFAULT_QR_LOGO': 'https://lean.sgp1.digitaloceanspaces.com/BD3C4A_LP_Logo.png',
                'DEFAULT_LOGO': 'https://lean.sgp1.digitaloceanspaces.com/BD3C4A_LP_Logo.png',
                'DEFAULT_WHATSAPP_SHARE_IMAGE': 'https://lean.sgp1.digitaloceanspaces.com/6973E2_LP_WhatsappShareImage.png',
                'DEFAULT_WHATSAPP_STORY': 'https://lean.sgp1.digitaloceanspaces.com/22FC79_LP_WhatsappStory.png',
                'DEFAULT_WHATSAPP_PROFILE_IMAGE': 'https://lean.sgp1.digitaloceanspaces.com/451A8A_LP_WhatsappProfileImage.png',
                'DEFAULT_OG_IMAGE': 'https://lean.sgp1.digitaloceanspaces.com/6EF060_LP_OGimage.png',
            },

        "subscription_time_period":
            {
                "MONTHLY": 1,
                "YEARLY": 2
            },

        "error_code":
            {
                '4000': "GENERAL ERROR",
                '2111': "RECORD NOT FOUND",
                '3333': "THE RECORD CANNOT BE DELETED",
                '5788': "QUOTA LIMIT EXCEED",
                '10289': "DUPLICATE INVOICE",
                '10288': "NO LIST IN THE RECORD",
                '9100': "SERVICE IS NOT AVAILABLE RIGHT NOW",
                '3614': "THE INVOICE NUMBER IS NOT AVAILABLE",
                '10018': "PAYMENT SERVICE NOT ACTIVE",
                '7651': "INSUFFICIENT PRODUCT",
                '4637': "NOT A VALID PRODUCT ID",
                '4626': "BILL INVOICES ALREADY EXIST",
                '10201': "UNAUTHORIZED DUE TO PERMISSION",
                '6666': "JWT TOKEN INVALID",
                '1106': "AUDIT WRITING PROBLEMS OCCUR",
                '1104': "TRANSACTION CHARGES PROBLEMS OCCUR",
                '7777': "JWT TOKEN EXPIRED",
                '10041': "PAYMENT SERVICE ID NOT ACTIVE",
                '2610': "TRANSACTION CANNOT BE PERFORMED",
                '2644': "PAYOUT CANNOT BE PERFORMED",
                '6899': "PRICE MUST BE ABOVE ZERO",
                '6214': "VALUE MUST BE ABOVE ZERO",
                '6825': "MAX PRICE MUST BE ABOVE ZERO",
                '5789': "QUANTITY_LIMIT_MUST_BE_ZERO_AND_ABOVE",
                '1304': "POOL_REFERENCE_NOT_FOUND",
                '5993': "INSUFFICIENT_POOL_BALANCE",

            },

        "banks":
            [
                {
                    "name": "BANK PERTANIAN MALAYSIA BHD (AGROBANK)",
                    "id": 2,
                    "unique_reference": "AGROBANK_WEBPAY"
                },
                {
                    "name": "Ambank Malaysia Berhad",
                    "id": 3,
                    "unique_reference": "AMBANK_WEBPAY"
                },
                {
                    "name": "Bank Islam Malaysia Berhad",
                    "id": 4,
                    "unique_reference": "BANK_ISLAM_WEBPAY"
                },
                {
                    "name": "Bank of China (M) Berhad",
                    "id": 5,
                    "unique_reference": "BANK_OF_CHINA_WEBPAY"
                },
                {
                    "name": "Bank Kerjasama Rakyat Malaysia Berhad",
                    "id": 6,
                    "unique_reference": "BANK_RAKYAT_WEBPAY"
                },
                {
                    "name": "Bank Muamalat Malaysia Bhd",
                    "id": 7,
                    "unique_reference": "BANK_MUAMALAT_WEBPAY"
                },
                {
                    "name": "BNP Paribas Malaysia Berhad",
                    "id": 8,
                    "unique_reference": "BNP_PARIBAS_WEBPAY"
                },
                {
                    "name": "BANK OF AMERICA MALAYSIA BERHAD",
                    "id": 9,
                    "unique_reference": "BANK_OF_AMERICA_MALAYSIA_BERHAD_WEBPAY"
                },
                {
                    "name": "MUFG BANK (MALAYSIA) BERHAD",
                    "id": 10,
                    "unique_reference": "MUFG_BANK_WEBPAY"
                },
                {
                    "name": "Bank Simpanan Nasional Berhad",
                    "id": 11,
                    "unique_reference": "BSN_WEBPAY"
                },
                {
                    "name": "JP MORGAN CHASE BANK BERHAD",
                    "id": 12,
                    "unique_reference": "JP_MORGAN_BANK_WEBPAY"
                },
                {
                    "name": "CIMB Bank Berhad",
                    "id": 13,
                    "unique_reference": "CIMB_WEBPAY"
                },
                {
                    "name": "Citibank Berhad",
                    "id": 14,
                    "unique_reference": "CITIBANK_WEBPAY"
                },
                {
                    "name": "DEUTSCHE BANK",
                    "id": 15,
                    "unique_reference": "DEUTSCHE_BANK_WEBPAY"
                },
                {
                    "name": "HSBC Bank Malaysia Berhad",
                    "id": 16,
                    "unique_reference": "HSBC_WEBPAY"
                },
                {
                    "name": "Hong Leong Bank Berhad",
                    "id": 17,
                    "unique_reference": "HONG_LEONG_BANK_WEBPAY"
                },
                {
                    "name": "IND AND COM BK OF CHINA (M)BHD",
                    "id": 18,
                    "unique_reference": "IND_AND_COM_BANK_WEBPAY"
                },
                {
                    "name": "Kuwait Finance House",
                    "id": 19,
                    "unique_reference": "KUWAIT_WEBPAY"
                },
                {
                    "name": "Maybank Berhad",
                    "id": 20,
                    "unique_reference": "MAYBANK_WEBPAY"
                },
                {
                    "name": "Alliance Bank Malaysia Berhad",
                    "id": 21,
                    "unique_reference": "ALLIANCE_BANK_WEBPAY"
                },
                {
                    "name": "MIZUHO BANK (MALAYSIA) BHD",
                    "id": 22,
                    "unique_reference": "MIZUHO_BANK_WEBPAY"
                },
                {
                    "name": "OCBC Bank Berhad",
                    "id": 23,
                    "unique_reference": "OCBC_BANK_WEBPAY"
                },
                {
                    "name": "Public Bank Berhad",
                    "id": 24,
                    "unique_reference": "PUBLIC_BANK_WEBPAY"
                },
                {
                    "name": "Affin Bank Berhad",
                    "id": 25,
                    "unique_reference": "AFFIN_BANK_WEBPAY"
                },
                {
                    "name": "RHB Bank Berhad",
                    "id": 26,
                    "unique_reference": "RHB_BANK_WEBPAY"
                },
                {
                    "name": "Al-Rajhi",
                    "id": 27,
                    "unique_reference": "ALRAJHI_BANK_WEBPAY"
                },
                {
                    "name": "Standard Chartered Bank Malaysia Berhad",
                    "id": 28,
                    "unique_reference": "STANDARD_CHARTERED_BANK_WEBPAY"
                },
                {
                    "name": "SUMITOMO MITSUI BANKING CORPORATION (M)",
                    "id": 29,
                    "unique_reference": "SUMITUMO_BANK_WEBPAY"
                },
                {
                    "name": "United Overseas Bank Berhad (UOB)",
                    "id": 30,
                    "unique_reference": "UOB_BANK_WEBPAY"
                }
            ],
    }
