import datetime
from typing import Any

from app.constants.system_constant import SystemConstant
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Body
from app.constants.payload_example import PayloadExample
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.Utils.shared_utils import get_next_billing_date, SharedUtils

router = APIRouter(prefix="/subscription-payment", tags=["subscription-payment"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_subscription_payments(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available subscription payment.
    """
    try:
        list_data = crud.subscription_payment.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_subscription_payment(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.SubscriptionPaymentCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new subscription payment.
    """
    try:
        query = crud.subscription_payment.get_by_invoice_no(db, invoice_no=_in.invoice_no)
        if query:
            return schemas.ResponseApiBase(
                response_code=10289,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                breakdown_errors="DUPLICATE_INVOICE_CREATED",
                token=new_token["token"]
            )
        query_crate = crud.subscription_payment.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_crate,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_subscription_payment(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.Api,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update subscription payment.
    """
    try:
        query = crud.subscription_payment.get_by_subscription_payment_id(db, _id=_id)
        if query:
            update_api = crud.api.update_self_date(db, db_obj=query, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_api,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_subscription_payment(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete Subscription payment.
    """
    try:
        query = crud.subscription_payment.get_by_subscription_payment_id(db, _id=_id)
        _in = schemas.SubscriptionPaymentUpdate(
            id=_id,
            record_status=4,
        )
        if query:
            update_api = crud.api.update_self_date(db, db_obj=query, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_api,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/auto-subscription-plan", response_model=Any)
def automate_subscription_payment(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new subscription payment billing automatically.
    """
    try:
        query = crud.account.get_multi(db, skip=0, limit=100)
        if not query:
            return schemas.ResponseApiBase(
                response_code=10288,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )

        _sub_creation = []
        for _sub in query:
            # date_of_billing = get_next_billing_date(start_date=_sub.created_at,
            # end_date=_sub.current_subscription_ends, repetition="Months")
            if _sub.current_subscription_ends.date() <= datetime.datetime.today().date():
                if _sub.system_preference_id == 2:
                    if _sub.current_subscription_time_period == SystemConstant.SYSTEM_CONS.get("subscription_time_period", None).__getitem__("MONTHLY"):
                        _sub_creation.append(_sub.id)
                        invoice_ = f'SUBS-{SharedUtils.my_random_string(10)}-BILLING'
                        sub_schema = schemas.SubscriptionPaymentCreate(
                            account_id=_sub.id,
                            subscription_plan_id=_sub.subscription_plan_id,
                            month=datetime.datetime.now().month,
                            year=datetime.datetime.now().year,
                            invoice_no=invoice_,
                            payment_status_id=1,
                            amount=_sub.subscription_plan.plan_charges
                        )
                        same_billing = crud.subscription_payment.get_by_account_id_no_month(db, invoice_no=_sub.id)
                        if same_billing:
                            continue
                        sub_payment_invoice = crud.subscription_payment.create_self_date(db, obj_in=sub_schema)
                    elif _sub.current_subscription_time_period == SystemConstant.SYSTEM_CONS.get("subscription_time_period", None).__getitem__("YEARLY"):
                        _sub_creation.append(_sub.id)
                        invoice_ = f'SUBS-{SharedUtils.my_random_string(10)}-BILLING'
                        sub_schema = schemas.SubscriptionPaymentCreate(
                            account_id=_sub.id,
                            subscription_plan_id=_sub.subscription_plan_id,
                            month=datetime.datetime.now().month,
                            year=datetime.datetime.now().year,
                            invoice_no=invoice_,
                            payment_status_id=1,
                            amount=_sub.subscription_plan.plan_charges*12
                        )
                        same_billing = crud.subscription_payment.get_by_account_id_no_year(db, invoice_no=_sub.id)
                        if same_billing:
                            continue
                        sub_payment_invoice = crud.subscription_payment.create_self_date(db, obj_in=sub_schema)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data="",
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/pending-subscription-payment", response_model=schemas.ResponseApiBase)
def get_pending_subscription_payments(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available stores based on merchant id.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="POST", _url="/subscription-payment/pending-subscription-payment",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_LIST_OF_PENDING_SUBSCRIPTION_PAYMENT",
                                  _data="NO_DATA", db_=db)

        db_count = crud.subscription_payment.get_count_account(db=db, _id=new_token["account_id"],
                                                               start_date=_in.start_date, end_date=_in.end_date,)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.subscription_payment.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
