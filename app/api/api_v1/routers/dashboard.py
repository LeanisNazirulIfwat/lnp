import json
import calendar
from sqlalchemy import func, text, literal, bindparam, tuple_, or_
from datetime import datetime, timedelta
from typing import Any, List
from app import models, schemas, crud
from fastapi import APIRouter, Body, Depends, HTTPException, Security, Header, BackgroundTasks
from jose import jws
from sqlalchemy.orm import Session
from sqlalchemy import extract
from app.constants.role import Role
from app.api import deps
from app.core.config import settings
from fastapi.encoders import jsonable_encoder
from app.constants.system_constant import SystemConstant
from app.models.customer_bill import CustomerBill
import pandas

router = APIRouter(prefix="/dashboard", tags=["dashboard"])


@router.get("/merchant-dashboard", response_model=schemas.ResponseApiBase)
async def get_dashboard_merchant(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    merchant dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/merchant-dashboard",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_MERCHANT_DASHBOARD",
                                  _data="NO_DATA", db_=db)

        yesterday_date = datetime.today() - timedelta(days=1)
        last_week_date = datetime.today() - timedelta(days=6)
        # last_week_date = datetime.today() - timedelta(weeks=1)
        last_month_date = datetime.today().date().replace(day=1) - timedelta(days=1)
        last_year_date = datetime.today().date() - timedelta(days=365)

        monthly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                     CustomerBill.account_id == new_token["account_id"],
                                                     extract('month',
                                                             CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        yesterday_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == new_token["account_id"],
                                                       extract("day", CustomerBill.payment_date) == yesterday_date.day,
                                                       extract("month",
                                                               CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == new_token["account_id"],
                                                       func.date(CustomerBill.payment_date) >= last_week_date
                                                       .date()).offset(skip).limit(limit).all()

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == new_token["account_id"],
                                                   extract("day", CustomerBill.payment_date) == datetime.today().day,
                                                   extract("month", CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_month_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                        CustomerBill.account_id == new_token["account_id"],
                                                        extract("month",
                                                                CustomerBill.payment_date) == last_month_date.month) \
            .offset(skip).limit(limit).all()

        year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                  CustomerBill.account_id == new_token["account_id"],
                                                  extract("year",
                                                          CustomerBill.payment_date) == datetime.today().year) \
            .offset(skip).limit(limit).all()

        last_year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == new_token["account_id"],
                                                       extract("year",
                                                               CustomerBill.payment_date) == last_year_date.year) \
            .offset(skip).limit(limit).all()

        total_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == new_token["account_id"]) \
            .offset(skip).limit(limit).all()

        total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
        total_last_monthly_sale = sum(c.customer_bill_payment_record.amount for c in last_month_sale)
        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)
        total_yesterday_sale = sum(c.customer_bill_payment_record.amount for c in yesterday_sale)
        total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
        total_year_sale = sum(c.customer_bill_payment_record.amount for c in year_sale)
        total_last_year_sale = sum(c.customer_bill_payment_record.amount for c in last_year_sale)
        total_all_sale = sum(c.customer_bill_payment_record.amount for c in total_sale)

        total_monthly_count = len(monthly_sale)
        total_last_monthly_count = len(last_month_sale)
        total_today_count = len(today_sale)
        total_yesterday_count = len(yesterday_sale)
        total_last_week_count = len(last_week_sale)
        total_year_count = len(year_sale)
        total_last_year_count = len(last_year_sale)
        total_all_count = len(total_sale)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, datetime.today().year)
        yesterday_range, last_range_month, last_range_year = schemas.GenerateReport \
            .get_range_date(last_month_date.month, datetime.today().year)
        last_week_range = await schemas.GenerateReport.get_range_week_dynamic()

        mini_range_day, mini_range_month, mini_range_year, mini_yesterday_range, mini_last_range_month \
            , mini_last_range_year, mini_last_week_range = ([] for _ in range(7))

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.payment_date.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1
            [mini_range_day.append(x.y_axis) for x in range_day]
        else:
            [mini_range_day.append(0) for x in range_day]

        if monthly_sale:
            for _day in monthly_sale:
                day_ = _day.payment_date.day
                amount = float(_day.total)
                for y in range_month:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
            [mini_range_month.append(x.y_axis) for x in range_month]
        else:
            [mini_range_month.append(0) for x in range_month]

        if year_sale:
            for _month in year_sale:
                month_ = _month.payment_date.month
                amount = float(_month.total)
                for z in range_year:
                    if z.min_value == month_:
                        z.y_axis = z.y_axis + amount
                        z.count += 1
            [mini_range_year.append(x.y_axis) for x in range_year]
        else:
            [mini_range_year.append(0) for x in range_year]

        if yesterday_sale:
            for _yesterday_date in yesterday_sale:
                yesterday_date_ = float(_yesterday_date.payment_date.strftime("%H.%M"))
                amount = float(_yesterday_date.total)
                for xx in yesterday_range:
                    if xx.min_value <= yesterday_date_ <= xx.max_value:
                        xx.y_axis = xx.y_axis + amount
                        xx.count += 1
            [mini_yesterday_range.append(x.y_axis) for x in yesterday_range]
        else:
            [mini_yesterday_range.append(0) for x in yesterday_range]

        if last_month_sale:
            for _last_day in last_month_sale:
                last_day_ = _last_day.payment_date.day
                amount = float(_last_day.total)
                for yy in last_range_month:
                    if yy.min_value == last_day_:
                        yy.y_axis = yy.y_axis + amount
                        yy.count += 1
            [mini_last_range_month.append(x.y_axis) for x in last_range_month]
        else:
            [mini_last_range_month.append(0) for x in last_range_month]

        if last_year_sale:
            for last_month in monthly_sale:
                last_month_ = last_month.payment_date.month
                amount = float(last_month.total)
                for zz in last_range_year:
                    if zz.min_value == last_month_:
                        zz.y_axis = zz.y_axis + amount
                        zz.count += 1
            [mini_last_range_year.append(x.y_axis) for x in last_range_year]
        else:
            [mini_last_range_year.append(0) for x in last_range_year]

        if last_week_sale:
            for _week in last_week_sale:
                week_ = _week.payment_date.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "days": {
                    "today_sale": total_today_sale,
                    "today_range": range_day,
                    "mini_today_range": mini_range_day,
                    "yesterday_sale": total_yesterday_sale,
                    "yesterday_range": yesterday_range,
                    "mini_yesterday_range": mini_yesterday_range,
                    "total_today_count": total_today_count,
                    "total_yesterday_count": total_yesterday_count
                },
                "months": {
                    "this_month_sale": total_monthly_sale,
                    "last_month_sale": total_last_monthly_sale,
                    "monthly_range": range_month,
                    "mini_monthly_range": mini_range_month,
                    "last_monthly_range": last_range_month,
                    "mini_last_monthly_range": mini_last_range_month,
                    "total_monthly_count": total_monthly_count,
                    "total_last_monthly_count": total_last_monthly_count
                },
                "years": {
                    "yearly_sale": total_year_sale,
                    "last_year_sale": total_last_year_sale,
                    "yearly_range": range_year,
                    "mini_yearly_range": mini_range_year,
                    "last_yearly_range": last_range_year,
                    "mini_last_yearly_range": mini_last_range_year,
                    "total_year_count": total_year_count,
                    "total_last_year_count": total_last_year_count
                },
                "others": {
                    "total_sales_received": total_all_sale,
                    "last_week_range": last_week_range,
                    "total_last_week_sales": total_last_week_sale,
                    "mini_last_week_range": mini_last_week_range,
                    "total_last_week_count": total_last_week_count,
                    "total_all_count": total_all_count
                }
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin-dashboard", response_model=schemas.ResponseApiBase)
def get_dashboard_admin(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/admin-dashboard",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ADMIN_DASHBOARD",
                                  _data="NO_DATA", db_=db)

        # total user
        total_user = db.query(models.User).count()
        # total account
        total_account = db.query(models.Account).count()
        # total collection
        total_collection = db.query(models.Collection).count()
        # total bills
        total_bill = db.query(CustomerBill).count()

        # list of onboard and activated
        account_inactive = db.query(models.Account).filter(models.Account.record_status == 2).all()
        account_activated = db.query(models.Account).filter(models.Account.record_status == 1).all()

        # total onboard and activated
        total_account_inactive = db.query(models.Account).filter(models.Account.record_status == 2).count()
        total_account_activated = db.query(models.Account).filter(models.Account.record_status == 1).count()

        # total due and paid bill
        total_bill_paid = db.query(CustomerBill).filter(CustomerBill.invoice_status_id == 2).all()
        total_bill_pending = db.query(CustomerBill).filter(CustomerBill.invoice_status_id == 1).all()

        # total due and paid bill count
        total_bill_paid_count = db.query(CustomerBill).filter(CustomerBill.invoice_status_id == 2).count()
        total_bill_pending_count = db.query(CustomerBill).filter(CustomerBill.invoice_status_id == 1).count()

        # range date
        list_day = pandas.date_range(datetime.today() - timedelta(days=30), datetime.today(), freq='d') \
            .strftime('%d-%m-%Y').tolist()

        # process bill paid
        mini_graph_data_bill_paid = []
        graph_data_bill_paid = [
            schemas.ReportCreate(x_axis=x, y_axis=0, min_value=0, max_value=0,
                                 count=0) for x in list_day]
        for x in graph_data_bill_paid:
            for y in total_bill_paid:
                if x.x_axis == y.payment_date.strftime('%d-%m-%Y'):
                    x.count = x.count + 1
                    x.y_axis = x.y_axis + float(y.total)
            mini_graph_data_bill_paid.append(x.y_axis)

        # process pending bill
        mini_graph_data_bill_pending = []
        graph_data_bill_pending = [
            schemas.ReportCreate(x_axis=x, y_axis=0, min_value=0, max_value=0,
                                 count=0) for x in list_day]

        for x in graph_data_bill_pending:
            for y in total_bill_pending:
                if x.x_axis == y.created_at.strftime('%d-%m-%Y'):
                    x.count = x.count + 1
                    x.y_axis = x.y_axis + float(y.total)
            mini_graph_data_bill_pending.append(x.y_axis)

        # process account active
        mini_graph_data_account_active = []
        graph_data_account_active = [
            schemas.ReportCreate(x_axis=x, y_axis=0, min_value=0, max_value=0,
                                 count=0) for x in list_day]
        for x in graph_data_account_active:
            for y in total_bill_paid:
                if x.x_axis == y.payment_date.strftime('%d-%m-%Y'):
                    x.y_axis = x.y_axis + 1
            mini_graph_data_account_active.append(x.y_axis)

        # process account inactive
        mini_graph_data_account_inactive = []
        graph_data_account_inactive = [
            schemas.ReportCreate(x_axis=x, y_axis=0, min_value=0, max_value=0,
                                 count=0) for x in list_day]
        for x in graph_data_account_inactive:
            for y in total_bill_pending:
                if x.x_axis == y.created_at.strftime('%d-%m-%Y'):
                    x.y_axis = x.y_axis + 1
            mini_graph_data_account_inactive.append(x.y_axis)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"total_user": total_user,
                  "total_account": total_account,
                  "total_collection": total_collection,
                  "total_bills": total_bill,
                  "total_account_inactive": total_account_inactive,
                  "total_account_activated": total_account_activated,
                  "total_bill_paid": total_account_activated,
                  "total_bill_pending": total_account_activated,
                  "bill_paid_list": graph_data_bill_paid,
                  "mini_bill_paid_list": mini_graph_data_bill_paid,
                  "bill_pending_list": graph_data_bill_pending,
                  "mini_bill_pending_list": mini_graph_data_bill_pending,
                  "account_active_list": graph_data_account_active,
                  "mini_account_active_list": mini_graph_data_account_active,
                  "account_inactive_list": graph_data_account_inactive,
                  "mini_account_inactive_list": mini_graph_data_account_inactive,
                  },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/dashboard-generate-report/day-{date}", response_model=schemas.ResponseApiBase)
def get_day_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        date: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report day
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-day",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_DAY",
                                  _data="NO_DATA", db_=db)

        current_date = datetime.strptime(date, "%d-%m-%Y")

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(current_date.month, datetime.today().year)

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == new_token["account_id"],
                                                   func.date(CustomerBill.payment_date) == current_date
                                                   .date()).offset(skip).limit(limit).all()

        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.payment_date.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-today-sale": total_today_sale,
                "total-today-count": int(len(today_sale)),
                "today-graph": range_day
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/dashboard-generate-report/month-{month}", response_model=schemas.ResponseApiBase)
def get_month_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        month: int = 1,
        year: int = 2022,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        # populate graph
        last_month_range_date = []
        range_month = calendar.monthrange(year=year, month=month)
        for x in range(1, range_month[1] + 1):
            xdate = datetime.strptime(f"{x}-{month}-{year}", "%d-%m-%Y")
            xdate_name = xdate.strftime("%d-%b-%Y")
            last_month_range_date.append(
                schemas.ReportCreate(x_axis=f"{xdate_name}", y_axis=0, min_value=float(x), max_value=0, count=0))

        monthly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                     CustomerBill.account_id == new_token["account_id"],
                                                     extract('month',
                                                             CustomerBill.payment_date) == month,
                                                     extract('year',
                                                             CustomerBill.payment_date) == year,
                                                     ) \
            .order_by(text("payment_date desc")).offset(skip).limit(limit).all()

        invoice = "invoice_no"
        filters = [x for x in monthly_sale if "BP-D905D55A04-LNP" in x.invoice_no]

        if monthly_sale:
            total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
            for _day in monthly_sale:
                day_ = _day.payment_date.day
                amount = float(_day.total)
                for y in last_month_range_date:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
        else:
            total_monthly_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_monthly_sale,
                "total-monthly-count": int(len(monthly_sale)),
                "monthly-graph": last_month_range_date
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/dashboard-generate-report/last-week-{_date}", response_model=schemas.ResponseApiBase)
async def get_last_week_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _date: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        _date_start = datetime.strptime(_date, "%d-%m-%Y")
        last_week_date = _date_start - timedelta(days=6)

        # populate graph
        last_week_range = await schemas.GenerateReport.get_range_week_based_on_date(_date=_date)
        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == new_token["account_id"],
                                                       func.date(CustomerBill.payment_date) >= last_week_date
                                                       .date()) \
            .order_by(text("payment_date desc")).offset(skip).limit(limit).all()

        mini_last_week_range = []
        if last_week_sale:
            total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
            for _week in last_week_sale:
                week_ = _week.payment_date.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]
            total_last_week_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-weekly-sale": total_last_week_sale,
                "total-weekly-count": int(len(last_week_sale)),
                "weekly-graph": last_week_range
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/dashboard-generate-report/{year}", response_model=schemas.ResponseApiBase)
def get_year_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        year: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report year
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-year",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_YEAR",
                                  _data="NO_DATA", db_=db)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, year)

        yearly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                    CustomerBill.account_id == new_token["account_id"],
                                                    extract('year',
                                                            CustomerBill.payment_date) == year) \
            .offset(skip).limit(limit).all()

        if yearly_sale:
            total_yearly_sale = sum(c.customer_bill_payment_record.amount for c in yearly_sale)

            for _month in yearly_sale:
                month_ = _month.payment_date.month
                amount = float(_month.total)
                for y in range_year:
                    if y.min_value == month_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
        else:
            total_yearly_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_yearly_sale,
                "total-monthly-count": int(len(yearly_sale)),
                "monthly-graph": range_year
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/dashboard-generate-report/sort", response_model=schemas.ResponseApiBase)
def get_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:
        cur_start_date = datetime.strptime(_in.start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(_in.end_date, "%d-%m-%Y")

        yearly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == _in.invoice_status.upper(),
                                                    CustomerBill.account_id == new_token["account_id"],
                                                    func.date(models.CustomerBill.payment_date) >= cur_start_date.date(),
                                                    func.date(models.CustomerBill.payment_date) <= cur_end_date.date()) \
            .filter(text(f"customer_bills.{_in.search.search_column} LIKE '%{_in.search.search_key}%'")) \
            .order_by(text(f"{_in.sort.parameter_name} {_in.sort.sort_type}")).offset(skip).limit(limit).all()

        invoice = "invoice_no"
        # filters = [x for x in yearly_sale if "BP-D905D55A04-LNP" in x["_in.search.search_key"]]

        total_yearly_sale = sum(c.customer_bill_payment_record.amount for c in yearly_sale)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_yearly_sale,
                "total-monthly-count": int(len(yearly_sale)),
                "yearly_sale": yearly_sale
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin/merchant-dashboard/{_id}", response_model=schemas.ResponseApiBase)
async def get_dashboard_merchant(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    merchant dashboard.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/admin-merchant-dashboard",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ADMIN_DASHBOARD_ADMIN",
                                  _data="NO_DATA", db_=db)

        yesterday_date = datetime.today() - timedelta(days=1)
        last_week_date = datetime.today() - timedelta(days=6)
        # last_week_date = datetime.today() - timedelta(weeks=1)
        last_month_date = datetime.today().date() - timedelta(days=30)
        last_year_date = datetime.today().date() - timedelta(days=365)

        monthly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                     CustomerBill.account_id == _id,
                                                     extract('month',
                                                             CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        yesterday_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == _id,
                                                       extract("day", CustomerBill.payment_date) == yesterday_date.day,
                                                       extract("month",
                                                               CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == _id,
                                                       func.date(CustomerBill.payment_date) >= last_week_date
                                                       .date()).offset(skip).limit(limit).all()

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == _id,
                                                   extract("day", CustomerBill.payment_date) == datetime.today().day,
                                                   extract("month", CustomerBill.payment_date) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_month_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                        CustomerBill.account_id == _id,
                                                        extract("month",
                                                                CustomerBill.payment_date) == last_month_date.month) \
            .offset(skip).limit(limit).all()

        year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                  CustomerBill.account_id == _id,
                                                  extract("year",
                                                          CustomerBill.payment_date) == datetime.today().year) \
            .offset(skip).limit(limit).all()

        last_year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == _id,
                                                       extract("year",
                                                               CustomerBill.payment_date) == last_year_date.year) \
            .offset(skip).limit(limit).all()

        total_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == _id) \
            .offset(skip).limit(limit).all()

        total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
        total_last_monthly_sale = sum(c.customer_bill_payment_record.amount for c in last_month_sale)
        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)
        total_yesterday_sale = sum(c.customer_bill_payment_record.amount for c in yesterday_sale)
        total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
        total_year_sale = sum(c.customer_bill_payment_record.amount for c in year_sale)
        total_last_year_sale = sum(c.customer_bill_payment_record.amount for c in last_year_sale)
        total_all_sale = sum(c.customer_bill_payment_record.amount for c in total_sale)

        total_monthly_count = len(monthly_sale)
        total_last_monthly_count = len(last_month_sale)
        total_today_count = len(today_sale)
        total_yesterday_count = len(yesterday_sale)
        total_last_week_count = len(last_week_sale)
        total_year_count = len(year_sale)
        total_last_year_count = len(last_year_sale)
        total_all_count = len(total_sale)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, datetime.today().year)
        yesterday_range, last_range_month, last_range_year = schemas.GenerateReport \
            .get_range_date(last_month_date.month, datetime.today().year)
        last_week_range = await schemas.GenerateReport.get_range_week_dynamic()

        mini_range_day, mini_range_month, mini_range_year, mini_yesterday_range, mini_last_range_month \
            , mini_last_range_year, mini_last_week_range = ([] for _ in range(7))

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.payment_date.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1
            [mini_range_day.append(x.y_axis) for x in range_day]
        else:
            [mini_range_day.append(0) for x in range_day]

        if monthly_sale:
            for _day in monthly_sale:
                day_ = _day.payment_date.day
                amount = float(_day.total)
                for y in range_month:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
            [mini_range_month.append(x.y_axis) for x in range_month]
        else:
            [mini_range_month.append(0) for x in range_month]

        if year_sale:
            for _month in year_sale:
                month_ = _month.payment_date.month
                amount = float(_month.total)
                for z in range_year:
                    if z.min_value == month_:
                        z.y_axis = z.y_axis + amount
                        z.count += 1
            [mini_range_year.append(x.y_axis) for x in range_year]
        else:
            [mini_range_year.append(0) for x in range_year]

        if yesterday_sale:
            for _yesterday_date in yesterday_sale:
                yesterday_date_ = float(_yesterday_date.payment_date.strftime("%H.%M"))
                amount = float(_yesterday_date.total)
                for xx in yesterday_range:
                    if xx.min_value <= yesterday_date_ <= xx.max_value:
                        xx.y_axis = xx.y_axis + amount
                        xx.count += 1
            [mini_yesterday_range.append(x.y_axis) for x in yesterday_range]
        else:
            [mini_yesterday_range.append(0) for x in yesterday_range]

        if last_month_sale:
            for _last_day in last_month_sale:
                last_day_ = _last_day.payment_date.day
                amount = float(_last_day.total)
                for yy in last_range_month:
                    if yy.min_value == last_day_:
                        yy.y_axis = yy.y_axis + amount
                        yy.count += 1
            [mini_last_range_month.append(x.y_axis) for x in last_range_month]
        else:
            [mini_last_range_month.append(0) for x in last_range_month]

        if last_year_sale:
            for last_month in monthly_sale:
                last_month_ = last_month.payment_date.month
                amount = float(last_month.total)
                for zz in last_range_year:
                    if zz.min_value == last_month_:
                        zz.y_axis = zz.y_axis + amount
                        zz.count += 1
            [mini_last_range_year.append(x.y_axis) for x in last_range_year]
        else:
            [mini_last_range_year.append(0) for x in last_range_year]

        if last_week_sale:
            for _week in last_week_sale:
                week_ = _week.payment_date.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "days": {
                    "today_sale": total_today_sale,
                    "today_range": range_day,
                    "mini_today_range": mini_range_day,
                    "yesterday_sale": total_yesterday_sale,
                    "yesterday_range": yesterday_range,
                    "mini_yesterday_range": mini_yesterday_range,
                    "total_today_count": total_today_count,
                    "total_yesterday_count": total_yesterday_count
                },
                "months": {
                    "this_month_sale": total_monthly_sale,
                    "last_month_sale": total_last_monthly_sale,
                    "monthly_range": range_month,
                    "mini_monthly_range": mini_range_month,
                    "last_monthly_range": last_range_month,
                    "mini_last_monthly_range": mini_last_range_month,
                    "total_monthly_count": total_monthly_count,
                    "total_last_monthly_count": total_last_monthly_count
                },
                "years": {
                    "yearly_sale": total_year_sale,
                    "last_year_sale": total_last_year_sale,
                    "yearly_range": range_year,
                    "mini_yearly_range": mini_range_year,
                    "last_yearly_range": last_range_year,
                    "mini_last_yearly_range": mini_last_range_year,
                    "total_year_count": total_year_count,
                    "total_last_year_count": total_last_year_count
                },
                "others": {
                    "total_sales_received": total_all_sale,
                    "last_week_range": last_week_range,
                    "total_last_week_sales": total_last_week_sale,
                    "mini_last_week_range": mini_last_week_range,
                    "total_last_week_count": total_last_week_count,
                    "total_all_count": total_all_count
                }
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin-{_id}/dashboard-generate-report/day-{date}", response_model=schemas.ResponseApiBase)
def get_admin_day_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        date: str,
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report day
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-day",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_DAY",
                                  _data="NO_DATA", db_=db)

        current_date = datetime.strptime(date, "%d-%m-%Y")

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(current_date.month, datetime.today().year)

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.account_id == _id,
                                                   func.date(CustomerBill.payment_date) == current_date
                                                   .date()).offset(skip).limit(limit).all()

        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.payment_date.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-today-sale": total_today_sale,
                "total-today-count": int(len(today_sale)),
                "today-graph": range_day
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin-{_id}/dashboard-generate-report/month-{month}", response_model=schemas.ResponseApiBase)
def get_admin_month_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        month: int = 1,
        year: int = 2022,
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        # populate graph
        last_month_range_date = []
        range_month = calendar.monthrange(year=year, month=month)
        for x in range(1, range_month[1] + 1):
            xdate = datetime.strptime(f"{x}-{month}-{year}", "%d-%m-%Y")
            xdate_name = xdate.strftime("%d-%b-%Y")
            last_month_range_date.append(
                schemas.ReportCreate(x_axis=f"{xdate_name}", y_axis=0, min_value=float(x), max_value=0, count=0))

        monthly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                     CustomerBill.account_id == _id,
                                                     extract('month',
                                                             CustomerBill.payment_date) == month,
                                                     extract('year',
                                                             CustomerBill.payment_date) == year,
                                                     ) \
            .order_by(text("payment_date desc")).offset(skip).limit(limit).all()

        invoice = "invoice_no"
        filters = [x for x in monthly_sale if "BP-D905D55A04-LNP" in x.invoice_no]

        if monthly_sale:
            total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
            for _day in monthly_sale:
                day_ = _day.payment_date.day
                amount = float(_day.total)
                for y in last_month_range_date:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_monthly_sale,
                "total-monthly-count": int(len(monthly_sale)),
                "monthly-graph": last_month_range_date
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin-{_id}/dashboard-generate-report/last-week-{_date}", response_model=schemas.ResponseApiBase)
async def get_admin_last_week_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _date: str,
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        _date_start = datetime.strptime(_date, "%d-%m-%Y")
        last_week_date = _date_start - timedelta(days=6)

        # populate graph
        last_week_range = await schemas.GenerateReport.get_range_week_based_on_date(_date=_date)
        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.account_id == _id,
                                                       func.date(CustomerBill.payment_date) >= last_week_date
                                                       .date()) \
            .order_by(text("payment_date desc")).offset(skip).limit(limit).all()

        mini_last_week_range = []
        if last_week_sale:
            total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
            for _week in last_week_sale:
                week_ = _week.payment_date.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_last_week_sale,
                "total-monthly-count": int(len(last_week_sale)),
                "monthly-graph": last_week_range
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin-{_id}/dashboard-generate-report/{year}", response_model=schemas.ResponseApiBase)
def get_admin_year_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        year: int,
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report year
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-year",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_YEAR",
                                  _data="NO_DATA", db_=db)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, year)

        yearly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                    CustomerBill.account_id == _id,
                                                    extract('year',
                                                            CustomerBill.payment_date) == year) \
            .offset(skip).limit(limit).all()

        if yearly_sale:
            total_yearly_sale = sum(c.customer_bill_payment_record.amount for c in yearly_sale)

            for _month in yearly_sale:
                month_ = _month.payment_date.month
                amount = float(_month.total)
                for y in range_year:
                    if y.min_value == month_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
        else:
            total_yearly_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_yearly_sale,
                "total-monthly-count": int(len(yearly_sale)),
                "monthly-graph": range_year
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
