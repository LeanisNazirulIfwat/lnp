import datetime
import json
from typing import Any

from app.constants.system_constant import SystemConstant
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.Utils.shared_utils import get_next_subscription_activation
from app.models.subscription_history import SubscriptionHistory
from app.constants.payload_example import PayloadExample
from sqlalchemy.ext.serializer import loads, dumps
from dateutil.relativedelta import relativedelta

router = APIRouter(prefix="/subscription-histories", tags=["subscription-histories"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_subscription_histories(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="get", _url="subscription-histories/admin/list",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_ALL_LIST_SUBSCRIPTION_HISTORIES",
                                  _data="NO_DATA", db_=db)

        db_count = len(crud.subscription_history.get_multi(db, skip=skip, limit=limit))

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.subscription_history.get_multi_search_user(db=db, skip=skip, limit=limit,
                                                                    _id=current_user.id,
                                                                    record_status=_in.record_status,
                                                                    start_date=_in.start_date,
                                                                    end_date=_in.end_date,
                                                                    search_column=_in.search.search_column,
                                                                    search_key=_in.search.search_key,
                                                                    parameter_name=_in.sort.parameter_name,
                                                                    sort_type=_in.sort.sort_type,
                                                                    search_enable=_in.search.search_enable,
                                                                    invoice_status=_in.invoice_status,
                                                                    table_name="subscription_histories",
                                                                    role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_subscription_histories(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.SubscriptionHistoryCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new subscription histories.
    """
    try:
        query_get = crud.subscription_history.get_by_name(db, plan_name=_in.plan_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        query_create = crud.subscription_history.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_subscription_histories(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.SubscriptionHistory,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update subscription histories.
    """
    try:
        query_update = crud.subscription_history.get_by_subscription_history_id(db, _id=_id)
        if query_update:
            update_ = crud.subscription_history.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_subscription_histories(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        api_in: schemas.SubscriptionHistory,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete subscription histories.
    """
    try:
        query_delete = crud.subscription_history.get_by_recurring_id(db, _id=_id)
        _in = schemas.SubscriptionHistory(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.subscription_history.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_subscription_history_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search subscription history id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/subscription-histories/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SUBSCRIPTION_HISTORY_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.subscription_history.get_by_subscription_history_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/upgrade-plan", response_model=schemas.ResponseApiBase)
def get_subscription_upgrade(
        *,
        db: Session = Depends(deps.get_db),
        plan_id: int,
        time_period: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    upgrade plan
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/subscription-histories/upgrade-plan",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPGRADE_PLAN",
                                  _data="NO_DATA", db_=db)

        upgrade_schema = schemas.SubscriptionPlanUpdateCreate(
            subscribed_at=datetime.datetime.today(),
            plan_name=plan_id,
            current_plan_name=current_user.account[0].subscription_plan_id,
            start_date=datetime.datetime.today(),
            end_date=datetime.datetime.today(),
            activated_at=get_next_subscription_activation(datetime.datetime.today()),
            upgrade_complete=False,
            account_id=new_token["account_id"]
        )
        query_create = crud.subscription_history.create_self_date(db, obj_in=upgrade_schema)

        update_subscription = schemas.AccountUpdate(
            current_subscription_ends=get_next_subscription_activation(datetime.datetime.today()),
            subscription_plan_id=plan_id,
            current_subscription_time_period=time_period
        )

        if time_period == SystemConstant.SYSTEM_CONS.get("subscription_time_period", None).__getitem__("MONTHLY"):
            update_subscription.current_subscription_ends = update_subscription.current_subscription_ends \
                                                            + relativedelta(months=1)
        elif time_period == SystemConstant.SYSTEM_CONS.get("subscription_time_period", None).__getitem__("YEARLY"):
            update_subscription.current_subscription_ends = update_subscription.current_subscription_ends \
                                                            + relativedelta(year=1)

        # updating account current end date
        query_update = crud.account.get_account_by_id(db, id=new_token['account_id'])
        _update = crud.account.update_self_date(db, db_obj=query_update, obj_in=update_subscription)

        if query_create:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_create,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_create,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/activate-plan", response_model=schemas.ResponseApiBase)
def get_activate_plan(
        *,
        db: Session = Depends(deps.get_db),
        plan_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """.
    upgrade plan
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/subscription-histories/upgrade-plan",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPGRADE_PLAN",
                                  _data="NO_DATA", db_=db)

        activation_list = db.query(SubscriptionHistory).filter(SubscriptionHistory.account_id == new_token["account_id"]
                                                               , SubscriptionHistory.upgrade_complete == False).all()

        updated_list = []
        for x in activation_list:
            upgrade_schema = schemas.SubscriptionHistoryUpdate(
                upgrade_complete=True,
                account_id=new_token["account_id"]
            )
            update_ = crud.subscription_history.update_self_date(db, db_obj=x, obj_in=upgrade_schema)
            updated_list.append(loads(dumps(update_)))

        if activation_list:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=updated_list,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=activation_list,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-subscription-history", response_model=schemas.ResponseApiBase)
def get_merchant_subscription_history(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available product based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/product/merchant-product",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SUBSCRIPTION_HISTORY_LIST_BASED_ON_ACCOUNT_ID",
                                  _data=_in, db_=db)

        db_count = crud.subscription_history.get_count_account(db=db, _id=new_token["account_id"],
                                                               start_date=_in.start_date, end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.subscription_history.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
