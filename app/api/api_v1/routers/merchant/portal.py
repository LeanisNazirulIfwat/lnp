import asyncio
import base64
import json
from datetime import datetime
from typing import Any
from app import models, schemas
from fastapi import APIRouter, Body, Depends, Security, BackgroundTasks, Response
from app.api import deps
from fastapi.encoders import jsonable_encoder
from pydantic.networks import EmailStr
from sqlalchemy.orm import Session
import requests
from app.core.config import settings
from app.constants.role import Role
from app import schemas, crud
from app.Utils.shared_utils import generate_password, SharedUtils

router = APIRouter(prefix="/portal", tags=["portal"])


@router.post("/manual-trigger-registration", response_model=schemas.ResponseApiBase)
def manual_register_merchant(*,
                             user_id: int = Body(...),
                             db: Session = Depends(deps.get_db),
                             current_user: models.User = Security(
                                 deps.get_current_active_user,
                                 scopes=[Role.SUPER_ADMIN["name"]],
                             ),
                             new_token: str = Depends(deps.get_current_new_token),
                             background_tasks: BackgroundTasks
                             ) -> Any:
    """
    encode string.
    """
    try:
        if user_id:
            background_tasks.add_task(deps.write_audit, _method="GET", _url="/portal/manual-trigger-registration",
                                      _email="admin.portal@leanis.com.my",
                                      _account_id=99999, _description="MANUAL_TRIGGER_MERCHANT",
                                      _data=user_id, db_=db)

            url = settings.REGISTER_PORTAL_URL

            payload = {'userId': user_id}
            headers = {
                'leanis-signature': settings.PORTAL_SIGNATURE,
                'wl-key': ""
            }

            response = requests.request("POST", url, headers=headers, data=payload)
            if response.status_code == requests.codes.ok:
                data = json.loads(response.text)
                if data["response_code"] == 2100:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=data,
                        breakdown_errors="",
                        token=new_token["token"]
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=9122,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=data,
                        breakdown_errors="API_ERROR_OCCUR",
                        token=new_token["token"]
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=9182,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="ERROR_OCCUR",
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/forgot-password", response_model=schemas.ResponseApiBase,
             )
def portal_update_password(*,
                           new_password: str = Body(...),
                           confirm_password: str = Body(...),
                           db: Session = Depends(deps.get_db),
                           current_user: models.User = Security(
                               deps.get_current_active_user,
                               scopes=[Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
                           ),
                           new_token: str = Depends(deps.get_current_new_token),
                           background_tasks: BackgroundTasks
                           ) -> Any:
    """
    encode string.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/portal/forgot-password",
                                  _email="admin.portal@leanis.com.my",
                                  _account_id=99999, _description="MANUAL_TRIGGER_MERCHANT",
                                  _data=current_user.email, db_=db)

        user = crud.user.get(db, id=current_user.id)
        update_user = schemas.UserUpdate(
            password=confirm_password
        )

        new_update_user = ""
        if new_password == confirm_password:
            new_update_user = crud.user.update(db, db_obj=user, obj_in=update_user)

        url = settings.PORTAL_UPDATE_PASSWORD

        # url = "http://localhost:65142/v1/portalUpdatePassword"

        payload = {'merchantId': new_update_user.account[0].merchant_id,
                   'newPassword': new_update_user.hashed_password}
        files = [

        ]
        headers = {
            'leanis-signature': '9f73371d-f8d3-442d-b5fb-93898a9f4dc9'
        }
        response = requests.request("POST", url, headers=headers, data=payload, files=files)

        if response.status_code == requests.codes.ok:
            user = crud
            data = json.loads(response.text)
            if data["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json(),
                    breakdown_errors="",
                    token=new_token["token"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9122,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=data,
                    breakdown_errors="API_ERROR_OCCUR",
                    token=new_token["token"]
                )
        else:
            return schemas.ResponseApiBase(
                response_code=9182,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="ERROR_OCCUR",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/reset-password", response_model=schemas.ResponseApiBase,
             )
async def portal_reset_password(*,
                                email: EmailStr = Body(...),
                                db: Session = Depends(deps.get_db),
                                background_tasks: BackgroundTasks
                                ) -> Any:
    """
    encode string.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/portal/forgot-password",
                                  _email="admin.portal@leanis.com.my",
                                  _account_id=99999, _description="MANUAL_TRIGGER_MERCHANT",
                                  _data=email, db_=db)

        new_password = generate_password()

        user = crud.user.get_by_email(db, email=email)
        if not user:
            return schemas.ResponseApiBase(
                response_code=7754,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INVALID_EMAIL",
                token=""
            )

        update_user = schemas.UserUpdate(
            password=new_password
        )

        new_update_user = ""
        new_update_user = crud.user.update(db, db_obj=user, obj_in=update_user)

        reset_data = schemas.TwilioResetPassword(
            to_=new_update_user.email,
            recipient_name=new_update_user.full_name,
            reset_password_new_password=new_password
        )

        message = await asyncio.get_event_loop().run_in_executor(
            None, SharedUtils.send_reset_password_sendgrid_email, reset_data.from_, email, reset_data.bcc_,
            reset_data.json())

        url = settings.PORTAL_UPDATE_PASSWORD

        # url = "http://localhost:65142/v1/portalUpdatePassword"

        payload = {'merchantId': new_update_user.account[0].merchant_id,
                   'newPassword': new_update_user.hashed_password}
        files = [

        ]
        headers = {
            'leanis-signature': '9f73371d-f8d3-442d-b5fb-93898a9f4dc9'
        }
        response = requests.request("POST", url, headers=headers, data=payload, files=files)

        if response.status_code == requests.codes.ok:
            user = crud
            data = json.loads(response.text)
            if data["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json(),
                    breakdown_errors="",
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9122,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=data,
                    breakdown_errors="API_ERROR_OCCUR",
                    token=""
                )
        else:
            return schemas.ResponseApiBase(
                response_code=9182,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="ERROR_OCCUR",
                token=""
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/portal-form-list", response_model=schemas.ResponseApiBase,
             )
def portal_form_list(*,
                     db: Session = Depends(deps.get_db),
                     current_user: models.User = Security(
                         deps.get_current_active_user,
                         scopes=[Role.SUPER_ADMIN["name"]],
                     ),
                     form_status: int = Body(..., embed=True, example=1),
                     page: int = Body(..., embed=True, example=1),
                     per_page: int = Body(..., embed=True, example=10),
                     start_date: str = Body(..., embed=True, example='01-05-2020'),
                     end_date: str = Body(..., embed=True, example='01-05-2022'),
                     search_column: str = Body(None, embed=True, example='company_type'),
                     search_key: str = Body(None, embed=True, example='6'),
                     sort_column: str = Body(..., embed=True, example='created_at'),
                     sort_type: str = Body(..., embed=True, example='asc'),
                     new_token: str = Depends(deps.get_current_new_token),
                     background_tasks: BackgroundTasks
                     ) -> Any:
    """
   portal form list.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/portal/portal-form-list",
                                  _email="admin.portal@leanis.com.my",
                                  _account_id=99999, _description="PORTAL_FORM_LIST",
                                  _data="form list", db_=db)

        url = settings.PORTAL_GET_FORM_LIST

        payload = {'formStatus': form_status,
                   'page': page,
                   'per_page': per_page,
                   'start_date': start_date,
                   'end_date': end_date,
                   'search_column': search_column,
                   'search_key': search_key,
                   'sort_column': sort_column,
                   'sort_type': sort_type}

        if not search_column:
            payload = {'formStatus': form_status,
                       'page': page,
                       'per_page': per_page,
                       'start_date': start_date,
                       'end_date': end_date,
                       'sort_column': sort_column,
                       'sort_type': sort_type}
        files = {}
        headers = {
            'leanis-signature': settings.PORTAL_SIGNATURE,
            'wl-key': ""
        }

        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == requests.codes.ok:
            data = json.loads(response.text)
            if data["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json(),
                    breakdown_errors="",
                    token=new_token["token"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9122,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=data,
                    breakdown_errors="API_ERROR_OCCUR",
                    token=new_token["token"]
                )
        else:
            return schemas.ResponseApiBase(
                response_code=9182,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=response.status_code,
                breakdown_errors="ERROR_OCCUR",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/portal-approve-form", response_model=schemas.ResponseApiBase,
             )
def portal_approve_form(*,
                        _in: Any = Body(..., example={
                            "form_id": 34
                        }),
                        db: Session = Depends(deps.get_db),
                        current_user: models.User = Security(
                            deps.get_current_active_user,
                            scopes=[Role.SUPER_ADMIN["name"]],
                        ),
                        new_token: str = Depends(deps.get_current_new_token),
                        background_tasks: BackgroundTasks
                        ) -> Any:
    """
   portal approve form.
    """
    try:
        if _in:
            background_tasks.add_task(deps.write_audit, _method="GET", _url="/portal/portal-approve-form",
                                      _email="admin.portal@leanis.com.my",
                                      _account_id=99999, _description="APPROVE_MERCHANT_WHITE_LABEL",
                                      _data=json.dumps(_in), db_=db)

            url = settings.PORTAL_APPROVE_FORM

            payload = {'formId': _in["form_id"]}

            files = [
            ]
            headers = {
                'leanis-signature': settings.PORTAL_SIGNATURE,
                'wl-key': ""
            }

            response = requests.request("POST", url, headers=headers, data=payload, files=files)
            if response.status_code == requests.codes.ok:
                data = json.loads(response.text)
                if data["response_code"] == 2100:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=response.json(),
                        breakdown_errors="",
                        token=new_token["token"]
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=9122,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=data,
                        breakdown_errors="API_ERROR_OCCUR",
                        token=new_token["token"]
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=9182,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="ERROR_OCCUR",
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/portal-reject-form", response_model=schemas.ResponseApiBase,
             )
def portal_reject_form(*,
                       form_id: int = Body(...),
                       user_type: int = Body(None),
                       db: Session = Depends(deps.get_db),
                       current_user: models.User = Security(
                           deps.get_current_active_user,
                           scopes=[Role.SUPER_ADMIN["name"]],
                       ),
                       new_token: str = Depends(deps.get_current_new_token),
                       background_tasks: BackgroundTasks
                       ) -> Any:
    """
    portal reject form
    """
    try:
        if form_id:
            background_tasks.add_task(deps.write_audit, _method="GET", _url="/portaL/manual-trigger-registration",
                                      _email="admin.portal@leanis.com.my",
                                      _account_id=99999, _description="PORTAL_REJECT_FORM",
                                      _data=form_id, db_=db)

            url = settings.PORTAL_REJECT_FORM

            payload = {'formId': form_id}
            files = [

            ]
            headers = {
                'leanis-signature': settings.PORTAL_SIGNATURE,
                'wl-key': ""
            }

            response = requests.request("POST", url, headers=headers, data=payload, files=files)
            if response.status_code == requests.codes.ok:
                data = json.loads(response.text)
                if data["response_code"] == 2100:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=response.json(),
                        breakdown_errors="",
                        token=new_token["token"]
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=9122,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=data,
                        breakdown_errors="API_ERROR_OCCUR",
                        token=new_token["token"]
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=9182,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="ERROR_OCCUR",
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/portal-download-file", response_model=schemas.ResponseApiBase,
             )
def portal_download_file(*,
                         file_name: str = Body(..., embed=True),
                         db: Session = Depends(deps.get_db),
                         current_user: models.User = Security(
                             deps.get_current_active_user,
                             scopes=[Role.SUPER_ADMIN["name"]],
                         ),
                         new_token: str = Depends(deps.get_current_new_token),
                         background_tasks: BackgroundTasks
                         ) -> Any:
    """
    portal download file
    """
    try:
        if file_name:
            background_tasks.add_task(deps.write_audit, _method="GET", _url="/portaL/manual-trigger-registration",
                                      _email="admin.portal@leanis.com.my",
                                      _account_id=99999, _description="MANUAL_TRIGGER_MERCHANT",
                                      _data=file_name, db_=db)

            url = settings.PORTAL_DOWNLOAD_FILE

            payload = {'filename': file_name}
            files = []
            headers = {
                'leanis-signature': settings.PORTAL_SIGNATURE,
                'wl-key': ""
            }

            response = requests.request("POST", url, headers=headers, data=payload, files=files)
            if response.status_code == requests.codes.ok:
                data = json.loads(response.text)
                if data["response_code"] == 2100:
                    data = data["data"]["base64"]
                    # convert_pdf = b64_to_pdf(data)
                    if data:
                        headers = {
                            f'Content-Disposition': f'attachment; filename="{file_name}"'
                        }

                        return Response(base64.b64decode(data), headers=headers, media_type="application/pdf")
                    else:
                        return schemas.ResponseApiBase(
                            response_code=9122,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.today(),
                            data=data,
                            breakdown_errors="API_ERROR_OCCUR",
                            token=new_token["token"]
                        )
                else:
                    return schemas.ResponseApiBase(
                        response_code=9199,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=data["response_message"],
                        breakdown_errors="ERROR_OCCUR",
                        token=new_token["token"]
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=9182,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="ERROR_OCCUR",
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/portal-constant", response_model=schemas.ResponseApiBase,
             )
def portal_constant_list(*,
                         db: Session = Depends(deps.get_db),
                         current_user: models.User = Security(
                             deps.get_current_active_user,
                             scopes=[Role.SUPER_ADMIN["name"]],
                         ),
                         new_token: str = Depends(deps.get_current_new_token),
                         background_tasks: BackgroundTasks
                         ) -> Any:
    """
   portal constant list.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/portal/portal-form-list",
                                  _email="admin.portal@leanis.com.my",
                                  _account_id=99999, _description="PORTAL_CONSTANT",
                                  _data="CONSTANT", db_=db)

        url = settings.PORTAL_GET_CONSTANT

        url = "http://dev.onboard.leanpay.my/api/systemconstants"

        payload = {}
        headers = {}

        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == requests.codes.ok:
            data = json.loads(response.text)
            if data["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json(),
                    breakdown_errors="",
                    token=new_token["token"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9122,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=data,
                    breakdown_errors="API_ERROR_OCCUR",
                    token=new_token["token"]
                )
        else:
            return schemas.ResponseApiBase(
                response_code=9182,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="ERROR_OCCUR",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
