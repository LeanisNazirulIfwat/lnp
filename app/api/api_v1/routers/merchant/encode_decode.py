import json
from datetime import datetime
from typing import Any
from app import models, schemas
from fastapi import APIRouter, Body, Depends, Security, BackgroundTasks, File, UploadFile, \
    Request
from jose import jws
from app.api import deps
from app.core.config import settings
from fastapi.encoders import jsonable_encoder
from fastapi.responses import StreamingResponse
from sqlalchemy.orm import Session
from app.constants.role import Role
from app.Utils.shared_utils import SharedUtils
import base64
from pandas import read_csv
from sqlalchemy import func
from app.Utils.web_request import http_get_with_requests_parallel
from app.Utils.subscription_checker import Subscription
from app import models, schemas, crud
from app.constants.system_constant import SystemConstant

router = APIRouter(prefix="/jwt", tags=["jwt"])


@router.post("/encode", response_model=schemas.ResponseApiBase)
def get_encode(message: str = Body(..., embed=True)) -> Any:
    """
    encode string.
    """
    try:
        msg = {
            "data": message
        }
        payload = {
            "image_of_media_social": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAB+UlEQVRoge2ZTUrDQBiGn4pCo3Vp0TO4EFd1rRU3IughPIZW6aIewAt4BkFci+61UBctWBRc1LWgIsbFTOyQn8mkTc2g80BI8zUzed9838w0KTgcjn9BCfCLFpEHU0ULyItp5XOpMBXj4cMfyogzYhvOiG3YZGQWOAZ6wLvcNwDPtAOf4hdFD7hRtKjbNXozP/ptMHIkNTwAG8AcUAf6Mn6oaWuVkZ7UsB6K12W8q2lrlZF3qaESis/L+JumrQ/4WQZ7FVjJJM+cJ7mvheJrcv9o0olJRqpAG+gAZVN1GWhIDX1EOVWATYZj5EDT1ri0AhPBeSfjqo7BQ8xOcbPWFfqbZ2RENdEFPuUWLoE8KCNmpy5izHQRmUirgFQjC8Cd/O4eWERkIzg2XqgmjNaImom2PAZxdzoy3kroeCZvpSkkGkkyEVBjWGLbwC7QBC6AQUKbSRJrJK6c4mgRPzB9pe3ShISHiRhJy4RKUGIDRCaawB5inTHtIy8iRrIKSJpNstyQPIgYSSunLKglqiu/1RyuFTGSl4kAEzMfiFV9nOei2DHyW3jAKfAlr33J6JODFb9+t4Bnef0XYGeEPqwwAqISzhUdZ4gHK1OsMQLiLec+8Ip+TOk2K4wELAO3jGBEfRvv3v3agDNiG86IbTgjtuH+1XU4HI5C+AYybe7UfoPmRQAAAABJRU5ErkJggg==",
            "full_name": "dolla@gmail.com",
            "email": "dolla@gmail.com",
            "password": "$2b$10$Js.kxno3O6LEBpaEtRmtiOyD3dMver0eQcW.hA8PDDIZmZc.yx5VW",
            "phone_number": "6892899172",
            "is_active": 'true',
            "response_additional_message": "Thank You",
            "redirect_url": "feraci4218@lenfly.com",
            "response_title": "feraci4218",
            "description": "Merchant feraci4218",
            "api_interface": "default",
            "fpx_bank_selection": "grid",
            "user_type": 2,
            "parent_key": "",
            "application_name": "WHITE_LABEL_dolla@gmail.com",
            "meta_description": "WHITE LABEL dolla@gmail.com",
            "main_site": "feraci4218.com",
            "background_color": "#fffff",
            "company_name": "FERACI4218",
            "platform_version": "V.1"
        }
        encode = jws.sign(payload, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                          algorithm='HS256')
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=encode,
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/decode", response_model=schemas.ResponseApiBase)
def get_decode(signed: str = Body(..., embed=True)) -> Any:
    """
    decode string.
    """
    try:
        decode = jws.verify(signed, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                            algorithms=['HS256'])
        st = str(decode, 'utf-8')
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=json.loads(st),
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/generate-daily-settlement", response_model=Any, response_description='xlsx')
def get_excel(
        *,
        db: Session = Depends(deps.get_db), _id: int = 0, _date: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        cur_date = datetime.strptime(_date, "%d-%m-%Y")
        list_data_customer_bill = db.query(models.CustomerBill).filter(
            models.CustomerBill.account_id == _id,
            func.date(models.CustomerBill.created_at) == cur_date.date()).all()

        current_account = []
        account = current_user.account
        for acc in account:
            if acc.is_active:
                current_account.append(acc)
        if not list_data_customer_bill:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=list_data_customer_bill,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )

        from Settlement.creating_settlement import creating_settlement
        result = creating_settlement(current_account[0].name, list_data_customer_bill, current_account[0])
        headers = {
            'Content-Disposition': 'attachment; filename="filename.xlsx"'
        }
        return StreamingResponse(result, headers=headers)

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/upload-file/file", response_model=Any)
async def upload_file(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request,
) -> Any:
    """
    decode string.
    """
    try:
        # result = SharedUtils.get_provider_list(55)
        url = "http://dev.switch.leanpay.my//api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
        urls = ["http://dev.switch.leanpay.my//api/v1/env/mobile/client/paymentServices/ListOfPaymentServices" for i in
                range(0, 10)]

        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'payment_model_reference_id': '1',
                   'amount_to_calculate': '25',
                   'payment_service_id': '1',
                   'payment_service_record_status_id': '1'}
        files = [
        ]
        headers = {}

        response, t = http_get_with_requests_parallel(headers=headers, list_of_urls=urls, payload=payload)
        # session = ClientSession()
        # response, t = await http_post_with_aiohttp_parallel(session=session, headers=headers, list_of_urls=urls,
        #                                                     payload=payload)
        # await session.close()
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "response": response,
                "time_taken": t
            },
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4100,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=e,
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/postcode/", response_model=schemas.ResponseApiBase)
def get_postcode(
        background_task: BackgroundTasks,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
) -> Any:
    """
    encode string.
    """
    try:
        file_path = f"./assets/csv_file/all_postcodes.csv"

        # reading CSV file
        data = read_csv(file_path, engine='python', header=None)

        extract_data = []
        for w, x, y, z in zip(data[0], data[1], data[2], data[3]):
            extract_data.append({
                f"{w}": {
                    "address": f"{x}",
                    "city": f"{y}",
                    "state": f"{z}"
                }
            })
            with open('app.json', 'w', encoding='utf-8') as f:
                f.write(json.dumps(extract_data, ensure_ascii=False, indent=4))

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/convert-image", response_model=Any)
async def image_convert(
        *,
        file: UploadFile = File(None)
) -> Any:
    """
    get image for payment provider
    """
    try:
        if file:
            contents = await file.read()
            encoded_img = base64.b64encode(contents)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=encoded_img,
                breakdown_errors="",
                token=""
            )
        else:
            # _image = SystemConstant.SYSTEM_CONS.get("Leanis_image_64", None).__getitem__("LOGO_ALT")
            # imgstr = _image
            # imgdata = base64.b64decode(imgstr)
            # return Response(imgdata, media_type="image/jpeg")
            return SharedUtils.generate_payload(**{
                "payload": 1,
                "payment": None,
                "hello": "ME"
            })
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/testing-logic/logic", response_model=Any)
async def testing_logic(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks,
        request: Request,
        subs=Depends(deps.check_subscription_package),
        token=Depends(deps.get_current_new_token)
) -> Any:
    """
    decode string.
    """
    try:
        if subs is None:
            return schemas.ResponseApiBase(
                response_code=5622,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="SUBSCRIBER_NOT_VALID",
                token=""
            )
        subscription_ = Subscription(models.Collection, subs, token["account_id"])
        checker = subscription_.check_subscription_payment_form(db)
        if checker:
            response = schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=subs,
                breakdown_errors="NO_DATA",
                token="NO_DATA"
            )
            hash_, data_ = SharedUtils.hash_payload(response.dict(),
                                                    "78214125442A472D4B6150645367566B59703373367639792F423F4528482B4D")
            return {
                "hash": hash_,
                "data": response.dict(),
                "key": "78214125442A472D4B6150645367566B59703373367639792F423F4528482B4D"
            }

        else:
            return schemas.ResponseApiBase(
                response_code=7855,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="COLLECTION_CREATION_LIMIT",
                token=""
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4100,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=e,
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/testing-logic/response-logic", response_model=Any)
async def testing_response_logic(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.ParcelAsiaModel,
        background_tasks: BackgroundTasks,
        request: Request,
        token=Depends(deps.get_current_new_token)
) -> Any:
    """
    decode string.
    """
    try:
        data = _in.data['shipments']
        # data = _in.data
        keys = list(data.keys())
        _shipment = schemas.ShipmentResponse(**data[f'{keys[0]}'])
        shipment_data = schemas.ShipmentCreate(
            account_id=57,
            customer_id=216,
            shipping_profile_id=1,
            shipment_status=SystemConstant.SYSTEM_CONS.get("shipment_status", None).__getitem__(
                f'{_shipment.shipment_status}'),
            shipping_infos=json.dumps(_shipment.dict())
        )
        _create = crud.shipment.create_self_date(db=db, obj_in=shipment_data)
        _get = crud.shipment.get(db, id=2)
        ship_ = schemas.ShipmentResponse(**json.loads(_get.shipping_infos))
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=SystemConstant.SYSTEM_CONS.get("shipment_status", None).__getitem__(f'{ship_.shipment_status}'),
            breakdown_errors="NO_DATA",
            token="NO_DATA"
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4100,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=e,
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/generate/bill-id", response_model=schemas.ResponseApiBase)
def generate_bill_id() -> Any:
    """
    generate bill
    """
    try:
        code_gen = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=code_gen,
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
