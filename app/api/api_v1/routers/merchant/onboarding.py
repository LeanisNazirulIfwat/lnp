import json
from datetime import datetime
from typing import Any, List
from app import models, schemas, crud
from fastapi import APIRouter, Body, Depends, HTTPException, Security, Header, BackgroundTasks, status
from jose import jws
from app.api import deps
from app.core.config import settings
from fastapi.encoders import jsonable_encoder
from app.constants.system_constant import SystemConstant
from sqlalchemy.orm import Session
import requests
import requests as req

router = APIRouter(prefix="/onboard", tags=["onboard"])


@router.post("/register-master-merchant", response_model=schemas.ResponseApiBase,
             dependencies=[Depends(deps.get_token_onboard_header)])
def register_merchant(*,
                      message: str = Body(...),
                      db: Session = Depends(deps.get_db),
                      background_tasks: BackgroundTasks
                      ) -> Any:
    """
    encode string.
    """
    try:
        if message:
            background_tasks.add_task(deps.write_audit, _method="GET", _url="onboard/approve",
                                      _email="onboarding@leanis.com.my",
                                      _account_id=99999, _description="ONBOARDING_REGISTER_TO_MERCHANT",
                                      _data=message, db_=db)

            decode = jws.verify(message, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                algorithms=['HS256'])
            st = str(decode, 'utf-8')
            data = json.loads(st)
            merchant = crud.account.get_account_merchant_id(db, id=data.get('merchant_id'))
            if merchant:

                query_user = crud.user.get(db, id=merchant.user_id)
                if query_user:
                    # system_id = data["userType"],
                    user_update = schemas.UserUpdate(
                        record_status=1
                    )
                    user = crud.user.update_self_date(db, db_obj=query_user, obj_in=user_update)

                merchant_update = schemas.AccountCreate(
                    account_name=data["companyName"],
                    url=data["companyWebUrl"],
                    business_location=data["companyBusinessState"]
                )

                account_update = crud.account.get_account_merchant_id(db, id=data.get('merchant_id'))
                account = crud.account.update_self_date(db, db_obj=account_update, obj_in=merchant_update)

                query_company = crud.company_detail.get_by_company_detail_id(db, _id=merchant.company_details_id)

                company_update = schemas.CompanyDetailUpdate(
                    account_name=data["applicantName"],
                    company_name=data["companyName"],
                    business_website_url=data["companyWebUrl"],
                    nature_of_business=f"{data['companyNatureOfBusiness']}: {data['companyDescriptionOfProduct']}",
                    company_number=data["companyRegNo"],
                )

                company_update_data = crud.company_detail.update_self_date(db, db_obj=query_company,
                                                                           obj_in=company_update)

                query_business = crud.business_owner_detail \
                    .get_by_business_owner_detail_id(db, _id=company_update_data.business_owner_detail_id)

                if query_business:
                    business_owner_update = schemas.BusinessOwnerDetailCreate(
                        legal_name=data["signatureApplicantName"],
                        bank_name=data["companyBankName"],
                        individual_nric=data.get("signatureApplicantIc"),
                        business_bank_account_number=data["companyBankAccNo"],
                        address_city=data["companyBusinessCity"],
                        address_state=data["companyBusinessState"],
                        address_line_one=data["companyBusinessAddress1"],
                        address_line_two=data["companyBusinessAddress2"],
                        address_postcode=data["companyBusinessPostcode"],
                        address_country="MALAYSIA",
                        bank_account_statement_header=data.get("bank_statement", 'NO_DATA').get("base64", "NO_DATA")
                    )
                    business_update = crud.business_owner_detail.update(db, db_obj=query_business,
                                                                        obj_in=business_owner_update)

                query_bank = crud.bank.get_by_account_bank(db, name=data["companyBankName"], _id=account_update.id)
                if not query_bank:
                    create_bank = schemas.BankCreate(
                        name=data["companyBankName"],
                        code=f'{data["companyBankName"]}-{data["companyBankAccNo"]}',
                        account_number=data["companyBankAccNo"],
                        description=f'BANK ACCOUNT FOR {data["companyBankName"]}',
                        bank_account_statement=data.get("bank_statement", 'NO_DATA').get("base64", "NO_DATA"),
                        account_id=account_update.id
                    )

                    crud.bank.create_self_date(db, obj_in=create_bank)

                url = settings.ONBOARDING_CALLBACK

                encrypt = {
                    "onboarding_status_id": 1,
                    "onboarding_status": "SUCCESS",
                    "merchant_id": merchant.merchant_id,
                    "form_id": data["form_id"]
                }
                encode = jws.sign(encrypt, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                  algorithm='HS256')
                payload = {
                    'message': encode}
                files = [

                ]
                headers = {}

                response = requests.request("POST", url, headers=headers, data=payload, files=files)
                if response.status_code == requests.codes.ok:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=response.json(),
                        breakdown_errors="",
                        token=""
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=4000,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="INVALID_MERCHANT_ID",
                    breakdown_errors="INTERNAL_PROBLEM_OCCURS",
                    token=""
                )
        return schemas.ResponseApiBase(
            response_code=9182,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="EMPTY_HEADER",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/constant/system-constant", response_model=schemas.ResponseApiBase)
def get_all_constant(*,
                     db: Session = Depends(deps.get_db),
                     background_tasks: BackgroundTasks
                     ) -> Any:
    """
    display all system constant.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="constant",
                                  _email="onboard@leanis.com.my",
                                  _account_id=99999, _description="GET_CONSTANT",
                                  _data="NO_DATA", db_=db)

        constant_sys = SystemConstant.SYSTEM_CONS
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=constant_sys,
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/list-verification-channel", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_list_verification_channel() -> Any:
    """
    API for getting the listing for bank verification
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/ListOfPaymentChannels"
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder({'client_identifier': 'LEANPAY',
                                           'skip_encryption': 'false'})
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}

        files = {}
        headers = {}
        services: List[schemas.VerificationChannelBaseModel] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    data = schemas.VerificationChannelBaseModel(**x)
                    services.append(data)
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=services
                    )},
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/verify-bank-account", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def verify_bank_account(*,
                        db: Session = Depends(deps.get_db),
                        callback_in: schemas.VerificationCheckBaseModel = Body(..., example=
                        {"client_identifier": "LEANPAY",
                         "verification_channel_id": 1,
                         "third_party_account_no": "9011426016 or 1111111111"
                         }),
                        background_task: BackgroundTasks
                        ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/verify-bank-account",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="VERIFY_BANK_ACCOUNT",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        callback_in.payout_service_id = callback_in.verification_channel_id
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/AccountCreditorVerifier"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"]["output"],
                    breakdown_errors="",
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
