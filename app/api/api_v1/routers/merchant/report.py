import calendar
import io

from sqlalchemy import func
from datetime import datetime, timedelta
from typing import Any, List
from app import models, schemas, crud
from fastapi import APIRouter, Body, Depends, HTTPException, Security, Header, BackgroundTasks
from dataclasses import asdict
from app.api import deps
from app.core.config import settings
from fastapi.encoders import jsonable_encoder
from fastapi.responses import StreamingResponse
from sqlalchemy.orm import Session, selectinload, joinedload
from app.constants.role import Role
from app.constants.system_constant import SystemConstant
from sqlalchemy import extract
from sqlalchemy.ext.serializer import loads, dumps
import pandas as pd
import shutil

router = APIRouter(prefix="/reports", tags=["reports"])


@router.post("/generate-plan-bill", response_model=schemas.ResponseApiBase)
def generate_bill(
        *,
        db: Session = Depends(deps.get_db), _id: int = 0,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate bill statement
    """
    try:
        account_query = crud.account.get_account_by_id(db, id=_id)
        full_name = account_query.users.full_name
        address = account_query.users.kyc_information
        company_name = SystemConstant.SYSTEM_CONS["Billing_Details"]["COMPANY_NAME"]
        company_address = SystemConstant.SYSTEM_CONS["Billing_Details"]["COMPANY_ADDRESS"]
        current_account = crud.account.get_account_by_id(db, new_token["account_id"])
        account_number = current_account.merchant_id
        usage_charges = current_account.subscription_plan.plan_charges
        other_charges: int = 0
        discount_rebates = 0
        current_charges_before_tax = usage_charges + float(other_charges)
        gst = 0,
        total_current_charges = round(usage_charges + other_charges, 2)
        rounding_adjustment = current_charges_before_tax - total_current_charges
        total_amount_due = total_current_charges
        payment_due_date = datetime.today().date() + timedelta(days=30)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=company_address,
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/admin/generate-daily-settlement", response_model=Any, response_description='xlsx')
def get_excel(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:

        list_data_customer_bill = crud.customer_bill.get_multi(db, skip=skip, limit=limit)
        current_account = []
        account = current_user.account
        for acc in account:
            if acc.is_active:
                current_account.append(acc)
        if not list_data_customer_bill:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=list_data_customer_bill,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )

        from Settlement.creating_settlement import creating_settlement
        result = creating_settlement('ABC', list_data_customer_bill, current_account[0])
        headers = {
            'Content-Disposition': 'attachment; filename="filename.xlsx"'
        }
        return StreamingResponse(result, headers=headers)

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant/generate-settlement", response_model=Any, response_description='xlsx')
def get_settlement(
        *,
        db: Session = Depends(deps.get_db),
        start_date=Body(..., embed=True),
        end_date=Body(..., embed=True),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        list_data_customer_bill = db.query(models.CustomerBill).filter(
            models.CustomerBill.account_id == new_token["account_id"],
            func.date(models.CustomerBill.created_at) >= cur_start_date.date(),
            func.date(models.CustomerBill.created_at) <= cur_end_date.date()
        ).all()
        x = loads(dumps(list_data_customer_bill))
        current_account = []
        account = current_user.account
        for acc in account:
            if acc.is_active:
                current_account.append(acc)
        if not list_data_customer_bill:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=list_data_customer_bill,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )

        from Settlement.creating_settlement import creating_settlement
        result = creating_settlement(datetime.today().date().isoformat(), list_data_customer_bill, current_account[0])
        headers = {
            'Content-Disposition': f'attachment; filename="{current_account[0].name}__{start_date} to {end_date}__.xlsx"'
        }
        return StreamingResponse(result, headers=headers)

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant/daily-generate-settlement", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_daily_settlement(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        current_active_account = db.query(models.Account) \
            .filter(models.Account.record_status == settings.RECORD_ACTIVE).all()

        for acc in current_active_account:
            list_data_customer_bill = db.query(models.CustomerBill).filter(
                models.CustomerBill.account_id == acc.id,
                models.CustomerBill.invoice_status_id == settings.TRANSACTION_SUCCESS,
                func.date(models.CustomerBill.created_at) == yesterday_date.date()
            ).all()

            from Settlement.creating_settlement import creating_settlement_monthly
            result = creating_settlement_monthly(datetime.today().date().isoformat(), list_data_customer_bill, acc,
                                                 yesterday_date.date().isoformat())
            file = f"{acc.name}_{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
            df = pd.read_excel(result, engine='openpyxl')
            df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
            # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
            #                     root_dir="./Settlement/daily")

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant/monthly-generate-settlement", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_monthly_settlement(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        current_active_account = db.query(models.Account) \
            .filter(models.Account.record_status == settings.RECORD_ACTIVE).all()

        for acc in current_active_account:
            list_data_customer_bill = db.query(models.CustomerBill).filter(
                models.CustomerBill.account_id == acc.id,
                models.CustomerBill.invoice_status_id == settings.TRANSACTION_SUCCESS,
                extract('month', models.CustomerBill.created_at) == datetime.today().month,
            ).all()

            from Settlement.creating_settlement import creating_settlement_monthly
            result = creating_settlement_monthly(datetime.today().date().isoformat(), list_data_customer_bill, acc,
                                                 datetime.today().date().isoformat())
            file = f"{acc.name}_{datetime.today().strftime('%b')}.xlsx"
            df = pd.read_excel(result, engine='openpyxl')
            df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
            # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().month}", 'zip',
            #                     root_dir="./Settlement/daily")

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/generate-report", response_model=schemas.ResponseApiBase)
def generate_report(
        *,
        db: Session = Depends(deps.get_db), _id: int = 0,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate bill report
    """
    try:
        today = db.query(models.CustomerBill).filter(models.CustomerBill.invoice_status == "SUCCESS",
                                                     models.CustomerBill.account_id == new_token["account_id"],
                                                     extract("day",
                                                             models.CustomerBill.created_at) == datetime.today().day).all()

        this_year = db.query(models.CustomerBill).filter(models.CustomerBill.invoice_status == "SUCCESS",
                                                         models.CustomerBill.account_id == new_token["account_id"],
                                                         extract("year",
                                                                 models.CustomerBill.created_at) == datetime.today().year).all()

        last_week_date = datetime.today() - timedelta(weeks=1)
        last_week_sale = db.query(models.CustomerBill) \
            .filter(models.CustomerBill.invoice_status == "SUCCESS",
                    models.CustomerBill.account_id == new_token["account_id"],
                    func.date(
                        models.CustomerBill.created_at) >= last_week_date
                    .date()).all()
        last_month_range_date = []
        range_month = calendar.monthrange(datetime.today().year, datetime.today().month)
        for x in range(range_month[0], range_month[1] + 1):
            last_month_range_date.append \
                (schemas.ReportCreate(x_axis=f"{x}", y_axis=0, min_value=float(x), max_value=0))

        for y in last_month_range_date:
            for z in last_week_sale:
                first_value = y.min_value
                second_value = z.created_at.day
                if first_value == second_value:
                    y.y_axis = float(y.y_axis) + float(z.total)

        if today:
            date_ = float(today[0].created_at.strftime("%H.%M"))
            amount = float(today[0].total)
            for x in today:
                float(x.created_at.strftime("%H.%M"))
            # assign today collection money
            today_range_date = [
                schemas.ReportCreate(x_axis="00.00 - 00:59", y_axis=0, min_value=float(00.00), max_value=float(00.59)),
                schemas.ReportCreate(x_axis="01.00 - 01:59", y_axis=0, min_value=float(01.00), max_value=float(01.59)),
                schemas.ReportCreate(x_axis="02.00 - 02:59", y_axis=0, min_value=float(02.00), max_value=float(02.59)),
                schemas.ReportCreate(x_axis="03.00 - 03:59", y_axis=0, min_value=float(03.00), max_value=float(03.59)),
                schemas.ReportCreate(x_axis="04.00 - 04:59", y_axis=0, min_value=float(04.00), max_value=float(04.59)),
                schemas.ReportCreate(x_axis="05.00 - 05:59", y_axis=0, min_value=float(05.00), max_value=float(05.59)),
                schemas.ReportCreate(x_axis="06.00 - 06:59", y_axis=0, min_value=float(06.00), max_value=float(06.59)),
                schemas.ReportCreate(x_axis="07.00 - 07:59", y_axis=0, min_value=float(07.00), max_value=float(07.59)),
                schemas.ReportCreate(x_axis="08.00 - 08:59", y_axis=0, min_value=float(08.00), max_value=float(08.59)),
                schemas.ReportCreate(x_axis="09.00 - 09:59", y_axis=0, min_value=float(09.00), max_value=float(09.59)),
                schemas.ReportCreate(x_axis="10.00 - 10:59", y_axis=0, min_value=float(10.00), max_value=float(10.59)),
                schemas.ReportCreate(x_axis="10.00 - 10:59", y_axis=0, min_value=float(10.00), max_value=float(10.59)),
                schemas.ReportCreate(x_axis="11.00 - 11:59", y_axis=0, min_value=float(11.00), max_value=float(11.59)),
                schemas.ReportCreate(x_axis="12.00 - 12:59", y_axis=0, min_value=float(12.00), max_value=float(12.59)),
                schemas.ReportCreate(x_axis="13.00 - 13:59", y_axis=0, min_value=float(13.00), max_value=float(13.59)),
                schemas.ReportCreate(x_axis="14.00 - 14:59", y_axis=0, min_value=float(14.00), max_value=float(14.59)),
                schemas.ReportCreate(x_axis="15.00 - 15:59", y_axis=0, min_value=float(15.00), max_value=float(15.59)),
                schemas.ReportCreate(x_axis="16.00 - 16:59", y_axis=0, min_value=float(16.00), max_value=float(16.59)),
                schemas.ReportCreate(x_axis="17.00 - 17:59", y_axis=0, min_value=float(17.00), max_value=float(17.59)),
                schemas.ReportCreate(x_axis="18.00 - 18:59", y_axis=0, min_value=float(18.00), max_value=float(18.59)),
                schemas.ReportCreate(x_axis="19.00 - 19:59", y_axis=0, min_value=float(19.00), max_value=float(19.59)),
                schemas.ReportCreate(x_axis="20.00 - 20:59", y_axis=0, min_value=float(20.00), max_value=float(20.59)),
                schemas.ReportCreate(x_axis="21.00 - 21:59", y_axis=0, min_value=float(21.00), max_value=float(21.59)),
                schemas.ReportCreate(x_axis="22.00 - 22:59", y_axis=0, min_value=float(22.00), max_value=float(22.59)),
                schemas.ReportCreate(x_axis="23.00 - 23:59", y_axis=0, min_value=float(23.00), max_value=float(23.59)),
            ]
            for x in today_range_date:
                if x.min_value < date_ < x.max_value:
                    x.y_axis = x.y_axis + amount

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=last_month_range_date,
                breakdown_errors="",
                token=""
            )
        year_range_date = []
        for _month in range(1, 12 + 1):
            year_range_date.append(
                (schemas.ReportCreate(x_axis=f"{_month}", y_axis=0, min_value=int(_month), max_value=0))
            )
        x, y, z = schemas.GenerateReport.get_range_date(datetime.today().month, datetime.today().year)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=[{"today": x}, {"month": y}, {"year": z}],
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/admin/payment-settlement", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_payment_settlement(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        current_active_account = db.query(models.Account) \
            .filter(models.Account.record_status == settings.RECORD_ACTIVE,
                    models.Account.merchant_id != None).all()

        pay_out_data = []

        for acc in current_active_account:
            list_data_customer_bill = db.query(models.CustomerBill).filter(
                models.CustomerBill.account_id == acc.id,
                models.CustomerBill.invoice_status_id == settings.TRANSACTION_SUCCESS,
                func.date(models.CustomerBill.created_at) == yesterday_date.date()
            ).all()

            threshold_pool_id = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL")
            threshold_pool = crud.pool.get_by_pool_account_type(db, _id=threshold_pool_id,
                                                                _account_id=acc.id)

            total_sale = 0
            threshold_sale = 0
            balance_sale = 0

            if threshold_pool:
                total_sale = sum(c.customer_bill_payment_record.amount for c in list_data_customer_bill)
                balance_sale = total_sale - threshold_pool.value
                if balance_sale > 1:
                    threshold_sale = threshold_pool.value
                else:
                    balance_sale = total_sale
            else:
                total_sale = sum(c.customer_bill_payment_record.amount for c in list_data_customer_bill)

            account_number = None
            if acc.account_bank:
                account_number = acc.account_bank[0].account_number
            pay_out_data.append(
                {
                    "MERCHANT_NAME": acc.name,
                    "MERCHANT_ID": acc.merchant_id,
                    "TOTAL_SALE": total_sale,
                    "OUTSTANDING_AMOUNT": threshold_sale,
                    "TOTAL_REVENUE": balance_sale,
                    "BANK_ACCOUNT_NUMBER": account_number
                }
            )

        from Settlement.creating_settlement import creating_admin_payment_settlement
        result = creating_admin_payment_settlement(datetime.today().date().isoformat(), pay_out_data)
        file = f"settlement_merchant_{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
        df = pd.read_excel(result, engine='openpyxl')
        df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
        # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
        #                     root_dir="./Settlement/daily")

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/admin/payout-report", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_pay_out_settlement(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        current_active_account = db.query(models.Account) \
            .filter(models.Account.record_status == settings.RECORD_ACTIVE,
                    models.Account.merchant_id != None).all()

        pay_out_data = []

        for acc in current_active_account:
            list_data_payout = db.query(models.PayOutRecord).filter(
                models.PayOutRecord.account_id == acc.id,
                models.PayOutRecord.transaction_status == "SUCCESS",
                func.date(models.PayOutRecord.created_at) == yesterday_date.date()
            ).all()
            query_get = crud.virtual_account.get_by_pool_name(db, name="DEFAULT_ACCOUNT", _id=acc.id)
            va_account = "NO_VA_ACCOUNT"
            if query_get:
                va_account = query_get.merchant_payout_pool_id
            total_sale = sum(c.value for c in list_data_payout)
            account_number = None
            if acc.account_bank:
                account_number = acc.account_bank[0].account_number
            pay_out_data.append(
                {
                    "MERCHANT_NAME": acc.name,
                    "MERCHANT_ID": acc.merchant_id,
                    "TOTAL_PAYOUT_SALE": total_sale,
                    "VIRTUAL_ACCOUNT_NUMBER": va_account
                }
            )

        from Settlement.creating_settlement import creating_admin_pay_out_report
        result = creating_admin_pay_out_report(datetime.today().date().isoformat(), pay_out_data)
        file = f"payout_merchant_{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
        df = pd.read_excel(result, engine='openpyxl')
        df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
        # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
        #                     root_dir="./Settlement/daily")

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant/payout-report", response_model=Any)
def get_merchant_pay_out_settlement(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                    Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        pay_out_data = []
        va_account = "NO_VA_ACCOUNT"
        if current_user.account:
            list_data_payout = db.query(models.PayOutRecord).filter(
                models.PayOutRecord.account_id == new_token['account_id'],
                models.PayOutRecord.transaction_status == "SUCCESS",
                func.date(models.PayOutRecord.created_at) == yesterday_date.date()
            ).all()
            query_get = crud.virtual_account.get_by_pool_name(db, name="DEFAULT_ACCOUNT", _id=new_token['account_id'])
            if query_get:
                va_account = query_get.merchant_payout_pool_id
            for item in list_data_payout:
                pay_out_data.append(
                    {
                        "SWITCH_INVOICE_ID": item.switch_invoice_id,
                        "MERCHANT_INVOICE_ID": item.merchant_invoice_id,
                        "INVOICE_NO": item.transaction_invoice_no,
                        "AMOUNT": item.value,
                        "TRANSACTION_STATUS": item.transaction_status,
                        "VA_ACCOUNT": va_account
                    }
                )

        from Settlement.creating_settlement import creating_pay_out_report
        result = creating_pay_out_report(datetime.today().date().isoformat(), pay_out_data, va_account)
        file = f"{va_account}_payout_report{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
        # df = pd.read_excel(result, engine='openpyxl')
        # df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
        return StreamingResponse(io.BytesIO(result.getvalue()),
                                 media_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                 headers={
                                     "Content-Disposition": f'attachment; filename="{file}"'
                                 })
        # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
        #                     root_dir="./Settlement/daily")

        # return schemas.ResponseApiBase(
        #     response_code=2000,
        #     description='SUCCESS',
        #     app_version=settings.API_V1_STR,
        #     talk_to_server_before=datetime.today(),
        #     data="",
        #     breakdown_errors="",
        #     token=""
        # )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/admin/prefund-report", response_model=Any,
             dependencies=[Depends(deps.get_token_onboard_header)])
def get_prefund_settlement(
        *,
        db: Session = Depends(deps.get_db),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        current_active_account = db.query(models.Account) \
            .filter(models.Account.record_status == settings.RECORD_ACTIVE,
                    models.Account.merchant_id != None).all()

        prefund_data = []

        for acc in current_active_account:
            list_data_prefund = db.query(models.PrefundRecord).filter(
                models.PrefundRecord.account_id == acc.id,
                func.date(models.PrefundRecord.created_at) == yesterday_date.date()
            ).all()

            total_sale = sum(c.value for c in list_data_prefund)
            account_number = None
            if acc.account_bank:
                account_number = acc.account_bank[0].account_number
            prefund_data.append(
                {
                    "MERCHANT_NAME": acc.name,
                    "MERCHANT_ID": acc.merchant_id,
                    "TOTAL_PREFUND_SALE": total_sale,
                }
            )

        from Settlement.creating_settlement import creating_admin_prefund_report
        result = creating_admin_prefund_report(datetime.today().date().isoformat(), prefund_data)
        file = f"prefund_merchant_{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
        df = pd.read_excel(result, engine='openpyxl')
        df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
        # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
        #                     root_dir="./Settlement/daily")

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant/prefund-report", response_model=Any)
def get_merchant_prefund_settlement(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                    Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate settlement excel file
    """
    try:
        yesterday_date = datetime.today() - timedelta(days=1)

        prefund_data = []
        if current_user.account:
            list_data_prefund = db.query(models.PrefundRecord).filter(
                models.PrefundRecord.account_id == new_token['account_id'],
                func.date(models.PrefundRecord.created_at) == yesterday_date.date()
            ).all()

            if list_data_prefund:
                for item in list_data_prefund:
                    prefund_data.append(
                        {
                            "MERCHANT_POOL_ID": item.merchant_pool_id,
                            "INVOICE_NO": item.transaction_invoice_no,
                            "AMOUNT": item.value,
                            "TRANSACTION_STATUS": "SUCCESS",
                        }
                    )

        from Settlement.creating_settlement import creating_prefund_report
        result = creating_prefund_report(datetime.today().date().isoformat(), prefund_data,
                                         current_user.account[0].merchant_id)
        file = f"{current_user.account[0].merchant_id}_prefund_report_{yesterday_date.strftime('%m_%d_%Y')}.xlsx"
        # df = pd.read_excel(result, engine='openpyxl')
        # df.to_excel(f"./Settlement/daily/{file}", engine='xlsxwriter', header=False)
        return StreamingResponse(io.BytesIO(result.getvalue()),
                                 media_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                 headers={
                                     "Content-Disposition": f'attachment; filename="{file}"'
                                 })
        # shutil.make_archive(f"./Settlement/daily/{datetime.today().date().isoformat()}", 'zip',
        #                     root_dir="./Settlement/daily")

        # return schemas.ResponseApiBase(
        #     response_code=2000,
        #     description='SUCCESS',
        #     app_version=settings.API_V1_STR,
        #     talk_to_server_before=datetime.today(),
        #     data="",
        #     breakdown_errors="",
        #     token=""
        # )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
