import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
from app.Utils.shared_utils import SharedUtils
import app.Utils.web_cdn_do as wcd
from sqlalchemy.ext.serializer import loads, dumps

router = APIRouter(prefix="/merchant-cloud-image", tags=["merchant-cloud-image"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-cloud-image",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_ALL_LIST_MERCHANT_CLOUD_IMAGE",
                                  _data="NO_DATA", db_=db)

        db_count = crud.merchant_cloud_image.get_count_admin_record(db=db, start_date=_in.start_date,
                                                                    end_date=_in.end_date,
                                                                    _id=new_token["account_id"],
                                                                    _record=_in.record_status,
                                                                    role=current_user.user_role.role.name)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.merchant_cloud_image.get_multi_search_account(db=db, skip=skip, limit=limit,
                                                                       _id=new_token["account_id"],
                                                                       record_status=_in.record_status,
                                                                       start_date=_in.start_date,
                                                                       end_date=_in.end_date,
                                                                       search_column=_in.search.search_column,
                                                                       search_key=_in.search.search_key,
                                                                       parameter_name=_in.sort.parameter_name,
                                                                       sort_type=_in.sort.sort_type,
                                                                       search_enable=_in.search.search_enable,
                                                                       invoice_status=_in.invoice_status,
                                                                       table_name="merchant_cloud_images",
                                                                       role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.MerchantFormModel = Depends(),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant cloud image.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/merchant-cloud-image",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_MERCHANT_CLOUD_IMAGE",
                                  _data=_in, db_=db)
        _in.file_name = f'{_in.file_name}_{SharedUtils.my_random_string(4)}_LEANX'
        query_get = crud.merchant_cloud_image.get_by_file_name(db, file_name=_in.file_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        client = wcd.get_spaces_client(
            region_name="sgp1",
            endpoint_url="https://sgp1.digitaloceanspaces.com",
            key_id=settings.S3_ACCESS_ID,
            secret_access_key=settings.S3_SECRET_KEY
        )
        generated_filename = f'{SharedUtils.my_random_string(6)}_{_in.file_name}_{_in.file.filename}'

        if _in.file.content_type in ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff',
                                     'image/x-xbitmap', 'application/vnd.ms-excel',
                                     'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                     'text/csv', 'application/pdf', 'application/x-pdf']:
            wcd.upload_file_to_space(spaces_client=client, space_name="lean", file_src=_in.file.file,
                                     save_as=generated_filename,
                                     is_public=True, content_type=_in.file.content_type)
            upload_link = f"https://lean.sgp1.digitaloceanspaces.com/-{_in.file.content_type}-{generated_filename}"

            new_data = schemas.MerchantCloudImageCreate(
                file_name=_in.file_name,
                description=_in.description,
                image_direct_link=upload_link,
                account_id=new_token["account_id"],
                upload_date=datetime.datetime.today(),
            )

            query_create = crud.merchant_cloud_image.create_self_date(db, obj_in=new_data)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_create,
                token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-cloud-image-records", response_model=schemas.ResponseApiBase)
def get_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available payment record based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-cloud-image-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_MERCHANT_CLOUD_IMAGE_LIST",
                                  _data=_in.json(), db_=db)

        db_count = crud.merchant_cloud_image.get_count(db=db, _id=new_token["account_id"],
                                                       start_date=_in.start_date,
                                                       end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.merchant_cloud_image.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.MerchantFormModel = Depends(),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update merchant collection.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/update-merchant-cloud-image",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_MERCHANT_CLOUD_IMAGE",
                                  _data=_in, db_=db)
        query_update = crud.merchant_cloud_image.get_by_merchant_cloud_id(db, _id=_id)
        if query_update:
            if _in.file:
                client = wcd.get_spaces_client(
                    region_name="sgp1",
                    endpoint_url="https://sgp1.digitaloceanspaces.com",
                    key_id=settings.S3_ACCESS_ID,
                    secret_access_key=settings.S3_SECRET_KEY
                )
                if _in.file.content_type in ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff',
                                             'image/x-xbitmap']:
                    generated_filename = f'{SharedUtils.my_random_string(6)}_{_in.file.filename}'
                    wcd.upload_file_to_space(spaces_client=client, space_name="lean", file_src=_in.file.file,
                                             save_as=generated_filename,
                                             is_public=True, content_type=_in.file.content_type)
                    upload_link = f"https://lean.sgp1.digitaloceanspaces.com/{generated_filename}"

            update_ = crud.merchant_cloud_image.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete payment records.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url=f"/payment-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_PAYMENT_RECORD",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.merchant_cloud_image.get_by_merchant_cloud_id(db, _id=_id)
        _in = schemas.MerchantCloudImageUpdate(
            is_active=False,
        )
        if query_delete:
            update_ = crud.merchant_cloud_image.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_merchant_cloud_image_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search bank id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payment-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_PAYMENT_RECORD_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.merchant_cloud_image.get_by_merchant_cloud_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/bulk-create-cloud-image", response_model=schemas.ResponseApiBase)
def create_bulk_merchant_cloud_image(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.BulkMerchantFormModel = Depends(),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant cloud image.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/merchant-cloud-image",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_MERCHANT_CLOUD_IMAGE",
                                  _data=_in, db_=db)

        _in.file_name = f'{_in.file_name}_{SharedUtils.my_random_string(4)}_LEANX'

        query_get = crud.merchant_cloud_image.get_by_file_name(db, file_name=_in.file_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        client = wcd.get_spaces_client(
            region_name="sgp1",
            endpoint_url="https://sgp1.digitaloceanspaces.com",
            key_id=settings.S3_ACCESS_ID,
            secret_access_key=settings.S3_SECRET_KEY
        )
        data = []
        for file in _in.files:
            if file.content_type in ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff',
                                     'image/x-xbitmap']:
                generated_filename = f'{SharedUtils.my_random_string(6)}_{_in.file_name}_{file.filename}'

                wcd.upload_file_to_space(spaces_client=client, space_name="lean", file_src=file.file,
                                         save_as=generated_filename,
                                         is_public=True, content_type=file.content_type)
                upload_link = f"https://lean.sgp1.digitaloceanspaces.com/{generated_filename}"

                new_data = schemas.MerchantCloudImageCreate(
                    file_name=_in.file_name,
                    description=_in.description,
                    image_direct_link=upload_link,
                    account_id=new_token["account_id"],
                    upload_date=datetime.datetime.today(),
                )

                query_create = crud.merchant_cloud_image.create_self_date(db, obj_in=new_data)
                query_create_data = loads(dumps(query_create))
                data.append(query_create_data)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=data,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
