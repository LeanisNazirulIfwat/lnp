import base64
import json
from datetime import datetime
from typing import Any, List
from app import models, schemas, crud
from fastapi import APIRouter, Body, Depends, HTTPException, BackgroundTasks, Request, status
from jose import jws
from app.api import deps
from fastapi.encoders import jsonable_encoder
from sqlalchemy import func
from sqlalchemy.orm import Session
from sqlalchemy.ext.serializer import loads, dumps
import requests as req
from app.Utils.shared_utils import SharedUtils
from app.constants.system_constant import SystemConstant
from app.core.config import settings
from itertools import groupby
from app.constants.payload_example import PayloadExample
from app.Utils.subscription_checker import Subscription

router = APIRouter(prefix="/public-merchant", tags=["public-merchant"])


@router.post("/validate", response_model=schemas.ResponseApiBase)
def validate_token(
        *,
        db: Session = Depends(deps.get_db),
        auth_token=Depends(deps.get_token_header)
) -> Any:
    """
    validate user.
    """
    try:
        account = crud.account.get_account_by_id(db, auth_token["account_id"])
        if account:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={
                    "merchant-id": account.merchant_id,
                    "merchant-name": account.name,
                    "merchant-created": account.created_at.isoformat()
                },
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="TOKEN_NOT_VALID",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/list-payment-services", response_model=Any)
def public_list_payment_services(
        _in: schemas.PublicPaymentList = Body(..., example=PayloadExample.CREATE_PAYLOAD_PUBLIC_LIST["WEB_PAYMENT"]),
        auth_token=Depends(deps.get_token_header)
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
        _in_dict = SharedUtils.generate_payload(**json.loads(_in.json()))
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'amount_to_calculate': '25',
                   'payment_service_record_status_id': '1'}
        payload.update(_in_dict)
        files = {}
        headers = {}
        services: List[schemas.SwitchPublicPaymentResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]["data"]
                for x in list_services:
                    if x["best_rate"]["response_code"] == 2100:
                        data = schemas.SwitchPublicPaymentResponses(
                            payment_service_id=x["payment_service_id"],
                            payment_model_reference_id=x["payment_model_reference_id"],
                            payment_model_reference=x["payment_model_reference"],
                            unique_reference=x["unique_reference"],
                            payment_service=x["switch_payment_service"],
                            name=x["name"],
                            record_status=x["record_status"],
                            record_status_id=x["record_status_id"],
                            rate=x["best_rate"]["selected"]["fullPaymentServiceObj"]["rate"]
                        )
                        services.append(data)
                services = [x for x in services if x.payment_service == _in.payment_type.upper()
                            and x.record_status == _in.payment_status.upper()]
                sorted_services = sorted(services, key=lambda z: z.payment_service)
                sorted_group = [{key: list(result)} for key, result in
                                groupby(sorted_services, key=lambda y: y.payment_service)]
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=sorted_group
                    ),
                    },
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/collection-payment", response_model=Any)
def public_payment_collection(*,
                              db: Session = Depends(deps.get_db),
                              invoice_no: str,
                              auth_token=Depends(deps.get_token_header),
                              callback_in: schemas.SwitchCallbackUrlCreate
                              = Body(..., example=PayloadExample.CREATE_PUBLIC_COLLECTION_PAYMENT["PAYLOAD"]),
                              ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        secret_key = settings.SWITCH_SECRET_KEY
        input_callback = callback_in.callback_url
        input_redirect = callback_in.redirect_url
        callback_in.callback_url = settings.PUBLIC_CALLBACK_URL_LEANPAY
        callback_in.redirect_url = f"{settings.REDIRECT_URL_LEANPAY}additional_message='Arigaatoo'&bill_id={invoice_no}" \
                                   f"&extra_param='hello'"

        query_get = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4125,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_ALREADY_EXIST",
                token=""
            )

        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_DELETE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                charge_rate = 0
                if response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'WEB_PAYMENT':
                    charge_rate = accounts.subscription_plan.fpx_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'GLOBAL_CARD_PAYMENT':
                    charge_rate = accounts.subscription_plan.credit_card_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'DIGITAL_PAYMENT':
                    charge_rate = accounts.subscription_plan.ewallet_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'BUY_NOW_PAY_LATER':
                    charge_rate = accounts.subscription_plan.bnpl_charges

                # create payment record
                if accounts.subscription_plan.fpx_charges:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=charge_rate,
                        net_amount=callback_in.amount + float(charge_rate)
                    )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=charge_rate,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)

                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) + float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=1,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                    api_key=auth_token["access_uuid"]
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query.invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
                else:
                    bill_create.invoice_no = invoice_no

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if new_bill:
                    if collection_ids.collection_method == 2:
                        collection_items = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)
                        for each in collection_items:
                            cust_bill_item = schemas.CustomerBillItemCreate(
                                product_id=each[0].product_id,
                                customer_bill_id=new_bill.id,
                                quantity=each[0].quantity,
                                amount=each[0].amount,
                                total_amount=each[0].total_amount
                            )
                            new_customer_bill_item = crud.customer_bill_item.create_self_date(db, obj_in=cust_bill_item)

                    callback_create = schemas.MerchantCallbackCreate(
                        bill_invoice=bill_create.invoice_no,
                        transaction_invoice=response.json()["data"]["invoice_no"],
                        callback_merchant_url=settings.PUBLIC_CALLBACK_URL_LEANPAY,
                        callback_user_url=input_callback,
                        api_key=auth_token["access_uuid"]
                    )

                    user_callback = crud.merchant_callback.create_self_date(db=db, obj_in=callback_create)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data={
                            "invoice_no": user_callback.bill_invoice,
                            "transaction_invoice_no": response.json()["data"]["invoice_no"],
                            "redirect_url": response.json()["data"]["output"]["response_data"]["data"]
                        },
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/collection-payment-portal", response_model=Any)
def public_payment_collection_portal(*,
                                     db: Session = Depends(deps.get_db),
                                     invoice_no: str = ...,
                                     auth_token=Depends(deps.get_token_header),
                                     callback_in: schemas.SwitchCallbackUrlCreate
                                     = Body(..., example=PayloadExample.CREATE_PUBLIC_COLLECTION_PAYMENT["PAYLOAD"]),
                                     ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        secret_key = settings.SWITCH_SECRET_KEY
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)
        input_callback = callback_in.callback_url
        input_redirect = callback_in.redirect_url
        callback_in.callback_url = settings.PUBLIC_CALLBACK_URL_LEANPAY
        callback_in.redirect_url = f"{settings.REDIRECT_URL_LEANPAY}additional_message=Arigaatoo&bill_id={invoice_no}" \
                                   f"&extra_param='hello'&merchant_return_url={callback_in.redirect_url}"

        b64_redirect_url = base64.b64encode(bytes(callback_in.redirect_url, "utf-8"))

        redirect_url = f"http://dev.portal.leanpay.my/pay/collections/{callback_in.collection_uuid}?" \
                       f"fullname={callback_in.full_name}&email={callback_in.email}&phone_number={callback_in.phone_number}" \
                       f"&invoice_no={invoice_no}" \
                       f"&redirect_url={b64_redirect_url.decode('utf-8')}&callback_url={callback_in.callback_url}"

        callback_create = schemas.MerchantCallbackCreate(
            bill_invoice=invoice_no,
            transaction_invoice="NO_DATA",
            callback_merchant_url=settings.PUBLIC_CALLBACK_URL_LEANPAY,
            callback_user_url=input_callback,
            redirect_user_url=input_redirect,
            redirect_merchant_url=callback_in.redirect_url,
            api_key=auth_token["access_uuid"]
        )

        user_callback = crud.merchant_callback.create_self_date(db=db, obj_in=callback_create)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "collection_uuid": callback_in.collection_uuid,
                "redirect_url": redirect_url
            },
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/bill-payment", response_model=Any)
def public_payment_bill(*,
                        db: Session = Depends(deps.get_db),
                        callback_in: schemas.SwitchCallbackUrlCreate
                        = Body(..., example=PayloadExample.CREATE_PUBLIC_BILL_PAYMENT["PAYLOAD"]),
                        auth_token=Depends(deps.get_token_header),
                        _invoice: str,
                        ) -> Any:
    """
    API for creating payment through payment switch using bill creation
    """
    try:
        query_get = crud.customer_bill.get_by_customer_by_active_invoice_no(db, _invoice=_invoice)
        if not query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_NOT_VALID",
                token=""
            )

        input_callback = callback_in.callback_url
        input_redirect = callback_in.redirect_url
        callback_in.callback_url = settings.PUBLIC_CALLBACK_URL_LEANPAY
        callback_in.redirect_url = f"{settings.REDIRECT_URL_LEANPAY}additional_message='Arigaatoo'&bill_id={_invoice}" \
                                   f"&extra_param='hello'"

        query_get = loads(dumps(query_get[0]))

        collection_ids = crud.collection.get_by_collection_id(db, _id=query_get.collection_id)
        if collection_ids.enable_quantity_limit:
            if collection_ids.quantity_limit - callback_in.quantity == 0:
                collection_update = schemas.CollectionUpdate(
                    quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                    record_status=settings.RECORD_NOT_ACTIVE
                )
            else:
                collection_update = schemas.CollectionUpdate(
                    quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                )

            update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

        # query customer from db
        existing_customer = crud.customer.get_by_name_account(db, _id=query_get.account_id, full_name=query_get.email)
        if not existing_customer:
            if query_get.customer_bill_collection.enable_billing_address:
                # create customer id
                customer_create = schemas.CustomerCreate(
                    account_id=query_get.account_id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    address_line_one=callback_in.address_line_one,
                    address_line_two=callback_in.address_line_two,
                    address_postcode=callback_in.address_postcode,
                    address_state=callback_in.address_state,
                    address_city=callback_in.address_city,
                    address_country=callback_in.address_country,
                )
                shipping_create = schemas.ShippingAddressCreate(
                    name=callback_in.full_name,
                    phone_number=callback_in.phone_number,
                    address_line_one=callback_in.address_line_one,
                    address_line_two=callback_in.address_line_two,
                    address_postcode=callback_in.address_postcode,
                    address_state=callback_in.address_state,
                    address_city=callback_in.address_city,
                    address_country=callback_in.address_country
                )
                new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                callback_in.shipping_address_id = new_shipping_address.id
            else:
                # create customer id
                if not callback_in.full_name or not callback_in.email or not callback_in.phone_number:
                    return schemas.ResponseApiBase(
                        response_code=5568,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=" ",
                        breakdown_errors="PLEASE_ENSURE_THAT_NAME_EMAIL_PHONE_NUMBER_MUST_NOT_NULL",
                        token=""
                    )

                customer_create = schemas.CustomerCreate(
                    account_id=query_get.account_id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number
                )
            create_customer = crud.customer.create_self_date(db=db, obj_in=customer_create)

        callback_in.amount = query_get.total
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}
        charge_rate = 0

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:

                if not query_get.payment_record_id:
                    sub = crud.account.get_account_by_id(db, query_get.account_id)
                    if response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'WEB_PAYMENT':
                        charge_rate = sub.subscription_plan.fpx_charges
                    elif response.json()["data"]["payment_service_obj"][
                        "switch_payment_service"] == 'GLOBAL_CARD_PAYMENT':
                        charge_rate = sub.subscription_plan.credit_card_charges
                    elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'DIGITAL_PAYMENT':
                        charge_rate = sub.subscription_plan.ewallet_charges
                    elif response.json()["data"]["payment_service_obj"][
                        "switch_payment_service"] == 'BUY_NOW_PAY_LATER':
                        charge_rate = sub.subscription_plan.bnpl_charges

                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=query_get.total,
                        transaction_fee=charge_rate,
                        net_amount=query_get.total + charge_rate
                    )

                    new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
                    query_get.payment_record_id = new_payment_record.id

                # update bill payment
                bill_update = schemas.CustomerBillCreate(
                    account_id=query_get.account_id,
                    payment_record_id=query_get.payment_record_id,
                    customer_id=query_get.customer_id,
                    collection_id=query_get.collection_id,
                    full_name=query_get.full_name,
                    email=query_get.email,
                    phone_number=query_get.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    invoice_no=query_get.invoice_no,
                    quantity=1,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    shipping_address_id=callback_in.shipping_address_id,
                    api_key=auth_token["access_uuid"],
                    created_at=datetime.today()
                )
                query_new = crud.customer_bill.get_by_invoice_no(db, invoice_no=_invoice)
                new_bill = crud.customer_bill.update_self_date(db, db_obj=query_new, obj_in=bill_update)
                if new_bill:
                    # add callback url
                    callback_create = schemas.MerchantCallbackCreate(
                        bill_invoice=_invoice,
                        transaction_invoice=response.json()["data"]["invoice_no"],
                        callback_merchant_url=settings.PUBLIC_CALLBACK_URL_LEANPAY,
                        callback_user_url=input_callback,
                        api_key=auth_token["access_uuid"]
                    )

                    user_callback = crud.merchant_callback.create_self_date(db=db, obj_in=callback_create)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=10018,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        else:
            return schemas.ResponseApiBase(
                response_code=10228,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=response.json()["data"],
                breakdown_errors="ADDRESS_NOT_FOUND",
                token=""
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/bill-payment-portal", response_model=Any)
def public_payment_bill_portal(*,
                               db: Session = Depends(deps.get_db),
                               callback_in: schemas.SwitchCallbackUrlCreate
                               = Body(..., example=PayloadExample.CREATE_PUBLIC_BILL_PAYMENT["PAYLOAD"]),
                               auth_token=Depends(deps.get_token_header),
                               _invoice: str,
                               ) -> Any:
    """
    API for creating payment through payment switch using bill creation
    """
    try:
        query_get = crud.customer_bill.get_by_customer_by_active_invoice_no(db, _invoice=_invoice)
        if not query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_NOT_VALID",
                token=""
            )

        query_get = loads(dumps(query_get[0]))

        input_callback = callback_in.callback_url
        input_redirect = callback_in.redirect_url
        callback_in.callback_url = settings.PUBLIC_CALLBACK_URL_LEANPAY
        callback_in.redirect_url = f"{settings.REDIRECT_URL_LEANPAY}additional_message=Arigaatoo&bill_id={_invoice}" \
                                   f"&extra_param='hello'&merchant_return_url={callback_in.redirect_url}"

        b64_redirect_url = base64.b64encode(bytes(callback_in.redirect_url, "utf-8"))

        redirect_url = f"{settings.PORTAL_BILL_URL}pay/invoice/{_invoice}?" \
                       f"fullname={callback_in.full_name}&email={callback_in.email}&phone_number={callback_in.phone_number}" \
                       f"&invoice_no={_invoice}" \
                       f"&redirect_url={b64_redirect_url.decode('utf-8')}&callback_url={callback_in.callback_url}"

        callback_create = schemas.MerchantCallbackCreate(
            bill_invoice=_invoice,
            transaction_invoice="NO_DATA",
            callback_merchant_url=settings.PUBLIC_CALLBACK_URL_LEANPAY,
            callback_user_url=input_callback,
            redirect_user_url=input_redirect,
            redirect_merchant_url=callback_in.redirect_url,
            api_key=auth_token["access_uuid"]
        )

        user_callback = crud.merchant_callback.create_self_date(db=db, obj_in=callback_create)

        if user_callback:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={
                    "collection_uuid": callback_in.collection_uuid,
                    "invoice_number": _invoice,
                    "redirect_url": redirect_url
                },
                breakdown_errors="",
                token=""
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/manual-checking-transaction", response_model=Any)
def public_manual_checking_payment(
        *,
        db: Session = Depends(deps.get_db),
        auth_token=Depends(deps.get_token_header),
        invoice_no: str,
) -> Any:
    """
    get image for payment provider
    """
    try:

        payment_bill = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)

        if not payment_bill:
            return schemas.ResponseApiBase(
                response_code=3614,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors=" INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )

        msg = {
            "invoice_no": payment_bill[0].transaction_invoice_no
        }
        encode = jws.sign(msg, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09', algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/GetPaymentDetails"

        payload = {'skip_encryption': 'false',
                   'client_identifier': 'LEANPAY',
                   'payload': encode}
        files = [

        ]
        headers = {}

        response_payment = req.request("POST", url, headers=headers, data=payload, files=files)
        if response_payment.status_code == req.codes.ok:
            if response_payment.json()["response_code"] == 2100:
                response_data = schemas.SwitchManualPaymentResponse(
                    **response_payment.json()["data"]["output"]["output"])
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={
                        'transaction_details': {
                            "invoice_no": invoice_no,
                            "fpx_invoice_no": response_data.invoice_no,
                            "amount": response_data.amount,
                            "invoice_status": response_data.invoice_status,
                            "providerTypeReference": response_data.selected.providerTypeReference,
                            "bank_provider": response_data.selected.fullPaymentServiceObj.name,
                            "category_code": response_data.selected.fullPaymentServiceObj.category_code
                        },
                        'company_name': payment_bill[1].account_company_details.company_name,
                        'company_number': payment_bill[1].account_company_details.company_number,
                        'nature_of_business': payment_bill[1].account_company_details.nature_of_business,
                        'company_details': payment_bill[1].account_company_details.company_detail_business_owner_detail,
                    },
                    breakdown_errors="",
                    token=""
                )
        return schemas.ResponseApiBase(
            response_code=3614,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data="",
            breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/public/transaction-list/", response_model=schemas.ResponseApiBase)
def public_transaction_list(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        start_date: str,
        end_date: str,
        invoice_status: str,
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(cur_start_date.month,
                                                                                   datetime.today().year)

        today_sale = db.query(models.CustomerBill) \
            .filter(models.CustomerBill.invoice_status == invoice_status.upper(),
                    models.CustomerBill.api_key == auth_token["access_uuid"],
                    func.date(models.CustomerBill.created_at) >= cur_start_date.date(),
                    func.date(models.CustomerBill.created_at) <= cur_end_date).offset(skip).limit(limit).all()

        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)
        transaction_details = []
        for trans in today_sale:
            if trans.customer_bill_payment_record.payment_date:
                new_trans = {
                    "Invoice No": trans.invoice_no,
                    "Invoice Status": trans.invoice_status,
                    "Name": trans.full_name,
                    "Email": trans.email,
                    "Phone Number": trans.phone_number,
                    "Transaction Invoice No": trans.transaction_invoice_no,
                    "Payment Mode": trans.customer_bill_payment_record.payment_mode,
                    "Amount": trans.total,
                    "Payment Date": trans.customer_bill_payment_record.payment_date,

                }
                transaction_details.append(new_trans)
            else:

                new_trans = {
                    "Invoice No": trans.invoice_no,
                    "Invoice Status": trans.invoice_status,
                    "Name": trans.full_name,
                    "Email": trans.email,
                    "Phone Number": trans.phone_number,
                    "Transaction Invoice No": trans.transaction_invoice_no,
                    "Payment Mode": trans.customer_bill_payment_record.payment_mode,
                    "Amount": trans.total,
                    "Payment Date": "NO_PAYMENT_DATE",

                }
                transaction_details.append(new_trans)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-date-sale": total_today_sale,
                "total-date-count": int(len(today_sale)),
                "transaction": transaction_details
            },
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/bill-list", response_model=schemas.ResponseApiBase)
def get_public_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        skip: int, limit: int,
        _in: schemas.PayloadListQuery
        = Body(..., example={
            "start_date": "01-02-2022",
            "end_date": "27-02-2022",
            "record_status": 1,
            "invoice_status": "success",
            "search": {
                "search_enable": False,
                "search_key": "WEB_PAYMENT",
                "search_column": "payment_mode",
            },
            "sort": {
                "parameter_name": "created_at",
                "sort_type": "desc"
            }
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Retrieve all available customer bill based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-customer-bills/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILLS_MERCHANT_LIST_USING_AUTH",
                                  _data=_in.json(), db_=db)

        db_count = crud.customer_bill.get_count_date(db=db, _id=auth_token["account_id"],
                                                     start_date=_in.start_date, end_date=_in.end_date)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.customer_bill.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=auth_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        if _in.invoice_status:
            list_data = [x for x in list_data if x.invoice_status == _in.invoice_status.upper()]

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable and not _in.invoice_status:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list},
                token=auth_token["access_uuid"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list
                  },
            token=auth_token["access_uuid"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid  kk"]
        )


@router.post("/public/create-payment-bill", response_model=schemas.ResponseApiBase)
def create_public_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerBillPublicCreate = Body(..., example={
            "full_name": "email email",
            "email": "email@gmail.com",
            "phone_number": "0112569587",
            "status": True,
            "total": 120,
            "collection_uuid": 'CL-177FF45196-LNP',
            "quantity": 6,
            "price_per_quantity": 20,
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Create New Customer Bill Using Auth-Key.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST   ",
                                  _url="/customer-bills/switch/create-payment-bill-direct",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="PUBLIC_CREATE_NEW_BILL_USING_API",
                                  _data=_in.json(), db_=db)

        # check collection id
        valid_collection_id = crud.collection.get_by_uuid(db=db, uuid=_in.collection_uuid)
        if not valid_collection_id:
            return schemas.ResponseApiBase(
                response_code=4566,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=_in.json(),
                breakdown_errors="INVALID_COLLECTION_ID",
                token=auth_token["access_uuid"]
            )

        # create customer id
        query_get_customer = crud.customer.get_by_name(db, full_name=_in.email)
        if not query_get_customer:
            customer_create = schemas.CustomerCreate(
                account_id=auth_token["account_id"],
                full_name=_in.full_name,
                email=_in.email,
                phone_number=_in.phone_number,
                # address=_in.address
            )

            # add customer to db
            new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
            _in.customer_id = new_customer.id
        else:
            _in.customer_id = query_get_customer.id

        # generate invoice
        _in.invoice_no = "BP-AUTH-" + SharedUtils.my_random_string(10) + "-LNP"
        # create payment record
        sub = crud.account.get_account_by_id(db, auth_token["account_id"])
        payment_record_create = schemas.PaymentRecordCreate(
            amount=_in.total,
            transaction_fee=sub.subscription_plan.fpx_charges,
            net_amount=_in.total + sub.subscription_plan.fpx_charges
        )

        new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
        _in.payment_record_id = new_payment_record.id
        _in.account_id = auth_token["account_id"]
        _in.base_url = str(request.base_url) + f"api/v1/customer-bills/{_in.invoice_no}/leanpay-bills"
        _in.api_key = auth_token["access_uuid"]
        _in.transaction_fee = sub.subscription_plan.fpx_charges
        _in.total_amount_with_fee = _in.total + sub.subscription_plan.fpx_charges

        json_in = jsonable_encoder(_in)
        new_in = schemas.CustomerBillCreate(**json_in)

        new_in.collection_id = valid_collection_id.id
        query_create = crud.customer_bill.create_self_date(db, obj_in=new_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=query_create,
            token=auth_token["access_uuid"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=6320,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=e.__class__,
            breakdown_errors="SOMETHING WHEN WRONG",
            token=auth_token["access_uuid"]
        )


@router.post("/public/bill-id", response_model=schemas.ResponseApiBase)
def get_public_customer_bill_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search customer bill id using Auth Key.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"/customers-bill/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_CUSTOMER_BILL_WITH_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_uuid = db.query(models.CustomerBill).filter(models.CustomerBill.id == _id).first()
        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=4561,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_NOT_VALID",
                token=""
            )
        query_uuid.base_url = str(request.base_url) + f"api/v1/customer-bills/{query_uuid.invoice_no}/leanpay-bills"
        if query_uuid:
            query_collection = crud.collection.get_by_collection_id(db, _id=query_uuid.collection_id)
            if query_collection.collection_method == 2:
                query_item = crud.customer_bill_item.get_by_customer_bill_id(db, _id=query_uuid.id)
                if query_item:
                    new_schema = schemas.CustomerBillMethodView(
                        full_name=query_uuid.full_name,
                        email=query_uuid.email,
                        phone_number=query_uuid.phone_number,
                        status=query_uuid.status,
                        total=query_uuid.total,
                        collection_id=query_uuid.collection_id,
                        payment_record_id=query_uuid.payment_record_id,
                        total_amount_with_fee=query_uuid.total_amount_with_fee,
                        transaction_fee=query_uuid.transaction_fee,
                        quantity=query_uuid.quantity,
                        account_id=query_uuid.account_id,
                        invoice_no=query_uuid.invoice_no,
                        customer_id=query_uuid.customer_id,
                        transaction_invoice_no=query_uuid.transaction_invoice_no,
                        invoice_status_id=query_uuid.invoice_status_id,
                        shipping_address_id=query_uuid.shipping_address_id,
                        invoice_status=query_uuid.invoice_status,
                        base_url=query_uuid.base_url,
                        checking_counter=query_uuid.checking_counter,
                        api_key=query_uuid.api_key,
                        product_listing=query_item,
                        id=query_uuid.id,
                        # collection=query_uuid.customer_bill_collection,
                        payment_record=query_uuid.customer_bill_payment_record
                    )
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=new_schema,
                        token=auth_token["access_uuid"])

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/update-bill", response_model=schemas.ResponseApiBase)
def public_update_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerCollectionBill = Body(..., example={
            "full_name": "string",
            "email": "string",
            "phone_number": "string",
            "total": 0,
            "collection_id": 0,
            "total_amount_with_fee": 0,
            "transaction_fee": 0,
            "quantity": 1,
            "customer_id": 0,
            "id": 0
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"update/customer-bills/{_in.id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="UPDATE_CUSTOMER_BILlS_ID_USING_AUTH",
                                  _data=_in.id, db_=db)

        query_update = db.query(models.CustomerBill) \
            .filter(models.CustomerBill.id == _in.id,
                    models.CustomerBill.account_id == auth_token["account_id"]).first()

        if query_update:
            if query_update.invoice_status_id == 2:
                return schemas.ResponseApiBase(
                    response_code=2111,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=f"{query_update.invoice_no}: {query_update.invoice_status}",
                    breakdown_errors=f"BILL_SUCCESS_CANT_EDIT",
                    token=auth_token["access_uuid"]
                )

            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/delete-bill", response_model=schemas.ResponseApiBase)
def public_delete_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"delete/customer-bills/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="UPDATE_CUSTOMER_BILlS_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_update = db.query(models.CustomerBill) \
            .filter(models.CustomerBill.id == _id,
                    models.CustomerBill.account_id == auth_token["account_id"]).first()

        if query_update:
            if query_update.invoice_status_id == 2:
                return schemas.ResponseApiBase(
                    response_code=2112,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=f"{query_update.invoice_no}: {query_update.invoice_status}",
                    breakdown_errors=f"BILL_SUCCESS_CANT_DELETE",
                    token=auth_token["access_uuid"]
                )

            _in = schemas.CustomerBillUpdate(
                record_status=4
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/payment-collection", response_model=schemas.ResponseApiBase)
def get_public_payment_collection_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search collection id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"/public-collections/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_COLLECTION_DETAILS_WITH_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_uuid = db.query(models.Collection).filter(models.Collection.id == _id,
                                                        models.Collection.account_id == auth_token[
                                                            "account_id"]).first()
        if query_uuid:
            query_uuid.base_url = str(request.base_url) + f"api/v1/collections/{query_uuid.uuid}/leanpay-bills"
            if query_uuid.collection_method == 2:
                new_schema = schemas.CollectionMethodView(
                    title=query_uuid.title,
                    base_url=query_uuid.base_url,
                    short_url=query_uuid.short_url,
                    min_amount=query_uuid.min_amount,
                    tax=query_uuid.tax,
                    discount=query_uuid.discount,
                    custom_field=query_uuid.custom_field,
                    description=query_uuid.description,
                    secondary_description=query_uuid.secondary_description,
                    footer_description=query_uuid.footer_description,
                    collection_method=query_uuid.collection_method,
                    record_status=query_uuid.record_status,
                    fixed_amount=query_uuid.fixed_amount,
                    allow_customers_to_enter_quantity=query_uuid.allow_customers_to_enter_quantity,
                    enable_quantity_limit=query_uuid.enable_quantity_limit,
                    quantity_limit=query_uuid.quantity_limit,
                    enable_total_amount_tracking=query_uuid.enable_total_amount_tracking,
                    enable_billing_address=query_uuid.enable_billing_address,
                    total_target_amount=query_uuid.total_target_amount,
                    disable_payment_when_target_reached=query_uuid.disable_payment_when_target_reached,
                    share_setting_id=query_uuid.share_setting_id,
                    success_field_id=query_uuid.success_field_id,
                    payment_setting_id=query_uuid.payment_setting_id,
                    account_id=query_uuid.account_id,
                    collection_share_setting=query_uuid.collection_share_setting,
                    collection_payment_setting=query_uuid.collection_payment_setting,
                    collection_success_field=query_uuid.collection_success_field,
                    collection_support_detail=query_uuid.collection_support_detail,
                    uuid=query_uuid.uuid
                )
                query_item = crud.collection_item.get_item_by_collection_id(db, _id=_id)
                new_schema.product_listing = query_item
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=new_schema,
                    token=auth_token["access_uuid"])

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/create-payment-collection", response_model=schemas.ResponseApiBase)
def create_public_payment_collections(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CollectionCreate = Body(..., example={
            "title": "string",
            "base_url": "string",
            "short_url": "string",
            "min_amount": 0,
            "tax": 0,
            "discount": 0,
            "custom_field": "string",
            "description": "string",
            "secondary_description": "string",
            "footer_description": "string",
            "collection_method": 0,
            "record_status": 0,
            "fixed_amount": True,
            "allow_customers_to_enter_quantity": True,
            "enable_quantity_limit": True,
            "quantity_limit": 0,
            "enable_total_amount_tracking": True,
            "enable_billing_address": True,
            "total_target_amount": 0,
            "disable_payment_when_target_reached": True,
            "show_seller_information_enable": False
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Create new collection.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="public/collections/create",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="CREATE_COLLECTION_PAYMENT_METHOD",
                                  _data=_in.json(), db_=db)

        query_get = crud.collection.get_by_name(db, title=_in.title)

        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=auth_token["access_uuid"]
            )

        _in.account_id = auth_token["account_id"]
        _in.uuid = "CL-AUTH-" + SharedUtils.my_random_string(10) + "-LNP"
        # check uuid and recreate if same
        query_uuid = crud.collection.get_by_uuid(db, uuid=_in.uuid)
        if query_uuid:
            while query_uuid.uuid == _in.uuid:
                _in.uuid = "CL-AUTH-" + SharedUtils.my_random_string(10) + "-LNP"

        share_setting = db.query(models.ShareSetting) \
            .filter(models.ShareSetting.account_id == auth_token["account_id"]) \
            .order_by(models.ShareSetting.created_at.desc()).first()
        _in.share_setting_id = share_setting.id

        success_field = db.query(models.SuccessField) \
            .filter(models.SuccessField.account_id == auth_token["account_id"]) \
            .order_by(models.SuccessField.created_at.desc()).first()
        _in.success_field_id = success_field.id

        payment_setting = db.query(models.PaymentSetting) \
            .filter(models.PaymentSetting.account_id == auth_token["account_id"]) \
            .order_by(models.PaymentSetting.created_at.desc()).first()
        _in.payment_setting_id = payment_setting.id

        support_detail = db.query(models.SupportDetail) \
            .filter(models.SupportDetail.account_id == auth_token["account_id"]) \
            .order_by(models.SupportDetail.created_at.desc()).first()
        _in.support_detail_id = support_detail.id

        # Check Create Limit for Collection in Account
        account_ = crud.account.get_account_by_id(db, id=auth_token["account_id"])
        subscription = crud.subscription.get_by_subscription_id(db=db, sub_id=account_.subscription_plan_id)

        subscription_ = Subscription(models.Collection, subscription, auth_token["account_id"])
        checker = subscription_.check_subscription_payment_form(db)
        if not checker:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="QUOTA_LIMIT_EXCEED",
                token=""
            )

        query_create = crud.collection.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=query_create,
            token=auth_token["access_uuid"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/update-payment-collection", response_model=schemas.ResponseApiBase)
def public_update_payment_collection(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.Collection = Body(..., example={
            'id': 1
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"update/customer-bills/{_in.id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="UPDATE_COLLECTIONS_CUSTOMER_BILlS_ID_USING_AUTH",
                                  _data=_in.id, db_=db)

        query_update = db.query(models.Collection) \
            .filter(models.Collection.id == _in.id,
                    models.Collection.account_id == auth_token["account_id"]).first()

        if query_update:
            if query_update.record_status == 2:
                return schemas.ResponseApiBase(
                    response_code=2111,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=f"",
                    breakdown_errors=f"COLLECTION_NOT_ACTIVE_CANT_EDIT",
                    token=auth_token["access_uuid"]
                )

            update_ = crud.collection.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/delete-payment-collection", response_model=schemas.ResponseApiBase)
def public_delete_payment_collections(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url=f"delete/collections/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="UPDATE_COLLECTIONS_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_update = db.query(models.Collection) \
            .filter(models.Collection.id == _id,
                    models.Collection.account_id == auth_token["account_id"]).first()

        if query_update:
            if query_update.record_status == 1:
                return schemas.ResponseApiBase(
                    response_code=1911,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=f"",
                    breakdown_errors=f"ACTIVE_COLLECTION_CANT_DELETE",
                    token=auth_token["access_uuid"]
                )

            _in = schemas.CollectionUpdate(
                record_status=settings.RECORD_DELETE
            )
            update_ = crud.collection.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/activate-payment-collection", response_model=schemas.ResponseApiBase)
def public_activate_payment_collections(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url=f"activate/collections/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="INACTIVE_COLLECTIONS_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_update = db.query(models.Collection) \
            .filter(models.Collection.id == _id,
                    models.Collection.account_id == auth_token["account_id"]).first()

        if query_update:
            if query_update.enable_quantity_limit:
                if query_update.quantity_limit == 0:
                    return schemas.ResponseApiBase(
                        response_code=2111,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=f"",
                        breakdown_errors=f"QUANTITY_LIMIT_ALREADY_ZERO",
                        token=auth_token["access_uuid"]
                    )

            _in = schemas.CollectionUpdate(
                record_status=settings.RECORD_ACTIVE
            )
            update_ = crud.collection.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.put("/public/deactivate-payment-collection", response_model=schemas.ResponseApiBase)
def public_deactivate_payment_collections(
        *,
        db: Session = Depends(deps.get_db),
        _id: int = Body(..., embed=True),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url=f"deactivate/collections/{_id}",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="INACTIVE_COLLECTIONS_ID_USING_AUTH",
                                  _data=_id, db_=db)

        query_update = db.query(models.Collection) \
            .filter(models.Collection.id == _id,
                    models.Collection.account_id == auth_token["account_id"]).first()

        if query_update:
            _in = schemas.CollectionUpdate(
                record_status=settings.RECORD_NOT_ACTIVE
            )
            update_ = crud.collection.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/collection-list/", response_model=schemas.ResponseApiBase)
def get_public__collection_list(
        *,
        db: Session = Depends(deps.get_db),
        skip: int, limit: int,
        _in: schemas.PayloadListQuery
        = Body(..., example={
            "start_date": "01-02-2022",
            "end_date": "27-02-2022",
            "record_status": 1,
            "search": {
                "search_enable": False,
                "search_key": "WEB_PAYMENT",
                "search_column": "payment_mode",
            },
            "sort": {
                "parameter_name": "created_at",
                "sort_type": "desc"
            }
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Retrieve all available customer based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/public/collection-list",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_COLLECTION_LIST_BASED_ON_AUTH_KEY",
                                  _data=_in.json(), db_=db)

        db_count = crud.collection.get_count_merchant(db=db,
                                                      _id=auth_token["account_id"], start_date=_in.start_date,
                                                      end_date=_in.end_date, record_status=_in.record_status)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.collection.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=auth_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list},
                token=auth_token["access_uuid"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list},
            token=auth_token["access_uuid"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/list-payout-services", response_model=Any)
def public_list_payout_services(
        db: Session = Depends(deps.get_db),
        auth_token=Depends(deps.get_token_header)
) -> Any:
    """
    API for getting the listing for payout services through payment switch
    """
    try:
        _account = crud.account.get_account_by_id(db, id=auth_token["account_id"])
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/ListOfPayoutServices"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'amount_to_calculate': '25',
                   'payment_model_reference_id': '1'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPublicPayoutResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    if x["best_rate"]["response_code"] == 2100:
                        data = schemas.SwitchPublicPayoutResponses(
                            payout_service_id=x["payout_service_id"],
                            payment_model_reference_id=x["payment_model_reference_id"],
                            payment_model_reference=x["payment_model_reference"],
                            unique_reference=x["unique_reference"],
                            payment_service=x["category_code"],
                            name=x["name"],
                            record_status=x["record_status"],
                            record_status_id=x["record_status_id"],
                            rate=_account.subscription_plan.payout_charges
                        )
                        services.append(data)
                # services = [x for x in services if x.payment_service == _in.payment_type.upper()
                #             and x.record_status == _in.payment_status.upper()]
                sorted_services = sorted(services, key=lambda z: z.payment_service)
                sorted_group = [{key: list(result)} for key, result in
                                groupby(sorted_services, key=lambda y: y.payment_service)]
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=sorted_group
                    ),
                    },
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/public/create-payout-invoice", response_model=Any)
def create_payout_invoice(*,
                          db: Session = Depends(deps.get_db),
                          auth_token=Depends(deps.get_token_header),
                          callback_in: schemas.SwitchCallbackPayoutResponse = Body(..., example=
                          {"client_identifier": "LEANPAY",
                           "virtual_pool_reference": "VA-0FC9DD-143418755516-PAYOUT",
                           "payout_service_id": "23",
                           "amount": "1.3",
                           "client_redirect_url": "https://stag-portal.leanpay.my/pay/thankyou?additional_message=xx&bill_id=xx&extra_param=leanpay-extraneous-param",
                           "client_callback_url": "https://stag-api.leanpay.my/api/v1/callback-url/call-invoice-payout-encrypt",
                           "third_party_account_no": "8011408168 for success // 1111111111 for failed",
                           "recipient_reference": "test",
                           "payment_description": "test payment description",
                           "client_data": ""
                           }),
                          background_task: BackgroundTasks
                          ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/create-payout-invoice",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PAYOUT_INVOICE",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        query_virtual = crud.virtual_account.get_by_virtual_account_unique_id(db,
                                                                              _id=callback_in.virtual_pool_reference)

        if not query_virtual:
            return schemas.ResponseApiBase(
                response_code=8522,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="VIRTUAL_POOL_INVALID",
                token=auth_token["access_uuid"]
            )

        merchant_payout_pool = callback_in.virtual_pool_reference
        callback_in.virtual_pool_reference = query_virtual.switch_payout_pool_id
        payout_invoice = f"PAYOUT-{SharedUtils.my_random_string(4)}:{datetime.today().strftime('%H%M%S')}-LEANX"
        callback_in.client_data = json.dumps({
            "virtual_pool_reference": payout_invoice
        })
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        create_payout_record = schemas.PayoutRecordCreate(
            switch_invoice_id=query_virtual.switch_payout_pool_id,
            merchant_invoice_id=query_virtual.merchant_payout_pool_id,
            payout_account_number=callback_in.third_party_account_no,
            transaction_invoice_no="NO_DATA",
            transaction_status="PENDING",
            value=callback_in.amount,
            payout_invoice_no=payout_invoice,
            account_id=auth_token['account_id'],
            api_key=auth_token['access_uuid'],
            data=callback_in.json()
        )

        crud.payout_record.create_self_date(db, obj_in=create_payout_record)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/CreatePayout"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                pool_payout = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL")
                transaction_type = "ADDITION"
                query_payout_pool = crud.pool.get_by_pool_account_type(db, _id=pool_payout,
                                                                       _account_id=auth_token["account_id"])
                create_audit_pool = schemas.AuditPoolCreate(
                    transaction_type=transaction_type,
                    previous_value=query_payout_pool.value,
                    current_value=query_payout_pool.value,
                    charges=callback_in.amount,
                    pool_id=query_payout_pool.id,
                    api_key=auth_token["access_uuid"],
                    account_id=auth_token["account_id"],
                    pool_type=pool_payout,
                    invoice_no=None,
                    invoice_status=settings.PENDING_PAYMENT,
                    description="INITIATE_PAYOUT_INVOICE"
                )
                crud.audit_pool.create_self_date(db, obj_in=create_audit_pool)

                b64_redirect_url = base64.b64encode(bytes(callback_in.client_redirect_url, "utf-8"))
                account = crud.account.get_account_by_id(db, auth_token["account_id"])
                redirect_url = f"{settings.PORTAL_BILL_URL}payout/invoice/{payout_invoice}?" \
                               f"fullname={account.name}&merchant_id={account.merchant_id}" \
                               f"&invoice_no={payout_invoice}" \
                               f"&redirect_url={b64_redirect_url.decode('utf-8')}&invoice_status=SUCCESS"

                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={
                        'status': "SUCCESS",
                        'Invoice': payout_invoice,
                        'redirect': redirect_url
                    },
                    breakdown_errors="",
                    token=auth_token["access_uuid"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=auth_token["access_uuid"]
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/payout-transaction-list/", response_model=schemas.ResponseApiBase)
def get_public_payout_transaction_list(
        *,
        db: Session = Depends(deps.get_db),
        skip: int, limit: int,
        _in: schemas.PayloadListQuery
        = Body(..., example={
            "start_date": "01-02-2022",
            "end_date": "27-02-2022",
            "record_status": 1,
            "search": {
                "search_enable": False,
                "search_key": "WEB_PAYMENT",
                "search_column": "payment_mode",
            },
            "sort": {
                "parameter_name": "created_at",
                "sort_type": "desc"
            }
        }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Retrieve all available customer based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/public/collection-list",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_COLLECTION_LIST_BASED_ON_AUTH_KEY",
                                  _data=_in.json(), db_=db)

        db_count = crud.payout_record.get_count_based_account(db=db,
                                                              _id=auth_token["account_id"], start_date=_in.start_date,
                                                              end_date=_in.end_date)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.payout_record.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=auth_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list},
                token=auth_token["access_uuid"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list},
            token=auth_token["access_uuid"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/get-payout-transaction-by-id", response_model=schemas.ResponseApiBase)
def get_public_payout_records_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: str = 1,
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search tier pricing id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/public/get-payout-transaction-by-id",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_PAYOUT_TRANSACTION_BASED_ON_ID",
                                  _data=_id, db_=db)

        query_uuid = crud.payout_record.get_by_account_payout_record_merchant_id(db, _id=_id,
                                                                                 account_id=auth_token["account_id"])
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                token=auth_token["access_uuid"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=auth_token["access_uuid"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/list-verification-channel", response_model=Any)
def public_list_verification_channel(
        *,
        db: Session = Depends(deps.get_db),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    API for getting the listing for bank verification
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/public/list-bank-verification-channel",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_LIST_VERIFICATION_CHANNEL",
                                  _data="NO_DATA", db_=db)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/ListOfPaymentChannels"
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder({'client_identifier': 'LEANPAY',
                                           'skip_encryption': 'false'})
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}

        files = {}
        headers = {}
        services: List[schemas.VerificationChannelBaseModel] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    data = schemas.VerificationChannelBaseModel(**x)
                    services.append(data)
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=services
                    )},
                    token=auth_token["access_uuid"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=auth_token["access_uuid"]
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/check-verification-bank", response_model=Any)
def create_verification_bank(
        *,
        db: Session = Depends(deps.get_db),
        callback_in: schemas.VerificationCheckBaseModel = Body(..., example=
        {"client_identifier": "LEANPAY",
         "verification_channel_id": 1,
         "third_party_account_no": "9011426016 or 1111111111"
         }),
        auth_token=Depends(deps.get_token_header),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    API for getting the listing for bank verification
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/public/check-verification-bank",
                                  _email=auth_token["access_uuid"],
                                  _account_id=auth_token["account_id"],
                                  _description="GET_LIST_VERIFICATION_CHANNEL",
                                  _data="NO_DATA", db_=db)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/AccountCreditorVerifier"
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder({'client_identifier': 'LEANPAY',
                                           'skip_encryption': 'false'})
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"]["output"],
                    breakdown_errors="",
                    token=auth_token["access_uuid"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=auth_token["access_uuid"]
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=auth_token["access_uuid"]
        )


@router.post("/public/bank-list-test", response_model=Any)
def public_list_payout_services(
        db: Session = Depends(deps.get_db),
        auth_token=Depends(deps.get_token_header)
) -> Any:
    """
    API for getting the listing for payout services through payment switch
    """
    try:
        _account = crud.account.get_account_by_id(db, id=auth_token["account_id"])
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/ListOfPayoutServices"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'amount_to_calculate': '25',
                   'payment_model_reference_id': '1'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPublicPayoutResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    if x["best_rate"]["response_code"] == 2100:
                        data = schemas.SwitchPublicPayoutResponses(
                            payout_service_id=x["payout_service_id"],
                            payment_model_reference_id=x["payment_model_reference_id"],
                            payment_model_reference=x["payment_model_reference"],
                            unique_reference=x["unique_reference"],
                            payment_service=x["category_code"],
                            name=x["name"],
                            record_status=x["record_status"],
                            record_status_id=x["record_status_id"],
                            rate=_account.subscription_plan.payout_charges
                        )
                        services.append(data)
                # services = [x for x in services if x.payment_service == _in.payment_type.upper()
                #             and x.record_status == _in.payment_status.upper()]
                # sorted_services = sorted(services, key=lambda z: z.payment_service)
                # sorted_group = [{key: list(result)} for key, result in
                #                 groupby(sorted_services, key=lambda y: y.payment_service)]
                output = [{'name': item.name, 'id': item.payout_service_id,
                           'unique_reference': item.unique_reference} for item in services]
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=output
                    ),
                    },
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
