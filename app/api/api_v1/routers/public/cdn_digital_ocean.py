import asyncio
import json
from datetime import datetime
from io import BytesIO
from typing import Any, List

import boto3

from app import schemas
from fastapi import APIRouter, Body, Depends, BackgroundTasks, Request
from app.api import deps
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.Utils.shared_utils import SharedUtils
from app.core.config import settings
import app.Utils.web_cdn_do as wcd
from fastapi import File, UploadFile

router = APIRouter(prefix="/cdn-digital-ocean", tags=["cdn-digital-ocean"])


@router.post("/upload-cdn-do", response_model=schemas.ResponseApiBase)
def upload_cdn_do(
        *,
        db: Session = Depends(deps.get_db),
        file: UploadFile = File(...),
        background_tasks: BackgroundTasks
) -> Any:
    """
    upload cdn do.
    """
    try:
        ACCESS_ID = 'DO00D9MUBJAH3RLL3EEF'
        SECRET_KEY = 'kGXcmSWQ9o+XKss8XJfOI7ct7w4nwlqkrIsyb7GxvPw'

        client = wcd.get_spaces_client(
            region_name="sgp1",
            endpoint_url="https://sgp1.digitaloceanspaces.com",
            key_id=ACCESS_ID,
            secret_access_key=SECRET_KEY
        )
        if file.content_type in ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff',
                                 'image/x-xbitmap', 'application/vnd.ms-excel',
                                 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                 'text/csv', 'application/pdf', 'application/x-pdf']:
            generated_filename = f'{SharedUtils.my_random_string(6)}_{file.filename}'
            wcd.upload_file_to_space(spaces_client=client, space_name="lean", file_src=file.file,
                                     save_as=generated_filename,
                                     is_public=True, content_type=file.content_type)

            if client:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=f"https://lean.sgp1.digitaloceanspaces.com/{generated_filename}",
                    breakdown_errors="",
                    token=""
                )
        else:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="TOKEN_NOT_VALID",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/bulk-upload-cdn-do", response_model=schemas.ResponseApiBase)
def bulk_upload_cdn_do(
        *,
        db: Session = Depends(deps.get_db),
        files: List[UploadFile] = File(...),
        background_tasks: BackgroundTasks
) -> Any:
    """
    upload cdn do.
    """
    try:
        ACCESS_ID = 'DO00D9MUBJAH3RLL3EEF'
        SECRET_KEY = 'kGXcmSWQ9o+XKss8XJfOI7ct7w4nwlqkrIsyb7GxvPw'

        client = wcd.get_spaces_client(
            region_name="sgp1",
            endpoint_url="https://sgp1.digitaloceanspaces.com",
            key_id=ACCESS_ID,
            secret_access_key=SECRET_KEY
        )
        if client:
            data = []
            for file in files:
                if file.content_type in ['image/jpeg', 'image/png', 'image/bmp', 'image/gif', 'image/tiff',
                                         'image/x-xbitmap']:
                    generated_filename = f'{SharedUtils.my_random_string(6)}_{file.filename}'
                    wcd.upload_file_to_space(spaces_client=client, space_name="lean", file_src=file.file,
                                             save_as=generated_filename,
                                             is_public=True, content_type=file.content_type)

                    data.append(f"https://lean.sgp1.digitaloceanspaces.com/{generated_filename}")
                else:
                    data.append(f'{file.filename.upper()}_WRONG_FORMAT_FILE')

            with open('upload_image.txt', 'a') as f:
                f.write('\n'.join(data))

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=data,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="TOKEN_NOT_VALID",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
