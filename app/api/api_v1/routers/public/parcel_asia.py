import json
from datetime import datetime
from typing import Any, Dict
from sqlalchemy.orm import Session
import requests
from app import schemas
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from fastapi.encoders import jsonable_encoder
from app.core.config import settings
from app.Utils.web_request import http_post_with_aiohttp, parcel_asia_with_aiohttp
from aiohttp import ClientSession
from hashlib import md5
from app.constants.system_constant import SystemConstant
from app import models, schemas, crud
from app.api import deps

router = APIRouter(prefix="/parcel-asia", tags=["parcel-asia-services"])


@router.post("/verify-account", response_model=schemas.ResponseApiBase)
async def parcel_verified_account(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for verify parcel asia
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}user'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/postcode-details", response_model=schemas.ResponseApiBase)
async def parcel_postcode_details(
        *,
        _in: schemas.PostcodeModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_postcode_details'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/price-check", response_model=schemas.ResponseApiBase)
async def parcel_price_check(
        *,
        _in: schemas.CheckPriceModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}check_price'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/ssd-price-check", response_model=schemas.ResponseApiBase)
async def same_day_price_check(
        *,
        _in: schemas.SddPriceModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}sdd_price'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/parcel-size", response_model=schemas.ResponseApiBase)
async def parcel_size(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_parcel_sizes'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/content-type", response_model=schemas.ResponseApiBase)
async def content_type(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for getting the listing of content type
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_content_types'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/create-shipment", response_model=schemas.ResponseApiBase)
async def create_shipment(
        *,
        _in: schemas.CreateShipmentModel
) -> Any:
    """
    API to create shipment
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}create_shipment'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/cart-items", response_model=schemas.ResponseApiBase)
async def cart_items(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_cart_items'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/parcel-checkout", response_model=schemas.ResponseApiBase)
async def parcel_checkout(
        *,
        _in: schemas.CheckOutModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}checkout'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/shipment-status", response_model=schemas.ResponseApiBase)
async def shipment_status(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_shipment_statuses'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/get-shipments", response_model=schemas.ResponseApiBase)
async def get_shipments(
        *,
        _in: schemas.CheckOutModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_shipments'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/shipment-history", response_model=schemas.ResponseApiBase)
async def shipment_history(
        *,
        _in: schemas.ParcelBaseModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_shipment_history'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/consignment-note", response_model=schemas.ResponseApiBase)
async def get_consignment_note(
        *,
        _in: schemas.ConsignmentNoteModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}get_shipment_history'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/check-price-bulk", response_model=schemas.ResponseApiBase)
async def check_price_bulk(
        *,
        _in: schemas.CheckPriceBulkModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}check_price_bulk'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/parcel-trace", response_model=schemas.ResponseApiBase)
async def parcel_trace(
        *,
        _in: schemas.TraceModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}trace'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/create-bulk-awb", response_model=schemas.ResponseApiBase)
async def create_bulk_awb(
        *,
        _in: schemas.BulkCreateAwbModel
) -> Any:
    """
    API for getting the listing for postcode details
    """
    try:
        if _in:
            urls = f'{settings.PARCEL_ASIA_DEMO_URI}create_bulk_awb'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()
            status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                          url=urls,
                                                                          payload=jsonable_encoder(_in))

            parcel_status, parcel_response, parcel_content = await parcel_asia_with_aiohttp(session=session,
                                                                                            headers=headers1,
                                                                                            url=url,
                                                                                            payload=response)
            await session.close()

            if parcel_status == requests.codes.ok:
                if parcel_response["data"]["output"] == response["hash"]:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[parcel_response, response["hash"]],
                        breakdown_errors="",
                        token=""
                    )
                else:
                    return schemas.ResponseApiBase(
                        response_code=5899,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data=[response, parcel_response["data"]["output"]],
                        breakdown_errors="INVALID_HASH",
                        token=""
                    )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant/create-shipping-payment/{_id}", response_model=schemas.ResponseApiBase)
async def create_shipment(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CreateShipmentModel
) -> Any:
    """
    API to create shipment
    """
    try:
        if _in:
            shipment_urls = f'{settings.PARCEL_ASIA_DEMO_URI}create_shipment'
            checkout_urls = f'{settings.PARCEL_ASIA_DEMO_URI}checkout'
            url = f"{settings.SWITCH_DEV_URL}api/v1/env/mobile/superadmin/test/createCheckSumParcelAsia"
            headers = {}
            headers1 = {
                'Content-Type': 'application/json'
            }
            session = ClientSession()

            # create shipment

            shipment_status_code, shipment_response, shipment_content = await http_post_with_aiohttp(session=session,
                                                                                                     headers=headers,
                                                                                                     url=shipment_urls,
                                                                                                     payload=jsonable_encoder(
                                                                                                         _in))

            if shipment_status_code == requests.codes.ok:
                if shipment_response['status']:
                    shipment_key = shipment_response['data']['shipment_key']
                    new_checkout = schemas.CheckOutModel(
                        api_key=settings.PARCEL_ASIA_DEMO_API_KEY,
                        shipment_keys=[shipment_key]
                    )

                    checkout_status_code, checkout_response, checkout_content = \
                        await http_post_with_aiohttp(session=session,
                                                     headers=headers,
                                                     url=checkout_urls,
                                                     payload=jsonable_encoder(
                                                         _in))
                    await session.close()

                    if checkout_status_code == requests.codes.ok:
                        data = checkout_response['data']['shipments']
                        _shipment = schemas.ShipmentResponse(**data[f'{shipment_key}'])
                        shipment_data = crud.shipment.get_by_shipment_id(db, _id=_id)
                        update_shipment_data = schemas.ShipmentCreate(
                            shipment_status=SystemConstant.SYSTEM_CONS.get("shipment_status", None).__getitem__(
                                f'{_shipment.shipment_status}'),
                            shipping_infos=json.dumps(_shipment.dict()),
                            parcel_shipment_key=shipment_key,
                            parcel_tracking_no=_shipment.tracking_no

                        )
                        crud.shipment.update_self_date(db, db_obj=shipment_data, obj_in=update_shipment_data)
                    else:
                        return schemas.ResponseApiBase(
                            response_code=8674,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.today(),
                            data="ERROR_ON_CHECKOUT_SHIPMENT",
                            breakdown_errors=checkout_response,
                            token=""
                        )
                else:
                    return schemas.ResponseApiBase(
                        response_code=8674,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.today(),
                        data="ERROR_ON_CREATE_SHIPMENT",
                        breakdown_errors=shipment_response['message'],
                        token=""
                    )
            else:
                return schemas.ResponseApiBase(
                    response_code=8651,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data="ERROR_ON_CREATING_SHIPMENT",
                    breakdown_errors=shipment_response,
                    token=""
                )

        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="BODY_EMPTY_FIELD",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
