import asyncio
import json
from datetime import datetime
from typing import Any
from app import schemas
from fastapi import APIRouter, Body, Depends, BackgroundTasks, Request
from app.api import deps
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.Utils.shared_utils import SharedUtils
from app.core.config import settings

router = APIRouter(prefix="/twilio-services", tags=["twilio-services"])


@router.post("/twilio-sms-callback", response_model=schemas.ResponseApiBase)
def twilio_sms_callback(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.TwilioResponse,
        requests: Request,
        background_tasks: BackgroundTasks
) -> Any:
    """
    validate user.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/twilio-services", _email="twilio",
                                  _account_id=99999, _description="SMS_CALLBACK_FOR_TWILIO",
                                  _data=json.dumps(requests.body()), db_=db)
        if _in.status:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=_in,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="TOKEN_NOT_VALID",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/twilio-whatsapp-callback", response_model=Any)
def twilio_whatsapp_callback(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.TwilioResponse,
        auth_token=Depends(deps.get_token_header)
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if _in.status:

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sms", response_model=schemas.ResponseApiBase)
async def twilio_sms(
        *,
        db: Session = Depends(deps.get_db),
        phone_number: str = Body(..., embed=True),
        message_body: str = Body(..., embed=True)
) -> Any:
    """
    send sms with twilio.
    """
    try:
        if phone_number:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_sms, phone_number, message_body)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=schemas.TwilioResponse(
                    account_sid=message.account_sid,
                    api_version=message.api_version,
                    body=message.body,
                    date_created=message.date_created,
                    date_sent=message.date_sent,
                    date_updated=message.date_updated,
                    direction=message.direction,
                    error_code=message.error_code,
                    error_message=message.error_message,
                    from_=message.from_,
                    messaging_service_sid=message.messaging_service_sid,
                    num_media=message.num_media,
                    num_segments=message.num_segments,
                    price=message.price,
                    price_unit=message.price_unit,
                    sid=message.sid,
                    status=message.status,
                    subresource_uris=message.subresource_uris,
                    to=message.to,
                    uri=message.uri,
                ),
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="TOKEN_NOT_VALID",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-whatsapp", response_model=Any)
async def twilio_whatsapp(
        *,
        db: Session = Depends(deps.get_db),
        phone_number: str = Body(..., embed=True)
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if phone_number:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_whatsapp_sms, phone_number, 'Hello from FastAPI!')
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=schemas.TwilioResponse(
                    account_sid=message.account_sid,
                    api_version=message.api_version,
                    body=message.body,
                    date_created=message.date_created,
                    date_sent=message.date_sent,
                    date_updated=message.date_updated,
                    direction=message.direction,
                    error_code=message.error_code,
                    error_message=message.error_message,
                    from_=message.from_,
                    messaging_service_sid=message.messaging_service_sid,
                    num_media=message.num_media,
                    num_segments=message.num_segments,
                    price=message.price,
                    price_unit=message.price_unit,
                    sid=message.sid,
                    status=message.status,
                    subresource_uris=message.subresource_uris,
                    to=message.to,
                    uri=message.uri,
                ),
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-email", response_model=Any)
async def twilio_sendgrid_email(
        *,
        _in: schemas.TwilioSendEmail,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if _in.from_:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_sendgrid_email, _in.from_, _in.to_,
                _in.bcc_, _in.subject, _in.body, _in.template_id, _in.template_enable)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-verification-email", response_model=Any)
async def twilio_sendgrid_verification_email(
        *,
        from_: str = Body(..., embed=True),
        to_: str = Body(..., embed=True),
        bcc_: str = Body(..., embed=True),
        verif_: str = Body(..., embed=True),
        name_: str = Body(..., embed=True),
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if from_:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_verification_sendgrid_email, from_, to_, bcc_, verif_, name_)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-forgot-password", response_model=Any)
async def twilio_sendgrid_password_email(
        *,
        from_: str = Body(..., embed=True),
        to_: str = Body(..., embed=True),
        bcc_: str = Body(..., embed=True),
        verif_: str = Body(..., embed=True)
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if from_:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_reset_password_sendgrid_email, from_, to_, bcc_, verif_)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-reset-password", response_model=Any)
async def twilio_sendgrid_reset_password_email(
        *,
        _in: schemas.TwilioResetPassword
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if _in:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_reset_password_sendgrid_email, _in.from_, _in.to_, _in.bcc_, _in.json())
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-purchased-receipt", response_model=Any)
async def twilio_sendgrid_purchased_receipt(
        *,
        from_: str = Body(..., embed=True),
        to_: str = Body(..., embed=True),
        bcc_: str = Body(..., embed=True),
        new_password: str = Body(..., embed=True)
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        if from_:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_reset_password_sendgrid_email, from_, to_, bcc_, new_password)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-payment-invoice", response_model=Any)
async def twilio_sendgrid_payment_invoice(
        *,
        _in: schemas.TwilioPrefund
) -> Any:
    """
    API for sending payment receipt to customer
    """
    try:
        if _in:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_payment_invoice_sendgrid_email, _in.from_, _in.to_, _in.bcc_, _in.json())
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-add-fund-transaction", response_model=Any)
async def twilio_sendgrid_add_fund_transaction(
        *,
        _in: schemas.TwilioPrefund
) -> Any:
    """
    API for sending payment receipt to customer
    """
    try:
        if _in:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_fund_notification_sendgrid_email, _in.from_, _in.to_, _in.bcc_, _in.json())
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-prefund-transaction", response_model=Any)
async def twilio_sendgrid_prefund_transaction(
        *,
        _in: schemas.TwilioPrefund
) -> Any:
    """
    API for sending payment receipt to customer
    """
    try:
        if _in:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_prefund_notification_sendgrid_email, _in.from_, _in.to_, _in.bcc_, _in.json())
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/twilio-sendgrid-payout-transaction", response_model=Any)
async def twilio_sendgrid_payout_transaction(
        *,
        _in: schemas.TwilioPrefund
) -> Any:
    """
    API for sending payment receipt to customer
    """
    try:
        if _in:
            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_payout_notification_sendgrid_email, _in.from_, _in.to_, _in.bcc_, _in.json())
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=message,
                breakdown_errors="",
                token=""
            )
        else:
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="URL_RETURN_ERROR",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
