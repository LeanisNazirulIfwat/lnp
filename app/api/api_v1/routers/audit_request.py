import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, Request, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample

router = APIRouter(prefix="/audit-request", tags=["audit-request"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_audit_request(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available audit request.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/audit-request", _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]), _description="GET LIST OF AUDIT REQUEST",
                                  _data="NO_DATA", db_=db)

        db_count = len(crud.audit_request.get_multi(db, skip=skip, limit=limit))
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.audit_request.get_multi_search_account(db=db, skip=skip, limit=limit,
                                                                _id=new_token["account_id"],
                                                                record_status=_in.record_status,
                                                                start_date=_in.start_date,
                                                                end_date=_in.end_date,
                                                                search_column=_in.search.search_column,
                                                                search_key=_in.search.search_key,
                                                                parameter_name=_in.sort.parameter_name,
                                                                sort_type=_in.sort.sort_type,
                                                                search_enable=_in.search.search_enable,
                                                                invoice_status=_in.invoice_status,
                                                                table_name="audit_requests",
                                                                role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_audit_request(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.AuditRequestCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/audit-request", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="CREATE AUDIT REQUEST",
                                  _data=_in.json(), db_=db)

        audit_request = crud.audit_request.get_by_name(db, email_=_in.email)
        if audit_request:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=audit_request,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        audit_request = crud.audit_request.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=audit_request,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_audit_request(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.AuditRequestUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/audit-request/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="UPDATE AUDIT REQUEST",
                                  _data=_in.json(), db_=db)
        audit_request = crud.audit_request.get_by_api_id(db, _id=_id)
        if audit_request:
            update_audit = crud.audit_request.update_self_date(db, db_obj=audit_request, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_audit,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=audit_request,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_api(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/audit-request/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="DELETE RECORD OF AUDIT REQUEST",
                                  _data="NO_DATA", db_=db)
        audit_request = crud.audit_request.get_by_api_id(db, _id=_id)
        _in = schemas.Api(
            id=_id,
            record_status=4,
        )
        if audit_request:
            update_audit = crud.audit_request.update(db, db_obj=audit_request, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_audit,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=audit_request,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
