from datetime import timedelta, datetime
from typing import Any

from app import models, schemas, crud
from app.api import deps
from app.core import security
from app.core.config import settings
from fastapi import APIRouter, Body, Depends, HTTPException, BackgroundTasks
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

router = APIRouter(prefix="/auth", tags=["auth"])


@router.post("/access-token", response_model=schemas.Token)
def login_access_token(
        db: Session = Depends(deps.get_db),
        form_data: OAuth2PasswordRequestForm = Depends(),
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = crud.user.authenticate(
        db, email=form_data.username, password=form_data.password
    )
    if not user:
        raise HTTPException(
            status_code=10144, detail="INVALID_EMAIL_OR_PASSWORD"
        )
    elif not crud.user.is_active(user):
        raise HTTPException(status_code=10216, detail="INACTIVE_USER")
    access_token_expires = timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )
    if not user.user_role:
        role = "GUEST"
    else:
        role = user.user_role.role.name
    token_payload = {
        "id": str(user.id),
        "role": role,

    }
    return {
        "access_token": security.create_access_token(
            token_payload, expires_delta=access_token_expires
        ),
        "token_type": "bearer",
    }


@router.post("/access-token-viu", response_model=schemas.ResponseApiBase)
def login_access_token_viu(
        *,
        db: Session = Depends(deps.get_db),
        form_data: OAuth2PasswordRequestForm = Depends(),
        background_tasks: BackgroundTasks
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests for viu
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/auth/access-token-viu",
                                  _email="NO_DATA",
                                  _account_id=999999, _description="LOGIN PROCESS",
                                  _data="NO_DATA", db_=db)

        user = crud.user.authenticate(
            db, email=form_data.username, password=form_data.password
        )
        if not user:
            return schemas.ResponseApiBase(
                response_code=9000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INVALID_EMAIL_OR_PASSWORD",
                token=""
            )
        elif not crud.user.is_active(user):
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INACTIVE_USER",
                token=""
            )
        access_token_expires = timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
        if not user.user_role:
            role = "GUEST"
        else:
            role = user.user_role.role.name
        token_payload = {
            "id": str(user.id),
            "role": role,

        }

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "access_token": security.create_access_token(
                    token_payload, expires_delta=access_token_expires
                ),
                "token_type": "bearer",
            },
            token="")

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant-access-token", response_model=schemas.ResponseApiBase)
def merchant_login_access_token(
        *,
        db: Session = Depends(deps.get_db),
        form_data: OAuth2PasswordRequestForm = Depends(),
        background_tasks: BackgroundTasks
) -> Any:
    """
    merchant OAuth2 compatible token login, get an access token for future requests
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/auth/merchant-access-token",
                                  _email="NO_DATA",
                                  _account_id=999999, _description="DELETE AN API",
                                  _data="NO_DATA", db_=db)
        merchant_user = crud.merchant_user.authenticate(
            db, email=form_data.username, password=form_data.password
        )
        if not merchant_user:
            return schemas.ResponseApiBase(
                response_code=9000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INCORRECT_EMAIL_OR_PASSWORD",
                token=""
            )
        elif not crud.user.is_active(merchant_user):
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INACTIVE_USER",
                token=""
            )
        access_token_expires = timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
        if not merchant_user.user_role:
            role = "GUEST"
        else:
            role = merchant_user.user_role.role.name
        token_payload = {
            "id": str(merchant_user.id),
            "role": role,
        }
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "access_token": security.create_access_token(
                    token_payload, expires_delta=access_token_expires
                ),
                "token_type": "bearer",
            },
            token="")

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/white-label-access-token", response_model=schemas.ResponseApiBase)
def white_label_login_access_token(
        *,
        db: Session = Depends(deps.get_db),
        form_data: OAuth2PasswordRequestForm = Depends(),
        background_tasks: BackgroundTasks
) -> Any:
    """
    white label OAuth2 compatible token login, get an access token for future requests
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/auth/white-label-access",
                                  _email="NO_DATA",
                                  _account_id=1111111, _description="WHITE LABEL TOKEN",
                                  _data="TOKEN", db_=db)
        white_label_user = crud.white_label_user.authenticate(
            db, email=form_data.username, password=form_data.password
        )
        if not white_label_user:
            return schemas.ResponseApiBase(
                response_code=9000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INCORRECT_EMAIL_OR_PASSWORD",
                token=""
            )
        elif not crud.user.is_active(white_label_user):
            return schemas.ResponseApiBase(
                response_code=9100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="INACTIVE_USER",
                token=""
            )
        access_token_expires = timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
        if not white_label_user.user_role:
            role = "GUEST"
        else:
            role = white_label_user.user_role.role.name
        token_payload = {
            "id": str(white_label_user.id),
            "role": role,
        }
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "access_token": security.create_access_token(
                    token_payload, expires_delta=access_token_expires
                ),
                "token_type": "bearer",
            },
            token="")

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/test-token", response_model=schemas.ResponseApiBase)
def test_token(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Depends(deps.get_current_user),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Test access token
    """
    background_tasks.add_task(deps.write_audit, _method="POST", _url="/auth/test-token",
                              _email="NO_DATA",
                              _account_id=999999, _description="TEST JWT TOKEN ACCESS",
                              _data="NO_DATA", db_=db)
    return schemas.ResponseApiBase(
        response_code=2000,
        description='SUCCESS',
        app_version=settings.API_V1_STR,
        talk_to_server_before=datetime.today(),
        data=current_user,
        token=new_token["token"])


@router.post("/hash-password", response_model=schemas.ResponseApiBase)
def hash_password(password: str = Body(..., embed=True), ) -> Any:
    """
    Hash a password
    """
    return schemas.ResponseApiBase(
        response_code=2000,
        description='SUCCESS',
        app_version=settings.API_V1_STR,
        talk_to_server_before=datetime.today(),
        data={"hash_password": security.get_password_hash(password)},
        token="")
