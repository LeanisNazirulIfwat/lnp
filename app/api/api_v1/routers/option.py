import datetime
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample

router = APIRouter(prefix="/system-options", tags=["system-options"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_system_options(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available system options.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/system-options/admin/list",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SYSTEM_OPTIONS_LIST",
                                  _data="NO_DATA", db_=db)

        db_count = len(crud.system_option.get_multi(db, skip=skip, limit=limit))
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.system_option.get_multi_search_user(db=db, skip=skip, limit=limit, _id=current_user.id,
                                                             record_status=_in.record_status,
                                                             start_date=_in.start_date,
                                                             end_date=_in.end_date,
                                                             search_column=_in.search.search_column,
                                                             search_key=_in.search.search_key,
                                                             parameter_name=_in.sort.parameter_name,
                                                             sort_type=_in.sort.sort_type,
                                                             search_enable=_in.search.search_enable,
                                                             invoice_status=_in.invoice_status,
                                                             table_name="system_options",
                                                             role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_system_options(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.SystemOptionCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new system-options.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/system-options/create",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SYSTEM_OPTION_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_get = crud.system_option.get_by_name(db, _name=_in.company_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token["account_id"]
        query_create = crud.system_option.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_system_option(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.SystemOptionUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update system options.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/system-options/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_SYSTEM_OPTIONS",
                                  _data="NO_DATA", db_=db)

        query_update = crud.system_option.get_by_system_option_id(db, _id=_id)
        if query_update:
            update_ = crud.system_option.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_system_options(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete system option.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/system-options/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_SYSTEM_OPTION_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.system_option.get_by_system_option_id(db, _id=_id)
        _in = schemas.SystemOption(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.system_option.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/merchant-system-options", response_model=schemas.ResponseApiBase)
def get_merchant_system_option(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available system  option based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/system-options/merchant-system-options",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_LIST_OF_MERCHANT_SYSTEM_OPTION",
                                  _data="NO_DATA", db_=db)

        list_data = crud.system_option.get_multi_merchant(db, skip=skip, limit=limit, _id=new_token["account_id"])
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_system_option_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search system option id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/system-options/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SYSTEM_OPTION_WITH_ID",
                                  _data=_id, db_=db)

        query_uuid = crud.system_option.get_by_system_option_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/admin/me", response_model=schemas.ResponseApiBase)
def get_system_option_me(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    get system option for current login.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/system-options/me",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SYSTEM_OPTION_ME",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.system_option.get_by_system_own_id(db, _id=current_user.api_key)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
