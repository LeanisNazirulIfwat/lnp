import datetime
from typing import Any
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Request, Body
from sqlalchemy.orm import Session
from app.Utils.shared_utils import SharedUtils
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
from sqlalchemy.ext.serializer import loads, dumps

router = APIRouter(prefix="/customer-bills", tags=["customer-bills"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_customer_bills(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer bills.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customer-bills/list",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILlS_LIST",
                                  _data="NO_DATA", db_=db)
        list_data = crud.customer_bill.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerBillCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new customer bills.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/customer-bills/create",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_CUSTOMER_BILlS",
                                  _data=_in.json(), db_=db)

        query_get = crud.customer_bill.get_by_name(db, full_name=_in.full_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token["account_id"]
        query_create = crud.customer_bill.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CustomerBill,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customer-bills/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_CUSTOMER_BILlS",
                                  _data=_in.json(), db_=db)

        query_update = crud.customer_bill.get_by_customer_bill_id(db, _id=_id)
        if query_update:
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        api_in: schemas.CustomerBill,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/customer-bills/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_CUSTOMER_BILLS_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.customer_bill.get_by_customer_bill_id(db, _id=_id)
        _in = schemas.CustomerBill(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-customer-bill", response_model=schemas.ResponseApiBase)
def get_merchant_customer_bills(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer bill based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-customer-bills/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILLS_MERCHANT_LIST",
                                  _data=_in.json(), db_=db)

        db_count = crud.customer_bill.get_count(db=db, _id=new_token["account_id"])
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.customer_bill.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        if _in.invoice_status:
            list_data = [x for x in list_data if x.invoice_status == _in.invoice_status.upper()]

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable and not _in.invoice_status:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/switch/create-payment-bill-direct", response_model=schemas.ResponseApiBase)
def create_customer_bills_backend(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerBillCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Create new customer bill backend direct.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST   ",
                                  _url="/customer-bills/switch/create-payment-bill-direct",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="SWITCH_CREATE_NEW_CUSTOMER_DIRECTLY",
                                  _data=_in.json(), db_=db)
        # create customer id
        query_get_customer = crud.customer.get_by_name(db, full_name=_in.email)
        if not query_get_customer:
            customer_create = schemas.CustomerCreate(
                account_id=new_token["account_id"],
                full_name=_in.full_name,
                email=_in.email,
                phone_number=_in.phone_number,
                # address=_in.address
            )

            # add customer to db
            new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
            _in.customer_id = new_customer.id
        else:
            _in.customer_id = query_get_customer.id

        # generate invoice
        _in.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
        # create payment record
        sub = crud.account.get_account_by_id(db, new_token["account_id"])
        payment_record_create = schemas.PaymentRecordCreate(
            amount=_in.total,
            transaction_fee=sub.subscription_plan.fpx_charges,
            net_amount=_in.total + sub.subscription_plan.fpx_charges
        )

        new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
        _in.payment_record_id = new_payment_record.id
        _in.account_id = new_token["account_id"]
        _in.base_url = str(request.base_url) + f"api/v1/customer-bills/{_in.invoice_no}/leanpay-bills"
        query_create = crud.customer_bill.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=6320,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=e.__class__,
            breakdown_errors="SOMETHING WHEN WRONG",
            token=new_token["token"]
        )


@router.get("/switch/{_invoice}", response_model=schemas.ResponseApiBase)
def get_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        _invoice: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    get customer bill based on the invoice
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/customers-bill/switch/{_invoice}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_CUSTOMER_BILL_BASED_ON_INVOICE",
                                  _data=_invoice, db_=db)
        query_get = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=_invoice)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors=f"Blog with invoice number {_invoice} not found",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_customer_bill_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        request: Request,
        background_tasks: BackgroundTasks
) -> Any:
    """
    search customer bill id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customers-bill/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_CUSTOMER_BILL_WITH_ID",
                                  _data="NO_DATA", db_=db)

        account = crud.account.get_account_details_by_id(db, id=new_token["account_id"])
        account = loads(dumps(account))

        query_uuid = crud.customer_bill.get_by_customer_bill_id(db, _id=_id)
        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=4561,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_NOT_VALID",
                token=""
            )
        query_uuid.base_url = str(request.base_url) + f"api/v1/customer-bills/{query_uuid.invoice_no}/leanpay-bills"
        if query_uuid:
            query_collection = crud.collection.get_by_collection_id(db, _id=query_uuid.collection_id)
            if query_collection.collection_method == 2:
                query_item = crud.customer_bill_item.get_by_customer_bill_id(db, _id=query_uuid.id)
                if query_item:
                    new_schema = schemas.CustomerBillMethodView(
                        full_name=query_uuid.full_name,
                        email=query_uuid.email,
                        phone_number=query_uuid.phone_number,
                        status=query_uuid.status,
                        total=query_uuid.total,
                        collection_id=query_uuid.collection_id,
                        payment_record_id=query_uuid.payment_record_id,
                        total_amount_with_fee=query_uuid.total_amount_with_fee,
                        transaction_fee=query_uuid.transaction_fee,
                        quantity=query_uuid.quantity,
                        account_id=query_uuid.account_id,
                        invoice_no=query_uuid.invoice_no,
                        customer_id=query_uuid.customer_id,
                        transaction_invoice_no=query_uuid.transaction_invoice_no,
                        invoice_status_id=query_uuid.invoice_status_id,
                        shipping_address_id=query_uuid.shipping_address_id,
                        invoice_status=query_uuid.invoice_status,
                        base_url=query_uuid.base_url,
                        checking_counter=query_uuid.checking_counter,
                        api_key=query_uuid.api_key,
                        product_listing=query_item,
                        id=query_uuid.id,
                        collection=schemas.Collection(**jsonable_encoder(query_uuid.customer_bill_collection)),
                        payment_record=query_uuid.customer_bill_payment_record,
                        customer_shipping_address=query_uuid.customer_bill_shipping_address,
                        customer_bill_account=schemas.Account(**jsonable_encoder(account))
                    )
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=new_schema,
                        token=new_token["token"])

            new_schema = schemas.CustomerBillMethodView(
                full_name=query_uuid.full_name,
                email=query_uuid.email,
                phone_number=query_uuid.phone_number,
                status=query_uuid.status,
                total=query_uuid.total,
                collection_id=query_uuid.collection_id,
                payment_record_id=query_uuid.payment_record_id,
                total_amount_with_fee=query_uuid.total_amount_with_fee,
                transaction_fee=query_uuid.transaction_fee,
                quantity=query_uuid.quantity,
                account_id=query_uuid.account_id,
                invoice_no=query_uuid.invoice_no,
                customer_id=query_uuid.customer_id,
                transaction_invoice_no=query_uuid.transaction_invoice_no,
                invoice_status_id=query_uuid.invoice_status_id,
                shipping_address_id=query_uuid.shipping_address_id,
                invoice_status=query_uuid.invoice_status,
                base_url=query_uuid.base_url,
                checking_counter=query_uuid.checking_counter,
                api_key=query_uuid.api_key,
                id=query_uuid.id,
                collection=schemas.Collection(**jsonable_encoder(query_uuid.customer_bill_collection)),
                payment_record=query_uuid.customer_bill_payment_record,
                customer_shipping_address=query_uuid.customer_bill_shipping_address,
                customer_bill_account=schemas.Account(**jsonable_encoder(account))
            )

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=new_schema,
                token=new_token["token"])
        
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,




                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_invoice}/leanpay-bills", response_model=schemas.ResponseApiBase)
def get_customer_bill_invoice_id(
        *,
        db: Session = Depends(deps.get_db),
        _invoice: str,
        background_tasks: BackgroundTasks
) -> Any:
    """
    search customer bill id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customers-bill/{_invoice}/leanpay-bills",
                                  _email="NO_DATA",
                                  _account_id="NO_DATA",
                                  _description="GET_CUSTOMER_BILL_WITH_INVOICE_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=_invoice)

        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=3614,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )

        query_account = crud.account.get_account_by_id(db, id=query_uuid[0].account_id)
        query_options = crud.system_option.get_by_system_own_id(db, _id=query_account.parent_key)
        if query_uuid:
            query_collection = crud.collection.get_by_collection_id(db, _id=query_uuid[0].collection_id)
            if query_collection.collection_method == 2:
                query_item = crud.customer_bill_item.get_by_customer_bill_id(db, _id=query_uuid[0].id)
                if query_item:
                    new_schema = schemas.CustomerBillMethodView(
                        full_name=query_uuid[0].full_name,
                        email=query_uuid[0].email,
                        phone_number=query_uuid[0].phone_number,
                        status=query_uuid[0].status,
                        total=query_uuid[0].total,
                        collection_id=query_uuid[0].collection_id,
                        payment_record_id=query_uuid[0].payment_record_id,
                        total_amount_with_fee=query_uuid[0].total_amount_with_fee,
                        transaction_fee=query_uuid[0].transaction_fee,
                        quantity=query_uuid[0].quantity,
                        account_id=query_uuid[0].account_id,
                        invoice_no=query_uuid[0].invoice_no,
                        customer_id=query_uuid[0].customer_id,
                        transaction_invoice_no=query_uuid[0].transaction_invoice_no,
                        invoice_status_id=query_uuid[0].invoice_status_id,
                        shipping_address_id=query_uuid[0].shipping_address_id,
                        invoice_status=query_uuid[0].invoice_status,
                        base_url=query_uuid[0].base_url,
                        checking_counter=query_uuid[0].checking_counter,
                        api_key=query_uuid[0].api_key,
                        product_listing=query_item,
                        id=query_uuid[0].id,
                        collection=schemas.Collection(**jsonable_encoder(query_uuid[0].customer_bill_collection)),
                        payment_record=query_uuid[0].customer_bill_payment_record,
                        customer_bill_account=schemas.Account(**jsonable_encoder(query_uuid[1])),
                        created_at=query_uuid[0].created_at,
                        account_options=query_options,
                        payment_date=query_uuid[0].payment_date,
                        customer_shipping_address=query_uuid[0].customer_bill_shipping_address
                    )
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=new_schema,
                        token="")
                else:
                    new_schema = schemas.CustomerBillMethodView(
                        full_name=query_uuid[0].full_name,
                        email=query_uuid[0].email,
                        phone_number=query_uuid[0].phone_number,
                        status=query_uuid[0].status,
                        total=query_uuid[0].total,
                        collection_id=query_uuid[0].collection_id,
                        payment_record_id=query_uuid[0].payment_record_id,
                        total_amount_with_fee=query_uuid[0].total_amount_with_fee,
                        transaction_fee=query_uuid[0].transaction_fee,
                        quantity=query_uuid[0].quantity,
                        account_id=query_uuid[0].account_id,
                        invoice_no=query_uuid[0].invoice_no,
                        customer_id=query_uuid[0].customer_id,
                        transaction_invoice_no=query_uuid[0].transaction_invoice_no,
                        invoice_status_id=query_uuid[0].invoice_status_id,
                        shipping_address_id=query_uuid[0].shipping_address_id,
                        invoice_status=query_uuid[0].invoice_status,
                        base_url=query_uuid[0].base_url,
                        checking_counter=query_uuid[0].checking_counter,
                        api_key=query_uuid[0].api_key,
                        product_listing=query_item,
                        id=query_uuid[0].id,
                        collection=schemas.Collection(**jsonable_encoder(query_uuid[0].customer_bill_collection)),
                        payment_record=query_uuid[0].customer_bill_payment_record,
                        customer_bill_account=schemas.Account(**jsonable_encoder(query_uuid[1])),
                        created_at=query_uuid[0].created_at,
                        account_options=query_options,
                        payment_date=query_uuid[0].payment_date,
                        customer_shipping_address=query_uuid[0].customer_bill_shipping_address
                    )
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=new_schema,
                        token="")

            new_schema = schemas.CustomerBillMethodView(
                full_name=query_uuid[0].full_name,
                email=query_uuid[0].email,
                phone_number=query_uuid[0].phone_number,
                status=query_uuid[0].status,
                total=query_uuid[0].total,
                collection_id=query_uuid[0].collection_id,
                payment_record_id=query_uuid[0].payment_record_id,
                total_amount_with_fee=query_uuid[0].total_amount_with_fee,
                transaction_fee=query_uuid[0].transaction_fee,
                quantity=query_uuid[0].quantity,
                account_id=query_uuid[0].account_id,
                invoice_no=query_uuid[0].invoice_no,
                customer_id=query_uuid[0].customer_id,
                transaction_invoice_no=query_uuid[0].transaction_invoice_no,
                invoice_status_id=query_uuid[0].invoice_status_id,
                shipping_address_id=query_uuid[0].shipping_address_id,
                invoice_status=query_uuid[0].invoice_status,
                base_url=query_uuid[0].base_url,
                checking_counter=query_uuid[0].checking_counter,
                api_key=query_uuid[0].api_key,
                id=query_uuid[0].id,
                collection=schemas.Collection(**jsonable_encoder(query_uuid[0].customer_bill_collection)),
                payment_record=query_uuid[0].customer_bill_payment_record,
                customer_bill_account=schemas.Account(**jsonable_encoder(query_uuid[1])),
                created_at=query_uuid[0].created_at,
                account_options=query_options,
                payment_date=query_uuid[0].payment_date,
                customer_shipping_address=query_uuid[0].customer_bill_shipping_address
            )

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=new_schema,
                token="")
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/get-based-customer/", response_model=schemas.ResponseApiBase)
def get_customer_bills_based_customer(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        customer_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer bills.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customer-bills",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILlS_LIST_BASED_ON_CUSTOMER_ID",
                                  _data=customer_id, db_=db)
        list_data = crud.customer_bill.get_multi_customer_id(db, skip=skip, limit=limit, _id=customer_id)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/customer-bill-method", response_model=schemas.ResponseApiBase)
def create_customer_bill_method(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerCollectionBill
        = Body(..., example=PayloadExample.CREATE_CUSTOMER_BILL_OPTION_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Create new collection.
    """
    global customer_query
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="customer-bill/customer-bill-method",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_COLLECTION_BASED_ON_BILL_METHOD",
                                  _data=_in.json(), db_=db)
        # create customer id
        if _in.customer_id:
            query_get_customer = crud.customer.get_by_customer_id(db, _id=_in.customer_id)
        else:
            query_get_customer = crud.customer.get_by_name(db, full_name=_in.email)
        if not query_get_customer:
            customer_create = schemas.CustomerCreate(
                account_id=new_token["account_id"],
                full_name=_in.full_name,
                email=_in.email,
                phone_number=_in.phone_number,
                # address=_in.address
            )

            # add customer to db
            new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
            _in.customer_id = new_customer.id
        else:
            _in.customer_id = query_get_customer.id
            customer_query = {
                "full_name": query_get_customer.full_name,
                "email": query_get_customer.email,

            }
            _in.phone_number = query_get_customer.phone_number

        # generate invoice
        _in.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
        # check uuid and recreate if same
        query_uuid = crud.customer_bill.get_by_invoice_no(db, invoice_no=_in.invoice_no)
        if query_uuid:
            while query_uuid.uuid == _in.invoice_no:
                _in.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"

        # create payment record
        sub = crud.account.get_account_by_id(db, new_token["account_id"])
        payment_record_create = schemas.PaymentRecordCreate(
            amount=_in.total,
            transaction_fee=sub.subscription_plan.fpx_charges,
            net_amount=_in.total + sub.subscription_plan.fpx_charges
        )

        new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
        _in.payment_record_id = new_payment_record.id
        _in.account_id = new_token["account_id"]
        _in.base_url = str(request.base_url) + f"api/v1/customer-bills/{_in.invoice_no}/leanpay-bills"

        customer_bill_in = schemas.CustomerBillCreate(
            full_name=customer_query["full_name"],
            email=customer_query["email"],
            phone_number=_in.phone_number,
            collection_id=_in.collection_id,
            total=_in.total,
            payment_record_id=new_payment_record.id,
            customer_id=_in.customer_id,
            total_amount_with_fee=new_payment_record.net_amount,
            transaction_fee=new_payment_record.transaction_fee,
            account_id=int(new_token["account_id"]),
            invoice_no=_in.invoice_no,
            invoice_status_id=1,
            invoice_status="PENDING",
            quantity=_in.quantity,
            base_url=_in.base_url
        )
        query_create = crud.customer_bill.create_self_date(db, obj_in=customer_bill_in)
        _in.id = query_create.id
        _in.full_name = query_create.full_name
        _in.email = query_create.email
        for x in _in.product_listing:
            create_schema = schemas.CustomerBillItemCreate(
                customer_bill_id=query_create.id,
                product_id=x["product_id"],
                amount=x["amount"],
                quantity=x["quantity"],
                total_amount=x["total_amount"]
            )
            bill_customer_item_create = crud.customer_bill_item.create_self_date(db, obj_in=create_schema)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=_in,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/customer-bill-method/{_id}", response_model=schemas.ResponseApiBase)
def update_customer_bill_method(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CustomerCollectionBill
        = Body(..., example=PayloadExample.CREATE_CUSTOMER_BILL_OPTION_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    update customer item bill method.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/collections/customer-bill-method/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_COLLECTION",
                                  _data="NO_DATA", db_=db)
        query_update = crud.customer_bill.get_by_customer_bill_id(db, _id=_id)
        if query_update:
            customer_bill_in = schemas.CustomerBillCreate(
                full_name=_in.full_name,
                email=_in.email,
                phone_number=_in.phone_number,
                collection_id=_in.collection_id,
                total=_in.total,
                quantity=_in.quantity,
                account_id=int(new_token["account_id"])
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=customer_bill_in)

        for x in _in.product_listing:
            if x["id"]:
                query_update_bill = crud.customer_bill_item.get_by_customer_bill_item_id(db, _id=x["id"])
                if query_update_bill:
                    create_schema = schemas.CustomerBillItemCreate(
                        customer_bill_id=x["customer_bill_id"],
                        product_id=x["product_id"],
                        amount=x["amount"],
                        quantity=x["quantity"],
                        total_amount=x["total_amount"]
                    )
                    update_ = crud.customer_bill_item.update_self_date(db, db_obj=query_update_bill,
                                                                       obj_in=create_schema)
            else:
                create_schema = schemas.CustomerBillItemCreate(
                    customer_bill_id=x["customer_bill_id"],
                    product_id=x["product_id"],
                    amount=x["amount"],
                    quantity=x["quantity"],
                    total_amount=x["total_amount"]
                )
                create_ = crud.customer_bill_item.create_self_date(db, obj_in=create_schema)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=_in,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/get-based-collection/{collection_id}", response_model=schemas.ResponseApiBase)
def get_customer_bills_based_collection(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        collection_id: int,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer bills.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET",
                                  _url="/customer-bills/get-based-collection/{collection_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILlS_LIST_BASED_ON_COLLECTION_ID",
                                  _data="NO_DATA", db_=db)
        db_count = crud.customer_bill.get_count_collection(db=db, _id=collection_id)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.customer_bill.get_multi_collection_id \
            (db=db, skip=skip, limit=limit, _id=collection_id, record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        if _in.invoice_status:
            list_data = [x for x in list_data if x.invoice_status == _in.invoice_status.upper()]

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable and not _in.invoice_status:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/get-bill-search", response_model=schemas.ResponseApiBase)
def get_customer_bills_based_search_key(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        collection_id: int,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer bills.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET",
                                  _url="/customer-bills/get-based-collection/{collection_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_CUSTOMER_BILlS_LIST_BASED_ON_SEARCH_KEY",
                                  _data="NO_DATA", db_=db)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
