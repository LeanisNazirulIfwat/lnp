import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Request
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

router = APIRouter(prefix="/company-details", tags=["company-details"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_company_details(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available company details.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/company-details",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_ALL_COMPANY_DETAILS_LIST",
                                  _data="NO_DATA", db_=db)
        list_data = crud.company_detail.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_company_details(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CompanyDetailCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new company details.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/company-details/create",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_COMPANY_DETAILS",
                                  _data=_in.json(), db_=db)

        query_get = crud.company_detail.get_by_name(db, company_name=_in.company_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        query_create = crud.company_detail.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_company_details(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CompanyDetailUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update company details.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/company-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_COMPANY_DETAILS_BY_ID",
                                  _data=_in.json(), db_=db)

        query_update = crud.company_detail.get_by_company_detail_id(db, _id=_id)
        if query_update:
            update_ = crud.company_detail.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_company_details(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete company details.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/company-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_COMPANY_DETAILS_RECORD",
                                  _data="NO_DATA", db_=db)
        query_delete = crud.company_detail.get_by_company_detail_id(db, _id=_id)
        _in = schemas.CompanyDetail(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.company_detail.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_company_details_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search bank id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/company-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_COMPANY_DETAILS_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.company_detail.get_by_company_detail_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/options/company-me", response_model=schemas.ResponseApiBase)
def get_company_details_me(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[
                Role.WHITE_LABEL["name"],
                Role.SUPER_ADMIN["name"],
                Role.MERCHANT["name"],
            ],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve users for own account.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/users/me", _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="RETRIEVE COMPANY DETAILS FROM OWN ACCOUNT",
                                  _data="NO_DATA", db_=db)

        company_details = crud.company_detail.get_by_company_detail_account_id(db, _id=new_token["account_id"])
        if not company_details:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=company_details,
                breakdown_errors="ACCOUNT_NOT_EXIST",
                token=new_token["token"]
            )
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=company_details,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/info/{_id}", response_model=schemas.ResponseApiBase)
def update_info_details(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.InfoDetailUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    Update company details.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/company-details/info/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],

                                  _description="UPDATE_COMPANY_DETAILS_BY_ID",
                                  _data=_in.json(), db_=db)
        query_update = crud.company_detail.get_by_company_detail_id(db, _id=_id)
        data_in_company = schemas.CompanyDetailUpdate(
            company_name=_in.company_name,
            company_number=_in.company_number,
            business_website_url=_in.business_website_url,
            nature_of_business=_in.nature_of_business,
            logo=_in.logo,
            fav_icon=_in.fav_icon,
        )
        if query_update:
            update_ = crud.company_detail.update_self_date(db, db_obj=query_update, obj_in=data_in_company)
            query_business_owner = crud.business_owner_detail\
                .get_by_business_owner_detail_id(db, _id=update_.business_owner_detail_id)
            if query_business_owner:
                data_in_business_owner = schemas.BusinessOwnerDetailUpdate(
                    legal_name=_in.company_detail_business_owner_detail.legal_name,
                    individual_nric=_in.company_detail_business_owner_detail.individual_nric,
                    address_line_one=_in.company_detail_business_owner_detail.address_line_one,
                    address_line_two=_in.company_detail_business_owner_detail.address_line_two,
                    address_postcode=_in.company_detail_business_owner_detail.address_postcode,
                    address_city=_in.company_detail_business_owner_detail.address_city,
                    address_state=_in.company_detail_business_owner_detail.address_state,
                    address_country=_in.company_detail_business_owner_detail.address_country,
                    bank_name=_in.company_detail_business_owner_detail.bank_name,
                    bank_account_statement_header=_in.company_detail_business_owner_detail.bank_account_statement_header,
                    business_bank_account_number=_in.company_detail_business_owner_detail.business_bank_account_number
                )
                update_ = crud.business_owner_detail\
                    .update_self_date(db, db_obj=query_business_owner, obj_in=data_in_business_owner)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
