import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
from app.Utils.subscription_checker import Subscription

router = APIRouter(prefix="/products", tags=["products"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_products(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        list_data = crud.product.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_products(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.ProductCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        subs=Depends(deps.check_subscription_package),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new products.
    """
    try:
        query_get = crud.product.get_by_name(db, title=_in.title, code=_in.code)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token["account_id"]
        _in.image = json.dumps(_in.image)

        if _in.price <= 0:
            return schemas.ResponseApiBase(
                response_code=6899,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="PRICE_MUST_BE_ABOVE_ZERO",
                token=""
            )

        if _in.quantity <= 0 or _in.min_purchase_quantity <= 0:
            return schemas.ResponseApiBase(
                response_code=6214,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="VALUE_MUST_BE_ABOVE_ZERO",
                token=""
            )

        if _in.limited_stock_enable:
            if _in.min_purchase_quantity <= 0:
                return schemas.ResponseApiBase(
                    response_code=6825,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="MAX_PRICE_MUST_BE_ABOVE_ZERO",
                    token=""
                )

        subscription_ = Subscription(models.Product, subs, new_token["account_id"])
        checker = subscription_.check_subscription_product(db)
        if not checker:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="QUOTA_LIMIT_EXCEED",
                token=""
            )

        query_create = crud.product.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_products(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.ProductUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update catalogs.
    """
    try:
        query_update = crud.product.get_by_product_id(db, _id=_id)
        if _in.price <= 0:
            return schemas.ResponseApiBase(
                response_code=6899,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="PRICE_MUST_BE_ABOVE_ZERO",
                token=""
            )

        if _in.quantity <= 0 or _in.min_purchase_quantity <= 0:
            return schemas.ResponseApiBase(
                response_code=6214,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="VALUE_MUST_BE_ABOVE_ZERO",
                token=""
            )

        if _in.limited_stock_enable:
            if _in.min_purchase_quantity <= 0:
                return schemas.ResponseApiBase(
                    response_code=6825,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="MAX_PRICE_MUST_BE_ABOVE_ZERO",
                    token=""
                )
        if query_update:
            _in.image = json.dumps(_in.image)
            # update_ = schemas.ProductUpdate(**_in.dict())
            update_ = crud.product.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_products(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        api_in: schemas.Product,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete catalogs.
    """
    try:
        query_delete = crud.product.get_by_product_id(db, _id=_id)
        _in = schemas.Product(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.product.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-product", response_model=schemas.ResponseApiBase)
def get_merchant_product(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available product based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/product/merchant-product",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_PRODUCT_LIST_BASED_ON_ACCOUNT_ID",
                                  _data=_in, db_=db)

        db_count = crud.product.get_count(db=db, _id=new_token["account_id"])
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.product.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_product_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search product id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/products/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_BANK_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.product.get_by_product_id(db, _id=_id)
        if query_uuid.product_category_list:
            product_category_list = json.loads(query_uuid.product_category_list)
            prod_list = []
            for prod in product_category_list:
                product = crud.product_category.get_by_product_category_id(db, _id=prod)
                prod_list.append(product)

            new_schema = schemas.ProductInView(
                name=query_uuid.name,
                title=query_uuid.title,
                code=query_uuid.code,
                description=query_uuid.description,
                min_purchase_quantity=query_uuid.min_purchase_quantity,
                max_purchase_quantity=query_uuid.max_purchase_quantity,
                price=query_uuid.price,
                quantity=query_uuid.quantity,
                account_id=query_uuid.account_id,
                intro=query_uuid.intro,
                footer=query_uuid.footer,
                image=json.loads(query_uuid.image),
                product_category_list=query_uuid.product_category_list,
                product_category_details=prod_list,
                product_product_variant=query_uuid.product_product_variant,
                product_tier_pricing=query_uuid.product_tier_pricing,
                limited_stock_enable=query_uuid.limited_stock_enable,
                physical_product_enable=query_uuid.physical_product_enable,
                tier_pricing_enable=query_uuid.tier_pricing_enable,
                product_variant_enable=query_uuid.product_variant_enable
            )
            if query_uuid:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=new_schema,
                    token=new_token["token"])

        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
