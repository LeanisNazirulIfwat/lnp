import json
from datetime import datetime
from typing import Any, List
from sqlalchemy import func
from app.models.option import SystemOption

from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Body, Depends, Security, BackgroundTasks
from sqlalchemy.orm import Session, selectinload
from app.core.config import settings
from fastapi.encoders import jsonable_encoder
from app.Utils.shared_utils import SharedUtils
from app.constants.payload_example import PayloadExample

router = APIRouter(prefix="/accounts", tags=["accounts"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_accounts(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all accounts.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts", _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]), _description="GET LIST OF ACCOUNT",
                                  _data="NO_DATA", db_=db)

        db_count = crud.account.get_count_user(db, _id=current_user.id, start_date=_in.start_date,
                                               end_date=_in.end_date)

        cur_start_date = datetime.strptime(_in.start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(_in.end_date, "%d-%m-%Y")

        if current_user.user_role.role.name == Role.SUPER_ADMIN["name"]:
            db_count = len(db.query(models.Account).filter(
                func.date(models.Account.created_at) >= cur_start_date.date(),
                func.date(models.Account.created_at) <= cur_end_date.date()).all())

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.account.get_multi_search_user(db=db, skip=skip, limit=limit, _id=current_user.id,
                                                       record_status=_in.record_status,
                                                       start_date=_in.start_date,
                                                       end_date=_in.end_date, search_column=_in.search.search_column,
                                                       search_key=_in.search.search_key,
                                                       parameter_name=_in.sort.parameter_name,
                                                       sort_type=_in.sort.sort_type,
                                                       search_enable=_in.search.search_enable,
                                                       invoice_status=_in.invoice_status, table_name="accounts",
                                                       role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/me", response_model=schemas.ResponseApiBase)
def get_account_for_user(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve account for a logged in user.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="GET ACCOUNT FOR USER",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get_multi_account(db, id=current_user.id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=limit - skip,
                record_filtered=limit - skip,
                record_total=limit - skip,
                data=account
            )},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_account(
        *,
        db: Session = Depends(deps.get_db),
        account_in: schemas.AccountCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create an user account
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/accounts", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="CREATE USER ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get_by_name(db, name=account_in.name)
        if account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="An account with this name already exists",
                token=new_token["token"]
            )
        account_in = account_in.merchant_id = SharedUtils.my_random_string(6)
        account = crud.account.create_self_date(db, obj_in=account_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=account,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{account_id}", response_model=schemas.ResponseApiBase)
def update_account(
        *,
        db: Session = Depends(deps.get_db),
        account_id: int,
        account_in: schemas.AccountUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[
                Role.ADMIN["name"],
                Role.SUPER_ADMIN["name"],
                Role.ACCOUNT_ADMIN["name"],
            ],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update an account.
    """

    # If user is an account admin, check ensure they update their own account.
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/accounts/{account_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="UPDATE USER ACCOUNT",
                                  _data=account_in.json(), db_=db)
        if current_user.user_role.role.name == Role.SUPER_ADMIN["name"]:
            if current_user.account_id != account_id:
                return schemas.ResponseApiBase(
                    response_code=4000,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.today(),
                    data=current_user.email,
                    breakdown_errors="This user does not have the permissions to "
                                     "update this account",
                    token=new_token["token"]
                )
        account = crud.account.get(db, id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=current_user.email,
                breakdown_errors="This user does not have the permissions to "
                                 "update this account",
                token=new_token["token"]
            )
        account = crud.account.update_self_date(db, db_obj=account, obj_in=account_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=account,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/{account_id}/users", response_model=schemas.ResponseApiBase)
def add_user_to_account(
        *,
        db: Session = Depends(deps.get_db),
        account_id: int,
        user_id: str = Body(..., embed=True),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Add a user to an account.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/accounts/{account_id}/users",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="ADD USER TO AN ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get(db, id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="Account does not exist",
                token=new_token["token"]
            )
        user = crud.user.get(db, id=user_id)
        if not user:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=user,
                breakdown_errors="User does not exist",
                token=new_token["token"]
            )
        user_in = schemas.UserUpdate(account_id=account_id)
        updated_user = crud.user.update_self_date(db, db_obj=user, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=updated_user,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{account_id}/users", response_model=schemas.ResponseApiBase)
def retrieve_users_for_account(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        account_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve users for an account.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/{account_id}/users",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="RETRIEVE USER FROM AN ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get(db, id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="ACCOUNT_NOT_EXIST",
                token=new_token["token"]
            )
        account_users = crud.user.get_by_account_id(
            db, user_id=account.user_id, skip=skip, limit=limit
        )
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=account_users,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/users/me", response_model=schemas.ResponseApiBase)
def retrieve_users_for_own_account(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[
                Role.WHITE_LABEL["name"],
                Role.SUPER_ADMIN["name"],
                Role.MERCHANT["name"],
            ],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve users for own account.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/users/me", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="RETRIEVE USER FROM OWN ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get(db, id=new_token["account_id"])
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="ACCOUNT_NOT_EXIST",
                token=new_token["token"]
            )
        account_users = crud.user.get_by_account_id(
            db, user_id=account.user_id, skip=skip, limit=limit
        )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "full_name": account_users[0].full_name,
                "email": account_users[0].email,
                "user_role": account_users[0].user_role,
                "account": account_users[0].account,

            },
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/merchant/{account_id}/switch", response_model=schemas.ResponseApiBase)
def switch_account(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        account_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    switch account merchant.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/merchant/{account_id}/switch",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="SWITCH MERCHANT ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account: List[schemas.Account] = crud.account.switch_account(db, id=current_user.id, account_id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="NO_LIST_IN_THE_RECORD",
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=limit - skip,
                record_filtered=limit - skip,
                record_total=limit - skip,
                data=account
            )},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{account_id}/accounts", response_model=schemas.ResponseApiBase)
def retrieve_specific_account(
        *,
        db: Session = Depends(deps.get_db),
        account_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve specific user account
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/merchant/{account_id}/switch",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="SWITCH MERCHANT ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get(db, id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="Account does not exist",
                token=new_token["token"]
            )
        accounts = crud.account.get_account_by_id(
            db, id=account_id
        )
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=accounts,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/admin/merchant-account/{account_id}", response_model=schemas.ResponseApiBase)
def admin_retrieve_account(
        *,
        db: Session = Depends(deps.get_db),
        account_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve specific user account
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/accounts/merchant/{account_id}/switch",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="SWITCH MERCHANT ACCOUNT",
                                  _data="NO_DATA", db_=db)
        account = crud.account.get(db, id=account_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=account,
                breakdown_errors="Account does not exist",
                token=new_token["token"]
            )
        accounts = db.query(models.Account, SystemOption).join(SystemOption,
                                                               SystemOption.api_parent_key == models.Account.parent_key) \
            .options(selectinload(models.Account.account_company_details)) \
            .filter(models.Account.id == account_id).first()

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=accounts,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
