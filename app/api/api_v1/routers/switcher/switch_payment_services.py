import datetime
import json
from typing import Any, List
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, Body, BackgroundTasks
from fastapi.encoders import jsonable_encoder
from fastapi.responses import StreamingResponse
from sqlalchemy.orm import Session
import requests as req
from jose import jws
from app.Utils.shared_utils import SharedUtils
import os
from io import BytesIO
from PIL import Image
import base64
from app.core.config import settings
from app.constants.payload_example import PayloadExample
from sqlalchemy.ext.serializer import loads, dumps
from app.constants.system_constant import SystemConstant
from itertools import groupby
import asyncio

router = APIRouter(prefix="/payment-services", tags=["payment-services"])


@router.get("/switch/list-payment-services/{_id}", response_model=Any)
def get_list_payment_services(
        _id: int
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'payment_model_reference_id': f'{_id}',
                   'payment_service_record_status_id': '1'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPaymentResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]["data"]
                for x in list_services:
                    data = schemas.SwitchPaymentResponses(**x)
                    services.append(data)
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=services
                    )},
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/switch/selected-list-payment-services/{_id}", response_model=Any)
def get_selected_list_payment_services(
        _id: int,
        _invoice_uuid: str,
        db: Session = Depends(deps.get_db),
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'payment_model_reference_id': f'{_id}',
                   'payment_service_record_status_id': '1'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPaymentResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]["data"]
                for x in list_services:
                    data = schemas.SwitchPaymentResponses(**x)
                    services.append(data)

                sorted_services = sorted(services, key=lambda z: z.switch_payment_service)

                channel_sorted_group = [{key: list(result)} for key, result in
                                        groupby(sorted_services, key=lambda y: y.channel_code)]

                sorted_group = [{key: list(result)} for key, result in
                                groupby(sorted_services, key=lambda y: y.switch_payment_service)]

                # GET PLAN FROM USER
                exclude_payment_services = ["QR_PAYMENT", "GLOBAL_INSTALLMENT_PAYMENT_PLAN", "TOKENIZATION"]
                channel_code_to_include = ["SWITCH_PAYPAL"]

                subs_ = crud.collection.get_by_uuid_account(db, _uuid=_invoice_uuid)
                if subs_:
                    if not subs_.collection_account.subscription_plan.fpx_enable:
                        exclude_payment_services.append('WEB_PAYMENT')
                    if not subs_.collection_account.subscription_plan.credit_card_enable:
                        exclude_payment_services.append('GLOBAL_CARD_PAYMENT')
                        # exclude_payment_services.append('TOKENIZATION')
                    if not subs_.collection_account.subscription_plan.ewallet_enable:
                        # exclude_payment_services.append('QR_PAYMENT')
                        exclude_payment_services.append('DIGITAL_PAYMENT')
                    if not subs_.collection_account.subscription_plan.bnpl_enable:
                        # exclude_payment_services.append('GLOBAL_INSTALLMENT_PAYMENT_PLAN')
                        exclude_payment_services.append('BUY_NOW_PAY_LATER')
                    if not subs_.collection_account.subscription_plan.crypto_enable:
                        # exclude_payment_services.append('GLOBAL_INSTALLMENT_PAYMENT_PLAN')
                        exclude_payment_services.append('CRYPTOCURRENCY')
                    if subs_.collection_account.subscription_plan.paypal_enable:
                        channel_code_to_include.append("SWITCH_PAYPAL")
                if not subs_:
                    subs_invoice = crud.customer_bill.get_customer_by_invoice_no_account(db, _invoice=_invoice_uuid)
                    if subs_invoice:
                        if not subs_invoice[1].subscription_plan.fpx_enable:
                            exclude_payment_services.append('WEB_PAYMENT')
                        if not subs_invoice[1].subscription_plan.credit_card_enable:
                            exclude_payment_services.append('GLOBAL_CARD_PAYMENT')
                            # exclude_payment_services.append('TOKENIZATION')
                        if not subs_invoice[1].subscription_plan.ewallet_enable:
                            # exclude_payment_services.append('QR_PAYMENT')
                            exclude_payment_services.append('DIGITAL_PAYMENT')
                        if not subs_invoice[1].subscription_plan.bnpl_enable:
                            # exclude_payment_services.append('GLOBAL_INSTALLMENT_PAYMENT_PLAN')
                            exclude_payment_services.append('BUY_NOW_PAY_LATER')
                        if not subs_invoice[1].subscription_plan.crypto_enable:
                            # exclude_payment_services.append('GLOBAL_INSTALLMENT_PAYMENT_PLAN')
                            exclude_payment_services.append('CRYPTOCURRENCY')
                        if subs_invoice[1].subscription_plan.paypal_enable:
                            channel_code_to_include.append("SWITCH_PAYPAL")

                # Start Sorting
                channel_sorted_group = [service for service in channel_sorted_group if
                                        list(service.keys())[0] in channel_code_to_include]

                sorted_group = [service for service in sorted_group if
                                list(service.keys())[0] not in exclude_payment_services]

                sorted_group.extend(channel_sorted_group)

                services_limit = len(sorted_group)

                result = {}
                for d in sorted_group:
                    for key, value in d.items():
                        result[key] = value

                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(sorted_group),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=result
                    )},
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/create-payment-viu", response_model=Any)
async def create_payment_collection(*,
                                    db: Session = Depends(deps.get_db),
                                    invoice_no: str = None,
                                    callback_in: schemas.SwitchCallbackUrlCreate,
                                    background_task: BackgroundTasks
                                    ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST", _url="/payment-services/switch/create-payment-viu",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PAYMENT_USING_COLLECTION",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        collection_transaction = crud.collection.get_quantity_limit_count(db, _uuid=callback_in.collection_uuid)
        if collection_transaction.enable_quantity_limit:
            if collection_transaction.quantity_limit <= 0:
                return schemas.ResponseApiBase(
                    response_code=2610,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="TRANSACTION_CANNOT_BE_PERFORMED",
                    token=""
                )

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                account_pool_id = collection_ids.account_id
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                    if collection_ids.enable_total_amount_tracking and collection_ids.disable_payment_when_target_reached:
                        pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__(
                            "TRANSACTION_POOL")
                        pool = crud.pool.get_by_pool_account_type(db, _id=pool_constant, _account_id=account_pool_id)
                        if pool.value >= collection_ids.total_target_amount:
                            collection_update = schemas.CollectionUpdate(
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                            update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                product_list = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                charge_rate = 0
                if response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'WEB_PAYMENT':
                    charge_rate = accounts.subscription_plan.fpx_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'GLOBAL_CARD_PAYMENT':
                    charge_rate = accounts.subscription_plan.credit_card_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'DIGITAL_PAYMENT':
                    charge_rate = accounts.subscription_plan.ewallet_charges
                    if response.json()["data"]["payment_service_obj"]["channel_code"] == 'SWITCH_PAYPAL':
                        charge_rate = accounts.subscription_plan.paypal_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'BUY_NOW_PAY_LATER':
                    charge_rate = accounts.subscription_plan.bnpl_charges
                elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'CRYPTOCURRENCY':
                    charge_rate = accounts.subscription_plan.crypto_charges

                # create payment record
                if accounts.subscription_plan.fpx_charges:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=charge_rate,
                        net_amount=float(callback_in.amount) + float(charge_rate)
                    )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=charge_rate,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)

                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) * float(callback_in.quantity) +
                                          float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query[0].invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
                else:
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
                    if bill_query:
                        return schemas.ResponseApiBase(
                            response_code=4626,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=" ",
                            breakdown_errors="BILL_INVOICE_ALREADY_EXIST",
                            token=""
                        )
                    else:
                        bill_create.invoice_no = invoice_no

                if collection_transaction.sms_enable:
                    bill_create.sms_enable = True
                if collection_transaction.whatsapp_enable:
                    bill_create.whatsapp_enable = True
                if collection_transaction.email_enable:
                    bill_create.email_enable = True

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if bill_create:
                    if collection_ids.collection_method == 2:
                        if callback_in.product_listing:
                            for x in callback_in.product_listing:
                                product = crud.product.get_by_product_id(db, _id=x["product_id"])
                                if not product:
                                    return schemas.ResponseApiBase(
                                        response_code=4637,
                                        description='FAILED',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=" ",
                                        breakdown_errors="PRODUCT_ID_NOT_VALID",
                                        token=""
                                    )

                                if product.quantity - x["quantity"] >= 0:
                                    new_product = schemas.ProductCreate(
                                        quantity=product.quantity - x["quantity"]
                                    )
                                    update_product = crud.product.update_self_date(db, obj_in=new_product,
                                                                                   db_obj=product)
                                else:
                                    crud.customer_bill.remove(db, id=new_bill.id)
                                    return schemas.ResponseApiBase(
                                        response_code=7651,
                                        description='FAILED',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=" ",
                                        breakdown_errors="PRODUCT_INSUFFICIENT",
                                        token=""
                                    )

                                create_schema = schemas.CustomerBillItemCreate(
                                    customer_bill_id=new_bill.id,
                                    product_id=x["product_id"],
                                    amount=x["amount"],
                                    quantity=x["quantity"],
                                    total_amount=x["total_amount"]
                                )
                                bill_customer_item_create = crud.customer_bill_item. \
                                    create_self_date(db, obj_in=create_schema)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/payment-bill/_invoice", response_model=Any)
async def create_payment_bill(*,
                              db: Session = Depends(deps.get_db),
                              callback_in: schemas.SwitchCallbackUrlCreate,
                              _invoice: str,
                              background_task: BackgroundTasks
                              ) -> Any:
    """
    API for creating payment through payment switch using bill creation
    """
    try:

        background_task.add_task(deps.write_audit, _method="POST",
                                 _url=f"/payment-services/switch/payment-bill/{_invoice}",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PAYMENT_BILL_USING_INVOICE",
                                 _data=callback_in.json(), db_=db)

        query_get = crud.customer_bill.get_by_customer_by_active_invoice_no(db, _invoice=_invoice)
        if not query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=" ",
                breakdown_errors="INVOICE_NOT_VALID",
                token=""
            )

        query_get = loads(dumps(query_get[0]))

        bill_item = crud.customer_bill_item.get_by_customer_bill_id(db, _id=query_get.id)
        for x in bill_item:
            product = crud.product.get_by_product_id(db, _id=x[0].product_id)
            if not product:
                return schemas.ResponseApiBase(
                    response_code=4637,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=" ",
                    breakdown_errors="PRODUCT_ID_NOT_VALID",
                    token=""
                )

            if product.quantity - x[0].quantity >= 0:
                new_product = schemas.ProductCreate(
                    quantity=product.quantity - x[0].quantity
                )
                update_product = crud.product.update_self_date(db, obj_in=new_product,
                                                               db_obj=product)
            else:
                return schemas.ResponseApiBase(
                    response_code=7651,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=" ",
                    breakdown_errors="PRODUCT_INSUFFICIENT",
                    token=""
                )

        collection_ids = crud.collection.get_by_collection_id(db, _id=query_get.collection_id)
        if collection_ids.enable_quantity_limit:
            if collection_ids.quantity_limit - callback_in.quantity == 0:
                collection_update = schemas.CollectionUpdate(
                    quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                    record_status=settings.RECORD_NOT_ACTIVE
                )
            else:
                collection_update = schemas.CollectionUpdate(
                    quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                )

            update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

        if query_get.customer_bill_collection.enable_billing_address:
            # create customer id
            customer_create = schemas.CustomerCreate(
                account_id=query_get.account_id,
                full_name=callback_in.full_name,
                email=callback_in.email,
                phone_number=callback_in.phone_number,
                address_line_one=callback_in.address_line_one,
                address_line_two=callback_in.address_line_two,
                address_postcode=callback_in.address_postcode,
                address_state=callback_in.address_state,
                address_city=callback_in.address_city,
                address_country=callback_in.address_country,
            )
            shipping_create = schemas.ShippingAddressCreate(
                name=callback_in.full_name,
                phone_number=callback_in.phone_number,
                address_line_one=callback_in.address_line_one,
                address_line_two=callback_in.address_line_two,
                address_postcode=callback_in.address_postcode,
                address_state=callback_in.address_state,
                address_city=callback_in.address_city,
                address_country=callback_in.address_country
            )
            new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
            callback_in.shipping_address_id = new_shipping_address.id
        else:
            # create customer id
            customer_create = schemas.CustomerCreate(
                account_id=query_get.account_id,
                full_name=callback_in.full_name,
                email=callback_in.email,
                phone_number=callback_in.phone_number
            )

        # query customer from db
        existing_customer = crud.customer.get_by_name_account(db, _id=query_get.account_id, full_name=callback_in.email)
        if existing_customer:
            update_customer = crud.customer.update_self_date(db, db_obj=existing_customer, obj_in=customer_create)
        else:
            create_customer = crud.customer.create_self_date(db=db, obj_in=customer_create)

        callback_in.amount = query_get.total
        # callback_in.amount = query_get[0].total * query_get[0].quantity
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}
        charge_rate = 0
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:

                if query_get.account_id:
                    sub = crud.account.get_account_by_id(db, query_get.account_id)
                    if response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'WEB_PAYMENT':
                        charge_rate = sub.subscription_plan.fpx_charges
                    elif response.json()["data"]["payment_service_obj"][
                        "switch_payment_service"] == 'GLOBAL_CARD_PAYMENT':
                        charge_rate = sub.subscription_plan.credit_card_charges
                    elif response.json()["data"]["payment_service_obj"]["switch_payment_service"] == 'DIGITAL_PAYMENT':
                        charge_rate = sub.subscription_plan.ewallet_charges
                    elif response.json()["data"]["payment_service_obj"][
                        "switch_payment_service"] == 'BUY_NOW_PAY_LATER':
                        charge_rate = sub.subscription_plan.bnpl_charges

                if not query_get.payment_record_id:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=query_get.total,
                        transaction_fee=charge_rate,
                        net_amount=query_get.total + charge_rate
                    )

                    new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
                    query_get.payment_record_id = new_payment_record.id
                else:
                    query_get.transaction_fee = charge_rate

                # update bill payment
                bill_update = schemas.CustomerBillCreate(
                    account_id=query_get.account_id,
                    payment_record_id=query_get.payment_record_id,
                    customer_id=query_get.customer_id,
                    collection_id=query_get.collection_id,
                    full_name=query_get.full_name,
                    transaction_fee=charge_rate,
                    email=query_get.email,
                    phone_number=query_get.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    invoice_no=query_get.invoice_no,
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    shipping_address_id=callback_in.shipping_address_id
                )

                query_new = crud.customer_bill.get_by_invoice_no(db, invoice_no=_invoice)
                new_bill = crud.customer_bill.update_self_date(db, db_obj=query_new, obj_in=bill_update)
                if new_bill:
                    twilio_input = schemas.TwilioPaymentReceipt(
                        from_=settings.ADMIN_LEANX_EMAIL,
                        to_=bill_update.email,
                        bcc_=settings.ADMIN_LEANX_EMAIL,
                        customer=bill_update.full_name,
                        email=bill_update.email,
                        phone=bill_update.phone_number,
                        invoice=new_bill.invoice_no,
                        amount=new_bill.total,
                    )
                    message = await asyncio.get_event_loop().run_in_executor(
                        None, SharedUtils.send_payment_invoice_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                        callback_in.email, settings.ADMIN_LEANX_EMAIL, twilio_input)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=10018,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        else:
            return schemas.ResponseApiBase(
                response_code=10228,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=response.json()["data"],
                breakdown_errors="ADDRESS_NOT_FOUND",
                token=""
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/switch/payment-provider/{code}", response_model=Any)
def payment_provider_image(
        *,
        db: Session = Depends(deps.get_db),
        code: str,
        method: str,
        background_task: BackgroundTasks
) -> Any:
    """
    get image for payment provider
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url=f"/switch/payment-provider/{code}",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="GET_IMAGE_PROVIDER",
                                 _data=f"code: {code}, method: {method}", db_=db)

        file_path = f"./assets/payment_provider/{code}.png"
        if os.path.exists(file_path):
            if method == 'display':
                original_image = Image.open(file_path)
                filtered_image = BytesIO()
                original_image.save(filtered_image, "PNG")
                filtered_image.seek(0)
                return StreamingResponse(filtered_image, media_type="image/png")
            elif method == "byte":
                with open(file_path, "rb") as img_file:
                    b64_string = base64.b64encode(img_file.read())
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=b64_string.decode('utf-8'),
                        breakdown_errors="",
                        token=""
                    )
            # return FileResponse(file_path, media_type="image/png", filename=f"{code}.jpg")
        return schemas.ResponseApiBase(
            response_code=3614,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data="",
            breakdown_errors=" IMAGE_NOT_FOUND",
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/manual-checking-transaction-bulk", response_model=Any)
def manual_checking_payment_bulk(
        *,
        db: Session = Depends(deps.get_db),
        invoice_no: str,
        background_task: BackgroundTasks
) -> Any:
    """
    get image for payment provider
    """
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/switch/manual-checking-transaction-bulk",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="MANUAL_CHECKING_FOR_BULK_TRANSACTION",
                                 _data=invoice_no, db_=db)

        pending_transaction = db.query(models.CustomerBill).filter(
            models.CustomerBill.invoice_status_id == settings.RECORD_ACTIVE).all()

        msg = {
            "invoice_no": invoice_no
        }
        encode = jws.sign(msg, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09', algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/GetPaymentDetails"

        payload = {'skip_encryption': 'false',
                   'client_identifier': 'LEANPAY',
                   'payload': encode}
        files = [

        ]
        headers = {}

        response_payment = req.request("POST", url, headers=headers, data=payload, files=files)
        if response_payment.status_code == req.codes.ok:
            if response_payment.json()["response_code"] == 2100:
                query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no \
                    (db, _invoice=response_payment.json()["data"]["output"]["output"]["invoice_no"])
                _customer_id = query_update.customer_id
                if query_update:
                    update_bill = schemas.CustomerBillUpdate(
                        invoice_status_id=response_payment.json()["data"]["output"]["output"]["invoice_status_id"],
                        invoice_status=response_payment.json()["data"]["output"]["output"]["invoice_status"],
                    )
                    update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)

                    if update_:

                        payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                        if payment_update:
                            url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                            payload = {'client_identifier': 'LEANPAY',
                                       'skip_encryption': 'false',
                                       f'payment_model_reference_id':
                                           response_payment.json()["data"]["output"]["output"]
                                           ["payment_model_reference_id"],
                                       f'amount_to_calculate': response_payment.json()["data"]["output"]["output"]
                                       ["amount"],
                                       f'payment_service_id': response_payment.json()["data"]["output"]["output"]
                                       ["payment_service_id"]}
                            files = []
                            headers = {}
                            response = req.request("POST", url, headers=headers, data=payload, files=files)
                            if response.status_code == req.codes.ok:
                                if response.json()["response_code"] == 2100:
                                    update_payment = schemas.PaymentRecordUpdate(
                                        payment_mode=response.json()["data"]["output"]["data"][0][
                                            "switch_payment_service"],
                                        payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"],
                                        total=response.json()["data"]["output"]["data"][0]["best_rate"]
                                        ["selected"]["paymentSwitchChargeAmount"],
                                        transaction_fee=response.json()["data"]["output"]["data"][0]["best_rate"]
                                        ["selected"]["fullPaymentServiceObj"]["rate"],
                                        customer_id=_customer_id,
                                        payment_date=datetime.datetime.today(),
                                        reference_number=response_payment.json()["data"]["output"]["output"][
                                            "invoice_no"],
                                        bank_code=response.json()["data"]["output"]["data"][0][
                                            "payment_model_reference"]
                                    )
                                    update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                                   obj_in=update_payment)
                                    return schemas.ResponseApiBase(
                                        response_code=2123,
                                        description='SUCCESS',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data={
                                            "data": update_,
                                            "status": "update status success"
                                        },
                                        breakdown_errors="",
                                        token=""
                                    )
                                else:
                                    return schemas.ResponseApiBase(
                                        response_code=response.json()["response_code"],
                                        description=response.json()["description"],
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=response.json()["data"],
                                        breakdown_errors="PAYMENT SERVICE RETURN ERROR",
                                        token=""
                                    )
                            else:
                                return schemas.ResponseApiBase(
                                    response_code=10018,
                                    description='FAILED',
                                    app_version=settings.API_V1_STR,
                                    talk_to_server_before=datetime.datetime.today(),
                                    data=response.json()["data"],
                                    breakdown_errors="PAYMENT SERVICE API NOT FOUND",
                                    token=""
                                )
        return schemas.ResponseApiBase(
            response_code=3614,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data="",
            breakdown_errors=" INVOICE_NUMBER_NOT_AVAILABLE",
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/manual-checking-transaction", response_model=Any)
def manual_checking_payment(
        *,
        db: Session = Depends(deps.get_db),
        invoice_no: str,
        background_task: BackgroundTasks
) -> Any:
    """
    get image for payment provider
    """
    try:
        transaction_type = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__("TRANSACTION")
        prefund_type = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__("PREFUND")

        background_task.add_task(deps.write_audit, _method="POST", _url="/switch/manual-checking-transaction",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="MANUAL_CHECKING_TRANSACTION",
                                 _data=invoice_no, db_=db)

        payment_bill = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)

        if not payment_bill:
            return schemas.ResponseApiBase(
                response_code=3614,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors=" INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )

        payment_date = payment_bill[0].payment_date
        if payment_bill[0].bill_transaction_type_id in [transaction_type]:
            topup_que = crud.transaction_topup_record.get_by_invoice_id(db,
                                                                        _invoice=payment_bill[0].transaction_invoice_no)
            if topup_que:
                payment_bill = topup_que
        elif payment_bill[0].bill_transaction_type_id in [prefund_type]:
            prefund_que = crud.prefund_record.get_by_invoice_id(db, _invoice=payment_bill[0].transaction_invoice_no)
            if prefund_que:
                payment_bill = prefund_que

        msg = {
            "invoice_no": payment_bill[0].transaction_invoice_no
        }
        encode = jws.sign(msg, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09', algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/GetPaymentDetails"

        payload = {'skip_encryption': 'false',
                   'client_identifier': 'LEANPAY',
                   'payload': encode}
        files = [

        ]
        headers = {}

        response_payment = req.request("POST", url, headers=headers, data=payload, files=files)
        if response_payment.status_code == req.codes.ok:
            if response_payment.json()["response_code"] == 2100:
                support_details = crud.support_detail.get_by_support_detail_merchant_id(db,
                                                                                        _id=payment_bill[0].account_id)
                response_data = schemas.SwitchManualPaymentResponse(
                    **response_payment.json()["data"]["output"]["output"])
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={
                        'transaction_details': {
                            "invoice_no": invoice_no,
                            "fpx_invoice_no": response_data.invoice_no,
                            "amount": response_data.amount,
                            "invoice_status": response_data.invoice_status,
                            "providerTypeReference": response_data.selected.providerTypeReference,
                            "bank_provider": response_data.selected.fullPaymentServiceObj.name,
                            "category_code": response_data.selected.fullPaymentServiceObj.category_code,
                            "payment_date": payment_date
                        },
                        'company_name': payment_bill[1].account_company_details.company_name,
                        'company_number': payment_bill[1].account_company_details.company_number,
                        'nature_of_business': payment_bill[1].account_company_details.nature_of_business,
                        'company_details': payment_bill[1].account_company_details.company_detail_business_owner_detail,
                        'support_details': support_details
                    },
                    breakdown_errors="",
                    token=""
                )
        return schemas.ResponseApiBase(
            response_code=3614,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data="",
            breakdown_errors=" INVOICE_NUMBER_NOT_AVAILABLE",
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/prefund-virtual-pool/{invoice_no}", response_model=Any)
def create_prefund_bill(*,
                        db: Session = Depends(deps.get_db),
                        invoice_no: str = None,
                        callback_in: schemas.SwitchCallbackPrefundPool = Body(..., example=
                        {
                            "collection_uuid": "LEANX-CL",
                            "amount": 20.15,
                            "payment_service_id": 36,
                            "redirect_url": "https://leanis.com.my",
                            "callback_url": "https://stag-api.leanpay.my/api/v1/callback-url/call-encrypt",
                            "virtual_pool_reference": "VP16692707790UZAY",
                            "invoice_type_id": 2,
                            "full_name": "MAMOMAN",
                            "email": "MAMOMAN@GMAIL.COM",
                            "phone_number": "0112459861",
                            "address": "SERI KEMBANGAN",
                            "quantity": 1
                        }
                                                                              ),
                        background_task: BackgroundTasks
                        ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/prefund-virtual-pool/{_invoice}",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PREFUND_VIRTUAL_POOL",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        query_pool = crud.virtual_account.get_by_virtual_account_unique_id(db, _id=callback_in.virtual_pool_reference)
        callback_in.virtual_pool_reference = query_pool.switch_payout_pool_id
        callback_in.client_data = json.dumps({
            "virtual_pool_reference": query_pool.merchant_payout_pool_id
        })
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                product_list = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                # create payment record
                payment_record_create = Any
                if callback_in.invoice_type_id == 1:
                    if accounts.subscription_plan.fpx_charges:
                        payment_record_create = schemas.PaymentRecordCreate(
                            amount=callback_in.amount,
                            transaction_fee=accounts.subscription_plan.fpx_charges,
                            net_amount=callback_in.amount + float(accounts.subscription_plan.fpx_charges)
                        )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=0.50,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)

                bill_constant = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__("PREFUND")

                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) * float(callback_in.quantity) +
                                          float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                    bill_transaction_type_id=bill_constant
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-PREFUND-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query[0].invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-PREFUND-LNP"
                else:
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
                    if bill_query:
                        return schemas.ResponseApiBase(
                            response_code=4626,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=" ",
                            breakdown_errors="BILL_INVOICE_ALREADY_EXIST",
                            token=""
                        )
                    else:
                        bill_create.invoice_no = invoice_no

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if bill_create:
                    if collection_ids.collection_method == 2:
                        if callback_in.product_listing:
                            for x in callback_in.product_listing:
                                product = crud.product.get_by_product_id(db, _id=x["product_id"])
                                if not product:
                                    return schemas.ResponseApiBase(
                                        response_code=4637,
                                        description='FAILED',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=" ",
                                        breakdown_errors="PRODUCT_ID_NOT_VALID",
                                        token=""
                                    )

                                if product.quantity - x["quantity"] >= 0:
                                    new_product = schemas.ProductCreate(
                                        quantity=product.quantity - x["quantity"]
                                    )
                                    update_product = crud.product.update_self_date(db, obj_in=new_product,
                                                                                   db_obj=product)
                                else:
                                    crud.customer_bill.remove(db, id=new_bill.id)
                                    return schemas.ResponseApiBase(
                                        response_code=7651,
                                        description='FAILED',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=" ",
                                        breakdown_errors="PRODUCT_INSUFFICIENT",
                                        token=""
                                    )

                                create_schema = schemas.CustomerBillItemCreate(
                                    customer_bill_id=new_bill.id,
                                    product_id=x["product_id"],
                                    amount=x["amount"],
                                    quantity=x["quantity"],
                                    total_amount=x["total_amount"]
                                )
                                bill_customer_item_create = crud.customer_bill_item. \
                                    create_self_date(db, obj_in=create_schema)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/manual-prefund-virtual-account", response_model=Any)
def create_manual_prefund_bill(*,
                               db: Session = Depends(deps.get_db),
                               invoice_no: str = None,
                               callback_in: schemas.SwitchCallbackPrefundPool = Body(..., example=
                               {
                                   "collection_uuid": "LEANX-CL",
                                   "amount": 20.15,
                                   "payment_service_id": 36,
                                   "redirect_url": "https://leanis.com.my",
                                   "callback_url": f"{settings.API_UAT_URL}api/v1/callback-url/call-manual-prefund-encrypt",
                                   "virtual_pool_reference": "VA-0FC9DD-143418755516-PAYOUT",
                                   "invoice_type_id": 1,
                                   "full_name": "MAMOMAN",
                                   "email": "MAMOMAN@GMAIL.COM",
                                   "phone_number": "0112459861",
                                   "address": "SERI KEMBANGAN",
                                   "quantity": 1
                               }),
                               current_user: models.User = Security(
                                   deps.get_current_active_user,
                                   scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                                           Role.SUPER_ADMIN["name"]],
                               ),
                               new_token: str = Depends(deps.get_current_new_token),
                               background_task: BackgroundTasks
                               ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/prefund-virtual-pool/{_invoice}",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PREFUND_VIRTUAL_POOL",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        callback_in.callback_url = f"{settings.API_UAT_URL}api/v1/callback-url/call-manual-prefund-encrypt"
        query_pool = crud.virtual_account.get_by_virtual_account_unique_id(db, _id=callback_in.virtual_pool_reference)
        if not query_pool:
            return schemas.ResponseApiBase(
                response_code=1304,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_pool,
                breakdown_errors=f"POOL_REFERENCE_NOT_FOUND",
                token=new_token["token"]
            )
        callback_in.virtual_pool_reference = query_pool.switch_payout_pool_id
        callback_in.client_data = json.dumps({
            "virtual_pool_reference": query_pool.merchant_payout_pool_id
        })
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                product_list = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                # create payment record
                payment_record_create = Any
                # reset invoice type id
                callback_in.invoice_type_id = 2
                if callback_in.invoice_type_id == 1:
                    if accounts.subscription_plan.fpx_charges:
                        payment_record_create = schemas.PaymentRecordCreate(
                            amount=callback_in.amount,
                            transaction_fee=accounts.subscription_plan.fpx_charges,
                            net_amount=callback_in.amount + float(accounts.subscription_plan.fpx_charges)
                        )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=0.50,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)

                bill_constant = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__("PREFUND")

                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) * float(callback_in.quantity) +
                                          float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                    bill_transaction_type_id=bill_constant
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-PREFUND-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query[0].invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-PREFUND-LNP"
                else:
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
                    if bill_query:
                        return schemas.ResponseApiBase(
                            response_code=4626,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=" ",
                            breakdown_errors="BILL_INVOICE_ALREADY_EXIST",
                            token=""
                        )
                    else:
                        bill_create.invoice_no = invoice_no

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if bill_create:
                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/add-fund-transaction-pool", response_model=Any)
def add_fund_transaction_pool(*,
                              db: Session = Depends(deps.get_db),
                              invoice_no: str = None,
                              callback_in: schemas.SwitchCallbackTopupPool = Body(..., example=
                              {
                                  "collection_uuid": settings.ADMIN_COLLECTION_UUID,
                                  "amount": 20.00,
                                  "payment_service_id": 36,
                                  "redirect_url": "https://leanis.com.my",
                                  "callback_url": f"{settings.API_UAT_URL}api/v1/callback-url/call-encrypt",
                                  "full_name": "Nur Derwyna Maryam",
                                  "email": "wyna.maryam@leanis.com",
                                  "phone_number": "0112459861",
                                  "address": "SERI KEMBANGAN",
                                  "quantity": 1
                              }),
                              pool_type: int = 1,
                              current_user: models.User = Security(
                                  deps.get_current_active_user,
                                  scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                                          Role.SUPER_ADMIN["name"]],
                              ),
                              new_token: str = Depends(deps.get_current_new_token),
                              background_task: BackgroundTasks
                              ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/add-fund-transaction-pool",
                                 _email=current_user.email,
                                 _account_id=new_token["account_id"],
                                 _description="ADD_FUND_TO_TRANSACTION_POOL",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        callback_in.client_data = json.dumps({
            "topup_client_account_id": new_token["account_id"]
        })
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                product_list = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                # create payment record
                payment_record_create = Any
                if callback_in.invoice_type_id == 1:
                    if accounts.subscription_plan.fpx_charges:
                        payment_record_create = schemas.PaymentRecordCreate(
                            amount=callback_in.amount,
                            transaction_fee=accounts.subscription_plan.fpx_charges,
                            net_amount=callback_in.amount + float(accounts.subscription_plan.fpx_charges)
                        )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=0.50,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
                bill_constant = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__("TRANSACTION")
                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) * float(callback_in.quantity) +
                                          float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                    bill_transaction_type_id=bill_constant
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-POOL-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query[0].invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-POOL-LNP"
                else:
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
                    if bill_query:
                        return schemas.ResponseApiBase(
                            response_code=4626,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=" ",
                            breakdown_errors="BILL_INVOICE_ALREADY_EXIST",
                            token=""
                        )
                    else:
                        bill_create.invoice_no = invoice_no

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if bill_create:
                    query_pool = crud.pool.get_by_pool_account_type(db, _id=pool_type,
                                                                    _account_id=new_token["account_id"])
                    if query_pool:
                        create_audit_pool = schemas.AuditPoolCreate(
                            previous_value=query_pool.value,
                            current_value=query_pool.value,
                            charges=callback_in.amount,
                            pool_id=query_pool.id,
                            api_key=query_pool.api_key,
                            account_id=query_pool.account_id,
                            invoice_no=response.json()["data"]["invoice_no"],
                            invoice_status="PENDING",
                            pool_type=pool_type,
                            data=json.dumps(response.json()),
                        )
                        crud.audit_pool.create_self_date(db, obj_in=create_audit_pool)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=""
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/paid-bill-payment", response_model=Any)
def create_paid_bill_payment(*,
                             db: Session = Depends(deps.get_db),
                             invoice_no: str = None,
                             callback_in: schemas.SwitchCallbackTopupPool = Body(..., example=
                             {
                                 "collection_uuid": "LEANX-CL",
                                 "amount": 20,
                                 "payment_service_id": 36,
                                 "redirect_url": "https://leanis.com.my",
                                 "callback_url": f"{settings.SWITCH_UAT_URL}api/v1/callback-url/bill-payment-callback",
                                 "full_name": "Nur Derwyna Maryam",
                                 "email": "wyna.maryam@leanis.com",
                                 "phone_number": "0112459861",
                                 "address": "SERI KEMBANGAN",
                                 "quantity": 1
                             }),
                             pool_type: int = 1,
                             _id: int,
                             current_user: models.User = Security(
                                 deps.get_current_active_user,
                                 scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                                         Role.SUPER_ADMIN["name"]],
                             ),
                             new_token: str = Depends(deps.get_current_new_token),
                             background_task: BackgroundTasks
                             ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/add-fund-transaction-pool",
                                 _email=current_user.email,
                                 _account_id=new_token["account_id"],
                                 _description="ADD_FUND_TO_TRANSACTION_POOL",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        callback_in.client_data = json.dumps({
            "payment_client_account_id": new_token["account_id"],
            "subscription_payment_id": _id,
            "collection_uuid": callback_in.collection_uuid
        })
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/CreatePayment"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                collection_ids = crud.collection.get_by_uuid(db, uuid=callback_in.collection_uuid)
                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="UUID_NOT_VALID",
                        token=""
                    )
                else:
                    if collection_ids.enable_quantity_limit:
                        if collection_ids.quantity_limit - callback_in.quantity == 0:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity,
                                record_status=settings.RECORD_NOT_ACTIVE
                            )
                        else:
                            collection_update = schemas.CollectionUpdate(
                                quantity_limit=collection_ids.quantity_limit - callback_in.quantity
                            )

                        update_ = crud.collection.update(db, db_obj=collection_ids, obj_in=collection_update)

                product_list = crud.collection_item.get_item_by_collection_id(db, _id=collection_ids.id)

                if not collection_ids.account_id:
                    return schemas.ResponseApiBase(
                        response_code=4000,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_ID_NOT_VALID",
                        token=""
                    )

                accounts = crud.account.get_account_by_id(db, id=collection_ids.account_id)

                if not collection_ids:
                    return schemas.ResponseApiBase(
                        response_code=4624,
                        description='FAILED',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=" ",
                        breakdown_errors="ACCOUNT_NOT_VALID",
                        token=""
                    )

                # check customer
                customer_id: int = 0
                query_get_customer = crud.customer.get_by_name_account(db, full_name=callback_in.email,
                                                                       _id=collection_ids.account_id)
                if not query_get_customer:
                    # check enable address
                    if collection_ids.enable_billing_address:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country,
                        )

                        shipping_create = schemas.ShippingAddressCreate(
                            name=callback_in.full_name,
                            phone_number=callback_in.phone_number,
                            address_line_one=callback_in.address_line_one,
                            address_line_two=callback_in.address_line_two,
                            address_postcode=callback_in.address_postcode,
                            address_state=callback_in.address_state,
                            address_city=callback_in.address_city,
                            address_country=callback_in.address_country
                        )
                        new_shipping_address = crud.shipping_address.create_self_date(db, obj_in=shipping_create)
                        callback_in.shipping_address_id = new_shipping_address.id
                    else:
                        # create customer id
                        customer_create = schemas.CustomerCreate(
                            account_id=collection_ids.account_id,
                            full_name=callback_in.full_name,
                            email=callback_in.email,
                            phone_number=callback_in.phone_number
                        )

                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_id = new_customer.id
                else:
                    customer_id = query_get_customer.id

                # create payment record
                payment_record_create = Any
                if callback_in.invoice_type_id == 1:
                    if accounts.subscription_plan.fpx_charges:
                        payment_record_create = schemas.PaymentRecordCreate(
                            amount=callback_in.amount,
                            transaction_fee=accounts.subscription_plan.fpx_charges,
                            net_amount=callback_in.amount + float(accounts.subscription_plan.fpx_charges)
                        )
                else:
                    payment_record_create = schemas.PaymentRecordCreate(
                        amount=callback_in.amount,
                        transaction_fee=0.50,
                        net_amount=callback_in.amount
                    )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)
                bill_constant = SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__(
                    "BILL_PAYMENT")
                # create bill payment
                bill_create = schemas.CustomerBillCreate(
                    account_id=collection_ids.account_id,
                    payment_record_id=new_payment_record.id,
                    total=callback_in.amount,
                    total_amount_with_fee=float(callback_in.amount) * float(callback_in.quantity) +
                                          float(new_payment_record.transaction_fee),
                    customer_id=customer_id,
                    collection_id=collection_ids.id,
                    full_name=callback_in.full_name,
                    email=callback_in.email,
                    phone_number=callback_in.phone_number,
                    transaction_invoice_no=response.json()["data"]["invoice_no"],
                    quantity=callback_in.quantity,
                    invoice_status_id=1,
                    invoice_status="PENDING",
                    transaction_fee=payment_record_create.transaction_fee,
                    shipping_address_id=callback_in.shipping_address_id,
                    bill_transaction_type_id=bill_constant
                )

                if not invoice_no:
                    bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-POOL-LNP"
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=bill_create.invoice_no)
                    if bill_query:
                        while bill_query[0].invoice_no == bill_create.invoice_no:
                            bill_create.invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-POOL-LNP"
                else:
                    bill_query = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=invoice_no)
                    if bill_query:
                        return schemas.ResponseApiBase(
                            response_code=4626,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=" ",
                            breakdown_errors="BILL_INVOICE_ALREADY_EXIST",
                            token=""
                        )
                    else:
                        bill_create.invoice_no = invoice_no

                new_bill = crud.customer_bill.create_self_date(db, obj_in=bill_create)
                if bill_create:
                    query_pool = crud.pool.get_by_pool_account_type(db, _id=pool_type,
                                                                    _account_id=new_token["account_id"])
                    if query_pool:
                        create_audit_pool = schemas.AuditPoolCreate(
                            previous_value=query_pool.value,
                            current_value=query_pool.value,
                            charges=callback_in.amount,
                            pool_id=query_pool.id,
                            api_key=query_pool.api_key,
                            account_id=collection_ids.account_id,
                            invoice_no=response.json()["data"]["invoice_no"],
                            invoice_status="PENDING",
                            pool_type=pool_type,
                            data=json.dumps(response.json()),
                            description="INITIATE_BILL_PAYMENT"
                        )
                        crud.audit_pool.create_self_date(db, obj_in=create_audit_pool)

                    return schemas.ResponseApiBase(
                        response_code=2000,
                        description='SUCCESS',
                        app_version=settings.API_V1_STR,
                        talk_to_server_before=datetime.datetime.today(),
                        data=response.json()["data"],
                        breakdown_errors="",
                        token=new_token["token"]
                    )

            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=""
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/switch/list-payout-services/{_id}", response_model=Any)
def get_list_payout_services(
        _id: int
) -> Any:
    """
    API for getting the listing for payment services through payment switch
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/ListOfPayoutServices"
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder({'client_identifier': 'LEANPAY',
                                           'skip_encryption': 'false'})
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false',
            'payment_model_reference_id': f'{_id}'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPayOutResponse] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    data = schemas.SwitchPayOutResponse(**x)
                    services.append(data)
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=services
                    )},
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/create-payout-invoice", response_model=Any)
def create_payout_invoice(*,
                          db: Session = Depends(deps.get_db),
                          callback_in: schemas.SwitchCallbackPayoutResponse = Body(..., example=
                          {"client_identifier": "LEANPAY",
                           "virtual_pool_reference": "VA-0FC9DD-143418755516-PAYOUT",
                           "payout_service_id": "23",
                           "amount": "1.3",
                           "client_redirect_url": f"{settings.SWITCH_UAT_URL}pay/thankyou?additional_message=xx&bill_id=xx&extra_param=leanpay-extraneous-param",
                           "client_callback_url": f"{settings.SWITCH_UAT_URL}api/v1/callback-url/call-invoice-payout-encrypt",
                           "third_party_account_no": "8011408168 for success // 1111111111 for failed",
                           "recipient_reference": "test",
                           "payment_description": "test payment description",
                           "client_data": ""
                           }),
                          current_user: models.User = Security(
                              deps.get_current_active_user,
                              scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                                      Role.SUPER_ADMIN["name"]],
                          ),
                          new_token: str = Depends(deps.get_current_new_token),
                          background_task: BackgroundTasks
                          ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/create-payout-invoice",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CREATE_PAYOUT_INVOICE",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        query_virtual = crud.virtual_account.get_by_virtual_account_unique_id(db,
                                                                              _id=callback_in.virtual_pool_reference)

        if not query_virtual:
            return schemas.ResponseApiBase(
                response_code=8522,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="VIRTUAL_POOL_INVALID",
                token=new_token["token"]
            )

        pool_prefund = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
        query_prefund_pool = crud.pool.get_by_pool_account_type(db, _id=pool_prefund,
                                                                _account_id=new_token['account_id'])
        if query_prefund_pool:
            if float(callback_in.amount) >= float(query_prefund_pool.value):
                return schemas.ResponseApiBase(
                    response_code=5993,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="INSUFFICIENT_POOL_BALANCE",
                    token=new_token["token"]
                )

        merchant_payout_pool = callback_in.virtual_pool_reference
        callback_in.virtual_pool_reference = query_virtual.switch_payout_pool_id
        payout_invoice = f"PAYOUT-{SharedUtils.my_random_string(4)}:{datetime.datetime.today().strftime('%H%M%S')}-LEANX"
        callback_in.client_data = json.dumps({
            "virtual_pool_reference": payout_invoice
        })
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        # print(encode)
        # decode = jws.verify(encode, secret_key, algorithms=['HS256'])
        # print(decode)

        create_payout_record = schemas.PayoutRecordCreate(
            switch_invoice_id=query_virtual.switch_payout_pool_id,
            merchant_invoice_id=query_virtual.merchant_payout_pool_id,
            transaction_invoice_no="NO_DATA",
            payout_account_number=callback_in.third_party_account_no,
            payout_invoice_no=payout_invoice,
            transaction_status="PENDING",
            value=callback_in.amount,
            account_id=new_token['account_id'],
            data=callback_in.json()
        )

        crud.payout_record.create_self_date(db, obj_in=create_payout_record)

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/CreatePayout"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                pool_payout = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL")
                transaction_type = "ADDITION"
                query_payout_pool = crud.pool.get_by_pool_account_type(db, _id=pool_payout,
                                                                       _account_id=new_token["account_id"])
                create_audit_pool = schemas.AuditPoolCreate(
                    transaction_type=transaction_type,
                    previous_value=query_payout_pool.value,
                    current_value=query_payout_pool.value,
                    charges=callback_in.amount,
                    pool_id=query_payout_pool.id,
                    api_key=None,
                    account_id=new_token["account_id"],
                    pool_type=pool_payout,
                    invoice_no=None,
                    invoice_status="PENDING",
                    description="INITIATE_PAYOUT"
                )
                crud.audit_pool.create_self_date(db, obj_in=create_audit_pool)

                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="",
                    token=new_token["token"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=new_token["token"]
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/switch/list-verification-channel", response_model=Any)
def get_list_verification_channel() -> Any:
    """
    API for getting the listing for bank verification
    """
    try:
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/ListOfPaymentChannels"
        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        encode_message = jsonable_encoder({'client_identifier': 'LEANPAY',
                                           'skip_encryption': 'false'})
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')
        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}

        files = {}
        headers = {}
        services: List[schemas.VerificationChannelBaseModel] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    data = schemas.VerificationChannelBaseModel(**x)
                    services.append(data)
                services_limit = len(services)
                # return services
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={"list": schemas.ResponseListApiBase(
                        draw=len(services),
                        record_filtered=services_limit,
                        record_total=services_limit,
                        data=services
                    )},
                    token=""
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=9100,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="URL_RETURN_ERROR",
                    token=""
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"ADDRESS_NOT_FOUND",
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/switch/verify-bank-account", response_model=Any)
def verify_bank_account(*,
                        db: Session = Depends(deps.get_db),
                        callback_in: schemas.VerificationCheckBaseModel = Body(..., example=
                        {"client_identifier": "LEANPAY",
                         "verification_channel_id": 1,
                         "third_party_account_no": "9011426016 or 1111111111"
                         }),
                        current_user: models.User = Security(
                            deps.get_current_active_user,
                            scopes=[Role.WHITE_LABEL["name"], Role.MERCHANT["name"],
                                    Role.SUPER_ADMIN["name"]],
                        ),
                        new_token: str = Depends(deps.get_current_new_token),
                        background_task: BackgroundTasks
                        ) -> Any:
    """
    API for creating payment through payment switch
    """
    try:
        background_task.add_task(deps.write_audit, _method="POST",
                                 _url="/payment-services/switch/verify-bank-account",
                                 _email="customer@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="VERIFY_BANK_ACCOUNT",
                                 _data=callback_in.json(), db_=db)

        secret_key = '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09'
        callback_in.payout_service_id = callback_in.verification_channel_id
        encode_message = jsonable_encoder(callback_in)
        encode = jws.sign(encode_message, secret_key, algorithm='HS256')

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/accountVerifier/AccountCreditorVerifier"

        payload = {
            'payload': encode,
            'client_identifier': 'LEANPAY',
            'skip_encryption': 'false'}
        files = [

        ]
        headers = {}

        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"]["output"],
                    breakdown_errors="",
                    token=new_token["token"]
                )
            else:
                return schemas.ResponseApiBase(
                    response_code=response.json()["response_code"],
                    description=response.json()["description"],
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=response.json()["data"],
                    breakdown_errors="PAYMENT_SERVICE_NOT_ACTIVE",
                    token=new_token["token"]
                )
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"ADDRESS_NOT_FOUND",
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
