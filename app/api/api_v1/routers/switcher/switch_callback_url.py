import asyncio
import datetime
import json
from decimal import Decimal
from typing import Any
from app import schemas, crud
from app.api import deps
from fastapi import APIRouter, Depends, Body, BackgroundTasks
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
import requests as req
from jose import jws
from app.core.config import settings
from app.Utils.web_request import http_get_with_requests_parallel
from sqlalchemy.ext.serializer import loads, dumps
from app.Utils.shared_utils import calculate_transaction_fee, SharedUtils

router = APIRouter(prefix="/callback-url", tags=["callback-url"])


@router.post("/call", response_model=Any)
def callback_url_(
        *,
        db: Session = Depends(deps.get_db),
        callback_in: schemas.SwitchCallbackUrl = Body(None)) -> Any:
    try:
        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db, _invoice=callback_in.invoice_no)
        if query_update:
            update_bill = schemas.CustomerBillUpdate(
                invoice_status_id=callback_in.invoice_status_id,
                invoice_status=callback_in.invoice_status,
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)

            if update_:

                payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                if payment_update:
                    url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                    payload = {f"payment_service_id": callback_in.payment_service_id}
                    files = []
                    headers = {}
                    response = req.request("POST", url, headers=headers, data=payload, files=files)
                    if response.status_code == req.codes.ok:
                        if response.json()["response_code"] == 2100:
                            update_payment = schemas.PaymentRecordUpdate(
                                payment_mode=response.json()["data"]["output"]["data"][0]["switch_payment_service"],
                                payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"]
                            )
                            update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                           obj_in=update_payment)
                            return schemas.ResponseApiBase(
                                response_code=2123,
                                description='SUCCESS',
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data={
                                    "data": update_,
                                    "status": "update status succes"
                                },
                                breakdown_errors="",
                                token=""
                            )
                        else:
                            return schemas.ResponseApiBase(
                                response_code=response["response_code"],
                                description=response["description"],
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data=response.json()["data"],
                                breakdown_errors="PAYMENT SERVICE NOT ACTIVE",
                                token=""
                            )
                    else:
                        return schemas.ResponseApiBase(
                            response_code=10018,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=response.json()["data"],
                            breakdown_errors="PAYMENT SERVICE NOT ACTIVE",
                            token=""
                        )
        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-encrypt", response_model=Any)
async def callback_url_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/call-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db, _invoice=callback_in["invoice_no"])
        bill_account_id = query_update.account_id
        account = crud.account.get_account_by_id(db, id=query_update.account_id)

        if query_update.bill_transaction_type_id == 3:
            bill_account_id = callback_in["client_data"]["topup_client_account_id"]
            if callback_in["invoice_status"] == "SUCCESS":
                twilio_account = crud.account.get_account_by_id(db, id=bill_account_id)
                _twilio_topup = schemas.TwilioPrefund(
                    recipient_name=twilio_account.full_name,
                    transaction_data=schemas.TransactionData(
                        txn_date=callback_in["original_transaction_response_time"],
                        transaction_amount=query_update.total,
                        transaction_id=query_update.invoice_no

                    )
                )

                message = await asyncio.get_event_loop().run_in_executor(
                    None, SharedUtils.send_fund_notification_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                    query_update.email, settings.ADMIN_LEANX_EMAIL, _twilio_topup.json())

                topup_record = schemas.TransactionTopupRecordCreate(
                    invoice_no=query_update.invoice_no,
                    transaction_invoice_no=callback_in["invoice_no"],
                    transaction_status=callback_in["invoice_status"],
                    amount=callback_in["amount"],
                    account_id=callback_in["client_data"]["topup_client_account_id"],
                    payment_date=callback_in["original_transaction_response_time"],
                    data=json.dumps(callback_in),
                )

                topup_create = crud.transaction_topup_record.create_self_date(db, obj_in=topup_record)

        if query_update.bill_transaction_type_id == 4:
            bill_account_id = callback_in["client_data"]["payment_client_account_id"]

        if callback_in["invoice_status"] == "SUCCESS":
            query_update.transaction_fee = calculate_transaction_fee(rate_value=query_update.transaction_fee,
                                                                     rate_type=callback_in["rate_type_reference_id"],
                                                                     amount=callback_in["amount"])
            deps.transaction_charges(_charges=Decimal(query_update.transaction_fee),
                                     api_key=account.parent_key,
                                     _account_id=bill_account_id,
                                     db_=db, _transaction_amount=callback_in["amount"],
                                     invoice_status=callback_in["invoice_status"],
                                     invoice_no=callback_in["invoice_no"],
                                     bill_transaction_type=query_update.bill_transaction_type_id)

        _customer_id = query_update.customer_id
        if query_update:
            update_bill = schemas.CustomerBillUpdate(
                invoice_status_id=callback_in["invoice_status_id"],
                invoice_status=callback_in["invoice_status"],
                amount_fee=query_update.transaction_fee,
                fee_type=callback_in["rate_type_reference_id"],
                payment_date=datetime.datetime.strptime(callback_in["original_transaction_response_time"]
                                                        , "%Y-%m-%d %H:%M:%S"),
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)

            if update_:
                if update_.bill_transaction_type_id == 1:
                    _twilio_prefund = schemas.TwilioPrefund(
                        recipient_name=update_.full_name,
                        transaction_data=schemas.TransactionData(
                            txn_date=update_.payment_date.strftime("%B %d, %Y, %H:%M:%S %p"),
                            transaction_amount=update_.total,
                            transaction_id=update_.invoice_no

                        )
                    )

                    message = await asyncio.get_event_loop().run_in_executor(
                        None, SharedUtils.send_payment_invoice_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                        update_.email, settings.ADMIN_LEANX_EMAIL, _twilio_prefund.json())

                deps.add_shipment(db_=db, _account_id=query_update.account_id, _customer_id=_customer_id, _profile_id=1,
                                  _collection_id=update_.collection_id, _customer_bill_id=update_.id)
                payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                if payment_update:
                    url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                    payload = {'client_identifier': 'LEANPAY',
                               'skip_encryption': 'false',
                               f'payment_model_reference_id': callback_in["payment_model_reference_id"],
                               'amount_to_calculate': callback_in["amount"],
                               'payment_service_id': callback_in["payment_service_id"],
                               'payment_service_record_status_id': 1}
                    files = []
                    headers = {}
                    response = req.request("POST", url, headers=headers, data=payload, files=files)
                    if response.status_code == req.codes.ok:
                        if response.json()["response_code"] == 2100:
                            update_payment = schemas.PaymentRecordUpdate(
                                payment_mode=response.json()["data"]["output"]["data"][0]["switch_payment_service"],
                                payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"],
                                switch_transaction_fee=response.json()["data"]["output"]["data"][0]["best_rate"]
                                ["selected"]["paymentSwitchChargeAmount"],
                                amount_fee=query_update.transaction_fee,
                                fee_type=callback_in["rate_type_reference_id"],
                                customer_id=_customer_id,
                                payment_date=datetime.datetime.strptime(
                                    callback_in["original_transaction_response_time"]
                                    , "%Y-%m-%d %H:%M:%S"),
                                reference_number=callback_in["invoice_no"],
                                data=data,
                            )
                            update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                           obj_in=update_payment)
                            return schemas.ResponseApiBase(
                                response_code=2123,
                                description='SUCCESS',
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data={
                                    "data": update_,
                                    "status": "update status success"
                                },
                                breakdown_errors="",
                                token=""
                            )
                        else:
                            return schemas.ResponseApiBase(
                                response_code=response.json()["response_code"],
                                description=response.json()["description"],
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data=response.json()["data"],
                                breakdown_errors="PAYMENT SERVICE RETURN ERROR",
                                token=""
                            )
                    else:
                        return schemas.ResponseApiBase(
                            response_code=10018,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=response.json()["data"],
                            breakdown_errors="PAYMENT SERVICE API NOT FOUND",
                            token=""
                        )
        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-pool-encrypt", response_model=Any)
def callback_pool_url_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/call-pool-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_POOL_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)
        query_update = crud.audit_pool.get_by_invoice_no(db, invoice_no=callback_in["invoice_no"])
        if query_update:
            query_pool = crud.pool.get_by_pool_account_type(db, _id=query_update.pool_type,
                                                            _account_id=query_update.account_id)

            previous_value = Decimal(query_pool.value)
            current_value = Decimal(query_pool.value) + Decimal(query_update.charges)

            update_pool = schemas.PoolUpdate(
                value=current_value
            )

            update_create_pool = crud.pool.update_self_date(db, obj_in=update_pool, db_obj=query_pool)
            if update_create_pool:
                create_audit_pool = schemas.AuditPoolCreate(
                    previous_value=previous_value,
                    current_value=current_value,
                    charges=query_update.charges,
                    pool_id=query_pool.id,
                    api_key=query_pool.api_key,
                    account_id=query_pool.account_id,
                    invoice_no=callback_in["invoice_no"],
                    invoice_status=callback_in["invoice_status"],
                    data=json.dumps(callback_in),
                    description="ADD_AMOUNT_TO_POOL"
                )
                _create = crud.audit_pool.create_self_date(db, obj_in=create_audit_pool)
                return schemas.ResponseApiBase(
                    response_code=2123,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={
                        "data": _create,
                        "status": "update status success"
                    },
                    breakdown_errors="",
                    token=""
                )

        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/callback-redirect", response_model=Any)
async def callback_url_redirect_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    """callback for public"""
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/call-redirect",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db, _invoice=callback_in["invoice_no"])
        account = crud.account.get_account_by_id(db, id=query_update.account_id)

        if callback_in["invoice_status"] == "SUCCESS":
            query_update.transaction_fee = calculate_transaction_fee(rate_value=query_update.transaction_fee,
                                                                     rate_type=callback_in["rate_type_reference_id"],
                                                                     amount=callback_in["amount"])

            deps.transaction_charges(_charges=Decimal(query_update.transaction_fee),
                                     api_key=account.parent_key,
                                     _account_id=query_update.account_id,
                                     db_=db, _transaction_amount=callback_in["amount"],
                                     invoice_status=callback_in["invoice_status"],
                                     invoice_no=callback_in["invoice_no"],
                                     bill_transaction_type=query_update.bill_transaction_type_id)

        _customer_id = query_update.customer_id
        if query_update:
            update_bill = schemas.CustomerBillUpdate(
                invoice_status_id=callback_in["invoice_status_id"],
                invoice_status=callback_in["invoice_status"],
                payment_date=datetime.datetime.strptime(callback_in["original_transaction_response_time"]
                                                        , "%Y-%m-%d %H:%M:%S"),
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)

            if update_:
                payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                if payment_update:
                    url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                    payload = {'client_identifier': 'LEANPAY',
                               'skip_encryption': 'false',
                               f'payment_model_reference_id': callback_in["payment_model_reference_id"],
                               'amount_to_calculate': callback_in["amount"],
                               'payment_service_id': callback_in["payment_service_id"],
                               'payment_service_record_status_id': 1}
                    files = []
                    headers = {}
                    response = req.request("POST", url, headers=headers, data=payload, files=files)
                    if response.status_code == req.codes.ok:
                        if response.json()["response_code"] == 2100:
                            update_payment = schemas.PaymentRecordUpdate(
                                payment_mode=response.json()["data"]["output"]["data"][0]["switch_payment_service"],
                                payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"],
                                switch_transaction_fee=response.json()["data"]["output"]["data"][0]["best_rate"]
                                ["selected"]["paymentSwitchChargeAmount"],
                                transaction_fee=query_update.transaction_fee,
                                customer_id=_customer_id,
                                payment_date=datetime.datetime.strptime(
                                    callback_in["original_transaction_response_time"]
                                    , "%Y-%m-%d %H:%M:%S"),
                                reference_number=callback_in["invoice_no"],
                                data=data,
                            )
                            update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                           obj_in=update_payment)

                            user_response_json = str
                            user_response_content = str

                            callback_response = crud.merchant_callback \
                                .get_by_transaction_invoice(db, invoice_=callback_in["invoice_no"])
                            if callback_response:
                                user_url = callback_response.callback_user_url
                                user_payload = callback_in
                                user_response, t = \
                                    http_get_with_requests_parallel(headers=headers,
                                                                    list_of_urls=[user_url],
                                                                    payload=user_payload)

                                user_response_code = user_response[0][0]
                                user_response_json = user_response[0][1]
                                user_response_content = user_response[0][2]

                                if user_response_code != req.codes.ok:
                                    return schemas.ResponseApiBase(
                                        response_code=2244,
                                        description='SUCCESS',
                                        app_version=settings.API_V1_STR,
                                        talk_to_server_before=datetime.datetime.today(),
                                        data=user_response_json,
                                        breakdown_errors="CALLBACK SUCCESS BUT HOST DIDNT RETURN DATA",
                                        token=""
                                    )

                            return schemas.ResponseApiBase(
                                response_code=2123,
                                description='SUCCESS',
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data={
                                    "data": update_,
                                    "status": "update status success",
                                    "user_response": user_response_json
                                },
                                breakdown_errors="",
                                token=""
                            )
                        else:
                            return schemas.ResponseApiBase(
                                response_code=response.json()["response_code"],
                                description=response.json()["description"],
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data=response.json()["data"],
                                breakdown_errors="PAYMENT SERVICE RETURN ERROR",
                                token=""
                            )
                    else:
                        return schemas.ResponseApiBase(
                            response_code=10018,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=response.json()["data"],
                            breakdown_errors="PAYMENT SERVICE API NOT FOUND",
                            token=""
                        )
        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/redirect-url", response_model=Any)
async def redirect_url(
        *,
        db: Session = Depends(deps.get_db),
        _invoice_id: str = None,
        _payment_date: str = None,
        _invoice_status: str = None,
        _lean_x_sign: str = None,
        request=Depends(deps.get_params),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/call-redirect",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_MESSAGE_FROM_SWITCHER",
                                 _data=_invoice_id, db_=db)

        sign = f"_invoice_id={_invoice_id}&_payment_date={_payment_date}&_invoice_status={_invoice_status}&_lean_x_sign={_lean_x_sign}"
        dict_params = request
        account = crud.customer_bill.get_by_customer_by_invoice_no(db, _invoice=_invoice_id)
        if not account:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
        auth_key = crud.api.get
        return schemas.ResponseApiBase(
            response_code=2100,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=dict_params,
            breakdown_errors="",
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-prefund-encrypt", response_model=Any)
async def callback_prefund_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="call-prefund-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_PREFUND_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.virtual_account.get_by_virtual_account_switch_id(db, _id=callback_in["item"][
            "virtual_pool_reference"])
        account = crud.account.get_account_by_id(db, id=query_update.account_id)

        if callback_in["response_code"] == 2100:
            deps.prefund_transaction_charges(_charges=settings.PREFUND_TOPUP_CHARGES,
                                             api_key=account.parent_key,
                                             _account_id=query_update.account_id,
                                             db_=db,
                                             _transaction_amount=callback_in["item"]["prefund_collection_amount"],
                                             invoice_status="SUCCESS",
                                             invoice_no=callback_in["item"]["virtual_pool_reference"],
                                             data=callback_in)

        if query_update:

            _twilio_prefund = schemas.TwilioPrefund(
                recipient_name=query_update.full_name,
                transaction_data=schemas.TransactionData(
                    txn_date=query_update.payment_date.strftime("%B %d, %Y, %H:%M:%S %p"),
                    transaction_amount=query_update.total,
                    transaction_id=query_update.invoice_no

                )
            )

            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_prefund_notification_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                query_update.email, settings.ADMIN_LEANX_EMAIL, _twilio_prefund.json())

            _in = schemas.VirtualAccountUpdate(
                balance=Decimal(callback_in["item"]["balance"])
            )
            query_va = crud.virtual_account.get_by_virtual_account_id(db, _id=query_update.id)
            update_ = crud.virtual_account.update_self_date(db, db_obj=query_va, obj_in=_in)

            return schemas.ResponseApiBase(
                response_code=2123,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={
                    "data": update_,
                    "status": "update status success",
                    "user_response": data
                },
                breakdown_errors="",
                token=""
            )

        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-manual-prefund-encrypt", response_model=Any)
async def callback_manual_prefund_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="call-prefund-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_PREFUND_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db, _invoice=callback_in["invoice_no"])
        _customer_id = query_update.customer_id
        if query_update:
            # update bill
            update_bill = schemas.CustomerBillUpdate(
                invoice_status_id=callback_in["invoice_status_id"],
                invoice_status=callback_in["invoice_status"],
                payment_date=datetime.datetime.strptime(callback_in["original_transaction_response_time"]
                                                        , "%Y-%m-%d %H:%M:%S"),
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)
            if update_:
                # Sendgrid email
                _twilio_prefund = schemas.TwilioPrefund(
                    recipient_name=update_.full_name,
                    transaction_data=schemas.TransactionData(
                        txn_date=update_.payment_date.strftime("%B %d, %Y, %H:%M:%S %p"),
                        transaction_amount=update_.total,
                        transaction_id=update_.invoice_no

                    )
                )

                message = await asyncio.get_event_loop().run_in_executor(
                    None, SharedUtils.send_prefund_notification_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                    update_.email, settings.ADMIN_LEANX_EMAIL, _twilio_prefund.json())

                deps.add_shipment(db_=db, _account_id=query_update.account_id, _customer_id=_customer_id, _profile_id=1,
                                  _collection_id=update_.collection_id, _customer_bill_id=update_.id)
                payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                if payment_update:
                    url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                    payload = {'client_identifier': 'LEANPAY',
                               'skip_encryption': 'false',
                               f'payment_model_reference_id': callback_in["payment_model_reference_id"],
                               'amount_to_calculate': callback_in["amount"],
                               'payment_service_id': callback_in["payment_service_id"],
                               'payment_service_record_status_id': 1}
                    files = []
                    headers = {}
                    response = req.request("POST", url, headers=headers, data=payload, files=files)
                    if response.status_code == req.codes.ok:
                        if response.json()["response_code"] == 2100:
                            update_payment = schemas.PaymentRecordUpdate(
                                payment_mode=response.json()["data"]["output"]["data"][0]["switch_payment_service"],
                                payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"],
                                switch_transaction_fee=response.json()["data"]["output"]["data"][0]["best_rate"]
                                ["selected"]["paymentSwitchChargeAmount"],
                                transaction_fee=settings.PREFUND_TOPUP_CHARGES,
                                customer_id=_customer_id,
                                payment_date=datetime.datetime.strptime(
                                    callback_in["original_transaction_response_time"]
                                    , "%Y-%m-%d %H:%M:%S"),
                                reference_number=callback_in["invoice_no"],
                                data=data,
                            )
                            update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                           obj_in=update_payment)
                        else:
                            return schemas.ResponseApiBase(
                                response_code=response.json()["response_code"],
                                description=response.json()["description"],
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data=response.json()["data"],
                                breakdown_errors="PAYMENT SERVICE RETURN ERROR",
                                token=""
                            )
                    else:
                        return schemas.ResponseApiBase(
                            response_code=10018,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=response.json()["data"],
                            breakdown_errors="PAYMENT SERVICE API NOT FOUND",
                            token=""
                        )

        query_update_va = crud.virtual_account.get_by_virtual_account_unique_id(db, _id=callback_in["client_data"][
            "virtual_pool_reference"])

        account = crud.account.get_account_by_id(db, id=query_update_va.account_id)

        if callback_in["invoice_status"] == "SUCCESS":
            deps.prefund_transaction_charges(_charges=settings.PREFUND_TOPUP_CHARGES,
                                             api_key=account.parent_key,
                                             _account_id=query_update_va.account_id,
                                             db_=db,
                                             _transaction_amount=callback_in["amount"],
                                             invoice_status="SUCCESS",
                                             invoice_no=callback_in["invoice_no"],
                                             data=callback_in)

            create_prefund_record = schemas.PrefundRecordCreate(
                switch_pool_id=query_update_va.switch_payout_pool_id,
                merchant_pool_id=query_update_va.merchant_payout_pool_id,
                transaction_invoice_no=callback_in['invoice_no'],
                transaction_status="PENDING",
                value=callback_in["amount"],
                account_id=query_update_va.account_id,
                data=json.dumps(callback_in)
            )

            crud.prefund_record.create_self_date(db, obj_in=create_prefund_record)

        if query_update_va:
            _in = schemas.VirtualAccountUpdate(
                balance=query_update_va.balance - Decimal(callback_in["amount"])
            )
            query_va = crud.virtual_account.get_by_virtual_account_id(db, _id=query_update_va.id)
            update_ = crud.virtual_account.update_self_date(db, db_obj=query_va, obj_in=_in)

            return schemas.ResponseApiBase(
                response_code=2123,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={
                    "data": update_,
                    "status": "update status success",
                    "user_response": data
                },
                breakdown_errors="",
                token=""
            )

        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-payout-encrypt", response_model=Any)
async def callback_payout_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="call-payout-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_PAYOUT_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.virtual_account.get_by_virtual_account_switch_id(db, _id=callback_in["item"][
            "virtual_pool_reference"])
        account = crud.account.get_account_by_id(db, id=query_update.account_id)
        #
        if callback_in["response_code"] == 2100:
            # Sendgrid email
            # _twilio_prefund = schemas.TwilioSendEmail()
            #
            # message = await asyncio.get_event_loop().run_in_executor(
            #     None, SharedUtils.send_fund_notification_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
            #     account.users.email, settings.ADMIN_LEANX_EMAIL, _twilio_prefund)

            #     deps.payout_transaction_charges(_charges=Decimal(account.subscription_plan.payout_charges),
            #                                     api_key=account.parent_key,
            #                                     _account_id=query_update.account_id,
            #                                     db_=db,
            #                                     _transaction_amount=callback_in["item"]["prefund_collection_amount"],
            #                                     invoice_status="SUCCESS",
            #                                     invoice_no=callback_in["item"]["virtual_pool_reference"],
            #                                     data=callback_in)
            #
            # if query_update:
            #     _in = schemas.VirtualAccountUpdate(
            #         balance=Decimal(callback_in["item"]["balance"])
            #     )
            #     query_va = crud.virtual_account.get_by_virtual_account_id(db, _id=query_update.id)
            #     update_ = crud.virtual_account.update_self_date(db, db_obj=query_va, obj_in=_in)

            return schemas.ResponseApiBase(
                response_code=2123,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={
                    "data": account,
                    "status": "update status success",
                    "user_response": data
                },
                breakdown_errors="",
                token=""
            )

        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/call-invoice-payout-encrypt", response_model=Any)
async def callback_invoice_payout_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="call-payout-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_PAYOUT_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.payout_record.get_by_payout_invoice_id(db, _id=callback_in["output"]["client_data"][
            "virtual_pool_reference"])
        if not query_update:
            return schemas.ResponseApiBase(
                response_code=4529,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NOT_AVAILABLE",
                token=""
            )

        account = crud.account.get_account_by_id(db, id=query_update.account_id)

        if callback_in["response_code"] == 2100:
            deps.payout_transaction_charges(_charges=Decimal(account.subscription_plan.payout_charges),
                                            api_key=account.parent_key,
                                            _account_id=query_update.account_id,
                                            db_=db,
                                            _transaction_amount=callback_in["output"]["amount"],
                                            invoice_status="SUCCESS",
                                            invoice_no=callback_in["output"]["invoice_no"],
                                            data=callback_in)

            # Sendgrid email
            _twilio_payout = schemas.TwilioPaymentReceipt()

            message = await asyncio.get_event_loop().run_in_executor(
                None, SharedUtils.send_fund_notification_sendgrid_email, settings.ADMIN_LEANX_EMAIL,
                account.users.email, settings.ADMIN_LEANX_EMAIL, _twilio_payout)

        if query_update:
            _in = schemas.PayoutRecordUpdate(
                transaction_status="SUCCESS"
            )
            query_va = crud.payout_record.get_by_payout_record_id(db, _id=query_update.id)
            update_ = crud.payout_record.update_self_date(db, db_obj=query_va, obj_in=_in)

            query_update_va = crud.virtual_account.get_by_virtual_account_unique_id(db, _id=
            callback_in["output"]["client_data"][
                "virtual_pool_reference"])

            if query_update_va:
                _in = schemas.VirtualAccountUpdate(
                    balance=query_update_va.balance - Decimal(callback_in["amount"])
                )
                query_va = crud.virtual_account.get_by_virtual_account_id(db, _id=query_update_va.id)
                update_ = crud.virtual_account.update_self_date(db, db_obj=query_va, obj_in=_in)

            return schemas.ResponseApiBase(
                response_code=2123,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={
                    "data": update_,
                    "status": "update status success",
                    "user_response": data
                },
                breakdown_errors="",
                token=""
            )

        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/bill-payment-callback", response_model=Any)
def bill_payment_callback(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback//bill-payment-callback",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db, _invoice=callback_in["invoice_no"])
        bill_account_id = query_update.account_id
        account = crud.account.get_account_by_id(db, id=query_update.account_id)

        if query_update.bill_transaction_type_id == 4:
            bill_account_id = callback_in["client_data"]["payment_client_account_id"]
            update_payment = schemas.SubscriptionPaymentUpdate(
                payment_status_id=settings.PAID_PAYMENT,
                transaction_no=callback_in["invoice_no"]
            )
            query_payment = crud.subscription_payment. \
                get_by_subscription_payment_id(db, _id=callback_in["client_data"]["subscription_payment_id"])

            crud.subscription_payment.update_self_date(db, db_obj=query_payment, obj_in=update_payment)

        if callback_in["invoice_status"] == "SUCCESS":
            query_update.transaction_fee = calculate_transaction_fee(rate_value=query_update.transaction_fee,
                                                                     rate_type=callback_in["rate_type_reference_id"],
                                                                     amount=callback_in["amount"])
            deps.transaction_charges(_charges=Decimal(query_update.transaction_fee),
                                     api_key=account.parent_key,
                                     _account_id=bill_account_id,
                                     db_=db, _transaction_amount=callback_in["amount"],
                                     invoice_status=callback_in["invoice_status"],
                                     invoice_no=callback_in["invoice_no"],
                                     bill_transaction_type=query_update.bill_transaction_type_id)

        _customer_id = query_update.customer_id
        if query_update:
            update_bill = schemas.CustomerBillUpdate(
                invoice_status_id=callback_in["invoice_status_id"],
                invoice_status=callback_in["invoice_status"],
                payment_date=datetime.datetime.strptime(callback_in["original_transaction_response_time"]
                                                        , "%Y-%m-%d %H:%M:%S"),
            )
            update_ = crud.customer_bill.update_self_date(db, db_obj=query_update, obj_in=update_bill)

            if update_:
                deps.add_shipment(db_=db, _account_id=query_update.account_id, _customer_id=_customer_id, _profile_id=1,
                                  _collection_id=update_.collection_id, _customer_bill_id=update_.id)
                payment_update = crud.payment_record.get_by_payment_record_id(db, _id=update_.payment_record_id)
                if payment_update:
                    url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/paymentServices/ListOfPaymentServices"
                    payload = {'client_identifier': 'LEANPAY',
                               'skip_encryption': 'false',
                               f'payment_model_reference_id': callback_in["payment_model_reference_id"],
                               'amount_to_calculate': callback_in["amount"],
                               'payment_service_id': callback_in["payment_service_id"],
                               'payment_service_record_status_id': 1}
                    files = []
                    headers = {}
                    response = req.request("POST", url, headers=headers, data=payload, files=files)
                    if response.status_code == req.codes.ok:
                        if response.json()["response_code"] == 2100:
                            update_payment = schemas.PaymentRecordUpdate(
                                payment_mode=response.json()["data"]["output"]["data"][0]["switch_payment_service"],
                                payment_method=response.json()["data"]["output"]["data"][0]["unique_reference"],
                                switch_transaction_fee=response.json()["data"]["output"]["data"][0]["best_rate"]
                                ["selected"]["paymentSwitchChargeAmount"],
                                transaction_fee=query_update.transaction_fee,
                                customer_id=_customer_id,
                                payment_date=datetime.datetime.strptime(
                                    callback_in["original_transaction_response_time"]
                                    , "%Y-%m-%d %H:%M:%S"),
                                reference_number=callback_in["invoice_no"],
                                data=data,
                            )
                            update_ = crud.payment_record.update_self_date(db, db_obj=payment_update,
                                                                           obj_in=update_payment)
                            return schemas.ResponseApiBase(
                                response_code=2123,
                                description='SUCCESS',
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data={
                                    "data": update_,
                                    "status": "update status success"
                                },
                                breakdown_errors="",
                                token=""
                            )
                        else:
                            return schemas.ResponseApiBase(
                                response_code=response.json()["response_code"],
                                description=response.json()["description"],
                                app_version=settings.API_V1_STR,
                                talk_to_server_before=datetime.datetime.today(),
                                data=response.json()["data"],
                                breakdown_errors="PAYMENT SERVICE RETURN ERROR",
                                token=""
                            )
                    else:
                        return schemas.ResponseApiBase(
                            response_code=10018,
                            description='FAILED',
                            app_version=settings.API_V1_STR,
                            talk_to_server_before=datetime.datetime.today(),
                            data=response.json()["data"],
                            breakdown_errors="PAYMENT SERVICE API NOT FOUND",
                            token=""
                        )
        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/update-transaction-pool", response_model=Any)
def callback_update_transaction(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/update-transaction-pool",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_MESSAGE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)

        query_update = crud.customer_bill.get_by_customer_by_switch_invoice_no(db,
                                                                               _invoice=callback_in.get("invoice_no",
                                                                                                        None))
        bill_account_id = query_update.account_id
        account = crud.account.get_account_by_id(db, id=query_update.account_id)
        if query_update:

            if callback_in["invoice_status"] == "SUCCESS":
                deps.add_fund_collection_admin(api_key=account.parent_key,
                                               _account_id=bill_account_id,
                                               db_=db, _transaction_amount=callback_in["amount"],
                                               invoice_status=callback_in["invoice_status"],
                                               invoice_no=callback_in["invoice_no"])

            if query_update:
                return schemas.ResponseApiBase(
                    response_code=2123,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={
                        "data": callback_in,
                        "status": "update status success"
                    },
                    breakdown_errors="",
                    token=""
                )
        else:
            return schemas.ResponseApiBase(
                response_code=10017,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="INVOICE_NUMBER_NOT_AVAILABLE",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/customer-bare-encrypt", response_model=Any)
def callback_customer_bare_encrypt(
        *,
        db: Session = Depends(deps.get_db),
        data: str = Body(None),
        response_code=Body(None),
        background_task: BackgroundTasks) -> Any:
    try:

        background_task.add_task(deps.write_audit, _method="POST", _url="/callback/customer-bare-encrypt",
                                 _email="switch@leanis.com.my",
                                 _account_id="NO_DATA",
                                 _description="CALLBACK_CUSTOMER_BARE_FROM_SWITCHER",
                                 _data=data, db_=db)

        callback_in = jws.verify(data, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                                 algorithms=['HS256'])
        callback_in = json.loads(callback_in)
        return schemas.ResponseApiBase(
            response_code=2123,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={
                "data": callback_in,
                "status": "update status success"
            },
            breakdown_errors="",
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
