import datetime
import decimal
import io
import json
from typing import Any, Optional

from app.constants.payload_example import PayloadExample
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Request, UploadFile, File, \
    Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from io import BytesIO
from app.Utils.shared_utils import SharedUtils
import pandas as pd
from fastapi.responses import StreamingResponse

from app.constants.system_constant import SystemConstant
from app.core.config import settings

router = APIRouter(prefix="/payment-upload-files", tags=["payment-upload-files"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_payment_upload_file(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        list_data = crud.payment_upload_file.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_payment_upload_file(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.PaymentUploadFileCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new catalogs.
    """
    try:
        query_get = crud.payment_upload_file.get_by_name(db, title=_in.title)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        # _in.account_id = new_token["account_id"]
        query_create = crud.payment_upload_file.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_payment_upload_file(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.PaymentUploadFileUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update catalogs.
    """
    try:
        query_update = crud.payment_upload_file.get_by_payment_upload_file_id(db, _id=_id)
        if query_update:
            update_ = crud.payment_upload_file.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_payment_upload_file(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete catalogs.
    """
    try:
        query_delete = crud.payment_upload_file.get_by_page_id(db, _id=_id)
        _in = schemas.PaymentSetting(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.payment_upload_file.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-upload-bill-excel", response_model=schemas.ResponseApiBase)
def get_merchant_payment_upload_file(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        collection_id: int = ...,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available payment setting based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payment=records/merchant-payment-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_CUSTOMER_LIST_BASED_ON_ACCOUNT_ID",
                                  _data="NO_DATA", db_=db)

        db_count = crud.payment_upload_file.get_count(db=db, _id=collection_id)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.payment_upload_file.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=collection_id, record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_payment_upload_file_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search payment setting id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payment-settings/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_payment_upload_file_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.payment_upload_file.get_by_payment_upload_files_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/upload-customer-bills-excel", response_model=schemas.ResponseApiBase)
def upload_customer_bills(
        *,
        db: Session = Depends(deps.get_db),
        file: Optional[UploadFile] = File(...),
        title: str = Body(...),
        collection_id: int = Body(...),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    upload excel document.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST",
                                  _url="/payment-upload-files/upload-customer-bills-excel",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPLOAD_CUSTOMER_BILLS",
                                  _data=collection_id, db_=db)

        # create entry for payment upload
        accounts = crud.account.get_account_by_id(db, id=new_token["account_id"])
        query_upload_get = crud.payment_upload_file.get_by_name(db, title=title)
        if query_upload_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_upload_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        # _in.account_id = new_token["account_id"]
        _in = schemas.PaymentUploadFileCreate(
            title=title,
            status="COMPLETED",
            status_id=1,
            collection_id=collection_id,
        )
        query_create = crud.payment_upload_file.create_self_date(db, obj_in=_in)

        if not file:
            return schemas.ResponseApiBase(
                response_code=6520,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"message": "No upload file sent"},
                breakdown_errors="",
                token=new_token["token"]
            )
        else:
            files = BytesIO(file.file.read())
            data_read = pd.read_excel(files, engine='openpyxl')
            # data_read = xlrd.open_workbook_xls(file_contents=file.file.read())
            add_on = []
            add_schema_on = []
            excel_name = []
            excel_email = []
            excel_phone = []
            excel_amount = []
            excel_status = []
            excel_invoice = []
            excel_bill_link = []
            for w, x, y, z in zip(data_read["Name"], data_read["Email"], data_read["Amount"], data_read["Phone"]):
                add_on.append(schemas.ExcelCsvBase(
                    name=w,
                    email=x,
                    total=y,
                    phone_number=z
                ))

            for each in add_on:
                # create customer id
                customer_ids: int
                query_get_customer = crud.customer.get_by_name_account(db, full_name=each.email,
                                                                       _id=new_token["account_id"])
                if not query_get_customer:
                    customer_create = schemas.CustomerCreate(
                        account_id=new_token["account_id"],
                        full_name=each.name,
                        email=each.email,
                        phone_number=each.phone_number,
                    )
                    # add customer to db
                    new_customer = crud.customer.create_self_date(db, obj_in=customer_create)
                    customer_ids = new_customer.id
                else:
                    customer_ids = query_get_customer.id
                sub = crud.account.get_account_by_id(db, new_token["account_id"])
                payment_record_create = schemas.PaymentRecordCreate(
                    amount=each.total,
                    transaction_fee=sub.subscription_plan.fpx_charges,
                    net_amount=decimal.Decimal(each.total) + sub.subscription_plan.fpx_charges
                )

                new_payment_record = crud.payment_record.create_self_date(db, obj_in=payment_record_create)

                create_customer_bill = schemas.CustomerBillCreate(
                    full_name=each.name,
                    email=each.email,
                    phone_number=each.phone_number,
                    total=each.total,
                    invoice_no="BP-" + SharedUtils.my_random_string(10) + "-LNP",
                    account_id=int(new_token["account_id"]),
                    collection_id=_in.collection_id,
                    payment_record_id=new_payment_record.id,
                    customer_id=customer_ids,
                    price_per_quantity=1,
                    bill_transaction_type_id=SystemConstant.SYSTEM_CONS.get("bill_transaction_type", None).__getitem__(
                        "NORMAL"),
                    total_amount_with_fee=float(each.total)+float(accounts.subscription_plan.fpx_charges),
                    transaction_fee=accounts.subscription_plan.fpx_charges
                )
                add_schema_on.append(create_customer_bill)
                # create customer bill based on Excel upload
                # create_customer_bill.base_url = \
                #     str(request.base_url) + f"api/v1/customer-bills/{create_customer_bill.invoice_no}/leanpay-bills"
                create_customer_bill.base_url = f"{settings.PORTAL_UPLOAD_URL}{create_customer_bill.invoice_no}"
                create_bill = crud.customer_bill.create_self_date(db, obj_in=create_customer_bill)

                # process bill
                excel_name.append(create_bill.full_name)
                excel_email.append(create_bill.email)
                excel_phone.append(create_bill.phone_number)
                excel_amount.append(str(create_bill.total))
                excel_status.append(create_bill.invoice_status)
                excel_invoice.append(create_bill.invoice_no)
                excel_bill_link.append(create_bill.base_url)

            df_dict = {
                'Names': excel_name,
                'Email': excel_email,
                'Phone Number': excel_phone,
                'Total Amount': excel_amount,
                'Invoice No': excel_invoice,
                'Bill Link': excel_bill_link,
                'Status': excel_status
            }

            merchant_bill = json.dumps(df_dict)

            # update payment upload
            _update = schemas.PaymentUploadFileCreate(
                status="COMPLETED",
                status_id=1,
                customer_bills=merchant_bill
            )
            query_update = crud.payment_upload_file.get_by_payment_upload_files_id(db, _id=query_create.id)
            if query_update:
                update_ = crud.payment_upload_file.update_self_date(db, db_obj=query_update, obj_in=_update)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors="",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/download-excel", response_model=schemas.ResponseApiBase)
def get_payment_upload_file_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search payment setting id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payment-upload-files/download-excel",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_payment_upload_file_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.payment_upload_file.get_by_payment_upload_files_id(db, _id=_id)
        if query_uuid:
            if not query_uuid.customer_bills:
                return schemas.ResponseApiBase(
                    response_code=3344,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=query_uuid,
                    breakdown_errors=f"EMPTY CUSTOMER BILL INFORMATION",
                    token=new_token["token"]
                )

        query_deserialized = json.loads(query_uuid.customer_bills)
        df = pd.DataFrame(query_deserialized)
        output = io.BytesIO()
        writer = pd.ExcelWriter(output, engine='xlsxwriter')
        filename = query_uuid.title

        df.to_excel(writer)
        writer.save()
        xlsx_data = output.getvalue()

        if query_uuid:
            return StreamingResponse(io.BytesIO(xlsx_data),
                                     media_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                     headers={
                                         "Content-Disposition": f'attachment; filename="{filename}.xlsx"'
                                     })
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
