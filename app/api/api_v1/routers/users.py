import decimal
import json
import uuid
from datetime import datetime
from typing import Any
from sqlalchemy import func

from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from app.core.config import settings
from fastapi import APIRouter, Body, Depends, HTTPException, Security, BackgroundTasks
from fastapi.encoders import jsonable_encoder
from pydantic.networks import EmailStr
from sqlalchemy.orm import Session
from app.constants.payload_example import PayloadExample
from app.Utils.shared_utils import SharedUtils
import hashlib
from jose import jws
from app.constants.system_constant import SystemConstant

router = APIRouter(prefix="/users", tags=["users"])


@router.post("/admin/", response_model=schemas.ResponseApiBase)
def read_users(
        *,
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all users.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/users", _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]), _description="GET LIST OF USER",
                                  _data="NO_DATA", db_=db)

        cur_start_date = datetime.strptime(_in.start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(_in.end_date, "%d-%m-%Y")

        db_count = db.query(models.User).filter(
            func.date(models.User.created_at) >= cur_start_date.date(),
            func.date(models.User.created_at) <= cur_end_date.date(),
            models.User.record_status == _in.record_status
        ).count()

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.user.get_multi_search_user(db=db, skip=skip, limit=limit, _id=current_user.id,
                                                    record_status=_in.record_status,
                                                    start_date=_in.start_date,
                                                    end_date=_in.end_date, search_column=_in.search.search_column,
                                                    search_key=_in.search.search_key,
                                                    parameter_name=_in.sort.parameter_name,
                                                    sort_type=_in.sort.sort_type,
                                                    search_enable=_in.search.search_enable,
                                                    invoice_status=_in.invoice_status, table_name="users",
                                                    role=current_user.user_role.role.name)
        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_user(
        *,
        db: Session = Depends(deps.get_db),
        user_in: schemas.UserCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new user.
    """
    try:
        user = crud.user.get_by_email(db, email=user_in.email)
        if user:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="User already already exists in the system.",
                token=new_token["token"]
            )
        user = crud.user.create(db, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/me", response_model=schemas.ResponseApiBase)
def update_user_me(
        *,
        db: Session = Depends(deps.get_db),
        full_name: str = Body(None),
        phone_number: str = Body(None),
        email: EmailStr = Body(None),
        password: str = Body(None),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update own user.
    """
    try:
        current_user_data = crud.user.get(db, id=current_user.id)
        user_in = schemas.UserUpdate(**jsonable_encoder(current_user_data))
        if phone_number is not None:
            user_in.phone_number = phone_number
        if full_name is not None:
            user_in.full_name = full_name
        if email is not None:
            user_in.email = email
        if password is not None:
            user_in.password = password
        user = crud.user.update(db, db_obj=current_user_data, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/me", response_model=Any)
def read_user_me(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Get current user.
    """
    try:
        if not current_user.user_role:
            role = None
        else:
            role = current_user.user_role.role.name
        user_data = schemas.User(
            id=current_user.id,
            email=current_user.email,
            is_active=current_user.is_active,
            full_name=current_user.full_name,
            created_at=current_user.created_at,
            updated_at=current_user.updated_at,
            role_name=role,
            user_role=current_user.user_role,
        )
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user_data,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/open", response_model=schemas.User)
def create_user_open(
        *,
        db: Session = Depends(deps.get_db),
        password: str = Body(...),
        email: EmailStr = Body(...),
        full_name: str = Body(...),
        phone_number: str = Body(None),
        new_token: str = Depends(deps.get_current_new_token)
) -> Any:
    """
    Create new user without the need to be logged in.
    """
    try:
        if not settings.USERS_OPEN_REGISTRATION:
            return schemas.ResponseApiBase(
                response_code=4110,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="OPEN_REGISTRATION_OFF",
                token=new_token["token"]
            )
        user = crud.user.get_by_email(db, email=email)
        if user:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="USER_ALREADY_EXIST",
                token=new_token["token"]
            )
        user_in = schemas.UserCreate(
            password=password,
            email=email,
            full_name=full_name,
            phone_number=phone_number,
        )
        user = crud.user.create(db, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{user_id}", response_model=schemas.ResponseApiBase)
def read_user_by_id(
        user_id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        db: Session = Depends(deps.get_db),
        new_token: str = Depends(deps.get_current_new_token)
) -> Any:
    """
    Get a specific user by id.
    """
    try:
        user = crud.user.get(db, id=user_id)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{user_id}", response_model=schemas.ResponseApiBase)
def update_user(
        *,
        db: Session = Depends(deps.get_db),
        user_id: int,
        user_in: schemas.UserUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update a user.
    """
    try:
        user = crud.user.get(db, id=user_id)
        if not user:
            raise HTTPException(
                status_code=1104,
                detail="USERNAME_NOT_AVAILABLE",
            )
        user = crud.user.update(db, db_obj=user, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant", response_model=schemas.ResponseApiBase)
def create_merchant_user(
        *,
        db: Session = Depends(deps.get_db),
        user_in: schemas.UserMerchant
        = Body(..., example=PayloadExample.CREATE_MERCHANT_ACCOUNT_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant user. need to create with admin privilege
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/users/merchant",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_MERCHANT_USER",
                                  _data=user_in.json(), db_=db)

        user = crud.user.get_by_email(db, email=user_in.email)
        if user:
            return schemas.ResponseApiBase(
                response_code=8100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_EMAIL_ALREADY_EXIST",
                token=new_token["token"]
            )

        user = crud.user.get_by_full_name(db, full_name=user_in.full_name)
        if user:
            return schemas.ResponseApiBase(
                response_code=8200,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_FULL_NAME_ALREADY_EXIST",
                token=new_token["token"]
            )

        user = crud.user.get_by_phone_number(db, phone_number=user_in.phone_number)
        if user:
            return schemas.ResponseApiBase(
                response_code=8300,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_PHONE_NUMBER_ALREADY_EXIST",
                token=new_token["token"]
            )

        business_in = schemas.BusinessOwnerDetailCreate(
            created_at=datetime.today()
        )

        business_create = crud.business_owner_detail.create_self_date(db, obj_in=business_in)

        company_details_in = schemas.CompanyDetailCreate(
            created_at=datetime.today(),
            business_owner_detail_id=business_create.id,
            fav_icon=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("FAV_ICON"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("LOGO")
        )

        company_details_create = crud.company_detail.create_self_date(db, obj_in=company_details_in)

        # create user create template
        user_create = schemas.UserCreate(
            email=user_in.email,
            is_active=user_in.is_active,
            full_name=user_in.full_name,
            phone_number=user_in.phone_number,
            password=user_in.password,
            system_id=settings.MERCHANT_SYSTEM,
            created_at=datetime.today(),
            api_key=str(uuid.uuid4())
        )

        user = crud.user.create(db, obj_in=user_create)
        # adding user to role
        role_in = schemas.UserRoleCreate(
            user_id=user.id,
            role_id=6
        )

        # create account template
        merchant_id = f"LP-{SharedUtils.my_random_string(8)}-MM"
        account_in = schemas.AccountCreate(
            user_id=user.id,
            name=user.full_name,
            subscription_plan_id=1,
            system_preference_id=2,
            account_type="MASTER_MERCHANT",
            merchant_id=merchant_id,
            company_details_id=company_details_create.id,
            description=f"MERCHANT FOR {user.full_name.capitalize()}",
            created_at=datetime.today()
        )

        user_roles = crud.user_role.create(db, obj_in=role_in)
        account = crud.account.create_self_date(db, obj_in=account_in)

        # create success field template
        success_in = schemas.SuccessFieldCreate(
            additional_message=user_in.response_additional_message,
            redirect_url=user_in.redirect_url,
            account_id=account.id,
            created_at=datetime.today()
        )

        share_in = schemas.ShareSettingCreate(
            title=user_in.email,
            description=user_in.description,
            image_for_whatsapp=user_in.image_for_whatsapp,
            image_of_media_social=user_in.image_of_media_social,
            account_id=account.id,
            created_at=datetime.today()
        )

        payment_in = schemas.PaymentSettingCreate(
            account_id=account.id,
            api_interface=user_in.api_interface,
            fpx_bank_selection=user_in.fpx_bank_selection,
            created_at=datetime.today()
        )

        support_in = schemas.SupportDetailCreate(
            account_id=account.id,
            support_contact_person=user.full_name,
            support_email=user_in.email,
            support_phone_number=user_in.phone_number,
            created_at=datetime.today()
        )

        # bind option to user account
        option_create = schemas.SystemOptionCreate(
            application_name=user_in.get("application_name"),
            main_site=user_in.get("main_site"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO"),
            logo_alt=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO_ALT"),
            qr_code_logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            favicon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            meta_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_OG_IMAGE"),
            meta_description=user_in.get("meta_description"),
            background_color="#ffffff",
            background_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            primary_color="#263872",
            secondary_color="#6610f2",
            company_name=user_in.get("company_name"),
            platform_version=user_in.get("platform_version"),
            api_parent_key=user.api_key,
            account_id=account.id
        )
        option = crud.system_option.create_self_date(db, obj_in=option_create)

        success_create = crud.success_field.create_self_date(db, obj_in=success_in)
        support_create = crud.support_detail.create_self_date(db, obj_in=support_in)
        share_create = crud.share_setting.create_self_date(db, obj_in=share_in)
        payment_create = crud.payment_setting.create_self_date(db, obj_in=payment_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESSFULLY_CREATE',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "merchant_id": account.merchant_id
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=2111,
            description='ERROR',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/create/white-label", response_model=schemas.ResponseApiBase)
def create_white_label_user(
        *,
        db: Session = Depends(deps.get_db),
        user_in: schemas.UserWhiteLabel
        = Body(..., example=PayloadExample.CREATE_WHITE_LABEL_ACCOUNT_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant user. need to create with admin privilege
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/users/white-label",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_WHITE_LABEL_USER",
                                  _data=user_in.json(), db_=db)

        user = crud.user.get_by_email(db, email=user_in.email)
        if user:
            return schemas.ResponseApiBase(
                response_code=8100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_EMAIL_ALREADY_EXIST",
                token=new_token["token"]
            )

        user = crud.user.get_by_full_name(db, full_name=user_in.full_name)
        if user:
            return schemas.ResponseApiBase(
                response_code=8200,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_FULL_NAME_ALREADY_EXIST",
                token=new_token["token"]
            )

        user = crud.user.get_by_phone_number(db, phone_number=user_in.phone_number)
        if user:
            return schemas.ResponseApiBase(
                response_code=8300,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_PHONE_NUMBER_ALREADY_EXIST",
                token=new_token["token"]
            )

        business_in = schemas.BusinessOwnerDetailCreate(
            created_at=datetime.today()
        )

        business_create = crud.business_owner_detail.create_self_date(db, obj_in=business_in)

        company_details_in = schemas.CompanyDetailCreate(
            created_at=datetime.today(),
            business_owner_detail_id=business_create.id,
            fav_icon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO")
        )

        company_details_create = crud.company_detail.create_self_date(db, obj_in=company_details_in)

        # create user create template
        user_create = schemas.UserCreate(
            email=user_in.email,
            is_active=user_in.is_active,
            full_name=user_in.full_name,
            phone_number=user_in.phone_number,
            password=user_in.password,
            system_id=settings.WHITE_LABEL_SYSTEM,
            created_at=datetime.today(),
            api_key=str(uuid.uuid4())
        )

        user = crud.user.create(db, obj_in=user_create)

        # adding user to role
        role_in = schemas.UserRoleCreate(
            user_id=user.id,
            role_id=8
        )

        # create account template
        merchant_id = f"LP-{SharedUtils.my_random_string(8)}-WL"
        account_in = schemas.AccountCreate(
            user_id=user.id,
            name=user.full_name,
            subscription_plan_id=1,
            system_preference_id=2,
            account_type=Role.WHITE_LABEL.get("name", "WHITE_LABEL"),
            merchant_id=merchant_id,
            company_details_id=company_details_create.id,
            description=Role.WHITE_LABEL.get("description", "White Label"),
            created_at=datetime.today()
        )

        user_roles = crud.user_role.create(db, obj_in=role_in)
        account = crud.account.create_self_date(db, obj_in=account_in)

        option_create = schemas.SystemOptionCreate(
            application_name=user_in.application_name,
            main_site=user_in.main_site,
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO"),
            logo_alt=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO_ALT"),
            qr_code_logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            favicon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            meta_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_WHATSAPP_SHARE_IMAGE"),
            meta_description=user_in.meta_description,
            background_color="#ffffff",
            background_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_OG_IMAGE"),
            primary_color="#263872",
            secondary_color="#6610f2",
            company_name=user_in.company_name,
            platform_version=user_in.platform_version,
            api_parent_key=user.api_key,
            account_id=account.id
        )
        option = crud.system_option.create_self_date(db, obj_in=option_create)

        # create success field template
        success_in = schemas.SuccessFieldCreate(
            additional_message=user_in.response_additional_message,
            redirect_url=user_in.redirect_url,
            account_id=account.id,
            created_at=datetime.today()
        )

        share_in = schemas.ShareSettingCreate(
            title=user_in.email,
            description=user_in.description,
            image_for_whatsapp=user_in.image_for_whatsapp,
            image_of_media_social=user_in.image_of_media_social,
            account_id=account.id,
            created_at=datetime.today()
        )

        payment_in = schemas.PaymentSettingCreate(
            account_id=account.id,
            api_interface=user_in.api_interface,
            fpx_bank_selection=user_in.fpx_bank_selection,
            created_at=datetime.today()
        )

        support_in = schemas.SupportDetailCreate(
            account_id=account.id,
            support_contact_person=user.full_name,
            support_email=user_in.email,
            support_phone_number=user_in.phone_number,
            created_at=datetime.today()
        )

        transaction_pool = schemas.PoolCreate(
            pool_name="TRANSACTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL"),
        )
        collection_pool = schemas.PoolCreate(
            pool_name="COLLECTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
        )
        pay_out_pool = schemas.PoolCreate(
            pool_name="PAY_OUT",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL"),
        )
        loyalty_pool = schemas.PoolCreate(
            pool_name="LOYALTY",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("LOYALTY_POINT"),
        )

        threshold_pool = schemas.PoolCreate(
            pool_name="THRESHOLD",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL"),
        )

        prefund_pool = schemas.PoolCreate(
            pool_name="PREFUND",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
        )

        prefund_pool_create = crud.pool.create_self_date(db, obj_in=prefund_pool)
        threshold_pool_create = crud.pool.create_self_date(db, obj_in=threshold_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=transaction_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=collection_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=pay_out_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=loyalty_pool)

        success_create = crud.success_field.create_self_date(db, obj_in=success_in)
        support_create = crud.support_detail.create_self_date(db, obj_in=support_in)
        share_create = crud.share_setting.create_self_date(db, obj_in=share_in)
        payment_create = crud.payment_setting.create_self_date(db, obj_in=payment_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESSFULLY_CREATE',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "merchant_id": account.merchant_id
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=2111,
            description='ERROR',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/create/white-label/adonis-js", response_model=schemas.ResponseApiBase,
             dependencies=[Depends(deps.get_token_onboard_header)])
def create_white_label_user_adonis(
        *,
        db: Session = Depends(deps.get_db),
        message: str = Body(...),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant user. need to create with admin privilege
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/create/white-label/adonis-js",
                                  _email="LEANIS ADMIN",
                                  _account_id=99999,
                                  _description="CREATE_WHITE_LABEL_USER_ADONIS_JS",
                                  _data=message, db_=db)

        decode = jws.verify(message, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                            algorithms=['HS256'])
        st = str(decode, 'utf-8')
        user_in = json.loads(st)

        user = crud.user.get_by_email(db, email=user_in.get("email", None))
        if user:
            return schemas.ResponseApiBase(
                response_code=8100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_EMAIL_ALREADY_EXIST",
                token=""
            )

        user = crud.user.get_by_full_name(db, full_name=user_in.get("full_name", None))
        if user:
            return schemas.ResponseApiBase(
                response_code=8200,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_FULL_NAME_ALREADY_EXIST",
                token=""
            )

        user = crud.user.get_by_phone_number(db, phone_number=str(user_in.get("phone_number", None)))
        if user:
            return schemas.ResponseApiBase(
                response_code=8300,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_PHONE_NUMBER_ALREADY_EXIST",
                token=""
            )

        business_in = schemas.BusinessOwnerDetailCreate(
            created_at=datetime.today()
        )

        business_create = crud.business_owner_detail.create_self_date(db, obj_in=business_in)

        company_details_in = schemas.CompanyDetailCreate(
            created_at=datetime.today(),
            business_owner_detail_id=business_create.id,
            fav_icon=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("FAV_ICON"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image_64", None).__getitem__("LOGO")

        )

        company_details_create = crud.company_detail.create_self_date(db, obj_in=company_details_in)

        # create user create template
        user_create = schemas.UserCreate(
            email=user_in.get("email"),
            is_active=user_in.get("is_active"),
            full_name=user_in.get("full_name"),
            phone_number=str(user_in.get("phone_number")),
            password=user_in.get("password"),
            system_id=settings.WHITE_LABEL_SYSTEM,
            created_at=datetime.today(),
            api_key=str(uuid.uuid4())
        )

        user = crud.user.create_adonis(db, obj_in=user_create)

        # adding user to role
        role_in = schemas.UserRoleCreate(
            user_id=user.id,
            role_id=8
        )

        # create account template
        merchant_id = f"LP-{SharedUtils.my_random_string(8)}-WL"
        account_in = schemas.AccountCreate(
            user_id=user.id,
            name=user.full_name,
            subscription_plan_id=1,
            system_preference_id=3,
            account_type=Role.WHITE_LABEL.get("name", "WHITE_LABEL"),
            merchant_id=merchant_id,
            company_details_id=company_details_create.id,
            description=Role.WHITE_LABEL.get("description", "White Label"),
            created_at=datetime.today(),
            parent_key=user.api_key
        )

        user_roles = crud.user_role.create(db, obj_in=role_in)
        account = crud.account.create_self_date(db, obj_in=account_in)

        # bind option to user account
        option_create = schemas.SystemOptionCreate(
            application_name=user_in.get("application_name"),
            main_site=user_in.get("main_site"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO"),
            logo_alt=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO_ALT"),
            qr_code_logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            favicon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            meta_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_OG_IMAGE"),
            meta_description=user_in.get("meta_description"),
            background_color="#ffffff",
            background_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            primary_color="#263872",
            secondary_color="#6610f2",
            company_name=user_in.get("company_name"),
            platform_version=user_in.get("platform_version"),
            api_parent_key=user.api_key,
            account_id=account.id
        )
        option = crud.system_option.create_self_date(db, obj_in=option_create)

        # create success field template
        success_in = schemas.SuccessFieldCreate(
            additional_message=user_in.get("response_additional_message"),
            redirect_url=user_in.get("redirect_url"),
            account_id=account.id,
            created_at=datetime.today()
        )

        share_in = schemas.ShareSettingCreate(
            title=user_in.get("email"),
            description=user_in.get("description"),
            image_for_whatsapp=user_in.get("image_for_whatsapp"),
            image_of_media_social=user_in.get("image_of_media_social"),
            account_id=account.id,
            created_at=datetime.today()
        )

        payment_in = schemas.PaymentSettingCreate(
            account_id=account.id,
            api_interface=user_in.get("api_interface"),
            fpx_bank_selection=user_in.get("fpx_bank_selection"),
            created_at=datetime.today()
        )

        support_in = schemas.SupportDetailCreate(
            account_id=account.id,
            support_contact_person=user_in.get("full_name"),
            support_email=user_in.get("email"),
            support_phone_number=user_in.get("phone_number"),
            created_at=datetime.today()
        )

        transaction_pool = schemas.PoolCreate(
            pool_name="TRANSACTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL"),
        )
        collection_pool = schemas.PoolCreate(
            pool_name="COLLECTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
        )
        pay_out_pool = schemas.PoolCreate(
            pool_name="PAY_OUT",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL"),
        )
        loyalty_pool = schemas.PoolCreate(
            pool_name="LOYALTY",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("LOYALTY_POINT"),
        )

        threshold_pool = schemas.PoolCreate(
            pool_name="THRESHOLD",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL"),
        )

        prefund_pool = schemas.PoolCreate(
            pool_name="PREFUND",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
        )

        shipment_ = schemas.ShippingProfileCreate(
            name=f"{account.merchant_id}-DEFAULT-SHIPPING-PROFILE",
            account_id=account.id
        )
        unique_name = f'VA-{SharedUtils.my_random_string(6)}-{datetime.now().strftime("%H%M%S%f")}-PAYOUT'
        va_admin_account = crud.virtual_account.get_by_admin_pool_name(db, name=settings.PREFUND_POOL_NAME)
        virtual_account = schemas.VirtualAccountCreate(
            account_id=account.id,
            pool_name="DEFAULT_ACCOUNT",
            balance=0,
            switch_payout_pool_id=va_admin_account.switch_payout_pool_id,
            merchant_payout_pool_id=unique_name
        )

        shipment_create = crud.shipping_profile.create(db, obj_in=shipment_)
        va_create = crud.virtual_account.create_self_date(db, obj_in=virtual_account)
        prefund_pool_create = crud.pool.create_self_date(db, obj_in=prefund_pool)
        threshold_pool_create = crud.pool.create_self_date(db, obj_in=threshold_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=transaction_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=collection_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=pay_out_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=loyalty_pool)

        success_create = crud.success_field.create_self_date(db, obj_in=success_in)
        support_create = crud.support_detail.create_self_date(db, obj_in=support_in)
        share_create = crud.share_setting.create_self_date(db, obj_in=share_in)
        payment_create = crud.payment_setting.create_self_date(db, obj_in=payment_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESSFULLY_CREATE',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "merchant_id": account.merchant_id
            },
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=2111,
            description='ERROR',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant/adonis-js", response_model=schemas.ResponseApiBase,
             dependencies=[Depends(deps.get_token_onboard_header)])
def create_merchant_user_adonis(
        *,
        db: Session = Depends(deps.get_db),
        message: str = Body(...),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new merchant user. need to create with admin privilege
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/users/merchant",
                                  _email="LEANIS ADMIN",
                                  _account_id=99999,
                                  _description="CREATE_MERCHANT_USER_ADONIS_JS",
                                  _data=message, db_=db)

        decode = jws.verify(message, '9E936798778E8E21ABA8E7B620EF631E0C957BB1ADA14E4960022307A9726A09',
                            algorithms=['HS256'])
        st = str(decode, 'utf-8')
        user_in = json.loads(st)
        user = crud.user.get_by_email(db, email=user_in.get("email", None))
        if user:
            return schemas.ResponseApiBase(
                response_code=8100,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_EMAIL_ALREADY_EXIST",
                token=""
            )

        user = crud.user.get_by_full_name(db, full_name=user_in.get("full_name", None))
        if user:
            return schemas.ResponseApiBase(
                response_code=8200,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_FULL_NAME_ALREADY_EXIST",
                token=""
            )

        user = crud.user.get_by_phone_number(db, phone_number=str(user_in.get("phone_number", None)))
        if user:
            return schemas.ResponseApiBase(
                response_code=8300,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="RECORD_PHONE_NUMBER_ALREADY_EXIST",
                token=""
            )

        business_in = schemas.BusinessOwnerDetailCreate(
            created_at=datetime.today()
        )

        business_create = crud.business_owner_detail.create_self_date(db, obj_in=business_in)

        company_details_in = schemas.CompanyDetailCreate(
            created_at=datetime.today(),
            business_owner_detail_id=business_create.id,
            fav_icon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO")
        )

        company_details_create = crud.company_detail.create_self_date(db, obj_in=company_details_in)

        # create user create template
        user_create = schemas.UserCreate(
            email=user_in.get("email"),
            is_active=user_in["is_active"],
            full_name=user_in["full_name"],
            phone_number=str(user_in["phone_number"]),
            password=user_in["password"],
            system_id=settings.MERCHANT_SYSTEM,
            api_key=str(uuid.uuid4())
        )

        user = crud.user.create_adonis(db, obj_in=user_create)
        # adding user to role
        role_in = schemas.UserRoleCreate(
            user_id=user.id,
            role_id=6
        )

        # create account template
        merchant_id = f"LP-{SharedUtils.my_random_string(8)}-MM"
        user_type = user_in.get("user_type", None)
        acc_type = f"{Role.MASTER_MERCHANT.get('name', None)}"
        desc = f"MERCHANT FOR LEANPAY SDN BHD"

        if user_type == 3:
            merchant_id = f"WL-{SharedUtils.my_random_string(8)}-MM"
            role_in.role_id = 6
            acc_type = f"{Role.MASTER_MERCHANT.get('name', None)}-{Role.WHITE_LABEL.get('name', None)}"
            desc = f"MERCHANT FOR {user_in.get('parent_key', None)}"

        parent_key_merchant = user_in.get('parent_key', None)
        parent_api_query = crud.account.get_by_merchant_id(db, merchant_id=parent_key_merchant)
        if not parent_api_query:
            parent_key = None
        else:
            parent_key = parent_api_query.parent_key

        subs_id = 0
        _subs_id = user_in.get('plan', 1)
        if _subs_id == "BASIC":
            subs_id = settings.BASIC
        if _subs_id == "STANDARD":
            subs_id = settings.STANDARD
        if _subs_id == "ENTERPRISE":
            subs_id = settings.ENTERPRISE

        account_in = schemas.AccountCreate(
            user_id=user.id,
            name=user.full_name,
            account_name=user.full_name,
            subscription_plan_id=subs_id,
            system_preference_id=2,
            account_type=acc_type,
            merchant_id=merchant_id,
            company_details_id=company_details_create.id,
            description=desc,
            parent_key=parent_key
        )

        user_roles = crud.user_role.create(db, obj_in=role_in)
        account = crud.account.create(db, obj_in=account_in)

        # create success field template
        success_in = schemas.SuccessFieldCreate(
            additional_message=user_in["response_additional_message"],
            redirect_url=user_in["redirect_url"],
            account_id=account.id
        )

        share_in = schemas.ShareSettingCreate(
            title=user_in["email"],
            description=user_in["description"],
            image_for_whatsapp=user_in["image_of_media_social"],
            image_of_media_social=user_in["image_of_media_social"],
            account_id=account.id
        )

        payment_in = schemas.PaymentSettingCreate(
            account_id=account.id,
            api_interface=user_in["api_interface"],
            fpx_bank_selection=user_in["fpx_bank_selection"]
        )

        support_in = schemas.SupportDetailCreate(
            account_id=account.id,
            support_contact_person=user_in["full_name"],
            support_email=user_in["email"],
            support_phone_number=user_in["phone_number"]
        )

        transaction_pool = schemas.PoolCreate(
            pool_name="TRANSACTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL"),
        )
        collection_pool = schemas.PoolCreate(
            pool_name="COLLECTION",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
        )
        pay_out_pool = schemas.PoolCreate(
            pool_name="PAY_OUT",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL"),
        )
        loyalty_pool = schemas.PoolCreate(
            pool_name="LOYALTY",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("LOYALTY_POINT"),
        )

        threshold_pool = schemas.PoolCreate(
            pool_name="THRESHOLD",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL"),
        )

        prefund_pool = schemas.PoolCreate(
            pool_name="PREFUND",
            value=decimal.Decimal(0),
            account_id=account.id,
            api_key=account.parent_key,
            pool_type=SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
        )

        shipment_ = schemas.ShippingProfileCreate(
            name=f"{account.merchant_id}-DEFAULT-SHIPPING-PROFILE",
            account_id=account.id
        )
        unique_name = f'VA-{SharedUtils.my_random_string(6)}-{datetime.now().strftime("%H%M%S%f")}-PAYOUT'

        va_admin_account = crud.virtual_account.get_by_admin_pool_name(db, name=settings.PREFUND_POOL_NAME)
        if va_admin_account:
            virtual_account = schemas.VirtualAccountCreate(
                account_id=account.id,
                pool_name="DEFAULT_ACCOUNT",
                balance=0,
                switch_payout_pool_id=va_admin_account.switch_payout_pool_id,
                merchant_payout_pool_id=unique_name
            )

            va_create = crud.virtual_account.create_self_date(db, obj_in=virtual_account)

        # bind option to user account
        option_create = schemas.SystemOptionCreate(
            application_name=user_in.get("full_name"),
            main_site=user_in.get("email"),
            logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO"),
            logo_alt=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_LOGO_ALT"),
            qr_code_logo=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            favicon=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_FAVICON"),
            meta_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_OG_IMAGE"),
            meta_description=user_in.get("meta_description"),
            background_color="#ffffff",
            background_image=SystemConstant.SYSTEM_CONS.get("leanis_image", None).__getitem__("DEFAULT_QR_LOGO"),
            primary_color="#263872",
            secondary_color="#6610f2",
            company_name=user_in.get("company_name"),
            platform_version=user_in.get("platform_version"),
            api_parent_key=user.api_key,
            account_id=account.id
        )
        option = crud.system_option.create_self_date(db, obj_in=option_create)

        shipment_create = crud.shipping_profile.create(db, obj_in=shipment_)
        prefund_pool_create = crud.pool.create_self_date(db, obj_in=prefund_pool)
        threshold_pool_create = crud.pool.create_self_date(db, obj_in=threshold_pool)
        transaction_pool_create = crud.pool.create_self_date(db, obj_in=transaction_pool)
        collection_pool_create = crud.pool.create_self_date(db, obj_in=collection_pool)
        pay_out_pool_create = crud.pool.create_self_date(db, obj_in=pay_out_pool)
        loyalty_pool_create = crud.pool.create_self_date(db, obj_in=loyalty_pool)

        success_create = crud.success_field.create(db, obj_in=success_in)
        support_create = crud.support_detail.create(db, obj_in=support_in)
        share_create = crud.share_setting.create(db, obj_in=share_in)
        payment_create = crud.payment_setting.create(db, obj_in=payment_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESSFULLY_CREATE',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "merchant_id": account.merchant_id
            },
            token=""
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=2111,
            description='ERROR',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/{merchant_id}", response_model=schemas.ResponseApiBase,
             dependencies=[Depends(deps.get_token_onboard_header)])
def update_adonis_user(
        *,
        db: Session = Depends(deps.get_db),
        merchant_id: str,
        user_in: schemas.UserUpdate,
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update a user.
    """
    try:
        account = crud.account.get_account_merchant_id(db, id=merchant_id)
        if not account:
            raise HTTPException(
                status_code=1106,
                detail="MERCHANT_ID_NOT_AVAILABLE",
            )

        user = crud.user.get(db, account.user_id)
        if not user:
            raise HTTPException(
                status_code=1104,
                detail="USERNAME_NOT_AVAILABLE",
            )
        user = crud.user.update_adonis(db, db_obj=user, obj_in=user_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=user,
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/generate/one-time-connection", response_model=schemas.ResponseApiBase)
def one_time_connection(
        *,
        db: Session = Depends(deps.get_db),
        admin_name: str = Body(..., example={"leanis"}),
        admin_email: EmailStr = Body(..., example={"admin@leanis.com.my"})
) -> Any:
    """
    Create new user without the need to be logged in.
    """
    try:
        if not admin_name.lower() == "leanis":
            return schemas.ResponseApiBase(
                response_code=4258,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"exception": "INVALID_ADMIN_NAME"},
                breakdown_errors="INTERNAL_PROBLEM_OCCURS",
                token=""
            )
        if not admin_email == "admin@leanis.com.my":
            return schemas.ResponseApiBase(
                response_code=4259,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"exception": "INVALID_ADMIN_EMAIL"},
                breakdown_errors="INTERNAL_PROBLEM_OCCURS",
                token=""
            )

        access_id = "MM-" + SharedUtils.my_random_string(10) + "-LNP"
        access_uuid = uuid.uuid4()
        secret_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
        add = f"{access_id}|{access_uuid}|{secret_key}"
        sha_add = hashlib.sha512(add.encode())
        sha_hex = sha_add.hexdigest()

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "access_id": access_id,
                "access_uuid": access_uuid,
                "access_token": sha_hex
            },
            token=""
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
