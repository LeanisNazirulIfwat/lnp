import datetime
import json
from typing import Any
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
from sqlalchemy.ext.serializer import loads, dumps
from app.Utils.shared_utils import SharedUtils

router = APIRouter(prefix="/stores", tags=["stores"])


@router.get("/admin", response_model=schemas.ResponseApiBase)
def get_stores(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available stores.
    """
    try:
        list_data = crud.store.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_stores(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.StoreCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new stores.
    """
    try:
        query_get = crud.store.get_by_name(db, name=_in.name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token["account_id"]
        _in.unique_id = f"LP-{SharedUtils.my_random_string(10)}-STR"

        query_create = crud.store.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_stores(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.StoreUpdate = Body(None, example=PayloadExample.CREATE_PAYLOAD_ADD_STORE["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update stores.
    """
    try:
        query_update = crud.store.get_by_store_id(db, _id=_id)
        if query_update:
            _in.menu_items = json.dumps(_in.menu_items)
            update_ = crud.store.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_stores(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        api_in: schemas.Store,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete stores.
    """
    try:
        query_delete = crud.store.get_by_store_id(db, _id=_id)
        _in = schemas.Store(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.store.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"]
            )
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-stores", response_model=schemas.ResponseApiBase)
def get_merchant_stores(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available stores based on merchant id.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/stores/merchant-stores",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_LIST_OF_MERCHANT_DOMAIN",
                                  _data="NO_DATA", db_=db)

        db_count = crud.store.get_count(db=db, _id=new_token["account_id"])
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.store.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_store_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search store id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/stores/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_BANK_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.store.get_by_store_id(db, _id=_id)
        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=5011,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"INVALID_STORE_ID",
                token=""
            )
        query = loads(dumps(query_uuid))
        product_listing = []

        if product_listing:
            for prod in json.loads(query_uuid.product_list):
                product_query = crud.product.get_by_product_id(db=db, _id=prod)
                product_listing.append(product_query)

        # [product_listing.append(crud.product.get_by_product_id(db=db, _id=x)) for x in json.loads(
        # query_uuid.product_list)]

        menu_items = []
        if query_uuid.menu_items:
            for menu in json.loads(query_uuid.menu_items):
                menu_item = crud.page.get_by_page_id(db, _id=menu["page_id"])
                menu_items.append({"Label": menu["label"],
                                   "Page": menu_item})
        query.product_list = product_listing
        query.menu_items = menu_items
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("public/{_unique_id}", response_model=schemas.ResponseApiBase)
def get_store_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _unique_id: str,
        background_tasks: BackgroundTasks
) -> Any:
    """
    search store id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url=f"/stores/{_unique_id}",
                                  _email=_unique_id,
                                  _account_id="NO_DATA",
                                  _description="GET_BANK_WITH_UNIQUE_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.store.get_by_unique_id(db, _unique_id=_unique_id)
        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=5011,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"INVALID_STORE_ID",
                token=""
            )
        query = loads(dumps(query_uuid))
        product_listing = []

        if query_uuid.product_list:
            for prod in json.loads(query_uuid.product_list):
                cat_prod = []
                variant_prod = []
                product_query = crud.product.get_by_product_id(db=db, _id=prod)
                product_query_ = loads(dumps(product_query))
                product_query_schema = schemas.ProductVue(
                    name=product_query_.name,
                    price=product_query_.price,
                    weight_condition=product_query_.weight_condition,
                    account_id=product_query_.account_id,
                    quantity=product_query_.quantity,
                    value=product_query_.value,
                    title=product_query_.title,
                    base_url=product_query_.base_url,
                    product_category_list=product_query_.product_category_list,
                    record_status=product_query_.record_status,
                    code=product_query_.code,
                    short_url=product_query_.short_url,
                    limited_stock_enable=product_query_.limited_stock_enable,
                    description=product_query_.description,
                    intro=product_query_.intro,
                    physical_product_enable=product_query_.physical_product_enable,
                    created_at=product_query_.created_at,
                    min_purchase_quantity=product_query_.min_purchase_quantity,
                    footer=product_query_.footer,
                    tier_pricing_enable=product_query_.tier_pricing_enable,
                    updated_at=product_query_.updated_at,
                    id=product_query_.id,
                    max_purchase_quantity=product_query_.max_purchase_quantity,
                    image=product_query_.image,
                    product_variant_enable=product_query_.product_variant_enable,
                    weight=product_query_.weight,
                    product_product_variant=product_query_.product_product_variant,
                    product_tier_pricing=product_query_.product_tier_pricing,
                )
                for cat in json.loads(product_query.product_category_list):
                    prod = crud.product_category.get_by_product_category_id(db=db, _id=cat)
                    cat_prod.append(prod)
                product_query_schema.product_category_list = cat_prod
                for var in product_query.product_product_variant:
                    if var.custom_pricing_enable:
                        product_query_schema.custom_pricing_exist = True
                product_listing.append(product_query_schema)
            # [product_listing.append(crud.product.get_by_product_id(db=db, _id=x)) for x in json.loads(
            # query_uuid.product_list)]

        menu_items = []
        if query_uuid.menu_items:
            for menu in json.loads(query_uuid.menu_items):
                menu_item = crud.page.get_by_page_id(db, _id=menu["page_id"])
                menu_items.append({"Label": menu["label"],
                                   "Page": menu_item})
        query.product_list = product_listing
        query.menu_items = menu_items
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query,
                token="")
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
