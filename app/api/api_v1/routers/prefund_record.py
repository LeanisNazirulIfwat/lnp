import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample

router = APIRouter(prefix="/prefund-records", tags=["prefund-records"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_prefund_records(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available prefund records.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/prefund-records",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_ALL_LIST_PREFUND_RECORDS",
                                  _data=_in.json(), db_=db)

        db_count = crud.prefund_record.get_count(db=db, start_date=_in.start_date, end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.prefund_record.get_multi_search_user(db=db, skip=skip, limit=limit,
                                                              _id=new_token["account_id"],
                                                              record_status=_in.record_status,
                                                              start_date=_in.start_date,
                                                              end_date=_in.end_date,
                                                              search_column=_in.search.search_column,
                                                              search_key=_in.search.search_key,
                                                              parameter_name=_in.sort.parameter_name,
                                                              sort_type=_in.sort.sort_type,
                                                              search_enable=_in.search.search_enable,
                                                              invoice_status=_in.invoice_status,
                                                              table_name="prefund_records",
                                                              role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_prefund_records(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.PrefundRecordCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new prefund records
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/prefund-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_PREFUND_RECORD",
                                  _data=_in.json(), db_=db)

        query_get = crud.prefund_record.get_by_name(db, name=_in.switch_pool_id)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token['account_id']
        query_create = crud.prefund_record.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-prefund-records", response_model=schemas.ResponseApiBase)
def get_merchant_prefund_records(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available prefund record based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/prefund-records/merchant-prefund-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_MERCHANT_PREFUND_RECORDS_LIST",
                                  _data=_in.json(), db_=db)

        db_count = crud.prefund_record.get_count_based_account(db=db, _id=new_token["account_id"],
                                                               start_date=_in.start_date,
                                                               end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.prefund_record.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_prefund_records(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.PrefundRecordUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update prefund record.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url=f"/prefund-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_PREFUND_RECORD",
                                  _data=_in.json(), db_=db)

        query_update = crud.prefund_record.get_by_prefund_record_id(db, _id=_id)
        if query_update:
            update_ = crud.prefund_record.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_prefund_records(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete prefund records.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url=f"/prefund-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_prefund_record",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.prefund_record.get_by_prefund_record_id(db, _id=_id)
        _in = schemas.PrefundRecord(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.prefund_record.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_prefund_records_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search prefund record based on id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/prefund-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_PREFUND_RECORD_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.prefund_record.get_by_prefund_record_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/admin/bulk-prefund-update", response_model=schemas.ResponseApiBase)
def update_prefund_records(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.PrefundBulkUpdateBase = Body(..., example={
            "id": [
                0, 1, 2, 3, 4
            ],
            "transaction_status": [
                "string", "string", "string", "string"
            ],
            "image_path": [
                "string", "string", "string", "string"
            ]
        }),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update prefund record.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url=f"/admin/bulk-prefund-update",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_PREFUND_RECORD",
                                  _data=_in.json(), db_=db)

        if not len(_in.id) == len(_in.transaction_status) == len(_in.image_path):
            return schemas.ResponseApiBase(
                response_code=2336,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors=f"LIST_HAVE_DIFFERENT_LENGTH",
                token=new_token["token"]
            )

        for id_, transaction_, image_ in zip(_in.id, _in.transaction_status, _in.image_path):
            new_in = schemas.PrefundRecordUpdate(
                image_path=image_,
                transaction_status=transaction_
            )
            query_update = crud.prefund_record.get_by_prefund_record_id(db, _id=id_)
            if query_update:
                update_ = crud.prefund_record.update_self_date(db, db_obj=query_update, obj_in=_in)
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=update_,
                    token=new_token["token"])
            else:
                return schemas.ResponseApiBase(
                    response_code=2111,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=query_update,
                    breakdown_errors=f"NO_RECORD_FOUND",
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
