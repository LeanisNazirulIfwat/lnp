import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
import requests as req

router = APIRouter(prefix="/payout-records", tags=["payout-records"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_payout_records(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/payout-records",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_ALL_LIST_payout_records",
                                  _data=_in.json(), db_=db)

        db_count = crud.payout_record.get_count(db=db, start_date=_in.start_date, end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.payout_record.get_multi_search_user(db=db, skip=skip, limit=limit,
                                                             _id=new_token["account_id"],
                                                             record_status=_in.record_status,
                                                             start_date=_in.start_date,
                                                             end_date=_in.end_date,
                                                             search_column=_in.search.search_column,
                                                             search_key=_in.search.search_key,
                                                             parameter_name=_in.sort.parameter_name,
                                                             sort_type=_in.sort.sort_type,
                                                             search_enable=_in.search.search_enable,
                                                             invoice_status=_in.invoice_status,
                                                             table_name="payout_records",
                                                             role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_payout_records(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.PayoutRecordCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new payment record.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/payout-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_PAYOUT_RECORDS",
                                  _data=_in.json(), db_=db)

        query_get = crud.payout_record.get_by_name(db, name=_in.switch_invoice_id)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        _in.account_id = new_token['account_id']
        query_create = crud.payout_record.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-payout-records", response_model=schemas.ResponseApiBase)
def get_merchant_payout_records(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available payment record based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payout-records/merchant-payout-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_MERCHANT_PAYOUT_RECORDS_LIST",
                                  _data=_in.json(), db_=db)

        db_count = crud.payout_record.get_count_based_account(db=db, _id=new_token["account_id"],
                                                              start_date=_in.start_date,
                                                              end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        #############################################
        # GET PAYOUT LIST
        #############################################
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/client/payoutServices/ListOfPayoutServices"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'false',
                   'amount_to_calculate': '25',
                   'payment_model_reference_id': '1'}
        files = {}
        headers = {}
        services: List[schemas.SwitchPublicPayoutResponses] = []
        response = req.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == req.codes.ok:
            if response.json()["response_code"] == 2100:
                list_services = response.json()["data"]["output"]
                for x in list_services:
                    if x["best_rate"]["response_code"] == 2100:
                        data = schemas.SwitchPublicPayoutResponses(
                            payout_service_id=x["payout_service_id"],
                            payment_model_reference_id=x["payment_model_reference_id"],
                            payment_model_reference=x["payment_model_reference"],
                            unique_reference=x["unique_reference"],
                            payment_service=x["category_code"],
                            name=x["name"],
                            record_status=x["record_status"],
                            record_status_id=x["record_status_id"],
                            rate=0.50
                        )
                        services.append(data)
        #############################################

        #############################################

        list_data = crud.payout_record.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list_data = []
        for data in list_data:
            client = json.loads(data.data)
            _pay = schemas.IPayoutListing(**jsonable_encoder(data))
            bank_services = [data for data in services if data.payout_service_id == int(client["payout_service_id"])]
            if bank_services:
                _pay.bank_name = bank_services[0].name
            _pay.payment_description = client["payment_description"]
            _pay.recipient_reference = client["recipient_reference"]
            _pay.payout_bank_account = client["third_party_account_no"]
            new_list_data.append(_pay.dict(exclude={'data'},
                                           exclude_unset=True))

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=new_list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_payout_records(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.PayoutRecordUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update tier pricing.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url=f"/payout-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_payout_records",
                                  _data=_in.json(), db_=db)

        query_update = crud.payout_record.get_by_payout_record_id(db, _id=_id)
        if query_update:
            update_ = crud.payout_record.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_payout_records(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete payment records.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url=f"/payout-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_PAYOUT_RECORD",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.payout_record.get_by_payout_record_id(db, _id=_id)
        _in = schemas.PayoutRecord(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.payout_record.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_payout_records_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search tier pricing id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/payout-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_PAYOUT_RECORD_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.payout_record.get_by_payout_record_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
