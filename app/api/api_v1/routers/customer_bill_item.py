import datetime
from typing import Any, List, Optional
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

router = APIRouter(prefix="/customer-bill-items", tags=["customer-bill-items"])


@router.get("", response_model=schemas.ResponseApiBase)
async def get_customer_bill_items(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer_bill_items.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customer_bill_items/list", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="RETRIEVE_ALL_CUSTOMER_BILL_ITEM",
                                  _data="NO_DATA", db_=db)

        list_data = crud.customer_bill_item.get_multi_merchant(db, skip=skip, limit=limit, _id=1)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_customer_bill_items(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CustomerBillItemCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new customer_bill_items.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/customer_bill_items/CREATE", _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="CREATE_NEW_CUSTOMER_BILL_ITEM",
                                  _data=_in.json(), db_=db)

        # query_get = crud.collection_item.get_by_name(db, name=_in.name)
        # if query_get:
        #     return schemas.ResponseApiBase(
        #         response_code=4000,
        #         description='FAILED',
        #         app_version=settings.API_V1_STR,
        #         talk_to_server_before=datetime.datetime.today(),
        #         data=query_get,
        #         breakdown_errors="RECORD_ALREADY_EXIST",
        #         token=new_token["token"]
        #     )
        query_create = crud.customer_bill_item.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_customer_bill_items(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CustomerBillItemCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update customer_bill_items.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/customer_bill_items/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="UPDATE_CUSTOMER_BILL_ITEMS",
                                  _data=_in.json(), db_=db)
        query_update = crud.customer_bill_item.get_by_collection_item_id(db, _id=_id)
        if query_update:
            update_ = crud.customer_bill_item.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_customer_bill_items(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete customer_bill_items.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/customer_bill_items/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="DELETE customer_bill_items",
                                  _data=_id, db_=db)
        query_delete = crud.customer_bill_item.get_by_collection_item_id(db, _id=_id)
        _in = schemas.CollectionItem(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.customer_bill_item.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_customer_bill_item_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search customer bill item id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/customer_bill_items/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_CUSTOMER_BILL_ITEMS_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.customer_bill_item.get_by_collection_item_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/bulk-create", response_model=schemas.ResponseApiBase)
def create_bulk_customer_bill_items(
        *,
        db: Session = Depends(deps.get_db),
        _in: List[schemas.CustomerBillItemCreate],
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create bulk new collection_items.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/customer_bill_items/bulk-create",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="CREATE_BULK_CUSTOMER_BILL_ITEM",
                                  _data="NO_DATA", db_=db)
        for entry in _in:
            query_create = crud.customer_bill_item.create_self_date(db, obj_in=entry)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=_in,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )