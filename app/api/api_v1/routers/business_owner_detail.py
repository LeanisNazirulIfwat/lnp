import datetime
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

router = APIRouter(prefix="/business-owner-details", tags=["business-owner-details"])


@router.get("", response_model=schemas.ResponseApiBase)
def get_business_owner_detail(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available business owner detail.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/business-owner-details",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="RETRIEVE ALL BUSINESS OWNER DETAIL",
                                  _data="NO_DATA", db_=db)

        list_data = crud.business_owner_detail.get_multi(db, skip=skip, limit=limit)
        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": schemas.ResponseListApiBase(
                draw=len(list_data),
                record_filtered=limit - skip,
                record_total=len(list_data),
                data=list_data,
                next_page_start=skip + limit,
                next_page_length=limit,
                previous_page_start=skip,
                previous_page_length=limit,
            )
            },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_business_owner_detail(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.BusinessOwnerDetailCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new business owner detail.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/business-owner-details",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE BUSINESS OWNER DETAIL",
                                  _data="NO_DATA", db_=db)
        query_get = crud.business_owner_detail.get_by_name(db, legal_name=_in.legal_name)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        query_create = crud.business_owner_detail.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_business_owner_detail(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.BusinessOwnerDetail,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update business owner detail.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/business-owner-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_BUSINESS_OWNER_DETAIL",
                                  _data="NO_DATA", db_=db)
        query_update = crud.business_owner_detail.get_by_business_owner_detail_id(db, _id=_id)
        if query_update:
            update_ = crud.business_owner_detail.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_business_owner_detail(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete business owner detail.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/business-owner-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="RETRIEVE_ALL_BUSINESS_OWNER_DETAIL",
                                  _data="NO_DATA", db_=db)
        query_delete = crud.business_owner_detail.get_by_business_owner_detail_id(db, _id=_id)
        _in = schemas.BusinessOwnerDetail(
            id=_id,
            record_status=4,
        )
        if query_delete:
            update_ = crud.business_owner_detail.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"DATA_NOT_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_business_owner_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search business owner id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/business-owner-details/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_BUSINESS_OWNER_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.business_owner_detail.get_by_business_owner_detail_id(db, _id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
