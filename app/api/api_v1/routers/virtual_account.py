import datetime
import json
from typing import Any, List
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
from app.Utils.shared_utils import SharedUtils
import app.Utils.web_cdn_do as wcd
from sqlalchemy.ext.serializer import loads, dumps
from app.Utils.web_request import http_post_with_aiohttp
from aiohttp import ClientSession
import requests
from app.Utils.subscription_checker import Subscription

router = APIRouter(prefix="/virtual-accounts", tags=["virtual-accounts"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_virtual_account(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/virtual-accounts",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_ALL_LIST_VIRTUAL_ACCOUNT",
                                  _data="NO_DATA", db_=db)

        db_count = crud.virtual_account.get_count_admin_record(db=db, start_date=_in.start_date,
                                                               end_date=_in.end_date,
                                                               _id=new_token["account_id"],
                                                               _record=_in.record_status,
                                                               role=current_user.user_role.role.name)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.virtual_account.get_multi_search_account(db=db, skip=skip, limit=limit,
                                                                  _id=new_token["account_id"],
                                                                  record_status=_in.record_status,
                                                                  start_date=_in.start_date,
                                                                  end_date=_in.end_date,
                                                                  search_column=_in.search.search_column,
                                                                  search_key=_in.search.search_key,
                                                                  parameter_name=_in.sort.parameter_name,
                                                                  sort_type=_in.sort.sort_type,
                                                                  search_enable=_in.search.search_enable,
                                                                  invoice_status=_in.invoice_status,
                                                                  table_name="virtual_accounts",
                                                                  role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/admin", response_model=schemas.ResponseApiBase)
async def create_virtual_accounts(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.VirtualAccountCreate = Body(..., example={
            "pool_name": "ADMIN_DEFAULT_ACCOUNT"
        }),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        subs=Depends(deps.check_subscription_package),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new virtual account.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/virtual-accounts",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_VIRTUAL_ACCOUNT",
                                  _data=_in.json(), db_=db)

        unique_name = f'VA-{SharedUtils.my_random_string(6)}-{datetime.datetime.now().strftime("%H%M%S%f")}-PAYOUT'
        _in.merchant_payout_pool_id = unique_name
        query_get = crud.virtual_account.get_by_pool_name(db, name=_in.pool_name, _id=new_token['account_id'])
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        _in.account_id = new_token["account_id"]
        # create virtual pool account
        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/superadmin/virtualpools/clientCreateVirtualPool"
        payload = {'client_identifier': 'LEANPAY',
                   'skip_encryption': 'true'}
        files = [

        ]
        headers = {}

        session = ClientSession()
        status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                      url=url,
                                                                      payload=payload)
        await session.close()

        if status_code == requests.codes.ok:
            if response['response_code'] == 2100:
                _in.switch_payout_pool_id = response['data']['virtual_pool_reference']
                _in.balance = 0
                query_create = crud.virtual_account.create_self_date(db, obj_in=_in)

                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=query_create,
                    token=new_token["token"])
            else:
                return schemas.ResponseApiBase(
                    response_code=7622,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="FAILED_TO_CREATE_VIRTUAL_ACCOUNT",
                    breakdown_errors=response['breakdown_errors'],
                    token=new_token["token"]
                )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant", response_model=schemas.ResponseApiBase)
async def create_merchant_virtual_accounts(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.VirtualAccountCreate = Body(..., example={
            "pool_name": "DEFAULT_ACCOUNT"
        }),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        subs=Depends(deps.check_subscription_package),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new virtual account.
    """
    try:
        subscription_ = Subscription(models.VirtualAccount, subs, new_token["account_id"])
        checker = subscription_.check_subscription_virtual_account(db)
        if not checker:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="VA_QUOTA_LIMIT_EXCEED",
                token=""
            )

        background_tasks.add_task(deps.write_audit, _method="POST", _url="/virtual-accounts",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_NEW_MERCHANT_VIRTUAL_ACCOUNT",
                                  _data=_in.json(), db_=db)

        unique_name = f'VA-{SharedUtils.my_random_string(6)}-{datetime.datetime.now().strftime("%H%M%S%f")}-PAYOUT'
        query_account = crud.virtual_account.get_by_admin_pool_name(db, name=settings.PREFUND_POOL_NAME)
        _in.merchant_payout_pool_id = unique_name
        _in.switch_payout_pool_id = query_account.switch_payout_pool_id
        query_get = crud.virtual_account.get_by_pool_name(db, name=_in.pool_name, _id=new_token['account_id'])
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        _in.account_id = new_token["account_id"]
        _in.balance = 0
        query_create = crud.virtual_account.create_self_date(db, obj_in=_in)

        if query_create:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_create,
                token=new_token["token"])

        else:
            return schemas.ResponseApiBase(
                response_code=7622,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="FAILED_TO_CREATE_VIRTUAL_ACCOUNT",
                breakdown_errors="",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant-virtual-accounts", response_model=schemas.ResponseApiBase)
def get_merchant_virtual_accounts(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available payment record based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/virtual-accounts-records",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_MERCHANT_VIRTUAL_ACCOUNTS_LIST",
                                  _data=_in.json(), db_=db)

        db_count = crud.virtual_account.get_count(db=db, _id=new_token["account_id"],
                                                  start_date=_in.start_date,
                                                  end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.virtual_account.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_virtual_account_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search virtual account id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/virtual-accounts/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_SUBSCRIPTION_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.virtual_account.get_by_virtual_account_id(db, sub_id=_id)
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_virtual_account(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.VirtualAccountUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update merchant collection.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/update-virtual-accounts",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_MERCHANT_CLOUD_IMAGE",
                                  _data=_in, db_=db)

        query_update = crud.virtual_account.get_by_virtual_account_id(db, _id=_id)
        if query_update:
            update_ = crud.virtual_account.update_self_date(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_virtual_account(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete payment records.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url=f"/payment-records/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="DELETE_PAYMENT_RECORD",
                                  _data="NO_DATA", db_=db)

        query_delete = crud.virtual_account.get_by_virtual_account_id(db, _id=_id)
        _in = schemas.VirtualAccount(
            id=_id,
            record_status=settings.RECORD_DELETE
        )

        if query_delete:
            update_ = crud.virtual_account.update_self_date(db, db_obj=query_delete, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_delete,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/transaction-list/{_id}", response_model=schemas.ResponseApiBase)
async def get_virtual_account_transaction_list_by_id(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _id: str,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    list of transaction list.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/virtual-accounts/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_VIRTUAL_ACCOUNT_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.virtual_account.get_by_virtual_account_unique_id(db, _id=_id)
        if not query_uuid:
            return schemas.ResponseApiBase(
                response_code=5188,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"INVALID_VIRTUAL_ACCOUNT_ID",
                token=""
            )

        url = f"{settings.SWITCH_UAT_URL}api/v1/env/mobile/superadmin/virtualpools/listOfClientVirtualPoolTransactions"

        payload = {'client_identifier': 'LEANPAY',
                   'length': limit,
                   'start': skip,
                   'virtual_pool_reference': query_uuid.switch_payout_pool_id,
                   'skip_encryption': 'true'}
        files = []
        headers = {}

        session = ClientSession()
        status_code, response, content = await http_post_with_aiohttp(session=session, headers=headers,
                                                                      url=url,
                                                                      payload=payload)
        await session.close()
        new_list = schemas.ResponseListApiBase(
            draw=len(response['data']['data']),
            record_filtered=len(response['data']['data']),
            record_total=len(response['data']['data']),
            data=response['data']['data'],
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )
        if status_code == requests.codes.ok:
            if response['response_code'] == 2100:
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data={"list": new_list},
                    token=new_token["token"]
                )
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
