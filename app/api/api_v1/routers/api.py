from datetime import datetime, timedelta
import hashlib
import json
import uuid
from typing import Any, List
from sqlalchemy import text, extract, func
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, HTTPException, Security, status, BackgroundTasks, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.constants.payload_example import PayloadExample
import calendar
from app.models.customer_bill import CustomerBill
from app.models.api import Api
from app.Utils.subscription_checker import Subscription

router = APIRouter(prefix="/merchant-api", tags=["merchant-api"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_merchant_api(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-api",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]), _description="LIST OF AVAILABLE API",
                                  _data="NO_DATA", db_=db)

        db_count = len(crud.api.get_multi(db, skip=skip, limit=limit))
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.api.get_multi_search_account(db=db, skip=skip, limit=limit, _id=new_token["account_id"],
                                                      record_status=_in.record_status,
                                                      start_date=_in.start_date,
                                                      end_date=_in.end_date, search_column=_in.search.search_column,
                                                      search_key=_in.search.search_key,
                                                      parameter_name=_in.sort.parameter_name,
                                                      sort_type=_in.sort.sort_type,
                                                      search_enable=_in.search.search_enable,
                                                      invoice_status=_in.invoice_status, table_name="apis",
                                                      role=current_user.user_role.role.name)

        account = crud.account.get_multi(db, skip=skip, limit=limit)
        listing_data = []
        for data in account:
            [listing_data.append({
                "interface_name": x.interface_name,
                "callback_url": x.callback_url,
                "cancel_url": x.cancel_url,
                "time_out_url": x.time_out_url,
                "fpx_bank_selection": x.fpx_bank_selection,
                "uuid": x.uuid,
                "account_id": x.account_id,
                "created_at": x.created_at,
                "redirect_url": x.redirect_url,
                "id": x.id,
                "payment_mode": x.payment_mode,
                "payment_model": x.payment_model,
                "hash_key": x.hash_key,
                "record_status": x.record_status,
                "updated_at": x.updated_at,
                "auth_token": f"{data.merchant_id}|{x.uuid}|{x.hash_key}"
            }) for x in list_data if x.account_id == data.id]

        new_list = schemas.ResponseListApiBase(
            draw=len(listing_data),
            record_filtered=len(listing_data),
            record_total=db_count,
            data=listing_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_merchant_api(
        *,
        db: Session = Depends(deps.get_db),
        api_in: schemas.ApiCreate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        subs=Depends(deps.check_subscription_package),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Create new api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/merchant-api",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="CREATE A NEW API",
                                  _data=api_in.json(), db_=db)

        api = crud.api.get_by_name(db, interface_name=api_in.interface_name, _id=new_token["account_id"])
        if api:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=api,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        subscription_ = Subscription(Api, subs, new_token["account_id"])
        checker = subscription_.check_subscription_api(db)
        if not checker:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data="",
                breakdown_errors="QUOTA_LIMIT_EXCEED",
                token=""
            )

        account_query = crud.account.get_account_by_id(db, id=new_token["account_id"])
        access_id = account_query.merchant_id
        access_uuid = uuid.uuid4()
        secret_key = settings.API_SECRET_KEY
        add = f"{access_id}|{access_uuid}|{secret_key}"
        sha_add = hashlib.sha512(add.encode())
        sha_hex = sha_add.hexdigest()

        # assign to api_in
        api_in.uuid = access_uuid
        api_in.hash_key = sha_hex
        api_in.account_id = new_token["account_id"]
        api_in.created_at = datetime.today()
        api = crud.api.create_self_date(db, obj_in=api_in)

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data=api,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{api_id}", response_model=schemas.ResponseApiBase)
def update_merchant_api(
        *,
        db: Session = Depends(deps.get_db),
        api_id: int,
        api_in: schemas.ApiUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/merchant-api/{api_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="UPDATE AN API",
                                  _data=api_in.json(), db_=db)
        api = crud.api.get_by_api_id(db, api_id=api_id)
        if api:
            api_in.updated_at = datetime.today()
            update_api = crud.api.update_self_date(db, db_obj=api, obj_in=api_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_api,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=api,
                breakdown_errors=f"RECORD_NOT_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{api_id}", response_model=schemas.ResponseApiBase)
def delete_api(
        *,
        db: Session = Depends(deps.get_db),
        api_id: int,
        api_in: schemas.Api,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/merchant-api/{api_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="DELETE AN API",
                                  _data="NO_DATA", db_=db)
        api = crud.api.get_by_api_id(db, api_id=api_id)
        api_in = schemas.Api(
            id=api_id,
            record_status=4,
        )
        if api:
            update_api = crud.api.update(db, db_obj=api, obj_in=api_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=update_api,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=api,
                breakdown_errors=f"Query with id {api_id} not found",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_api_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    search merchant api id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-api/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_API_WITH_ID",
                                  _data="NO_DATA", db_=db)

        query_uuid = crud.api.get_by_api_id(db, api_id=_id)
        account = crud.account.get_account_by_id(db, id=new_token["account_id"])
        merchant = {
            "interface_name": query_uuid.interface_name,
            "callback_url": query_uuid.callback_url,
            "cancel_url": query_uuid.cancel_url,
            "time_out_url": query_uuid.time_out_url,
            "fpx_bank_selection": query_uuid.fpx_bank_selection,
            "uuid": query_uuid.uuid,
            "account_id": query_uuid.account_id,
            "created_at": query_uuid.created_at,
            "redirect_url": query_uuid.redirect_url,
            "id": query_uuid.id,
            "payment_mode": query_uuid.payment_mode,
            "payment_model": query_uuid.payment_model,
            "hash_key": query_uuid.hash_key,
            "record_status": query_uuid.record_status,
            "updated_at": query_uuid.updated_at,
            "auth_token": f"{account.merchant_id}|{query_uuid.uuid}|{query_uuid.hash_key}"
        }
        if query_uuid:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=merchant,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/merchant/list", response_model=schemas.ResponseApiBase)
def get_merchant_list_api(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available api.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/merchant-api",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"], _description="LIST OF AVAILABLE API",
                                  _data="NO_DATA", db_=db)

        db_count = crud.api.get_count(db=db, _id=new_token["account_id"])
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.api.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        account = crud.account.get_account_by_id(db, id=new_token["account_id"])
        listing_data = []
        [listing_data.append({
            "interface_name": x.interface_name,
            "callback_url": x.callback_url,
            "cancel_url": x.cancel_url,
            "time_out_url": x.time_out_url,
            "fpx_bank_selection": x.fpx_bank_selection,
            "uuid": x.uuid,
            "account_id": x.account_id,
            "created_at": x.created_at,
            "redirect_url": x.redirect_url,
            "id": x.id,
            "payment_mode": x.payment_mode,
            "payment_model": x.payment_model,
            "hash_key": x.hash_key,
            "record_status": x.record_status,
            "updated_at": x.updated_at,
            "auth_token": f"{account.merchant_id}|{x.uuid}|{x.hash_key}",
            "merchant_id": account.merchant_id
        }) for x in list_data]

        new_list = schemas.ResponseListApiBase(
            draw=len(listing_data),
            record_filtered=len(listing_data),
            record_total=db_count,
            data=listing_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.today(),
                data={"list": schemas.ResponseListApiBase(
                    draw=len(list_data),
                    record_filtered=limit - skip,
                    record_total=len(list_data),
                    data=list_data,
                    next_page_start=skip + limit,
                    next_page_length=limit,
                    previous_page_start=skip,
                    previous_page_length=limit,
                )
                },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/auth-key/merchant-dashboard/{_uuid}", response_model=schemas.ResponseApiBase)
async def get_auth_key_dashboard_merchant(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    merchant dashboard.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/admin-merchant-dashboard",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_AUTH_KEY_DASHBOARD",
                                  _data="NO_DATA", db_=db)

        yesterday_date = datetime.today() - timedelta(days=1)
        last_week_date = datetime.today() - timedelta(days=6)
        # last_week_date = datetime.today() - timedelta(weeks=1)
        last_month_date = datetime.today().date() - timedelta(days=30)
        last_year_date = datetime.today().date() - timedelta(days=365)

        monthly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                     CustomerBill.api_key == _uuid,
                                                     extract('month',
                                                             CustomerBill.created_at) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        yesterday_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.api_key == _uuid,
                                                       extract("day", CustomerBill.created_at) == yesterday_date.day,
                                                       extract("month",
                                                               CustomerBill.created_at) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.api_key == _uuid,
                                                       func.date(CustomerBill.created_at) >= last_week_date
                                                       .date()).offset(skip).limit(limit).all()

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.api_key == _uuid,
                                                   extract("day", CustomerBill.created_at) == datetime.today().day,
                                                   extract("month", CustomerBill.created_at) == datetime.today().month) \
            .offset(skip).limit(limit).all()

        last_month_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                        CustomerBill.api_key == _uuid,
                                                        extract("month",
                                                                CustomerBill.created_at) == last_month_date.month) \
            .offset(skip).limit(limit).all()

        year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                  CustomerBill.api_key == _uuid,
                                                  extract("year",
                                                          CustomerBill.created_at) == datetime.today().year) \
            .offset(skip).limit(limit).all()

        last_year_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.api_key == _uuid,
                                                       extract("year",
                                                               CustomerBill.created_at) == last_year_date.year) \
            .offset(skip).limit(limit).all()

        total_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.api_key == _uuid) \
            .offset(skip).limit(limit).all()

        total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
        total_last_monthly_sale = sum(c.customer_bill_payment_record.amount for c in last_month_sale)
        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)
        total_yesterday_sale = sum(c.customer_bill_payment_record.amount for c in yesterday_sale)
        total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
        total_year_sale = sum(c.customer_bill_payment_record.amount for c in year_sale)
        total_last_year_sale = sum(c.customer_bill_payment_record.amount for c in last_year_sale)
        total_all_sale = sum(c.customer_bill_payment_record.amount for c in total_sale)

        total_monthly_count = len(monthly_sale)
        total_last_monthly_count = len(last_month_sale)
        total_today_count = len(today_sale)
        total_yesterday_count = len(yesterday_sale)
        total_last_week_count = len(last_week_sale)
        total_year_count = len(year_sale)
        total_last_year_count = len(last_year_sale)
        total_all_count = len(total_sale)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, datetime.today().year)
        yesterday_range, last_range_month, last_range_year = schemas.GenerateReport \
            .get_range_date(last_month_date.month, datetime.today().year)
        last_week_range = await schemas.GenerateReport.get_range_week_dynamic()

        mini_range_day, mini_range_month, mini_range_year, mini_yesterday_range, mini_last_range_month \
            , mini_last_range_year, mini_last_week_range = ([] for _ in range(7))

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.created_at.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1
            [mini_range_day.append(x.y_axis) for x in range_day]
        else:
            [mini_range_day.append(0) for x in range_day]

        if monthly_sale:
            for _day in monthly_sale:
                day_ = _day.created_at.day
                amount = float(_day.total)
                for y in range_month:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
            [mini_range_month.append(x.y_axis) for x in range_month]
        else:
            [mini_range_month.append(0) for x in range_month]

        if year_sale:
            for _month in year_sale:
                month_ = _month.created_at.month
                amount = float(_month.total)
                for z in range_year:
                    if z.min_value == month_:
                        z.y_axis = z.y_axis + amount
                        z.count += 1
            [mini_range_year.append(x.y_axis) for x in range_year]
        else:
            [mini_range_year.append(0) for x in range_year]

        if yesterday_sale:
            for _yesterday_date in yesterday_sale:
                yesterday_date_ = float(_yesterday_date.created_at.strftime("%H.%M"))
                amount = float(_yesterday_date.total)
                for xx in yesterday_range:
                    if xx.min_value <= yesterday_date_ <= xx.max_value:
                        xx.y_axis = xx.y_axis + amount
                        xx.count += 1
            [mini_yesterday_range.append(x.y_axis) for x in yesterday_range]
        else:
            [mini_yesterday_range.append(0) for x in yesterday_range]

        if last_month_sale:
            for _last_day in last_month_sale:
                last_day_ = _last_day.created_at.day
                amount = float(_last_day.total)
                for yy in last_range_month:
                    if yy.min_value == last_day_:
                        yy.y_axis = yy.y_axis + amount
                        yy.count += 1
            [mini_last_range_month.append(x.y_axis) for x in last_range_month]
        else:
            [mini_last_range_month.append(0) for x in last_range_month]

        if last_year_sale:
            for last_month in monthly_sale:
                last_month_ = last_month.created_at.month
                amount = float(last_month.total)
                for zz in last_range_year:
                    if zz.min_value == last_month_:
                        zz.y_axis = zz.y_axis + amount
                        zz.count += 1
            [mini_last_range_year.append(x.y_axis) for x in last_range_year]
        else:
            [mini_last_range_year.append(0) for x in last_range_year]

        if last_week_sale:
            for _week in last_week_sale:
                week_ = _week.created_at.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "days": {
                    "today_sale": total_today_sale,
                    "today_range": range_day,
                    "mini_today_range": mini_range_day,
                    "yesterday_sale": total_yesterday_sale,
                    "yesterday_range": yesterday_range,
                    "mini_yesterday_range": mini_yesterday_range,
                    "total_today_count": total_today_count,
                    "total_yesterday_count": total_yesterday_count
                },
                "months": {
                    "this_month_sale": total_monthly_sale,
                    "last_month_sale": total_last_monthly_sale,
                    "monthly_range": range_month,
                    "mini_monthly_range": mini_range_month,
                    "last_monthly_range": last_range_month,
                    "mini_last_monthly_range": mini_last_range_month,
                    "total_monthly_count": total_monthly_count,
                    "total_last_monthly_count": total_last_monthly_count
                },
                "years": {
                    "yearly_sale": total_year_sale,
                    "last_year_sale": total_last_year_sale,
                    "yearly_range": range_year,
                    "mini_yearly_range": mini_range_year,
                    "last_yearly_range": last_range_year,
                    "mini_last_yearly_range": mini_last_range_year,
                    "total_year_count": total_year_count,
                    "total_last_year_count": total_last_year_count
                },
                "others": {
                    "total_sales_received": total_all_sale,
                    "last_week_range": last_week_range,
                    "total_last_week_sales": total_last_week_sale,
                    "mini_last_week_range": mini_last_week_range,
                    "total_last_week_count": total_last_week_count,
                    "total_all_count": total_all_count
                }
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/auth-key/{_uuid}/dashboard-generate-report/day-{date}", response_model=schemas.ResponseApiBase)
def get_auth_key_day_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        date: str,
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report day
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-day",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_DAY",
                                  _data="NO_DATA", db_=db)

        current_date = datetime.strptime(date, "%d-%m-%Y")

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(current_date.month, datetime.today().year)

        today_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                   CustomerBill.api_key == _uuid,
                                                   func.date(CustomerBill.created_at) == current_date
                                                   .date()).offset(skip).limit(limit).all()

        total_today_sale = sum(c.customer_bill_payment_record.amount for c in today_sale)

        if today_sale:
            for _date in today_sale:
                date_ = float(_date.created_at.strftime("%H.%M"))
                amount = float(_date.total)
                for x in range_day:
                    if x.min_value <= date_ <= x.max_value:
                        x.y_axis = x.y_axis + amount
                        x.count += 1

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-today-sale": total_today_sale,
                "total-today-count": int(len(today_sale)),
                "today-graph": range_day
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/auth-key/{_uuid}/dashboard-generate-report/month-{month}", response_model=schemas.ResponseApiBase)
def get_auth_key_month_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        month: int = 1,
        year: int = 2022,
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        # populate graph
        last_month_range_date = []
        range_month = calendar.monthrange(year=year, month=month)
        for x in range(1, range_month[1] + 1):
            xdate = datetime.strptime(f"{x}-{month}-{year}", "%d-%m-%Y")
            xdate_name = xdate.strftime("%d-%b-%Y")
            last_month_range_date.append(
                schemas.ReportCreate(x_axis=f"{xdate_name}", y_axis=0, min_value=float(x), max_value=0, count=0))

        monthly_sale = db.query(models.CustomerBill).filter(models.CustomerBill.invoice_status == "SUCCESS",
                                                            models.CustomerBill.api_key == _uuid,
                                                            extract('month',
                                                                    models.CustomerBill.created_at) == month,
                                                            extract('year',
                                                                    models.CustomerBill.created_at) == year,
                                                            ) \
            .order_by(text("created_at desc")).offset(skip).limit(limit).all()

        invoice = "invoice_no"
        filters = [x for x in monthly_sale if "BP-D905D55A04-LNP" in x.invoice_no]

        if monthly_sale:
            total_monthly_sale = sum(c.customer_bill_payment_record.amount for c in monthly_sale)
            for _day in monthly_sale:
                day_ = _day.created_at.day
                amount = float(_day.total)
                for y in last_month_range_date:
                    if y.min_value == day_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
        else:
            total_monthly_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_monthly_sale,
                "total-monthly-count": int(len(monthly_sale)),
                "monthly-graph": last_month_range_date
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/auth-key/{_uuid}/dashboard-generate-report/last-week-{_date}", response_model=schemas.ResponseApiBase)
async def get_auth_key_last_week_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _date: str,
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    admin dashboard.
    """
    try:

        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-monthly",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_MONTHLY",
                                  _data="NO_DATA", db_=db)

        _date_start = datetime.strptime(_date, "%d-%m-%Y")
        last_week_date = _date_start - timedelta(days=6)

        # populate graph
        last_week_range = await schemas.GenerateReport.get_range_week_based_on_date(_date=_date)
        last_week_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                       CustomerBill.api_key == _uuid,
                                                       func.date(CustomerBill.created_at) >= last_week_date
                                                       .date()) \
            .order_by(text("created_at desc")).offset(skip).limit(limit).all()

        mini_last_week_range = []
        if last_week_sale:
            total_last_week_sale = sum(c.customer_bill_payment_record.amount for c in last_week_sale)
            for _week in last_week_sale:
                week_ = _week.created_at.strftime("%A").upper()
                amount = float(_week.total)
                for xy in last_week_range:
                    if xy.min_value == week_:
                        xy.y_axis = xy.y_axis + amount
                        xy.count += 1
            [mini_last_week_range.append(x.y_axis) for x in last_week_range]
        else:
            [mini_last_week_range.append(0) for x in last_week_range]
            total_last_week_sale = 0


        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-weekly-sale": total_last_week_sale,
                "total-weekly-count": int(len(last_week_sale)),
                "weekly-graph": last_week_range
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.get("/auth-key/{_uuid}/dashboard-generate-report/{year}", response_model=schemas.ResponseApiBase)
def get_auth_key_year_sale_report(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        year: int,
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"], Role.MERCHANT["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    generate report year
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/dashboard/dashboard-generate-report-year",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DASHBOARD_GENERATE_REPORT_YEAR",
                                  _data="NO_DATA", db_=db)

        # populate graph
        range_day, range_month, range_year = schemas.GenerateReport.get_range_date(datetime.today().month, year)

        yearly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == "SUCCESS",
                                                    CustomerBill.api_key == _uuid,
                                                    extract('year',
                                                            CustomerBill.created_at) == year) \
            .offset(skip).limit(limit).all()

        if yearly_sale:
            total_yearly_sale = sum(c.customer_bill_payment_record.amount for c in yearly_sale)

            for _month in yearly_sale:
                month_ = _month.created_at.month
                amount = float(_month.total)
                for y in range_year:
                    if y.min_value == month_:
                        y.y_axis = y.y_axis + amount
                        y.count += 1
        else:
            total_yearly_sale = 0

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={
                "total-monthly-sale": total_yearly_sale,
                "total-monthly-count": int(len(yearly_sale)),
                "monthly-graph": range_year
            },
            breakdown_errors="",
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )
