import datetime
import json
from typing import Any
from app.core.config import settings
from app import models, schemas, crud
from app.api import deps
from app.constants.role import Role
from fastapi import APIRouter, Depends, Security, BackgroundTasks, Request, Body
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.Utils.shared_utils import SharedUtils
from app.constants.payload_example import PayloadExample
from app.Utils.subscription_checker import Subscription
from sqlalchemy.ext.serializer import loads, dumps

router = APIRouter(prefix="/collections", tags=["collections"])


@router.post("/admin/list", response_model=schemas.ResponseApiBase)
def get_collections(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token_white_label),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/collections/admin/list",
                                  _email=current_user.email,
                                  _account_id=json.dumps(new_token["account_id"]),
                                  _description="GET_COLLECTION_LIST",
                                  _data=_in.json(), db_=db)
        db_count = crud.collection.get_count(db=db, start_date=_in.start_date,
                                             end_date=_in.end_date, record_status=_in.record_status)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.collection.get_multi_search_account(db=db, skip=skip, limit=limit, _id=new_token["account_id"],
                                                             record_status=_in.record_status,
                                                             start_date=_in.start_date,
                                                             end_date=_in.end_date,
                                                             search_column=_in.search.search_column,
                                                             search_key=_in.search.search_key,
                                                             parameter_name=_in.sort.parameter_name,
                                                             sort_type=_in.sort.sort_type,
                                                             search_enable=_in.search.search_enable,
                                                             invoice_status=_in.invoice_status,
                                                             table_name="collections",
                                                             role=current_user.user_role.role.name)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list
                      },
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list
                  },
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("", response_model=schemas.ResponseApiBase)
def create_collections(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CollectionCreate
        = Body(..., example=PayloadExample.CREATE_COLLECTION_OPTION_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        subs=Depends(deps.check_subscription_package),
) -> Any:
    """
    Create new collection.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/collections/create",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_COLLECTION_PAYMENT_METHOD",
                                  _data=_in.json(), db_=db)
        query_get = crud.collection.get_by_name(db, title=_in.title)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )

        subscription_ = Subscription(models.Collection, subs, new_token["account_id"])
        checker = subscription_.check_subscription_payment_form(db)
        if not checker:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="QUOTA_LIMIT_EXCEED",
                token=""
            )

        if _in.enable_quantity_limit:
            if _in.quantity_limit <= 0:
                return schemas.ResponseApiBase(
                    response_code=5789,
                    description='FAILED',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data="",
                    breakdown_errors="QUANTITY_LIMIT_MUST_BE_ABOVE_ZERO",
                    token=""
                )

        if _in.min_amount <= 0:
            return schemas.ResponseApiBase(
                response_code=5792,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="MIN_AMOUNT_MUST_BE_ZERO_AND_ABOVE",
                token=""
            )

        _in.account_id = new_token["account_id"]
        _in.uuid = "CL-" + SharedUtils.my_random_string(10) + "-LNP"
        # check uuid and recreate if same
        query_uuid = crud.collection.get_by_uuid(db, uuid=_in.uuid)
        if query_uuid:
            while query_uuid.uuid == _in.uuid:
                _in.uuid = "CL-" + SharedUtils.my_random_string(10) + "-LNP"

        query_create = crud.collection.create_self_date(db, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_create,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/bill-method", response_model=schemas.ResponseApiBase)
def create_collections_bill_method(
        *,
        db: Session = Depends(deps.get_db),
        _in: schemas.CollectionCreateBill
        = Body(..., example=PayloadExample.CREATE_COLLECTION_BILL_OPTION_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        subs=Depends(deps.check_subscription_package)
) -> Any:
    """
    Create new collection.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="POST", _url="/collections/bill-method",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="CREATE_COLLECTION_METHOD_BILL",
                                  _data=_in.json(), db_=db)
        query_get = crud.collection.get_by_name(db, title=_in.title)
        if query_get:
            return schemas.ResponseApiBase(
                response_code=4000,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_get,
                breakdown_errors="RECORD_ALREADY_EXIST",
                token=new_token["token"]
            )
        subscription_ = Subscription(models.Collection, subs, new_token["account_id"])
        checker_ = subscription_.check_subscription_bill_form(db)
        if not checker_:
            return schemas.ResponseApiBase(
                response_code=5788,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data="",
                breakdown_errors="QUOTA_LIMIT_EXCEED",
                token=""
            )

        _in.account_id = new_token["account_id"]
        _in.uuid = "CL-" + SharedUtils.my_random_string(10) + "-LNP"

        # check uuid and recreate if same
        query_uuid = crud.collection.get_by_uuid(db, uuid=_in.uuid)
        if query_uuid:
            while query_uuid.uuid == _in.uuid:
                _in.uuid = "CL-" + SharedUtils.my_random_string(10) + "-LNP"
        collection_create = schemas.CollectionCreate(
            title=_in.title,
            collection_method=_in.collection_method,
            base_url=_in.base_url,
            min_amount=_in.min_amount,
            description=_in.description,
            fixed_amount=_in.fixed_amount,
            allow_customers_to_enter_quantity=_in.allow_customers_to_enter_quantity,
            enable_quantity_limit=_in.enable_quantity_limit,
            quantity_limit=_in.quantity_limit,
            enable_total_amount_tracking=_in.enable_total_amount_tracking,
            enable_billing_address=_in.enable_billing_address,
            disable_payment_when_target_reached=_in.disable_payment_when_target_reached,
            share_setting_id=_in.share_setting_id,
            success_field_id=_in.success_field_id,
            payment_setting_id=_in.payment_setting_id,
            support_detail_id=_in.support_detail_id,
            account_id=int(new_token["account_id"]),
            uuid=_in.uuid
        )
        # create collection
        query_create = crud.collection.create_self_date(db, obj_in=collection_create)
        collection_create_view = schemas.CollectionInDB(
            title=_in.title,
            collection_method=_in.collection_method,
            base_url=_in.base_url,
            min_amount=_in.min_amount,
            description=_in.description,
            fixed_amount=_in.fixed_amount,
            allow_customers_to_enter_quantity=_in.allow_customers_to_enter_quantity,
            enable_quantity_limit=_in.enable_quantity_limit,
            quantity_limit=_in.quantity_limit,
            enable_total_amount_tracking=_in.enable_total_amount_tracking,
            enable_billing_address=_in.enable_billing_address,
            disable_payment_when_target_reached=_in.disable_payment_when_target_reached,
            share_setting_id=_in.share_setting_id,
            success_field_id=_in.success_field_id,
            payment_setting_id=_in.payment_setting_id,
            support_detail_id=_in.support_detail_id,
            account_id=int(new_token["account_id"]),
            uuid=_in.uuid,
            id=query_create.id
        )
        for x in _in.product_listing:
            create_schema = schemas.CollectionItemCreate(
                collection_id=query_create.id,
                product_id=x.product_id,
                amount=x.amount,
                quantity=x.quantity,
                total_amount=x.total_amount
            )
            bill_collection_create = crud.collection_item.create_self_date(db, obj_in=create_schema)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=collection_create_view,
            token=new_token["token"])

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/{_id}", response_model=schemas.ResponseApiBase)
def update_collections(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CollectionUpdate,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update catalogs.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/collections/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_COLLECTION",
                                  _data=_in.json(), db_=db)
        query_update = crud.collection.get_by_collection_id(db, _id=_id)
        if query_update:
            update_ = crud.collection.update(db, db_obj=query_update, obj_in=_in)
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=update_,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_update,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.put("/bill-method/{_id}", response_model=schemas.ResponseApiBase)
def update_collections_bill_method(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        _in: schemas.CollectionUpdateBill,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Update collection bill.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="PUT", _url="/collections/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="UPDATE_COLLECTION_METHOD_BILL",
                                  _data=_in.json(), db_=db)
        query_update = crud.collection.get_by_collection_id(db, _id=_id)
        if query_update:
            collection_update = schemas.CollectionUpdate(
                title=_in.title,
                collection_method=_in.collection_method,
                base_url=_in.base_url,
                min_amount=_in.min_amount,
                description=_in.description,
                fixed_amount=_in.fixed_amount,
                allow_customers_to_enter_quantity=_in.allow_customers_to_enter_quantity,
                enable_quantity_limit=_in.enable_quantity_limit,
                quantity_limit=_in.quantity_limit,
                enable_total_amount_tracking=_in.enable_total_amount_tracking,
                enable_billing_address=_in.enable_billing_address,
                disable_payment_when_target_reached=_in.disable_payment_when_target_reached,
                share_setting_id=_in.share_setting_id,
                success_field_id=_in.success_field_id,
                payment_setting_id=_in.payment_setting_id,
                support_detail_id=_in.support_detail_id,
                account_id=int(new_token["account_id"]),
                uuid=_in.uuid,
                record_status=_in.record_status
            )
            update_ = crud.collection.update(db, db_obj=query_update, obj_in=collection_update)
            for _data in _in.product_listing:
                if _data["id"]:
                    cl_item_update_query = crud.collection_item.get_by_collection_item_id(db, _id=_data["id"])
                    if cl_item_update_query:
                        cl_item_update = schemas.CollectionItemUpdate(
                            collection_id=_data["collection_id"],
                            product_id=_data["product_id"],
                            amount=_data["amount"],
                            quantity=_data["quantity"],
                            total_amount=_data["total_amount"]
                        )
                        update_cl_item = crud.collection_item.update(db, db_obj=cl_item_update_query,
                                                                     obj_in=cl_item_update)
                else:
                    create_schema = schemas.CollectionItemCreate(
                        collection_id=_data["collection_id"],
                        product_id=_data["product_id"],
                        amount=_data["amount"],
                        quantity=_data["quantity"],
                        total_amount=_data["total_amount"]
                    )
                    bill_collection_create = crud.collection_item.create_self_date(db, obj_in=create_schema)

            collection_item_1 = crud.collection_item.get_by_collection_id(db, _id=_id)
            collection_item_1 = [x.id for x in collection_item_1]
            collection_item_2 = []
            if _in.product_listing:
                for x in _in.product_listing:
                    collection_item_2.append(x['id'])
                diff_collection = list(set(collection_item_1) - set(collection_item_2))
                if len(collection_item_1) > len(collection_item_2):
                    for item in diff_collection:
                        crud.collection_item.remove(db, id=item)
                else:
                    for item_x in _in.product_listing:
                        for item_y in diff_collection:
                            if item_x['id'] == item_y:
                                create_ = schemas.CollectionItemCreate(
                                    collection_id=item_x["collection_id"],
                                    product_id=item_x["product_id"],
                                    amount=item_x["amount"],
                                    quantity=item_x["quantity"],
                                    total_amount=item_x["total_amount"]
                                )
                                crud.collection_item.create_self_date(db, obj_in=create_)

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=_in,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=2111,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=_in,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.delete("/{_id}", response_model=schemas.ResponseApiBase)
def delete_collections(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Delete catalogs.
    """
    background_tasks.add_task(deps.write_audit, _method="DELETE", _url="/collections/{_id}",
                              _email=current_user.email,
                              _account_id=new_token["account_id"],
                              _description="DELETE_COLLECTION_BASED_ON_ID",
                              _data=_id, db_=db)
    query_delete = crud.collection.get_by_collection_id(db, _id=_id)
    _in = schemas.Collection(
        id=_id,
        account_id=new_token["account_id"],
        record_status=4,
    )
    if query_delete:
        update_ = crud.collection.update(db, db_obj=query_delete, obj_in=_in)
        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=update_,
            token=new_token["token"])
    else:
        return schemas.ResponseApiBase(
            response_code=3333,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data=query_delete,
            breakdown_errors=f"NO_RECORD_FOUND",
            token=new_token["token"]
        )


@router.get("/{_uuid}/bills", response_model=schemas.ResponseApiBase)
def get_collections_uuid(
        *,
        db: Session = Depends(deps.get_db),
        _uuid: str,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search link uuid to create bill.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/collections/{_uuid/bills}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_DATA_BASED_ON_UUID",
                                  _data=_uuid, db_=db)
        query_uuid = crud.collection.get_by_uuid(db, uuid=_uuid)
        if query_uuid:
            query_uuid.base_url = str(request.base_url) + f"api/v1/collections/{query_uuid.uuid}/leanpay-bills"
            if query_uuid.collection_method == 2:
                new_schema = schemas.CollectionMethodView(
                    title=query_uuid.title,
                    base_url=query_uuid.base_url,
                    short_url=query_uuid.short_url,
                    min_amount=query_uuid.min_amount,
                    tax=query_uuid.tax,
                    discount=query_uuid.discount,
                    custom_field=query_uuid.custom_field,
                    description=query_uuid.description,
                    secondary_description=query_uuid.secondary_description,
                    footer_description=query_uuid.footer_description,
                    collection_method=query_uuid.collection_method,
                    record_status=query_uuid.record_status,
                    fixed_amount=query_uuid.fixed_amount,
                    allow_customers_to_enter_quantity=query_uuid.allow_customers_to_enter_quantity,
                    enable_quantity_limit=query_uuid.enable_quantity_limit,
                    quantity_limit=query_uuid.quantity_limit,
                    enable_total_amount_tracking=query_uuid.enable_total_amount_tracking,
                    enable_billing_address=query_uuid.enable_billing_address,
                    total_target_amount=query_uuid.total_target_amount,
                    disable_payment_when_target_reached=query_uuid.disable_payment_when_target_reached,
                    share_setting_id=query_uuid.share_setting_id,
                    success_field_id=query_uuid.success_field_id,
                    payment_setting_id=query_uuid.payment_setting_id,
                    account_id=query_uuid.account_id,
                    collection_share_setting=query_uuid.collection_share_setting,
                    collection_payment_setting=query_uuid.collection_payment_setting,
                    collection_success_field=query_uuid.collection_success_field,
                    collection_support_detail=query_uuid.collection_support_detail
                )
                query_item = crud.collection_item.get_item_by_collection_id(db, _id=query_uuid.id)
                new_schema.product_listing = query_item
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=new_schema,
                    token=new_token["token"])

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_id}", response_model=schemas.ResponseApiBase)
def get_collection_by_id(
        *,
        db: Session = Depends(deps.get_db),
        _id: int,
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search collection id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/collections/{_id}",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_COLLECTION_DETAILS_WITH_ID",
                                  _data=_id, db_=db)

        query_uuid = crud.collection.get_by_collection_id(db, _id=_id)
        if query_uuid:
            query_uuid.base_url = str(request.base_url) + f"api/v1/collections/{query_uuid.uuid}/leanpay-bills"
            if query_uuid.collection_method == 2:
                new_schema = schemas.CollectionMethodView(
                    title=query_uuid.title,
                    base_url=query_uuid.base_url,
                    short_url=query_uuid.short_url,
                    min_amount=query_uuid.min_amount,
                    tax=query_uuid.tax,
                    discount=query_uuid.discount,
                    custom_field=query_uuid.custom_field,
                    description=query_uuid.description,
                    secondary_description=query_uuid.secondary_description,
                    footer_description=query_uuid.footer_description,
                    collection_method=query_uuid.collection_method,
                    record_status=query_uuid.record_status,
                    fixed_amount=query_uuid.fixed_amount,
                    allow_customers_to_enter_quantity=query_uuid.allow_customers_to_enter_quantity,
                    enable_quantity_limit=query_uuid.enable_quantity_limit,
                    quantity_limit=query_uuid.quantity_limit,
                    enable_total_amount_tracking=query_uuid.enable_total_amount_tracking,
                    enable_billing_address=query_uuid.enable_billing_address,
                    total_target_amount=query_uuid.total_target_amount,
                    disable_payment_when_target_reached=query_uuid.disable_payment_when_target_reached,
                    share_setting_id=query_uuid.share_setting_id,
                    success_field_id=query_uuid.success_field_id,
                    payment_setting_id=query_uuid.payment_setting_id,
                    account_id=query_uuid.account_id,
                    collection_share_setting=query_uuid.collection_share_setting,
                    collection_payment_setting=query_uuid.collection_payment_setting,
                    collection_success_field=query_uuid.collection_success_field,
                    collection_support_detail=query_uuid.collection_support_detail,
                    uuid=query_uuid.uuid
                )
                query_item = crud.collection_item.get_item_by_collection_id(db, _id=_id)
                new_schema.product_listing = query_item
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=new_schema,
                    token=new_token["token"])

            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                token=new_token["token"])
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=new_token["token"]
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.get("/{_uuid}/leanpay-bills", response_model=schemas.ResponseApiBase)
def get_collections_uuid_public(
        *,
        db: Session = Depends(deps.get_db),
        _uuid: str,
        background_tasks: BackgroundTasks,
        request: Request
) -> Any:
    """
    search collection uuid.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/collections/{_uuid}/leanpay-bills",
                                  _email="NO_DATA",
                                  _account_id="NO_DATA",
                                  _description="GET_COLLECTION_WITH_UUID",
                                  _data=_uuid, db_=db)

        query_uuid = crud.collection.get_by_uuid(db, uuid=_uuid)
        if query_uuid:
            query_uuid.base_url = str(request.base_url) + f"api/v1/collections/{query_uuid.uuid}/leanpay-bills"
            # generate invoice
            invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"
            # check uuid and recreate if same
            query_bill = crud.customer_bill.get_by_invoice_no(db, invoice_no=invoice_no)
            if query_bill:
                while query_bill.invoice_no == invoice_no:
                    invoice_no = "BP-" + SharedUtils.my_random_string(10) + "-LNP"

            query_account = crud.account.get_account_by_id(db, id=query_uuid.account_id)
            query_options = crud.system_option.get_by_system_own_id(db, _id=query_account.parent_key)

            if query_uuid.collection_method == 2:
                new_schema = schemas.CollectionMethodView(
                    title=query_uuid.title,
                    base_url=query_uuid.base_url,
                    short_url=query_uuid.short_url,
                    min_amount=query_uuid.min_amount,
                    tax=query_uuid.tax,
                    discount=query_uuid.discount,
                    custom_field=query_uuid.custom_field,
                    description=query_uuid.description,
                    secondary_description=query_uuid.secondary_description,
                    footer_description=query_uuid.footer_description,
                    collection_method=query_uuid.collection_method,
                    record_status=query_uuid.record_status,
                    fixed_amount=query_uuid.fixed_amount,
                    allow_customers_to_enter_quantity=query_uuid.allow_customers_to_enter_quantity,
                    enable_quantity_limit=query_uuid.enable_quantity_limit,
                    quantity_limit=query_uuid.quantity_limit,
                    enable_total_amount_tracking=query_uuid.enable_total_amount_tracking,
                    enable_billing_address=query_uuid.enable_billing_address,
                    total_target_amount=query_uuid.total_target_amount,
                    disable_payment_when_target_reached=query_uuid.disable_payment_when_target_reached,
                    share_setting_id=query_uuid.share_setting_id,
                    success_field_id=query_uuid.success_field_id,
                    payment_setting_id=query_uuid.payment_setting_id,
                    account_id=query_uuid.account_id,
                    collection_share_setting=query_uuid.collection_share_setting,
                    collection_payment_setting=query_uuid.collection_payment_setting,
                    collection_success_field=query_uuid.collection_success_field,
                    collection_support_detail=query_uuid.collection_support_detail,
                    uuid=query_uuid.uuid,
                    collection_account=query_uuid.collection_account,
                    bill_invoice_suggestion=invoice_no,
                    account_options=query_options,
                    # account_company_details=query_uuid.collection_account.account_company_details
                )
                query_item = crud.collection_item.get_item_by_collection_id(db, _id=query_uuid.id)
                new_schema.product_listing = query_item
                return schemas.ResponseApiBase(
                    response_code=2000,
                    description='SUCCESS',
                    app_version=settings.API_V1_STR,
                    talk_to_server_before=datetime.datetime.today(),
                    data=new_schema,
                    token="")

            new_query = schemas.CollectionView(
                title=query_uuid.title,
                base_url=query_uuid.base_url,
                short_url=query_uuid.short_url,
                min_amount=query_uuid.min_amount,
                tax=query_uuid.tax,
                discount=query_uuid.discount,
                custom_field=query_uuid.custom_field,
                description=query_uuid.description,
                secondary_description=query_uuid.secondary_description,
                footer_description=query_uuid.footer_description,
                collection_method=query_uuid.collection_method,
                record_status=query_uuid.record_status,
                fixed_amount=query_uuid.fixed_amount,
                allow_customers_to_enter_quantity=query_uuid.allow_customers_to_enter_quantity,
                enable_quantity_limit=query_uuid.enable_quantity_limit,
                quantity_limit=query_uuid.quantity_limit,
                enable_total_amount_tracking=query_uuid.enable_total_amount_tracking,
                enable_billing_address=query_uuid.enable_billing_address,
                total_target_amount=query_uuid.total_target_amount,
                disable_payment_when_target_reached=query_uuid.disable_payment_when_target_reached,
                share_setting_id=query_uuid.share_setting_id,
                success_field_id=query_uuid.success_field_id,
                payment_setting_id=query_uuid.payment_setting_id,
                account_id=query_uuid.account_id,
                collection_share_setting=query_uuid.collection_share_setting,
                collection_payment_setting=query_uuid.collection_payment_setting,
                collection_success_field=query_uuid.collection_success_field,
                collection_support_detail=query_uuid.collection_support_detail,
                uuid=query_uuid.uuid,
                collection_account=query_uuid.collection_account,
                bill_invoice_suggestion=invoice_no,
                account_options=query_options,
                # account_company_details=query_uuid.collection_account.account_company_details
            )
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=new_query,
                token="")
        else:
            return schemas.ResponseApiBase(
                response_code=3333,
                description='FAILED',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data=query_uuid,
                breakdown_errors=f"NO_RECORD_FOUND",
                token=""
            )
    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=""
        )


@router.post("/merchant-collection/", response_model=schemas.ResponseApiBase)
def get_merchant_collections(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.MERCHANT["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/collections/merchant-collection",
                                  _email=current_user.email,
                                  _account_id=new_token["account_id"],
                                  _description="GET_COLLECTION_LIST_BASED_ON_ACCOUNT_ID",
                                  _data=_in.json(), db_=db)

        db_count = crud.collection.get_count_merchant(db=db,
                                                      _id=new_token["account_id"], start_date=_in.start_date,
                                                      end_date=_in.end_date, record_status=_in.record_status)

        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.collection.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=new_token["account_id"], record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )


@router.post("/admin/merchant-collection/{_id}", response_model=schemas.ResponseApiBase)
def admin_merchant_collections(
        *,
        db: Session = Depends(deps.get_db), skip: int = 0, limit: int = 100,
        _id: int,
        _in: schemas.PayloadListQuery
        = Body(..., example=PayloadExample.PAYMENT_RECORD_LIST_PAYLOAD["PAYLOAD"]),
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.WHITE_LABEL["name"], Role.SUPER_ADMIN["name"]],
        ),
        new_token: str = Depends(deps.get_current_new_token),
        background_tasks: BackgroundTasks
) -> Any:
    """
    Retrieve all available customer based on merchant id.
    """
    try:
        background_tasks.add_task(deps.write_audit, _method="GET", _url="/collections/admin/merchant-collection",
                                  _email=current_user.email,
                                  _account_id=_id,
                                  _description="GET_ADMIN_COLLECTION_LIST_BASED_ON_ACCOUNT_ID",
                                  _data=_in.json(), db_=db)

        db_count = crud.collection.get_count_account(db=db, _id=_id, start_date=_in.start_date, end_date=_in.end_date)
        if _in.search.search_replace_word_enable:
            _in.search.search_key = _in.search.search_key.replace(_in.search.search_word_replace,
                                                                  _in.search.search_word_replace_to)

        list_data = crud.collection.get_multi_merchant \
            (db=db, skip=skip, limit=limit, _id=_id, record_status=_in.record_status,
             start_date=_in.start_date,
             end_date=_in.end_date, search_column=_in.search.search_column, search_key=_in.search.search_key,
             parameter_name=_in.sort.parameter_name, sort_type=_in.sort.sort_type,
             search_enable=_in.search.search_enable,
             invoice_status=_in.invoice_status)

        new_list = schemas.ResponseListApiBase(
            draw=len(list_data),
            record_filtered=len(list_data),
            record_total=db_count,
            data=list_data,
            next_page_start=skip + limit,
            next_page_length=limit,
            previous_page_start=skip,
            previous_page_length=limit,
        )

        if not _in.search.search_enable:
            new_list.record_filtered = db_count

        if not list_data:
            return schemas.ResponseApiBase(
                response_code=2000,
                description='SUCCESS',
                app_version=settings.API_V1_STR,
                talk_to_server_before=datetime.datetime.today(),
                data={"list": new_list},
                token=new_token["token"]
            )

        return schemas.ResponseApiBase(
            response_code=2000,
            description='SUCCESS',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"list": new_list},
            token=new_token["token"]
        )

    except Exception as e:
        return schemas.ResponseApiBase(
            response_code=4000,
            description='FAILED',
            app_version=settings.API_V1_STR,
            talk_to_server_before=datetime.datetime.today(),
            data={"exception": jsonable_encoder(e.__dict__)},
            breakdown_errors="INTERNAL_PROBLEM_OCCURS",
            token=new_token["token"]
        )
