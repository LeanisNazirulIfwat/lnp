from app.api.api_v1.routers import accounts, auth, roles, user_roles
from app.api.api_v1.routers import banks, users, api, business_owner_detail, company_detail, collection, catalog,\
    customer, customer_bill, discount, domain, page, payment_record, payment_setting, product, product_category, \
    recurring, rule, share_setting, shipment, shipping_profile, shipping_rate, shipping_zone, store, subscription, \
    subscription_history, success_field, support_detail, system_preference, subscription_payment, audit_request, \
    dashboard, option, collection_item, customer_bill_item, shipping_address, payment_upload_file, pool, audit_pool,\
    product_variants, tier_pricing, merchant_callback, merchant_cloud_image, virtual_account, payout_record,\
    prefund_record, transaction_topup_record
from app.api.api_v1.routers.merchant import encode_decode, onboarding, report, portal
from app.api.api_v1.routers.switcher import switch_callback_url, switch_payment_services
from app.api.api_v1.routers.public import public_merchant, twilio_lean, parcel_asia, cdn_digital_ocean
from fastapi import APIRouter

api_router = APIRouter()

api_router.include_router(auth.router)
api_router.include_router(users.router)
api_router.include_router(roles.router)
api_router.include_router(user_roles.router)
api_router.include_router(accounts.router)
api_router.include_router(banks.router)
api_router.include_router(api.router)
api_router.include_router(business_owner_detail.router)
api_router.include_router(company_detail.router)
api_router.include_router(collection.router)
api_router.include_router(catalog.router)
api_router.include_router(customer.router)
api_router.include_router(customer_bill.router)
api_router.include_router(discount.router)
api_router.include_router(domain.router)
api_router.include_router(page.router)
api_router.include_router(payment_record.router)
api_router.include_router(payment_setting.router)
api_router.include_router(product.router)
api_router.include_router(product_category.router)
api_router.include_router(recurring.router)
api_router.include_router(rule.router)
api_router.include_router(share_setting.router)
api_router.include_router(shipment.router)
api_router.include_router(shipping_profile.router)
api_router.include_router(shipping_rate.router)
api_router.include_router(shipping_zone.router)
api_router.include_router(store.router)
api_router.include_router(subscription.router)
api_router.include_router(subscription_history.router)
api_router.include_router(success_field.router)
api_router.include_router(support_detail.router)
api_router.include_router(system_preference.router)
api_router.include_router(encode_decode.router)
api_router.include_router(switch_callback_url.router)
api_router.include_router(switch_payment_services.router)
api_router.include_router(onboarding.router)
api_router.include_router(subscription_payment.router)
api_router.include_router(report.router)
api_router.include_router(audit_request.router)
api_router.include_router(dashboard.router)
api_router.include_router(option.router)
api_router.include_router(collection_item.router)
api_router.include_router(customer_bill_item.router)
api_router.include_router(shipping_address.router)
api_router.include_router(payment_upload_file.router)
api_router.include_router(public_merchant.router)
api_router.include_router(portal.router)
api_router.include_router(pool.router)
api_router.include_router(audit_pool.router)
api_router.include_router(twilio_lean.router)
api_router.include_router(product_variants.router)
api_router.include_router(tier_pricing.router)
api_router.include_router(merchant_callback.router)
api_router.include_router(parcel_asia.router)
api_router.include_router(cdn_digital_ocean.router)
api_router.include_router(merchant_cloud_image.router)
api_router.include_router(virtual_account.router)
api_router.include_router(payout_record.router)
api_router.include_router(prefund_record.router)
api_router.include_router(transaction_topup_record.router)
