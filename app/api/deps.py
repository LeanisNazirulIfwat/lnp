import decimal
import json
import logging
import time
from datetime import datetime, timedelta
from typing import Generator, Any, Optional
import hashlib
from app import models, schemas, crud
from app.constants.role import Role
from app.core import security
from app.core.config import settings
from app.db.session import SessionLocal
from fastapi import Depends, HTTPException, Security, status, Header, Request
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import jwt
from pydantic import ValidationError
from sqlalchemy.orm import Session
from app.constants.system_constant import SystemConstant
from aiohttp import ClientSession

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_STR}/auth/access-token",
    scopes={
        Role.GUEST["name"]: Role.GUEST["description"],
        Role.ACCOUNT_ADMIN["name"]: Role.ACCOUNT_ADMIN["description"],
        Role.ACCOUNT_MANAGER["name"]: Role.ACCOUNT_MANAGER["description"],
        Role.ADMIN["name"]: Role.ADMIN["description"],
        Role.SUPER_ADMIN["name"]: Role.SUPER_ADMIN["description"],
        Role.MERCHANT["name"]: Role.MERCHANT["description"],
    },
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_db() -> Generator:
    db: Session = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_current_user(
        security_scopes: SecurityScopes,
        db: Session = Depends(get_db),
        token: str = Depends(reusable_oauth2),
) -> models.User:
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = "Bearer"
    credentials_exception = HTTPException(
        status_code=6666,
        detail="JWT_TOKEN_INVALID",
        headers={"WWW-Authenticate": authenticate_value},
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        if payload.get("id") is None:
            raise credentials_exception
        token_data = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        logger.error("ERROR_DECODING_TOKEN", exc_info=True)
        raise HTTPException(
            status_code=7777,
            detail=f"JWT_TOKEN_EXPIRED",
        )
    user = crud.user.get(db, id=token_data.id)
    if not user:
        raise credentials_exception
    if security_scopes.scopes and not token_data.role:
        raise HTTPException(
            status_code=10201,
            detail="UNAUTHORIZED_DUE_TO_PERMISSIONS",
            headers={"WWW-Authenticate": authenticate_value},
        )
    if (
            security_scopes.scopes
            and token_data.role not in security_scopes.scopes
    ):
        raise HTTPException(
            status_code=10201,
            detail="UNAUTHORIZED_DUE_TO_PERMISSIONS",
            headers={"WWW-Authenticate": authenticate_value},
        )
    if datetime.today() > token_data.exp.today():
        raise HTTPException(
            status_code=7777,
            detail="JWT_TOKEN_EXPIRED",
            headers={"WWW-Authenticate": authenticate_value},
        )
    return user


def get_current_active_user(
        current_user: models.User = Security(get_current_user, scopes=[], ),
) -> models.User:
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=10121, detail="INACTIVE_USER")
    return current_user


def get_current_new_token(
        current_user: models.User = Security(get_current_user, scopes=[], ),
) -> Any:
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=10121, detail="INACTIVE_USER")
    access_token_expires = timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )
    role = current_user.user_role.role.name
    token_payload = {
        "id": str(current_user.id),
        "role": role,
    }
    token = security.create_access_token(
        token_payload, expires_delta=access_token_expires)
    account_id: int = 0
    for x in current_user.account:
        if x.is_active:
            account_id = x.id
    return {"token": token, "account_id": account_id}


def get_current_new_token_white_label(
        current_user: models.User = Security(get_current_user, scopes=[], ),
) -> Any:
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=10121, detail="INACTIVE_USER")
    access_token_expires = timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )
    role = current_user.user_role.role.name
    token_payload = {
        "id": str(current_user.id),
        "role": role,
    }
    token = security.create_access_token(
        token_payload, expires_delta=access_token_expires)
    account_id = []
    for x in current_user.account:
        if x.is_active:
            account_id.append(x.id)
    return {"token": token, "account_id": account_id}


def write_audit(_method: str, _url: str, _email: str, _account_id: int,
                _description: str, _data: str, db_: Session):
    try:
        create_data = schemas.AuditRequestCreate(
            method=_method,
            email=_email,
            url=_url,
            account_id=_account_id,
            description=_description,
            data=_data,
        )
        crud.audit_request.create(db_, obj_in=create_data)
    except Exception as e:
        raise HTTPException(
            status_code=1106,
            detail="AUDIT_WRITING_PROBLEM_OCCUR",
        )


async def get_token_header(
        db: Session = Depends(get_db),
        auth_token: str = Header(...)
):
    try:
        if auth_token:
            if auth_token.__contains__("|"):
                key_component = auth_token.split("|")
                access_id = key_component[0]
                access_uuid = key_component[1]
                access_token = key_component[2]
                secret_key = settings.API_SECRET_KEY
                join_token = f"{access_id}|{access_uuid}|{secret_key}"
                hash_token = hashlib.sha512(join_token.encode())
                hash_hex = hash_token.hexdigest()

                if not hash_hex == access_token:
                    raise HTTPException(
                        status_code=5688,
                        detail="INVALID_TOKEN",
                    )
                account_access = crud.api.get_by_api_uuid(db, _uuid=access_uuid)
                if account_access:
                    return {"access_uuid": access_uuid, "account_id": account_access.account_id}
                else:
                    raise HTTPException(
                        status_code=5699,
                        detail="INVALID_UUID",
                    )
            else:
                raise HTTPException(
                    status_code=5688,
                    detail="INVALID_TOKEN",
                )
        else:
            raise HTTPException(
                status_code=5688,
                detail="INVALID_TOKEN",
            )
    except Exception as e:
        raise HTTPException(
            status_code=5688,
            detail="INVALID_TOKEN",
        )


async def get_token_onboard_header(
        db: Session = Depends(get_db),
        auth_token: str = Header(..., example=settings.ONBOARD_AUTH_KEY)
):
    try:
        if auth_token:
            if auth_token.__contains__("|"):
                key_component = auth_token.split("|")
                access_id = key_component[0]
                access_uuid = key_component[1]
                access_token = key_component[2]
                secret_key = settings.API_SECRET_KEY
                join_token = f"{access_id}|{access_uuid}|{secret_key}"
                hash_token = hashlib.sha512(join_token.encode())
                hash_hex = hash_token.hexdigest()

                if not hash_hex == access_token:
                    raise HTTPException(
                        status_code=5688,
                        detail="INVALID_TOKEN",
                    )
            else:
                raise HTTPException(
                    status_code=5688,
                    detail="INVALID_TOKEN",
                )
        else:
            raise HTTPException(
                status_code=5688,
                detail="INVALID_TOKEN",
            )
    except Exception as e:
        raise HTTPException(
            status_code=5688,
            detail="INVALID_TOKEN",
        )


def transaction_charges(_charges: decimal.Decimal, api_key: str,
                        _account_id: int, _transaction_amount: decimal.Decimal, db_: Session,
                        invoice_no: str, invoice_status: str, bill_transaction_type: int):
    try:
        pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL")
        pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant, _account_id=_account_id)
        transaction_type = "SUBTRACTION"
        if pool:
            previous_value = decimal.Decimal(pool.value)
            current_value = previous_value - decimal.Decimal(_charges)
            if current_value < 0:
                pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL")
                transaction_type = "ADDITION"
                threshold_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant,
                                                                    _account_id=_account_id)
                previous_value = decimal.Decimal(threshold_pool.value)
                current_value = previous_value + decimal.Decimal(_charges)
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )
                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=threshold_pool)
            else:
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )

                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=pool)

            create_audit_pool = schemas.AuditPoolCreate(
                transaction_type=transaction_type,
                previous_value=previous_value,
                current_value=current_value,
                charges=_charges,
                pool_id=update_create_pool.id,
                api_key=api_key,
                account_id=_account_id,
                pool_type=pool_constant,
                invoice_no=invoice_no,
                invoice_status=invoice_status,
                description="SUBTRACTION_TRANSACTION_POOL"
            )

            crud.audit_pool.create_self_date(db_, obj_in=create_audit_pool)

            add_fund_collection_admin(db_, _transaction_amount=_charges, api_key=api_key, invoice_no=invoice_no,
                                      invoice_status=invoice_status, _account_id=None)

            if bill_transaction_type == 1:
                pool_collection = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
                query_collection_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_collection,
                                                                           _account_id=_account_id)
                collection_previous = query_collection_pool.value
                update_collection_pool = schemas.PoolUpdate(
                    value=collection_previous + decimal.Decimal(_transaction_amount)
                )

                update_collection = crud.pool.update_self_date(db_, obj_in=update_collection_pool,
                                                               db_obj=query_collection_pool)

                create_audit_collection_pool = schemas.AuditPoolCreate(
                    transaction_type="ADDITION",
                    previous_value=collection_previous,
                    current_value=update_collection.value,
                    charges=decimal.Decimal(_transaction_amount),
                    pool_id=update_collection.id,
                    api_key=api_key,
                    account_id=_account_id,
                    pool_type=pool_collection[0],
                    invoice_no=invoice_no,
                    invoice_status=invoice_status,
                    description="ADD_AMOUNT_TO_COLLECTION_POOL"
                )

                crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)

            if bill_transaction_type == 3:
                add_fund_collection_admin(db_, _transaction_amount=_transaction_amount, invoice_no=invoice_no,
                                          invoice_status=invoice_status, api_key=api_key, _account_id=None)

                pool_transaction = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL"),
                query_transaction_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_transaction,
                                                                            _account_id=_account_id)
                collection_previous = query_transaction_pool.value
                update_collection_pool = schemas.PoolUpdate(
                    value=collection_previous + decimal.Decimal(_transaction_amount)
                )

                update_collection = crud.pool.update_self_date(db_, obj_in=update_collection_pool,
                                                               db_obj=query_transaction_pool)

                create_audit_collection_pool = schemas.AuditPoolCreate(
                    transaction_type="ADDITION",
                    previous_value=collection_previous,
                    current_value=update_collection.value,
                    charges=decimal.Decimal(_transaction_amount),
                    pool_id=update_collection.id,
                    api_key=api_key,
                    account_id=_account_id,
                    pool_type=pool_transaction[0],
                    invoice_no=invoice_no,
                    invoice_status=invoice_status,
                    description="TOPUP_AND_ADD_AMOUNT_TO_TRANSACTION_POOL"
                )
                time.sleep(1)
                crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)

            if bill_transaction_type == 4:
                add_fund_collection_admin(db_, _transaction_amount=_transaction_amount, invoice_no=invoice_no,
                                          invoice_status=invoice_status, api_key=api_key, _account_id=None)

    except Exception as e:
        raise HTTPException(
            status_code=1104,
            detail="SOMETHING_GONE_WRONG",
        )


def prefund_transaction_charges(_charges: decimal.Decimal, api_key: str,
                                _account_id: int, _transaction_amount: decimal.Decimal, db_: Session,
                                invoice_no: str, invoice_status: str, data: str):
    try:
        pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL")
        # pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant, _account_id=_account_id)
        pool = crud.pool.get_by_name(db_, title="ADMIN_COLLECTION")
        transaction_type = "SUBTRACTION"
        if pool:
            previous_value = decimal.Decimal(pool.value)
            current_value = previous_value - decimal.Decimal(_charges)
            if current_value < 0:
                pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL")
                transaction_type = "ADDITION"
                # threshold_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant,
                #                                                     _account_id=_account_id)
                threshold_pool = crud.pool.get_by_name(db_, title="ADMIN_THRESHOLD")
                previous_value = decimal.Decimal(threshold_pool.value)
                current_value = previous_value + decimal.Decimal(_charges)
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )
                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=threshold_pool)
            else:
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )

                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=pool)

            create_audit_pool = schemas.AuditPoolCreate(
                transaction_type=transaction_type,
                previous_value=previous_value,
                current_value=current_value,
                charges=_charges,
                pool_id=update_create_pool.id,
                api_key=api_key,
                account_id=_account_id,
                pool_type=int(pool_constant),
                description="SUBTRACT_TRANSACTION_POOL_FOR_PREFUND_FROM_ADMIN_ACCOUNT"
            )

            crud.audit_pool.create_self_date(db_, obj_in=create_audit_pool)

            pool_prefund = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
            query_prefund_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_prefund,
                                                                    _account_id=_account_id)

            if query_prefund_pool:
                prefund_previous = query_prefund_pool.value
                update_prefund_pool = schemas.PoolUpdate(
                    value=prefund_previous + decimal.Decimal(_transaction_amount)
                )

                update_prefund = crud.pool.update_self_date(db_, obj_in=update_prefund_pool,
                                                            db_obj=query_prefund_pool)

                create_audit_collection_pool = schemas.AuditPoolCreate(
                    transaction_type="ADDITION",
                    previous_value=prefund_previous,
                    current_value=prefund_previous + decimal.Decimal(_transaction_amount),
                    charges=decimal.Decimal(_transaction_amount),
                    api_key=api_key,
                    account_id=_account_id,
                    pool_type=pool_prefund[0],
                    invoice_no=invoice_no,
                    invoice_status=invoice_status,
                    data=json.dumps(data),
                    description="ADD_AMOUNT_TO_PREFUND_POOL"
                )

                crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)

    except Exception as e:
        raise HTTPException(
            status_code=1104,
            detail="SOMETHING_GONE_WRONG",
        )


def get_params(request: Request):
    try:
        dict_params = dict(request.query_params)
        return dict_params
    except Exception as e:
        raise HTTPException(
            status_code=1104,
            detail="SOMETHING_GONE_WRONG",
        )


def check_subscription_package(
        current_user: models.User = Security(get_current_user, scopes=[], ),
        db: Session = Depends(get_db)) -> Any:
    try:
        subscription = crud.subscription.get_by_subscription_id(db=db, sub_id=current_user.account[0]
                                                                .subscription_plan_id)
        return subscription
    except Exception as e:
        raise HTTPException(
            status_code=1104,
            detail=f"SOMETHING_GONE_WRONG {e}",
        )


def add_shipment(db_: Session, _account_id: int, _customer_id: int, _profile_id: int, _collection_id: int,
                 _customer_bill_id: int):
    try:
        collection = crud.collection.get(db=db_, id=_collection_id)
        if collection.collection_method == 2:
            _shipment = schemas.ShipmentCreate(
                shipment_status=SystemConstant.SYSTEM_CONS.get("shipment_status", None).__getitem__('pending_shipping'),
                customer_id=_customer_id,
                shipping_profile_id=_profile_id,
                account_id=_account_id,
                customer_bill_id=_customer_bill_id
            )
            new_shipment = crud.shipment.create_self_date(db=db_, obj_in=_shipment)
            if new_shipment:
                return True
            return False
        else:
            return False
    except Exception as e:
        raise HTTPException(
            status_code=3468,
            detail=f"SOMETHING_GONE_WRONG {e}",
        )


def add_fund_collection_admin(db_: Session, _transaction_amount: decimal.Decimal, invoice_no: str, invoice_status: str,
                              api_key: str, _account_id: Optional[int]):
    try:
        if _account_id is None:
            _account_id = crud.collection.get_by_transaction_uuid(db_, _uuid="LEANX-CL").account_id

        pool_collection = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("COLLECTION_POOL"),
        query_collection_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_collection,
                                                                   _account_id=_account_id)
        collection_previous = query_collection_pool.value
        update_collection_pool = schemas.PoolUpdate(
            value=collection_previous + decimal.Decimal(_transaction_amount)
        )

        update_collection = crud.pool.update_self_date(db_, obj_in=update_collection_pool,
                                                       db_obj=query_collection_pool)

        create_audit_collection_pool = schemas.AuditPoolCreate(
            transaction_type="ADDITION",
            previous_value=collection_previous,
            current_value=update_collection.value,
            charges=decimal.Decimal(_transaction_amount),
            pool_id=update_collection.id,
            api_key=api_key,
            account_id=_account_id,
            pool_type=pool_collection[0],
            invoice_no=invoice_no,
            invoice_status=invoice_status,
            description="ADD_AMOUNT_TO_COLLECTION_ADMIN_POOL"
        )

        crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)
    except Exception as e:
        raise HTTPException(
            status_code=3468,
            detail=f"SOMETHING_GONE_WRONG {e}",
        )


def payout_transaction_charges(_charges: decimal.Decimal, api_key: str,
                               _account_id: int, _transaction_amount: decimal.Decimal, db_: Session,
                               invoice_no: str, invoice_status: str, data: str):
    try:
        pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("TRANSACTION_POOL")
        pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant, _account_id=_account_id)
        transaction_type = "SUBTRACTION"
        description_ = "SUBTRACT_AMOUNT_FROM_TRANSACTION_POOL"
        if pool:
            previous_value = decimal.Decimal(pool.value)
            current_value = previous_value - decimal.Decimal(_charges)
            if current_value < 0:
                pool_constant = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("THRESHOLD_POOL")
                transaction_type = "ADDITION"
                description_ = "ADD_AMOUNT_TO_THRESHOLD_POOL"
                threshold_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_constant,
                                                                    _account_id=_account_id)
                previous_value = decimal.Decimal(threshold_pool.value)
                current_value = previous_value + decimal.Decimal(_charges)
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )
                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=threshold_pool)
            else:
                update_pool = schemas.PoolUpdate(
                    value=current_value
                )

                update_create_pool = crud.pool.update_self_date(db_, obj_in=update_pool, db_obj=pool)

            create_audit_pool = schemas.AuditPoolCreate(
                transaction_type=transaction_type,
                previous_value=previous_value,
                current_value=current_value,
                charges=_charges,
                pool_id=update_create_pool.id,
                api_key=api_key,
                account_id=_account_id,
                pool_type=int(pool_constant),
                invoice_status=invoice_status,
                invoice_no=invoice_no,
                data=json.dumps(data),
                description=description_
            )

            crud.audit_pool.create_self_date(db_, obj_in=create_audit_pool)

            pool_payout = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PAY_OUT_POOL"),
            query_payout_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_payout,
                                                                    _account_id=_account_id)

            if query_payout_pool:
                payout_previous = query_payout_pool.value
                update_prefund_pool = schemas.PoolUpdate(
                    value=payout_previous + decimal.Decimal(_transaction_amount)
                )

                update_prefund = crud.pool.update_self_date(db_, obj_in=update_prefund_pool,
                                                            db_obj=query_payout_pool)

                create_audit_collection_pool = schemas.AuditPoolCreate(
                    transaction_type="ADDITION",
                    previous_value=payout_previous,
                    current_value=payout_previous + decimal.Decimal(_transaction_amount),
                    charges=decimal.Decimal(_transaction_amount),
                    api_key=api_key,
                    account_id=_account_id,
                    pool_id=query_payout_pool.id,
                    pool_type=pool_payout[0],
                    invoice_no=invoice_no,
                    invoice_status=invoice_status,
                    data=json.dumps(data),
                    description="ADD_AMOUNT_TO_PAYOUT_POOL"
                )

                crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)

                # deduct prefund pool
                pool_prefund = SystemConstant.SYSTEM_CONS.get("pool_type", None).__getitem__("PREFUND_POOL"),
                query_prefund_pool = crud.pool.get_by_pool_account_type(db_, _id=pool_prefund,
                                                                        _account_id=_account_id)

                if query_prefund_pool:
                    prefund_previous = query_prefund_pool.value
                    update_prefund_pool = schemas.PoolUpdate(
                        value=prefund_previous - decimal.Decimal(_transaction_amount)
                    )

                    update_prefund = crud.pool.update_self_date(db_, obj_in=update_prefund_pool,
                                                                db_obj=query_prefund_pool)

                    create_audit_collection_pool = schemas.AuditPoolCreate(
                        transaction_type="SUBTRACT",
                        previous_value=prefund_previous,
                        current_value=prefund_previous - decimal.Decimal(_transaction_amount),
                        charges=decimal.Decimal(_transaction_amount),
                        api_key=api_key,
                        account_id=_account_id,
                        pool_type=pool_prefund[0],
                        invoice_no=invoice_no,
                        invoice_status=invoice_status,
                        data=json.dumps(data)
                    )

                    crud.audit_pool.create_self_date(db_, obj_in=create_audit_collection_pool)

    except Exception as e:
        raise HTTPException(
            status_code=2644,
            detail="PAYOUT_CANNOT_BE_PERFORMED",
        )


def check_allow_transaction(
        current_user: models.User = Security(get_current_user, scopes=[], ),
        db: Session = Depends(get_db),
        data: Any = "INVOICE") -> Any:
    try:
        collection_ = db.query(schemas.Collection).filter(schemas.Collection.id == data).first()
        if collection_.enable_quantity_limit:
            if collection_.quantity_limit <= 0:
                raise HTTPException(
                    status_code=5891,
                    detail=f"QUANTITY_LIMIT_MUST_BE_MORE_THEN_ZERO",
                )
        return True
    except Exception as e:
        raise HTTPException(
            status_code=2610,
            detail=f"TRANSACTION_CANNOT_BE_PERFORMED",
        )
