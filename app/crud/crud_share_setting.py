from typing import Optional, List, Any
from sqlalchemy import desc, func, text, asc
from datetime import datetime
from app.crud.base import CRUDBase
from app.models.share_setting import ShareSetting
from app.schemas.share_setting import ShareSettingCreate, ShareSettingUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[ShareSetting, ShareSettingCreate, ShareSettingUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[ShareSetting]:
        return db.query(self.model).filter(ShareSetting.title == title).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[ShareSetting]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"share_settings.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"share_settings.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"share_settings.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_share_setting_id(
            db: Session, *, _id: int
    ) -> Optional[ShareSetting]:
        return db.query(ShareSetting).filter(ShareSetting.id == _id).first()

    @staticmethod
    def get_by_share_setting_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[ShareSetting]:
        return db.query(ShareSetting).filter(ShareSetting.account_id == _id)\
            .order_by(desc(ShareSetting.created_at)).first()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


share_setting = CRUDRole(ShareSetting)
