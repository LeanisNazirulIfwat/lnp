import datetime
from typing import Optional, List, Any

from app.crud.base import CRUDBase
from app.models.payment_record import PaymentRecord
from app.models.customer import Customer
from app.models.customer_bill import CustomerBill
from app.schemas.payment_record import PaymentRecordCreate, PaymentRecordUpdate
from sqlalchemy.orm import Session, selectinload
from sqlalchemy import desc, func, text
from datetime import datetime
from app.constants.role import Role


class CRUDRole(CRUDBase[PaymentRecord, PaymentRecordCreate, PaymentRecordUpdate]):
    def get_by_name(self, db: Session, *, reference_number: str) -> Optional[PaymentRecord]:
        return db.query(self.model).filter(PaymentRecord.reference_number == reference_number).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[PaymentRecord]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                self.model.record_status == record_status,
                Customer.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"payment_records.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                self.model.record_status == record_status,
                Customer.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_admin_search_account(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[Any]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).options(
                    selectinload(self.model.payment_record_customer_bill)) \
                    .filter(
                    self.model.record_status == record_status,
                    Customer.account_id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).options(
                    selectinload(self.model.payment_record_customer_bill)).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).options(
                    selectinload(self.model.payment_record_customer_bill)).filter(
                    self.model.record_status == record_status,
                    Customer.account_id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).options(
                    selectinload(self.model.payment_record_customer_bill)).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get_multi_merchant_customer_bill(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, bill_invoice: str
    ) -> List[PaymentRecord]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            return db.query(self.model).join(CustomerBill) \
                .options(selectinload(self.model.payment_record_customer_bill)) \
                .filter(CustomerBill.account_id == _id) \
                .filter(CustomerBill.invoice_no == bill_invoice).filter(
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date()) \
                .filter(text(f"payment_records.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).join(CustomerBill) \
                .options(selectinload(self.model.payment_record_customer_bill)) \
                .filter(CustomerBill.account_id == _id) \
                .filter(CustomerBill.invoice_no == bill_invoice).filter(
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date()) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_payment_record_id(
            db: Session, *, _id: int
    ) -> Optional[PaymentRecord]:
        return db.query(PaymentRecord).filter(PaymentRecord.id == _id).first()

    def get_count(self,
                  db: Session, *, _id: int, start_date: str, end_date: str
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
            Customer.account_id == _id,
            func.date(self.model.payment_date) >= cur_start_date.date(),
            func.date(self.model.payment_date) <= cur_end_date.date()
        ).count()

    def get_count_admin_record(self,
                               db: Session, *, _id: Any, start_date: str, end_date: str, _record: int, role: str
                               ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if not role == Role.SUPER_ADMIN["name"]:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                Customer.account_id.in_(_id),
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date(),
                self.model.record_status == _record
            ).count()
        else:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date(),
                self.model.record_status == _record
            ).count()

    def get_count_based_bill_invoice(self,
                                     db: Session, *, _id: int, start_date: str, end_date: str, _invoice: str
                                     ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).join(CustomerBill) \
            .options(selectinload(self.model.payment_record_customer_bill)).filter(
            CustomerBill.account_id == _id,
            CustomerBill.invoice_no == _invoice,
            func.date(self.model.payment_date) >= cur_start_date.date(),
            func.date(self.model.payment_date) <= cur_end_date.date()
        ).count()

    def get_multi_merchant_recent_payment(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[PaymentRecord]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                self.model.record_status == record_status,
                Customer.account_id == _id,
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date()) \
                .filter(text(f"payment_records.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).join(Customer, Customer.id == self.model.customer_id).filter(
                self.model.record_status == record_status,
                Customer.account_id == _id,
                func.date(self.model.payment_date) >= cur_start_date.date(),
                func.date(self.model.payment_date) <= cur_end_date.date()) \
                .order_by(text(f"payment_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()


payment_record = CRUDRole(PaymentRecord)
