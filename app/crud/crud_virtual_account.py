from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from datetime import datetime
from app.crud.base import CRUDBase
from app.models.virtual_account import VirtualAccount
from app.models.collection import Collection
from app.models.account import Account
from app.schemas.virtual_account import VirtualAccountCreate, VirtualAccountUpdate
from sqlalchemy.orm import Session, selectinload, joinedload, subqueryload


class CRUDRole(CRUDBase[VirtualAccount, VirtualAccountCreate, VirtualAccountUpdate]):
    def get_by_pool_name(self, db: Session, *, name: str, _id: int) -> Optional[VirtualAccount]:
        return db.query(self.model).filter(self.model.pool_name == name,
                                           self.model.account_id == _id).first()

    def get_by_admin_pool_name(self, db: Session, *, name: str) -> Optional[VirtualAccount]:
        return db.query(self.model).filter(self.model.pool_name == name).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[VirtualAccount]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"virtual_accounts.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"virtual_accounts.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"virtual_accounts.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_virtual_account_id(
            db: Session, *, _id: int
    ) -> Optional[VirtualAccount]:
        return db.query(VirtualAccount).filter(VirtualAccount.id == _id).first()

    @staticmethod
    def get_by_virtual_account_unique_id(
            db: Session, *, _id: str
    ) -> Optional[VirtualAccount]:
        return db.query(VirtualAccount).filter(VirtualAccount.merchant_payout_pool_id == _id).first()

    @staticmethod
    def get_by_virtual_account_switch_id(
            db: Session, *, _id: str
    ) -> Optional[VirtualAccount]:
        return db.query(VirtualAccount).filter(VirtualAccount.switch_payout_pool_id == _id).first()

    @staticmethod
    def get_by_virtual_account_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[VirtualAccount]:
        return db.query(VirtualAccount).filter(VirtualAccount.account_id == _id).first()

    def get_count(self,
                  db: Session, *, _id: int,
                  start_date: str, end_date: str,
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()).count()

    def get_count_admin_record(self,
                               db: Session, *, _id: Any, start_date: str, end_date: str, _record: int, role: str
                               ) -> Optional[Any]:
        _start_date = datetime.strptime(start_date, "%d-%m-%Y")
        _end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= _start_date.date(),
            func.date(self.model.created_at) <= _end_date.date(),
            self.model.record_status == _record
        ).count()

    def get_by_unique_id(self, db: Session, *, _unique_id: str) -> Optional[VirtualAccount]:
        return db.query(self.model).options(selectinload(self.model.store_collection))\
            .options(selectinload(self.model.store_collection)
                     .joinedload(Collection.collection_account)
                     .joinedload(Account.account_company_details))\
            .filter(VirtualAccount.merchant_payout_pool_id == _unique_id).first()


virtual_account = CRUDRole(VirtualAccount)
