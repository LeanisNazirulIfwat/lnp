import datetime
from typing import Optional, Any, List
from sqlalchemy import desc, func, text

from app.crud.base import CRUDBase
from app.models.subscription_payment import SubscriptionPayment
from app.schemas.subscription_payment import SubscriptionPaymentCreate, SubscriptionPaymentUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[SubscriptionPayment, SubscriptionPaymentCreate, SubscriptionPaymentUpdate]):
    def get_by_invoice_no(self, db: Session, *, invoice_no: str) -> Optional[SubscriptionPayment]:
        return db.query(self.model).filter(SubscriptionPayment.invoice_no == invoice_no).first()

    def get_by_account_id_no_month(self, db: Session, *, invoice_no: str) -> Optional[SubscriptionPayment]:
        return db.query(self.model).filter(SubscriptionPayment.month == datetime.datetime.today().month,
                                           SubscriptionPayment.year == datetime.datetime.today().year).\
            filter(SubscriptionPayment.account_id == invoice_no).first()

    def get_by_account_id_no_year(self, db: Session, *, invoice_no: str) -> Optional[SubscriptionPayment]:
        return db.query(self.model).filter(SubscriptionPayment.year == datetime.datetime.today().year).\
            filter(SubscriptionPayment.account_id == invoice_no).first()

    @staticmethod
    def get_by_subscription_payment_id(
            db: Session, *, _id: int
    ) -> Optional[SubscriptionPayment]:
        return db.query(SubscriptionPayment).filter(SubscriptionPayment.id == _id).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[SubscriptionPayment]:
        cur_start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"subscription_payments.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"subscription_payments.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"subscription_payments.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()


subscription_payment = CRUDRole(SubscriptionPayment)
