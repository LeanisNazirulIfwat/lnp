from datetime import datetime
from typing import Any, List, Optional
from app.constants.role import Role
from app.models.account import Account
from sqlalchemy import desc, func, text

from app.crud.base import CRUDBase
from app.models.subscription import Subscription
from app.models.account import Account
from app.schemas.subscription import SubscriptionCreate, SubscriptionUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[Subscription, SubscriptionCreate, SubscriptionUpdate]):
    def get_by_plan_name(self, db: Session, *, plan_name: str) -> Optional[Subscription]:
        return db.query(self.model).filter(Subscription.plan_name == plan_name).first()

    @staticmethod
    def get_by_subscription_id(
            db: Session, *, sub_id: int
    ) -> Optional[Subscription]:
        return db.query(Subscription).filter(Subscription.id == sub_id).first()

    @staticmethod
    def get_by_account_id(
            db: Session, *, _id: int
    ) -> Optional[Subscription]:
        return db.query(Subscription, Account).join(Account).filter(Account.id == _id).first()

    def get_admin_search_account(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[Any]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(Account.name, Account.description, self.model)\
                    .join(Account, Account.subscription_plan_id == self.model.id) \
                    .filter(
                    self.model.record_status == record_status,
                    Account.id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(Account.name, Account.description, self.model)\
                    .join(Account, Account.subscription_plan_id == self.model.id).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(Account.name, Account.description, self.model)\
                    .join(Account, Account.subscription_plan_id == self.model.id).filter(
                    self.model.record_status == record_status,
                    Account.id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(Account.name, Account.description, self.model)\
                    .join(Account, Account.subscription_plan_id == self.model.id).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()


subscription = CRUDRole(Subscription)
