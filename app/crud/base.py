import datetime
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from sqlalchemy import desc, func, text
from app.db.base import Base
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from app.constants.role import Role

# Define custom types for SQLAlchemy model, and Pydantic schemas
ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    def __init__(self, model: Type[ModelType]):
        """Base class that can be extend by other action classes.
           Provides basic CRUD and listing operations.

        :param model: The SQLAlchemy model
        :type model: Type[ModelType]
        """
        self.model = model

    def get_count_user(self,
                       db: Session, *, _id: int, start_date: str, end_date: str
                       ) -> Optional[Any]:
        cur_start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.user_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()

    def get_count_account(self,
                          db: Session, *, _id: int, start_date: str, end_date: str
                          ) -> Optional[Any]:
        cur_start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()).count()

    def get_multi(
            self, db: Session, *, skip: int = 0, limit: int = 100
    ) -> List[ModelType]:
        return db.query(self.model).offset(skip).limit(limit).all()

    def get_multi_search_account(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[ModelType]:

        cur_start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.account_id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.account_id.in_(_id),
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get_multi_search_user(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[ModelType]:

        cur_start_date = datetime.datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get(self, db: Session, id: int) -> Optional[ModelType]:
        return db.query(self.model).filter(self.model.id == id).first()

    def create(self, db: Session, *, obj_in: CreateSchemaType) -> ModelType:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)  # type: ignore
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
            self,
            db: Session,
            *,
            db_obj: ModelType,
            obj_in: Union[UpdateSchemaType, Dict[str, Any]]
    ) -> ModelType:
        obj_data = jsonable_encoder(db_obj)
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def remove(self, db: Session, *, id: int) -> ModelType:
        obj = db.query(self.model).get(id)
        db.delete(obj)
        db.commit()
        return obj

    def create_self_date(self, db: Session, *, obj_in: CreateSchemaType) -> ModelType:
        obj_in.created_at = datetime.datetime.today()
        obj_in.updated_at = datetime.datetime.today()
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)  # type: ignore
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update_self_date(
            self,
            db: Session,
            *,
            db_obj: ModelType,
            obj_in: Union[UpdateSchemaType, Dict[str, Any]]
    ) -> ModelType:
        obj_in.updated_at = datetime.datetime.today()
        obj_data = jsonable_encoder(db_obj)
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj
