from datetime import datetime
from typing import Optional, Any, List
from sqlalchemy import func, text
from app.crud.base import CRUDBase
from app.models.tier_pricing import TierPricing
from app.schemas.tier_pricing import TierPricingCreate, TierPricingUpdate
from sqlalchemy.orm import Session, selectinload
from app.constants.role import Role


class CRUDRole(CRUDBase[TierPricing, TierPricingCreate, TierPricingUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[TierPricing]:
        return db.query(self.model).filter(self.model.name == name).first()

    @staticmethod
    def get_by_tier_pricing_id(
            db: Session, *, _id: int
    ) -> Optional[TierPricing]:
        return db.query(TierPricing).filter(TierPricing.id == _id).first()

    @staticmethod
    def get_by_join_tier_pricing(
            db: Session
    ) -> Any:
        return db.query(TierPricing).join()

    def get_tier_pricing_search_user(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[TierPricing]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get_count(self,
                  db: Session, *,
                  start_date: str, end_date: str
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()


tier_pricing = CRUDRole(TierPricing)
