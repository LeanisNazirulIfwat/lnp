from typing import Optional, List

from app.crud.base import CRUDBase
from app.models.page import Page
from app.schemas.page import PageCreate, PageUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[Page, PageCreate, PageUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[Page]:
        return db.query(self.model).filter(Page.title == title).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[Page]:
        return db.query(self.model).filter(Page.account_id == _id).offset(skip).limit(limit).all()

    @staticmethod
    def get_by_page_id(
            db: Session, *, _id: int
    ) -> Optional[Page]:
        return db.query(Page).filter(Page.id == _id).first()

    @staticmethod
    def get_by_page_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Page]:
        return db.query(Page).filter(Page.account_id == _id).first()


page = CRUDRole(Page)
