from typing import Optional, List, Any

from app.crud.base import CRUDBase
from app.models.collection_item import CollectionItem
from app.models.product import Product
from app.models.collection import Collection
from app.schemas.collection_item import CollectionItemCreate, CollectionItemUpdate
from sqlalchemy.orm import Session
from sqlalchemy import desc


class CRUDRole(CRUDBase[CollectionItem, CollectionItemCreate, CollectionItemUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[CollectionItem]:
        return db.query(self.model).filter(CollectionItem.collection_id == name).first()

    def get_multi_merchant(
        self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[Any]:
        return db.query(self.model, Product)\
            .join(Product, Product.id == self.model.product_id)\
            .offset(skip).limit(limit).all()

    @staticmethod
    def get_by_collection_item_id(
            db: Session, *, _id: int
    ) -> Optional[CollectionItem]:
        return db.query(CollectionItem).filter(CollectionItem.id == _id).first()

    @staticmethod
    def get_by_collection_product_item_id(
            db: Session, *, _id: int, product_id: int
    ) -> Optional[CollectionItem]:
        return db.query(CollectionItem).filter(CollectionItem.collection_id == _id,
                                               CollectionItem.product_id == product_id).first()

    @staticmethod
    def get_by_collection_id(
            db: Session, *, _id: int
    ) -> List[CollectionItem]:
        return db.query(CollectionItem).filter(CollectionItem.collection_id == _id).all()


    @staticmethod
    def get_item_by_collection_id(
            db: Session, *, _id: int
    ) -> List[Any]:
        return db.query(CollectionItem, Product).join(Product, Product.id == CollectionItem.product_id)\
            .filter(CollectionItem.collection_id == _id).order_by(desc(CollectionItem.created_at)).all()

    @staticmethod
    def get_by_collection_item_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[CollectionItem]:
        return db.query(CollectionItem, Product).join(Product, Product.id == CollectionItem.product_id)\
            .filter(Collection.account_id == _id).first()


collection_item = CRUDRole(CollectionItem)
