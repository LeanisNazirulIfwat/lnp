from typing import Optional, List, Any

from app.crud.base import CRUDBase
from app.models.customer import Customer
from app.schemas.customer import CustomerCreate, CustomerUpdate
from sqlalchemy.orm import Session
from sqlalchemy import desc, func, text
from datetime import datetime


class CRUDRole(CRUDBase[Customer, CustomerCreate, CustomerUpdate]):
    def get_by_name(self, db: Session, *, full_name: str) -> Optional[Customer]:
        return db.query(self.model).filter(Customer.email == full_name).first()

    def get_by_name_account(self, db: Session, *, full_name: str, _id: int) -> Optional[Customer]:
        return db.query(self.model).filter(Customer.account_id == _id).filter(Customer.email == full_name).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[Customer]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"customers.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"customers.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"customers.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_by_customer_id(self,
                           db: Session, *, _id: int
                           ) -> Optional[Customer]:
        return db.query(self.model).filter(self.model.id == _id).first()

    @staticmethod
    def get_by_customer_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Customer]:
        return db.query(Customer).filter(Customer.account_id == _id).first()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


customer = CRUDRole(Customer)
