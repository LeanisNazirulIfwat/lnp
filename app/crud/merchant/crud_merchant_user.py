from typing import Any, Dict, List, Optional, Union

from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models.user import User
from app.schemas.user import UserCreate, UserUpdate
from sqlalchemy.orm import Session
from app.core.config import settings


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def get_by_merchant_email(self, db: Session, *, email: str) -> Optional[User]:
        return db.query(self.model).filter(User.system_id == settings.MERCHANT_SYSTEM) \
            .filter(User.email == email,
                    User.record_status == settings.RECORD_ACTIVE) \
            .first()

    def authenticate(
            self, db: Session, *, email: str, password: str
    ) -> Optional[User]:
        user = self.get_by_merchant_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        return user

    def is_active(self, user: User) -> bool:
        return user.is_active

    def get_by_account_id(
            self,
            db: Session,
            *,
            user_id: int,
            skip: int = 0,
            limit: int = 100,
    ) -> List[User]:
        return (
            db.query(self.model)
                .filter(User.id == user_id)
                .offset(skip)
                .limit(limit)
                .all()
        )


merchant_user = CRUDUser(User)
