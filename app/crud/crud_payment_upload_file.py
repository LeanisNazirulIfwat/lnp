from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from datetime import datetime
from app.crud.base import CRUDBase
from app.models.payment_upload_file import PaymentUploadFile
from app.schemas.payment_upload_file import PaymentUploadFileCreate, PaymentUploadFileUpdate
from sqlalchemy.orm import Session, selectinload


class CRUDRole(CRUDBase[PaymentUploadFile, PaymentUploadFileCreate, PaymentUploadFileUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[PaymentUploadFile]:
        return db.query(self.model).filter(PaymentUploadFile.title == title).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[PaymentUploadFile]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.collection_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"payment_upload_files.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"payment_upload_files.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.collection_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"payment_upload_files.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_payment_upload_files_id(
            db: Session, *, _id: int
    ) -> Optional[PaymentUploadFile]:
        return db.query(PaymentUploadFile).filter(PaymentUploadFile.id == _id).first()

    @staticmethod
    def get_by_payment_setting_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[PaymentUploadFile]:
        return db.query(PaymentUploadFile).filter(PaymentUploadFile.account_id == _id).first()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.collection_id == _id).count()


payment_upload_file = CRUDRole(PaymentUploadFile)
