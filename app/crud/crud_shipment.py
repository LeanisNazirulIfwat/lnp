from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from datetime import datetime
from app.crud.base import CRUDBase
from app.models.shipment import Shipment
from app.schemas.shipment import ShipmentCreate, ShipmentUpdate
from sqlalchemy.orm import Session
from app.constants.system_constant import SystemConstant


class CRUDRole(CRUDBase[Shipment, ShipmentCreate, ShipmentUpdate]):
    def get_by_name(self, db: Session, *, customer_id: int) -> Optional[Shipment]:
        return db.query(self.model).filter(Shipment.customer_id == customer_id).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[Shipment]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"shipments.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"shipments.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"shipments.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_shipment_id(
            db: Session, *, _id: int
    ) -> Optional[Shipment]:
        return db.query(Shipment).filter(Shipment.id == _id).first()

    @staticmethod
    def get_by_shipment_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Shipment]:
        return db.query(Shipment).filter(Shipment.account_id == _id).first()

    @staticmethod
    def get_pending_shipment_by_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Shipment]:
        return db.query(Shipment).filter(Shipment.account_id == _id,
                                         Shipment.shipment_status ==
                                         SystemConstant.SYSTEM_CONS.get("shipment_status", None)
                                         .__getitem__(f'pending_shipping')).first()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


shipment = CRUDRole(Shipment)
