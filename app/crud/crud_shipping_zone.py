from typing import Optional

from app.crud.base import CRUDBase
from app.models.shipping_zone import ShippingZone
from app.schemas.shipping_zone import ShippingZoneCreate, ShippingZoneUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[ShippingZone, ShippingZoneCreate, ShippingZoneUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[ShippingZone]:
        return db.query(self.model).filter(ShippingZone.name == name).first()

    @staticmethod
    def get_by_shipping_zone_id(
            db: Session, *, _id: int
    ) -> Optional[ShippingZone]:
        return db.query(ShippingZone).filter(ShippingZone.id == _id).first()


shipping_zone = CRUDRole(ShippingZone)
