from typing import Optional, List

from app.crud.base import CRUDBase
from app.models.shipping_address import ShippingAddress
from app.schemas.shipping_address import ShippingAddressCreate, ShippingAddressUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[ShippingAddress, ShippingAddressCreate, ShippingAddressUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[ShippingAddress]:
        return db.query(self.model).filter(ShippingAddress.name == name).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[ShippingAddress]:
        return db.query(self.model).filter(ShippingAddress.account_id == _id).offset(skip).limit(limit).all()

    @staticmethod
    def get_by_shipping_address_id(
            db: Session, *, _id: int
    ) -> Optional[ShippingAddress]:
        return db.query(ShippingAddress).filter(ShippingAddress.id == _id).first()


shipping_address = CRUDRole(ShippingAddress)
