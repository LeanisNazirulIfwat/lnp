from typing import Optional, List, Any
from app.crud.base import CRUDBase
from app.models.api import Api
from app.schemas.api import ApiCreate, ApiUpdate
from sqlalchemy.orm import Session
from sqlalchemy import desc, func, text
from datetime import datetime


class CRUDRole(CRUDBase[Api, ApiCreate, ApiUpdate]):
    def get_by_name(self, db: Session, *, interface_name: str, _id: int) -> Optional[Api]:
        return db.query(self.model).filter(Api.interface_name == interface_name,
                                           Api.account_id == _id).first()

    @staticmethod
    def get_by_api_id(
            db: Session, *, api_id: int
    ) -> Optional[Api]:
        return db.query(Api).filter(Api.id == api_id).first()

    @staticmethod
    def get_by_api_uuid(
            db: Session, *, _uuid: int
    ) -> Optional[Api]:
        return db.query(Api).filter(Api.uuid == _uuid).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[Api]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"apis.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"apis.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"apis.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


api = CRUDRole(Api)
