from typing import Optional

from app.crud.base import CRUDBase
from app.models.shipping_rate import ShippingRate
from app.schemas.shipping_rate import ShippingRateCreate, ShippingRateUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[ShippingRate, ShippingRateCreate, ShippingRateUpdate]):
    def get_by_name(self, db: Session, *, courier: str) -> Optional[ShippingRate]:
        return db.query(self.model).filter(ShippingRate.courier == courier).first()

    @staticmethod
    def get_by_shipping_rate_id(
            db: Session, *, _id: int
    ) -> Optional[ShippingRate]:
        return db.query(ShippingRate).filter(ShippingRate.id == _id).first()


shipping_rate = CRUDRole(ShippingRate)
