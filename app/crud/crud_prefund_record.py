from datetime import datetime
from typing import Optional, Any, List
from sqlalchemy import func, text
from app.crud.base import CRUDBase
from app.models.prefund_record import PrefundRecord
from app.schemas.prefund_record import PrefundRecordCreate, PrefundRecordUpdate
from sqlalchemy.orm import Session, selectinload
from app.constants.role import Role
from app.models.account import Account
from sqlalchemy.orm import Session, joinedload, selectinload
from app.models.company_detail import CompanyDetail


class CRUDRole(CRUDBase[PrefundRecord, PrefundRecordCreate, PrefundRecordUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[PrefundRecord]:
        return db.query(self.model).filter(self.model.switch_pool_id == name).first()

    @staticmethod
    def get_by_payout_record_id(
            db: Session, *, _id: int
    ) -> Optional[PrefundRecord]:
        return db.query(PrefundRecord).filter(PrefundRecord.id == _id).first()

    @staticmethod
    def get_by_payout_switch_id(
            db: Session, *, _id: int
    ) -> Optional[PrefundRecord]:
        return db.query(PrefundRecord).filter(PrefundRecord.switch_pool_id == _id).first()

    @staticmethod
    def get_by_invoice_id(
            db: Session, *, _invoice: int
    ) -> Optional[PrefundRecord]:
        return db.query(PrefundRecord, Account)\
            .join(Account, PrefundRecord.account_id == Account.id) \
            .options(joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)).filter(
            PrefundRecord.transaction_invoice_no == _invoice).first()

    @staticmethod
    def get_by_join_tier_pricing(
            db: Session
    ) -> Any:
        return db.query(PrefundRecord).join()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[PrefundRecord]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"prefund_records.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"prefund_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"prefund_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_payout_record_search_user(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[PrefundRecord]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get_count(self,
                  db: Session, *,
                  start_date: str, end_date: str
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()

    def get_count_based_account(self,
                                db: Session, *, _id: int,
                                start_date: str, end_date: str
                                ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()


prefund_record = CRUDRole(PrefundRecord)
