from datetime import datetime
from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from app.crud.base import CRUDBase
from app.models.pool import Pool
from app.schemas.pool import PoolCreate, PoolUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[Pool, PoolCreate, PoolUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[Pool]:
        return db.query(self.model).filter(Pool.pool_name == title).first()

    @staticmethod
    def get_by_pool_id(
            db: Session, *, _id: int
    ) -> Optional[Pool]:
        return db.query(Pool).filter(Pool.id == _id).first()

    @staticmethod
    def get_by_pool_account_type(
            db: Session, *, _id: int, _account_id: int
    ) -> Optional[Pool]:
        return db.query(Pool).filter(Pool.pool_type == _id,
                                     Pool.account_id == _account_id).first()

    @staticmethod
    def get_by_pool_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Pool]:
        return db.query(Pool).filter(Pool.account_id == _id).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[Pool]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"pools.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


pool = CRUDRole(Pool)
