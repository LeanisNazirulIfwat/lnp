from typing import Optional, List, Any

from app.crud.base import CRUDBase
from app.models.customer_bill_item import CustomerBillItem
from app.models.product import Product
from app.models.customer_bill import CustomerBill
from app.schemas.customer_bill_item import CustomerBillItemCreate, CustomerBillItemUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[CustomerBillItem, CustomerBillItemCreate, CustomerBillItemUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[CustomerBillItem]:
        return db.query(self.model).filter(CustomerBillItem.customer_bill_id == name).first()

    def get_multi_merchant(
        self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[Any]:
        return db.query(self.model, Product)\
            .join(Product, Product.id == self.model.product_id)\
            .offset(skip).limit(limit).all()

    @staticmethod
    def get_by_customer_bill_item_id(
            db: Session, *, _id: int
    ) -> Optional[CustomerBillItem]:
        return db.query(CustomerBillItem).filter(CustomerBillItem.id == _id).first()

    @staticmethod
    def get_by_customer_bill_id(
            db: Session, *, _id: int
    ) -> List[Any]:
        return db.query(CustomerBillItem, Product).join(Product, Product.id == CustomerBillItem.product_id)\
            .filter(CustomerBillItem.customer_bill_id == _id).all()

    @staticmethod
    def get_by_customer_bill_item_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[CustomerBillItem]:
        return db.query(CustomerBillItem, Product).join(Product, Product.id == CustomerBillItem.product_id)\
            .filter(CustomerBillItem.account_id == _id).first()


customer_bill_item = CRUDRole(CustomerBillItem)
