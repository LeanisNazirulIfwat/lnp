from typing import Optional

from app.crud.base import CRUDBase
from app.models.audit_request import AuditRequest
from app.schemas.audit_request import AuditRequestCreate, AuditRequestUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[AuditRequest, AuditRequestCreate, AuditRequestUpdate]):
    def get_by_name(self, db: Session, *, email_: str) -> Optional[AuditRequest]:
        return db.query(self.model).filter(AuditRequest.email == email_).first()

    @staticmethod
    def get_by_api_id(
            db: Session, *, _id: int
    ) -> Optional[AuditRequest]:
        return db.query(AuditRequest).filter(AuditRequest.id == _id).first()


audit_request = CRUDRole(AuditRequest)
