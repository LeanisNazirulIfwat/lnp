from typing import Any, Dict, List, Optional, Union
from sqlalchemy import desc, func, text
import datetime
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models.user import User
from app.schemas.user import UserCreate, UserUpdate
from sqlalchemy.orm import Session
from app.constants.role import Role


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def get_by_email(self, db: Session, *, email: str) -> Optional[User]:
        return db.query(self.model).filter(User.email == email).first()

    def get_by_phone_number(self, db: Session, *, phone_number: str) -> Optional[User]:
        return db.query(self.model).filter(User.phone_number == phone_number).first()

    def get_by_full_name(self, db: Session, *, full_name: str) -> Optional[User]:
        return db.query(self.model).filter(User.full_name == full_name).first()

    def create(self, db: Session, *, obj_in: UserCreate) -> User:
        db_obj = User(
            email=obj_in.email,
            hashed_password=get_password_hash(obj_in.password),
            full_name=obj_in.full_name,
            system_id=obj_in.system_id,
            phone_number=obj_in.phone_number,
            created_at=obj_in.created_at,
            api_key=obj_in.api_key
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def create_adonis(db: Session, *, obj_in: UserCreate) -> User:
        db_obj = User(
            email=obj_in.email,
            hashed_password=obj_in.password,
            full_name=obj_in.full_name,
            system_id=obj_in.system_id,
            phone_number=obj_in.phone_number,
            record_status=2,
            created_at=obj_in.created_at,
            api_key=obj_in.api_key
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
            self,
            db: Session,
            *,
            db_obj: User,
            obj_in: Union[UserUpdate, Dict[str, Any]],
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if "password" in update_data:
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def update_adonis(
            self,
            db: Session,
            *,
            db_obj: User,
            obj_in: Union[UserUpdate, Dict[str, Any]],
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
            if "password" in update_data:
                password = update_data["password"]
                del update_data["password"]
                update_data["hashed_password"] = password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def get_multi(
            self, db: Session, *, skip: int = 0, limit: int = 100,
    ) -> List[User]:
        return db.query(self.model).offset(skip).limit(limit).all()

    def authenticate(
            self, db: Session, *, email: str, password: str
    ) -> Optional[User]:
        user = self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        return user

    def is_active(self, user: User) -> bool:
        return user.is_active

    def get_by_account_id(
            self,
            db: Session,
            *,
            user_id: int,
            skip: int = 0,
            limit: int = 100,
    ) -> List[User]:
        return (
            db.query(self.model)
                .filter(User.id == user_id)
                .offset(skip)
                .limit(limit)
                .all()
        )


user = CRUDUser(User)
