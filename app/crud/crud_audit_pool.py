from datetime import datetime
from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from app.crud.base import CRUDBase
from app.models.audit_pool import AuditPool
from app.schemas.audit_pool import AuditPoolCreate, AuditPoolUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[AuditPool, AuditPoolCreate, AuditPoolUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[AuditPool]:
        return db.query(self.model).filter(AuditPool.title == title).first()

    def get_by_invoice_no(self, db: Session, *, invoice_no: str) -> Optional[AuditPool]:
        return db.query(self.model).filter(AuditPool.invoice_no == invoice_no).first()

    @staticmethod
    def get_by_audit_pool_id(
            db: Session, *, _id: int
    ) -> Optional[AuditPool]:
        return db.query(AuditPool).filter(AuditPool.id == _id).first()

    @staticmethod
    def get_by_audit_pool_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[AuditPool]:
        return db.query(AuditPool).filter(AuditPool.account_id == _id).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[AuditPool]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"audit_pools.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"audit_pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"audit_pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_multi_merchant_based_on_pool(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, pool_type_id: Any
    ) -> List[AuditPool]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == pool_type_id,
                self.model.pool_type == _id,
                self.model.invoice_status == "SUCCESS",
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"audit_pools.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"audit_pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                self.model.pool_type == pool_type_id,
                self.model.invoice_status == "SUCCESS",
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"audit_pools.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()

    def get_count_pool_type_id_based_on_account(self,
                                                db: Session, *, _id: int, start_date: str, end_date: str,
                                                pool_type_id: int,
                                                ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            self.model.pool_type == pool_type_id,
            self.model.invoice_status == "SUCCESS",
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()).count()


audit_pool = CRUDRole(AuditPool)
