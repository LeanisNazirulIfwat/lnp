from typing import Optional

from app.crud.base import CRUDBase
from app.models.account import Account
from app.models.company_detail import CompanyDetail
from app.schemas.account import AccountCreate, AccountUpdate
from sqlalchemy.orm import Session, selectinload, joinedload


class CRUDAccount(CRUDBase[Account, AccountCreate, AccountUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[Account]:
        return db.query(self.model).filter(Account.name == name).first()

    def get_by_merchant_id(self, db: Session, *, merchant_id: str) -> Optional[Account]:
        return db.query(self.model).filter(Account.merchant_id == merchant_id).first()

    def get_multi_account(self, db: Session, id: int) -> Optional[Account]:
        return db.query(self.model)\
            .options(selectinload(self.model.subscription_plan))\
            .filter(Account.user_id == id).all()

    def get_account_by_id(self, db: Session, id: int) -> Optional[Account]:
        return db.query(self.model).filter(Account.id == id).first()

    # get account with child
    def get_account_details_by_id(self, db: Session, id: int) -> Optional[Account]:
        return db.query(self.model).options(joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)).filter(Account.id == id).first()

    def get_account_merchant_id(self, db: Session, id: int) -> Optional[Account]:
        return db.query(self.model).filter(Account.merchant_id == id).first()

    def switch_account(self, db: Session, id: int, account_id: int) -> Optional[Account]:
        query = db.query(self.model).filter(Account.user_id == id).all()
        for x in query:
            if x.id == account_id:
                x.is_active = True
                db.add(x)
            else:
                x.is_active = False
                db.add(x)
        db.commit()
        db.refresh(x)
        return db.query(self.model).filter(Account.user_id == id).all()


account = CRUDAccount(Account)
