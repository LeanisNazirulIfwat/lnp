from typing import Optional

from app.crud.base import CRUDBase
from app.models.system_preference import SystemPreference
from app.schemas.system_preference import SystemPreferenceCreate, SystemPreferenceUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[SystemPreference, SystemPreferenceCreate, SystemPreferenceUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[SystemPreference]:
        return db.query(self.model).filter(SystemPreference.name == name).first()

    @staticmethod
    def get_by_system_preference_id(
            db: Session, *, _id: int
    ) -> Optional[SystemPreference]:
        return db.query(SystemPreference).filter(SystemPreference.id == _id).first()


system_preference = CRUDRole(SystemPreference)
