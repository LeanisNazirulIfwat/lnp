from datetime import datetime
from typing import Optional, Any, List
from sqlalchemy import func, text
from app.crud.base import CRUDBase
from app.models.transaction_topup_record import TransactionTopupRecord
from app.schemas.transaction_topup_record import TransactionTopupRecordCreate, TransactionTopupRecordUpdate
from sqlalchemy.orm import Session, selectinload
from app.constants.role import Role
from app.models.account import Account
from sqlalchemy.orm import Session, joinedload, selectinload
from app.models.company_detail import CompanyDetail

class CRUDRole(CRUDBase[TransactionTopupRecord, TransactionTopupRecordCreate, TransactionTopupRecordUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[TransactionTopupRecord]:
        return db.query(self.model).filter(self.model.invoice_no == name).first()

    @staticmethod
    def get_by_transaction_record_id(
            db: Session, *, _id: int
    ) -> Optional[TransactionTopupRecord]:
        return db.query(TransactionTopupRecord).filter(TransactionTopupRecord.id == _id).first()

    @staticmethod
    def get_by_transaction_invoice_id(
            db: Session, *, _invoice: str
    ) -> Optional[TransactionTopupRecord]:
        return db.query(TransactionTopupRecord).filter(TransactionTopupRecord.transaction_invoice_no == _invoice).first()

    @staticmethod
    def get_by_invoice_id(
            db: Session, *, _invoice: int
    ) -> Optional[TransactionTopupRecord]:
        return db.query(TransactionTopupRecord, Account)\
            .join(Account, TransactionTopupRecord.account_id == Account.id) \
            .options(joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)).filter(
            TransactionTopupRecord.transaction_invoice_no == _invoice).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[TransactionTopupRecord]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"transaction_topup_records.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"transaction_topup_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"transaction_topup_records.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_payout_record_search_user(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: Any, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any, table_name: str, role: str
    ) -> List[TransactionTopupRecord]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .filter(text(f"{table_name}.{search_column} LIKE '%{search_key}%'")) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
        else:
            if not role == Role.SUPER_ADMIN["name"]:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    self.model.user_id == _id,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()
            else:
                return db.query(self.model).filter(
                    self.model.record_status == record_status,
                    func.date(self.model.created_at) >= cur_start_date.date(),
                    func.date(self.model.created_at) <= cur_end_date.date()) \
                    .order_by(text(f"{table_name}.{parameter_name} {sort_type}")).offset(skip) \
                    .limit(limit).all()

    def get_count(self,
                  db: Session, *,
                  start_date: str, end_date: str
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()

    def get_count_based_account(self,
                                db: Session, *, _id: int,
                                start_date: str, end_date: str
                                ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()


transaction_topup_record = CRUDRole(TransactionTopupRecord)
