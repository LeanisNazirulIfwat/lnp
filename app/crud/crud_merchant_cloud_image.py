from typing import Optional, List, Any
from sqlalchemy import desc, func, text
from datetime import datetime
from app.constants.role import Role
from app.crud.base import CRUDBase
from app.models.merchant_cloud_image import MerchantCloudImage
from app.schemas.merchant_cloud_image import MerchantCloudImageCreate, MerchantCloudImageUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[MerchantCloudImage, MerchantCloudImageCreate, MerchantCloudImageUpdate]):
    def get_by_file_name(self, db: Session, *, file_name: str) -> Optional[MerchantCloudImage]:
        return db.query(self.model).filter(self.model.file_name == file_name).first()

    def get_active_image(self, db: Session, *, status: bool) -> Optional[MerchantCloudImage]:
        return db.query(self.model).filter(self.model.is_active == status).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[MerchantCloudImage]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"merchant_cloud_images.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"merchant_cloud_images.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"merchant_cloud_images.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_merchant_cloud_id(
            db: Session, *, _id: int
    ) -> Optional[MerchantCloudImage]:
        return db.query(MerchantCloudImage).filter(MerchantCloudImage.id == _id).first()

    @staticmethod
    def get_cloud_merchant_by_id(
            db: Session, *, _id: int
    ) -> Optional[MerchantCloudImage]:
        return db.query(MerchantCloudImage).filter(MerchantCloudImage.account_id == _id).first()

    def get_count(self,
                  db: Session, *, _id: int,
                  start_date: str, end_date: str,
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()).count()

    def get_count_admin_record(self,
                               db: Session, *, _id: Any, start_date: str, end_date: str, _record: int, role: str
                               ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if not role == Role.SUPER_ADMIN["name"]:
            return db.query(self.model).filter(
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date(),
                self.model.record_status == _record
            ).count()
        else:
            return db.query(self.model).filter(
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date(),
                self.model.record_status == _record
            ).count()


merchant_cloud_image = CRUDRole(MerchantCloudImage)
