from typing import Optional, List

from app.crud.base import CRUDBase
from app.models.merchant_shipping_address import MerchantShippingAddress
from app.schemas.merchant_shipping_address import MerchantShippingAddressCreate, MerchantShippingAddressUpdate
from sqlalchemy.orm import Session
from sqlalchemy.orm import Session, joinedload, selectinload


class CRUDRole(CRUDBase[MerchantShippingAddress, MerchantShippingAddressCreate, MerchantShippingAddressUpdate]):
    def get_by_name(self, db: Session, *, name: str) -> Optional[MerchantShippingAddress]:
        return db.query(self.model).filter(self.model.name == name).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[MerchantShippingAddress]:
        return db.query(self.model).options(selectinload(self.model.shipping_address_shipping_profile)) \
            .filter(self.model.shipping_address_shipping_profile.account_id == _id).offset(skip).limit(limit).all()

    def get_by_shipping_address_id(
            self, db: Session, *, _id: int
    ) -> Optional[MerchantShippingAddress]:
        return db.query(self.model).filter(self.model.id == _id).first()


shipping_address = CRUDRole(MerchantShippingAddress)
