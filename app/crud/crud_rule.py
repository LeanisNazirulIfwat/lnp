from typing import Optional

from app.crud.base import CRUDBase
from app.models.rule import Rule
from app.schemas.rule import RuleCreate, RuleUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[Rule, RuleCreate, RuleUpdate]):
    def get_by_name(self, db: Session, *, group: str) -> Optional[Rule]:
        return db.query(self.model).filter(Rule.group == group).first()

    @staticmethod
    def get_by_rule_id(
            db: Session, *, _id: int
    ) -> Optional[Rule]:
        return db.query(Rule).filter(Rule.id == _id).first()


rule = CRUDRole(Rule)
