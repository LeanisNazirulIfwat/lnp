from typing import Optional

from app.crud.base import CRUDBase
from app.models.company_detail import CompanyDetail
from app.models.account import Account
from app.schemas.company_detail import CompanyDetailCreate, CompanyDetailUpdate
from sqlalchemy.orm import Session, selectinload


class CRUDRole(CRUDBase[CompanyDetail, CompanyDetailCreate, CompanyDetailUpdate]):
    def get_by_name(self, db: Session, *, company_name: str) -> Optional[CompanyDetail]:
        return db.query(self.model).filter(CompanyDetail.company_name == company_name).first()

    @staticmethod
    def get_by_company_detail_id(
            db: Session, *, _id: int
    ) -> Optional[CompanyDetail]:
        return db.query(CompanyDetail).filter(CompanyDetail.id == _id)\
            .options(selectinload(CompanyDetail.company_detail_business_owner_detail)).first()

    @staticmethod
    def get_by_company_detail_account_id(
            db: Session, *, _id: int
    ) -> Optional[CompanyDetail]:
        return db.query(CompanyDetail, Account).join(Account, Account.company_details_id == CompanyDetail.id)\
            .filter(Account.id == _id).options(selectinload(CompanyDetail.company_detail_business_owner_detail)).first()


company_detail = CRUDRole(CompanyDetail)
