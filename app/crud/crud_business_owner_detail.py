from typing import Optional

from app.crud.base import CRUDBase
from app.models.business_owner_detail import BusinessOwnerDetail
from app.schemas.business_owner_detail import BusinessOwnerDetailCreate, BusinessOwnerDetailUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[BusinessOwnerDetail, BusinessOwnerDetailCreate, BusinessOwnerDetailUpdate]):
    def get_by_name(self, db: Session, *, legal_name: str) -> Optional[BusinessOwnerDetail]:
        return db.query(self.model).filter(BusinessOwnerDetail.legal_name == legal_name).first()

    @staticmethod
    def get_by_business_owner_detail_id(
            db: Session, *, _id: int
    ) -> Optional[BusinessOwnerDetail]:
        return db.query(BusinessOwnerDetail).filter(BusinessOwnerDetail.id == _id).first()


business_owner_detail = CRUDRole(BusinessOwnerDetail)
