from typing import Optional, Any, List
from app.crud.base import CRUDBase
from app.models.collection import Collection
from app.models.account import Account
from app.models.company_detail import CompanyDetail
from app.schemas.collection import CollectionCreate, CollectionUpdate, CollectionView
from sqlalchemy.orm import Session, selectinload, joinedload, subqueryload
from sqlalchemy import desc, func, text
from datetime import datetime


class CRUDRole(CRUDBase[Collection, CollectionCreate, CollectionUpdate]):
    def get_by_name(self, db: Session, *, title: str) -> Optional[Collection]:
        return db.query(self.model).filter(Collection.title == title).first()

    def get_by_transaction_uuid(self, db: Session, *, _uuid: str) -> Optional[Collection]:
        return db.query(self.model).filter(Collection.uuid == _uuid).first()

    def get_by_uuid(self, db: Session, *, uuid: str) -> Optional[Collection]:
        return db.query(self.model) \
            .options(selectinload(self.model.collection_share_setting)) \
            .options(selectinload(self.model.collection_success_field)) \
            .options(selectinload(self.model.collection_payment_setting)) \
            .options(selectinload(self.model.collection_support_detail)) \
            .options(selectinload(self.model.collection_account).joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)) \
            .filter(Collection.uuid == uuid).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[CollectionView]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"collections.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"collections.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"collections.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_collection_id(
            db: Session, *, _id: int
    ) -> Optional[Collection]:
        return db.query(Collection) \
            .options(selectinload(Collection.collection_share_setting)) \
            .options(selectinload(Collection.collection_success_field)) \
            .options(selectinload(Collection.collection_payment_setting)) \
            .options(selectinload(Collection.collection_support_detail)) \
            .filter(Collection.id == _id).first()

    def get_by_uuid_account(self, db: Session, *, _uuid: str) -> Optional[Collection]:
        return db.query(self.model).options(
            selectinload(self.model.collection_account).joinedload(Account.subscription_plan)).filter(
            Collection.uuid == _uuid).first()

    @staticmethod
    def get_quantity_limit_count(
            db: Session, *, _uuid: int
    ) -> Optional[Collection]:
        return db.query(Collection).filter(Collection.uuid == _uuid).first()

    @staticmethod
    def get_multi_view(
            db: Session, *, skip: int = 0, limit: int = 100
    ) -> List[CollectionView]:
        return db.query(Collection).offset(skip).limit(limit).all()

    def get_count(self,
                  db: Session, *,
                  start_date: str, end_date: str, record_status: int
                  ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date(),
            self.model.record_status == record_status
        ).count()

    def get_count_merchant(self,
                           db: Session, *, _id: int,
                           start_date: str, end_date: str, record_status: int
                           ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date(),
            self.model.record_status == record_status,
            self.model.account_id == _id
        ).count()


collection = CRUDRole(Collection)
