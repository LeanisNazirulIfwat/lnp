from typing import Optional, List

from app.crud.base import CRUDBase
from app.models.payment_setting import PaymentSetting
from app.schemas.payment_setting import PaymentSettingCreate, PaymentSettingUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[PaymentSetting, PaymentSettingCreate, PaymentSettingUpdate]):
    def get_by_name(self, db: Session, *, _id: str) -> Optional[PaymentSetting]:
        return db.query(self.model).filter(PaymentSetting.account_id == _id).first()

    def get_multi_merchant(
        self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[PaymentSetting]:
        return db.query(self.model).filter(PaymentSetting.account_id == _id).offset(skip).limit(limit).all()

    @staticmethod
    def get_by_payment_setting_id(
            db: Session, *, _id: int
    ) -> Optional[PaymentSetting]:
        return db.query(PaymentSetting).filter(PaymentSetting.id == _id).first()

    @staticmethod
    def get_by_payment_setting_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[PaymentSetting]:
        return db.query(PaymentSetting).filter(PaymentSetting.account_id == _id).first()


payment_setting = CRUDRole(PaymentSetting)
