from typing import Optional, List, Any
from datetime import datetime
from app.crud.base import CRUDBase
from app.models.customer_bill import CustomerBill
from app.models.account import Account
from app.models.company_detail import CompanyDetail
from app.schemas.customer_bill import CustomerBillCreate, CustomerBillUpdate, CustomerBillInDBBase
from sqlalchemy.orm import Session, joinedload, selectinload
from sqlalchemy import desc, func, text
from app.core.config import settings


class CRUDRole(CRUDBase[CustomerBill, CustomerBillCreate, CustomerBillUpdate]):
    def get_by_name(self, db: Session, *, full_name: str) -> Optional[CustomerBill]:
        return db.query(self.model).filter(CustomerBill.full_name == full_name).first()

    def get_by_invoice_no(self, db: Session, *, invoice_no: str) -> Optional[CustomerBill]:
        return db.query(self.model).filter(CustomerBill.invoice_no == invoice_no).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[CustomerBill]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        if search_enable:
            return db.query(self.model) \
                .options(selectinload(self.model.customer_bill_payment_record)) \
                .filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"customer_bills.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"customer_bills.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model) \
                .options(selectinload(self.model.customer_bill_payment_record)) \
                .filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"customer_bills.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    def get_multi_customer_id(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[CustomerBill]:
        return db.query(self.model).options(selectinload(self.model.customer_bill_payment_record)) \
            .filter(CustomerBill.customer_id == _id).offset(skip).limit(limit).all()

    def get_multi_collection_id(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[CustomerBill]:

        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model) \
                .options(selectinload(self.model.customer_bill_payment_record)) \
                .filter(
                self.model.record_status == record_status,
                self.model.collection_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"customer_bills.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"customer_bills.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model) \
                .options(selectinload(self.model.customer_bill_payment_record)) \
                .filter(
                self.model.record_status == record_status,
                self.model.collection_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"customer_bills.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_customer_bill_id(
            db: Session, *, _id: int
    ) -> Optional[CustomerBill]:
        return db.query(CustomerBill) \
            .options(joinedload(CustomerBill.customer_bill_collection)) \
            .options(joinedload(CustomerBill.customer_bill_shipping_address)) \
            .options(joinedload(CustomerBill.customer_bill_payment_record)) \
            .filter(CustomerBill.id == _id).first()

    @staticmethod
    def get_by_customer_by_invoice_no(
            db: Session, *, _invoice: int
    ) -> Optional[CustomerBill]:
        return db.query(CustomerBill, Account).options(joinedload(CustomerBill.customer_bill_collection)) \
            .join(Account, CustomerBill.account_id == Account.id) \
            .options(joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)).filter(
            CustomerBill.invoice_no == _invoice).first()

    @staticmethod
    def get_customer_by_invoice_no_account(
            db: Session, *, _invoice: int
    ) -> Optional[CustomerBill]:
        return db.query(CustomerBill, Account).join(Account, CustomerBill.account_id == Account.id).options(
            joinedload(Account.subscription_plan)).filter(
            CustomerBill.invoice_no == _invoice).first()

    @staticmethod
    def get_by_customer_by_active_invoice_no(
            db: Session, *, _invoice: int
    ) -> Optional[CustomerBill]:
        return db.query(CustomerBill, Account).options(joinedload(CustomerBill.customer_bill_collection)) \
            .join(Account, CustomerBill.account_id == Account.id) \
            .options(joinedload(Account.account_company_details)
                     .joinedload(CompanyDetail.company_detail_business_owner_detail)).filter(
            CustomerBill.invoice_status_id == settings.TRANSACTION_PENDING,
            CustomerBill.invoice_no == _invoice).first()

    @staticmethod
    def get_by_customer_by_switch_invoice_no(
            db: Session, *, _invoice: int
    ) -> Optional[CustomerBill]:
        return db.query(CustomerBill).filter(CustomerBill.transaction_invoice_no == _invoice).first()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()

    def get_count_date(self,
                       db: Session, *, _id: int, start_date, end_date
                       ) -> Optional[Any]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")
        return db.query(self.model).filter(
            self.model.account_id == _id,
            func.date(self.model.created_at) >= cur_start_date.date(),
            func.date(self.model.created_at) <= cur_end_date.date()
        ).count()

    def get_count_collection(self,
                             db: Session, *, _id: int
                             ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.collection_id == _id).count()


customer_bill = CRUDRole(CustomerBill)
