from typing import Optional, List

from app.crud.base import CRUDBase
from app.models.option import SystemOption
from app.schemas.option import SystemOptionCreate, SystemOptionUpdate
from sqlalchemy.orm import Session


class CRUDRole(CRUDBase[SystemOption, SystemOptionCreate, SystemOptionUpdate]):
    def get_by_name(self, db: Session, *, _name: str) -> Optional[SystemOption]:
        return db.query(self.model).filter(SystemOption.company_name == _name).first()

    def get_multi_merchant(
        self, db: Session, *, skip: int = 0, limit: int = 100, _id: int
    ) -> List[SystemOption]:
        return db.query(self.model).filter(SystemOption.account_id == _id).offset(skip).limit(limit).all()

    @staticmethod
    def get_by_system_option_id(
            db: Session, *, _id: int
    ) -> Optional[SystemOption]:
        return db.query(SystemOption).filter(SystemOption.id == _id).first()

    @staticmethod
    def get_by_system_option_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[SystemOption]:
        return db.query(SystemOption).filter(SystemOption.account_id == _id).first()

    @staticmethod
    def get_by_system_own_id(
            db: Session, *, _id: str
    ) -> Optional[SystemOption]:
        return db.query(SystemOption).filter(SystemOption.api_parent_key == _id).first()


system_option = CRUDRole(SystemOption)
