from typing import Optional, List, Any

from app.crud.base import CRUDBase
from app.models.product import Product
from app.schemas.product import ProductCreate, ProductUpdate
from sqlalchemy.orm import Session, selectinload
from sqlalchemy import or_
from sqlalchemy import desc, func, text
from datetime import datetime


class CRUDRole(CRUDBase[Product, ProductCreate, ProductUpdate]):
    def get_by_name(self, db: Session, *, title: str, code: str) -> Optional[Product]:
        return db.query(self.model).filter(or_(Product.title == title, Product.code == code)).first()

    def get_multi_merchant(
            self, db: Session, *, skip: int = 0, limit: int = 100, _id: int, record_status: Any,
            start_date: str, end_date: str, search_column: str, search_key: str,
            parameter_name: str, sort_type: str, search_enable: bool, invoice_status: Any
    ) -> List[Product]:
        cur_start_date = datetime.strptime(start_date, "%d-%m-%Y")
        cur_end_date = datetime.strptime(end_date, "%d-%m-%Y")

        if search_enable:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .filter(text(f"products.{search_column} LIKE '%{search_key}%'")) \
                .order_by(text(f"products.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()
        else:
            return db.query(self.model).filter(
                self.model.record_status == record_status,
                self.model.account_id == _id,
                func.date(self.model.created_at) >= cur_start_date.date(),
                func.date(self.model.created_at) <= cur_end_date.date()) \
                .order_by(text(f"products.{parameter_name} {sort_type}")).offset(skip) \
                .limit(limit).all()

    @staticmethod
    def get_by_product_id(
            db: Session, *, _id: int
    ) -> Optional[Product]:
        return db.query(Product)\
            .options(selectinload(Product.product_product_variant))\
            .options(selectinload(Product.product_tier_pricing))\
            .filter(Product.id == _id).first()

    @staticmethod
    def get_by_product_merchant_id(
            db: Session, *, _id: int
    ) -> Optional[Product]:
        return db.query(Product).filter(Product.account_id == _id).all()

    def get_count(self,
                  db: Session, *, _id: int
                  ) -> Optional[Any]:
        return db.query(self.model).filter(
            self.model.account_id == _id).count()


product = CRUDRole(Product)
