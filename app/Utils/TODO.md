#update model and schema
    pending on schema for settlement
    pending on schema for payload
    pending on schema for response field from fpx 2c2p etc

# TODO
    generate secret key for api usage
    shipping address
    payment bill
    create collection
    main logic on merchant management system
    sort role right module
    resizing image
    report
    dashboard
    subscription
    recurring
    settlement

# NOTE
    @app.middleware("http")
    async def request_middleware(request: Request, call_next):
        logger.info("Request started")
        try:
            return await call_next(request)
    
        except Exception as ex:
            logger.error(ex)

# NOTE 2

    sorted_services = sorted(services, key=lambda z: z.payment_service)
    sorted_group = [{key: list(result)} for key, result in
                                groupby(sorted_services, key=lambda y: y.payment_service)]