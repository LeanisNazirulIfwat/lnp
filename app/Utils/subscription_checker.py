import datetime
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union
from sqlalchemy import desc, func, text

from app import schemas
from app.db.base import Base
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from app.constants.role import Role

ModelType = TypeVar("ModelType", bound=Base)
SchemaType = TypeVar("SchemaType", bound=BaseModel)


class Subscription:

    def __init__(self, model: Type[ModelType], current_sub: Type[SchemaType], account_id: int):
        self.model = model
        self.sub = schemas.SubscriptionCreate(**jsonable_encoder(current_sub))
        self.id = account_id

    def check_subscription_payment_form(self, db: Session) -> bool:
        _collection = db.query(self.model).filter(self.model.account_id == self.id,
                                                  self.model.collection_method == 1).count()
        if _collection >= self.sub.payment_form:
            return False
        return True

    def check_subscription_bill_form(self, db: Session) -> bool:
        _collection = db.query(self.model).filter(self.model.account_id == self.id,
                                                  self.model.collection_method == 2).count()
        if _collection >= self.sub.payment_form:
            return False
        return True

    def check_subscription_domain(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= self.sub.domain:
            return False
        return True

    def check_subscription_short_url(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= int(self.sub.short_url):
            return False
        return True

    def check_subscription_store(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= self.sub.store:
            return False
        return True

    def check_subscription_product(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= self.sub.product:
            return False
        return True

    def check_subscription_api(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= int(self.sub.api):
            return False
        return True

    def check_subscription_virtual_account(self, db: Session) -> bool:
        _domain = db.query(self.model).filter(self.model.account_id == self.id).count()
        if _domain >= int(self.sub.virtual_account):
            return False
        return True
