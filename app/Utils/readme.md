# PIP INSTALL

    pip install "python-jose[cryptography]"
    pip install "passlib[bcrypt]"
    pip install "fastapi[all]"
    pip install alembic
    pip install SQLAlchemy
    pip install inflect
    pip install PyMySQL
    alembic init

#MIGRATION
    adjust env on alembic
    adjust meta data in the model so can detect auto migration

# Table Migration
    alembic revision --autogenerate -m "Initiate Table"
    alembic upgrade head

#adjust changes when updating data
    add model update info at db.base file
    add schema update info at schema.init file
    add crude update info at crud.init file
    add route update info at route.api file

#Assign role when using API
        current_user: models.User = Security(
            deps.get_current_active_user,
            scopes=[Role.ADMIN["name"], Role.SUPER_ADMIN["name"]],
        ),

#Using API only with token
    current_user: models.User = Depends(deps.get_current_active_user),

#query like
    yearly_sale = db.query(CustomerBill).filter(CustomerBill.invoice_status == _in.status.upper(),
    CustomerBill.account_id == new_token["account_id"],
    func.date(models.CustomerBill.created_at) >= cur_start_date.date(),
    func.date(models.CustomerBill.created_at) <= cur_end_date.date())\
    .filter(or_(CustomerBill.invoice_no.ilike(f"%{_in.search.search_key}%"),
    CustomerBill.full_name.ilike(f"%{_in.search.search_key}%")))\
    .order_by(text(f"{_in.sort.parameter_name} {_in.sort.sort_type}")).offset(skip).limit(limit).all()