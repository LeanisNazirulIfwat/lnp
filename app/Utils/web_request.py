import json
import time
import asyncio
from typing import Dict, Any, List, Tuple
import requests
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from itertools import repeat
from aiohttp import ClientSession
from app import schemas


def http_get_with_requests(url: str, headers: Dict = {}, proxies: Dict = {}, payload: dict = {}, timeout: int = 10) -> \
        (int, Dict[str, Any], bytes):
    response = requests.post(url, headers=headers, proxies=proxies, timeout=timeout, data=payload)
    response_json = None
    try:
        response_json = response.json()
    except:
        pass

    response_content = None
    try:
        response_content = response.content
    except:
        pass

    return response.status_code, response_json, response_content


def http_get_with_requests_parallel(list_of_urls: List[str], headers: Dict = {}, proxies: Dict = {}, payload: dict = {},
                                    timeout: int = 10) -> (List[Tuple[int, Dict[str, Any], bytes]], float):
    t1 = time.time()
    results = []
    executor = ThreadPoolExecutor(max_workers=100)
    for result in executor.map(http_get_with_requests, list_of_urls, repeat(headers), repeat(proxies), repeat(payload),
                               repeat(timeout)):
        results.append(result)
    t2 = time.time()
    t = t2 - t1
    return results, t


async def http_post_with_aiohttp(session: ClientSession, url: str, headers: Dict = {}, payload: dict = {},
                                 proxy: str = None,
                                 timeout: int = 10) -> (int, Dict[str, Any], bytes):
    response = await session.post(url=url, headers=headers, proxy=proxy, timeout=timeout, data=payload)

    response_json = None
    try:
        response_json = await response.json(content_type=None)
    except json.decoder.JSONDecodeError as e:
        pass

    response_content = None
    try:
        response_content = await response.read()
    except:
        pass

    return response.status, response_json, response_content


async def http_post_with_aiohttp_parallel(session: ClientSession, list_of_urls: List[str], headers: Dict = {},
                                          payload: dict = {}, proxy: str = None, timeout: int = 10) -> (
        List[Tuple[int, Dict[str, Any], bytes]], float):
    t1 = time.time()
    results = await asyncio.gather(
        *[http_post_with_aiohttp(session, url, headers, payload, proxy, timeout) for url in list_of_urls])
    t2 = time.time()
    t = t2 - t1
    return results, t


async def parcel_asia_with_aiohttp(session: ClientSession, url: str, headers: Dict = {}, payload: dict = {},
                                   proxy: str = None,
                                   timeout: int = 10) -> (int, Dict[str, Any], bytes):
    response_ = schemas.ParcelAsiaHashModel(**payload)
    response_.hash = ""
    response = await session.post(url=url, headers=headers, data=json.dumps(response_.dict()))

    response_json = None
    try:
        response_json = await response.json(content_type=None)
    except json.decoder.JSONDecodeError as e:
        pass

    response_content = None
    try:
        response_content = await response.read()
    except:
        pass

    return response.status, response_json, response_content
