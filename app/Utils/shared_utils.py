import calendar
import decimal
import json
import os
import uuid
from datetime import datetime, timedelta
from typing import Any, Union
from fastapi import Request
from sqlalchemy.orm import Session
from app import models, schemas, crud
from fastapi import APIRouter, Depends, HTTPException, Security, status, Request, BackgroundTasks
import io as BytesIO
import base64
from twilio.rest import Client
from app.core.config import settings
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from hashlib import sha256
from fastapi.encoders import jsonable_encoder
from dateutil.relativedelta import relativedelta
from decimal import Decimal
import string
import random


def get_next_billing_date(start_date: datetime, end_date: datetime, repetition):
    new_date = datetime.now()
    last_date_of_month = calendar.monthrange(start_date.year, datetime.now().month)[1]
    next_year = start_date + relativedelta(year=1)
    if repetition == "Months":
        new_date = datetime(start_date.year, datetime.now().month, last_date_of_month)
    elif repetition == "Yearly":
        new_date = datetime(next_year.year, datetime.now().month, last_date_of_month)
    return new_date


async def daterange(date1, date2):
    list_date = []
    for n in range(int((date1 - date2).days) + 1):
        list_date.append(date2 + timedelta(n))

    return list_date


def calculate_transaction_fee(rate_value: Decimal, rate_type: int, amount: Decimal):
    value = 0
    if rate_type == 2:
        value = (rate_value / 100) * Decimal(amount)
    else:
        value = rate_value

    return value


def get_next_subscription_activation(start_date: datetime):
    activation_day = 5
    activation_date = start_date + relativedelta(weeks=4)
    if activation_date.day > activation_day:
        return datetime(day=activation_day, month=activation_date.month, year=activation_date.year)
    else:
        return datetime(day=activation_day, month=(datetime.now()).month, year=datetime.now().year)


def b64_to_pdf(b64content):
    buffer = BytesIO.BytesIO()
    content = base64.b64decode(b64content)
    buffer.write(content)
    return buffer


def generate_password(length=10):
    """Generate a random password of specified length"""
    chars = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(chars) for _ in range(10))
    return password


class SharedUtils:

    def my_random_string(string_length=10):
        """Returns a random string of length string_length."""
        random = str(uuid.uuid4())  # Convert UUID format to a Python string.
        random = random.upper()  # Make all characters uppercase.
        random = random.replace("-", "")  # Remove the UUID '-'.
        return random[0:string_length]  # Return the random string.

    @staticmethod
    def get_provider_list(value):
        payment_service_id = value

        file_path = f"./assets/json_file/payment_list.json"

        if os.path.exists(file_path):
            with open(file_path, 'r') as f:
                data = json.load(f)
        new_list = []
        [new_list.append(schemas.SwitchPaymentResponses(**x)) for x in data]
        new_list_search = [x for x in new_list if x.payment_service_id == payment_service_id]

        return new_list_search[0].dict(exclude={'c2pServices', 'preferred_integration_provider_code'},
                                       exclude_unset=True)

    @staticmethod
    def create_pagination(data: Any, page_num: int = 1, page_size: int = 10):
        start = (page_num - 1) * page_size
        end = start + page_size
        data_length = len(data)
        response = {
            "data": data[start:end],
            "total": data_length,
            "count": page_size,
            "pagination": {}
        }

        if end >= data_length:
            response["pagination"]["next"] = None

            if page_num > 1:
                response["pagination"]["previous"] = f"page_num={page_num - 1}&page_size={page_size}"
            else:
                response["pagination"]["previous"] = None

        else:
            if page_num > 1:
                response["pagination"]["previous"] = f"page_num={page_num - 1}&page_size={page_size}"
            else:
                response["pagination"]["previous"] = None

            response["pagination"]["next"] = f"page_num={page_num + 1}&page_size={page_size}"

    @staticmethod
    def discount_for():
        list_dis = ['Product', 'Bill']
        return list_dis

    @staticmethod
    def initiate_by():
        list_init = ['Customer', 'Merchant']
        return list_init

    @staticmethod
    def kind():
        list_kind = ['Repetition', 'Date Range']
        return list_kind

    @staticmethod
    def recurrable_type():
        list_rec = ['Collection', 'Store']
        return list_rec

    @staticmethod
    def discount_type():
        list_disc = ['Fixed', 'Bill']
        return list_disc

    @staticmethod
    def frequency():
        list_freq = ['Daily', 'Weekly', 'Monthly', 'yearly']
        return list_freq

    @staticmethod
    def payment_mode():
        list_pay = ['Merchant', 'Customer', 'Merchant Credit']
        return list_pay

    @staticmethod
    def send_sms(to_number, body):
        client = Client(settings.TWILIO_LEAN_ACCOUNT_SID, settings.TWILIO_LEAN_AUTH_TOKEN)
        return client.messages.create(messaging_service_sid=settings.TWILIO_LEAN_MESSAGING_SERVICE_ID,
                                      to=to_number, body=body, status_callback=
                                      "https://stag-api.leanpay.my/api/v1/twilio-services/twilio-sms-callback")

    @staticmethod
    def send_whatsapp_sms(to_number, body):
        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        return client.messages.create(from_=f'whatsapp:{settings.TWILIO_WHATSAPP_NUMBER}',
                                      to=f'whatsapp:{to_number}', body=body)

    @staticmethod
    def send_sendgrid_email(from_email, to_email, bcc_email, subject, body, template_id,
                            template_enable: bool):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject=subject,
            html_content=f'<strong>{body}</strong>')

        if template_enable:
            message.dynamic_template_data = {
                'name': 'Leanis'
            }

            message.template_id = 'd-4e4663a182e2421f98932fc8700717d1'
            if template_id:
                message.template_id = template_id
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_verification_sendgrid_email(from_email, to_email, bcc_email, verif_link, name):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = {
            "name": name,
            "account_confirmation_link": verif_link,
            "contact_support_link": "admin@leanis.com.my"
        }
        message.template_id = 'd-a620181ad0514bedaa3b12e52dfcf672'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_invoice_sendgrid_email(from_email, to_email, bcc_email, verif_link):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = {
            "name": "Merchant",
            "account_confirmation_link": verif_link,
            "contact_support_link": "admin@leanis.com.my"
        }
        message.template_id = 'd-a620181ad0514bedaa3b12e52dfcf672'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_reset_password_sendgrid_email(from_email, to_email, bcc_email, reset_password_new_password):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = json.loads(reset_password_new_password)
        message.template_id = 'd-be2c75eab6da44b195668c742c7231a8'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_payment_invoice_sendgrid_email(from_email, to_email, bcc_email, data):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = json.loads(data)
        message.template_id = 'd-88c3a2c40c00419c9f90b3041aedc436'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_payment_invoice_sendgrid_email_code(from_email, to_email, bcc_email, data):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = {
            "subject": "Payment Invoice for #BL-2123123123-LP",
            "recipient_name": "John Dow Ray Me",
            "reset_password_new_password": "#Pa5$w0Rd%",

            "leanx_support_email": "support@leanx.io",
            "leanx_add_line_1": "Unit 8, Level 13A, Tower 1, ",
            "leanx_add_line_2": "8trium, Jalan Cempaka SD12/5, ",
            "leanx_city": "Bandar Sri Damansara ",
            "leanx_postcode": "52200 ",
            "leanx_company_name": "LeanX ",
            "leanx_io_name": "leanx.io ",
            "leanx_state": "Kuala Lumpur",
            "leanx_country": "Malaysia",
            "leanis_website": "https://leanis.com.my",
            "leanx_website": "https://leanx.io",

            "bill_id": "BL-2123123123-LP",
            "order_total": 3333.33,
            "merchant_company_name": "Sahabat Sahambal",
            "payment_link": "https://leanx.io",
            "order_status": 2,
            "orderHistory": [
                {
                    "name": "Washable Marker Pen",
                    "quantity": 22,
                    "price": "200.00"
                },
                {
                    "name": "Disposable White board wipe",
                    "quantity": 112,
                    "price": "30.00"
                },
                {
                    "name": "Permanent Marker Pen",
                    "quantity": 22,
                    "price": "888.88"
                }
            ]
        }
        message.template_id = 'd-84fd5348427745ea8e4cb46b4bc29fa5'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_fund_notification_sendgrid_email(from_email, to_email, bcc_email, data):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = json.loads(data)
        message.template_id = 'd-291997bf0b0b4f78ae0d88a473bd131d'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_payout_notification_sendgrid_email(from_email, to_email, bcc_email, data):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = json.loads(data)
        message.template_id = 'd-7190b6a33e5749d1a9243614bd488fbc'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def send_prefund_notification_sendgrid_email(from_email, to_email, bcc_email, data):
        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        message.dynamic_template_data = json.loads(data)
        message.template_id = 'd-c3625b0ba1d04d32a6e36289512db266'
        message.add_bcc(bcc_email)
        sg = SendGridAPIClient(settings.TWILIO_LEAN_SENDGRID_API)
        response = sg.send(message)
        return response

    @staticmethod
    def generate_payload(*args, **kwargs):
        payload = {name: kwargs[name] for name in kwargs if kwargs[name] is not None}
        return payload

    @staticmethod
    def hash_payload(data: dict, secret_key: str) -> Union[str, Any]:
        key_data = f"{jsonable_encoder(data)}{secret_key}"
        m = sha256()
        m.update(key_data.encode())
        m.digest()
        return m.hexdigest(), key_data

    @staticmethod
    def get_available_payment_services(data: schemas.SubscriptionCreate):
        result = []
        if data.fpx_enable:
            result.append('FPX')
        if data.credit_card_enable:
            result.append('CARD_CREDIT')
        if data.ewallet_enable:
            result.append('EWALLET')
        if data.bnpl_enable:
            result.append('BNPL')

        return result
