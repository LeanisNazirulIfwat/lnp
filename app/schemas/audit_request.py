from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator


# Shared properties
class AuditRequestBase(BaseModel):
    method: Optional[str]
    url: Optional[str]
    email: Optional[str]
    account_id: Optional[str]
    description: Optional[str]
    data: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via AuditRequest on creation
class AuditRequestCreate(AuditRequestBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


class AuditRequestInDBBase(AuditRequestBase):
    id: int

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via AuditRequest on update
class AuditRequestUpdate(AuditRequestBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via AuditRequest
class AuditRequest(AuditRequestInDBBase):
    pass


class AuditRequestInDB(AuditRequestInDBBase):
    pass
