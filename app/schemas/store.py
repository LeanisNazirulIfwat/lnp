from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator
from app.schemas.domain import Domain
from app.schemas.share_setting import ShareSetting
from app.schemas.support_detail import SupportDetail
from app.schemas.account import Account
from app.schemas.payment_setting import PaymentSetting
from app.schemas.collection import Collection


# Shared properties
class StoreBase(BaseModel):
    name: Optional[str]
    base_url: Optional[str]
    tax: Optional[str]
    short_url: Optional[str]
    slug: Optional[str]
    custom_field: Optional[str]
    status: Optional[str]
    banner: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Store on creation
class StoreCreate(StoreBase):
    account_id: Optional[int]
    domain_id: Optional[int]
    share_setting_id: Optional[int]
    support_detail_id: Optional[int]
    payment_setting_id: Optional[int]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    custom_field: Optional[str]
    unique_id: Optional[str]
    collection_id: Optional[int]

    billing_address_enable: Optional[bool] = False
    shipping_address_enable: Optional[bool] = False
    shipping_rate_enable: Optional[bool] = False
    discount_enable: Optional[bool] = False

    pass


class StoreInDBBase(StoreBase):
    id: int

    account: Account
    domain: Domain
    share_setting: Optional[ShareSetting]
    support_detail: Optional[SupportDetail]
    payment_setting: Optional[PaymentSetting]
    store_collection: Optional[Collection]

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Store on update
class StoreUpdate(StoreBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    product_list: Optional[Any]
    menu_items: Optional[Any]
    pass


# Additional properties to return via Store
class Store(StoreInDBBase):
    pass


class StoreInDB(StoreInDBBase):
    pass
