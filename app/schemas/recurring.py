from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account
from decimal import Decimal


# Shared properties
class RecurringBase(BaseModel):
    title: Optional[str]
    initiate_by: Optional[int]
    start_at: Optional[datetime]
    frequency: Optional[int]
    amount: Optional[Decimal]
    tax: Optional[int]

    kind: Optional[int]
    repetition: Optional[int]
    api_interface: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Recurring on creation
class RecurringCreate(RecurringBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class RecurringInDBBase(RecurringBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Recurring on update
class RecurringUpdate(RecurringBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Recurring
class Recurring(RecurringInDBBase):
    pass


class RecurringInDB(RecurringInDBBase):
    pass
