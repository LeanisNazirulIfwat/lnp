from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator


# Shared properties
class ExcelCsvBase(BaseModel):
    name: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]
    total: Optional[float]
    status: Optional[str]
    invoice_id: Optional[str]
    bill_link: Optional[str]
