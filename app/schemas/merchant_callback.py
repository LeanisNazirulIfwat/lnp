from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.customer_bill import CustomerBill


# Shared properties
class MerchantCallbackBase(BaseModel):
    bill_invoice: Optional[str]
    transaction_invoice: Optional[str]
    callback_merchant_url: Optional[str]
    redirect_merchant_url: Optional[str]
    callback_user_url: Optional[str]
    redirect_user_url: Optional[str]

    api_key: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    class Config:
        schema_extra = {
            "example": {
                "bill_invoice": 'BP-851A0D4971-LNP',
                "transaction_invoice": "LEANPAY16620147254LJIZ8yD",
                "callback_merchant_url": "www.leanpay.com.my",
                "callback_user_url": "www.leanis.com.my",
                "api_key": "key@api"
            }
        }


# Properties to receive via Customer on creation
class MerchantCallbackCreate(MerchantCallbackBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


class MerchantCallbackInDBBase(MerchantCallbackBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via MerchantCallr on update
class MerchantCallbackUpdate(MerchantCallbackBase):
    updated_at: Optional[datetime] = datetime.today()
    record_status: Optional[int] = 1

# Additional properties to return via MerchantCallr
class MerchantCallback(MerchantCallbackInDBBase):
    pass


class MerchantCallbackInDB(MerchantCallbackInDBBase):
    pass
