from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel, validator
from app.schemas.rule import Rule
from fastapi import FastAPI, Form, Depends
from pydantic.dataclasses import dataclass
from fastapi import File, UploadFile


# Shared properties
class MerchantCloudImageBase(BaseModel):
    file_name: Optional[str]
    description: Optional[str]
    image_link: Optional[str]
    image_direct_link: Optional[str]
    account_id: Optional[int]
    upload_date: Optional[datetime]
    download_count: Optional[int]
    is_active: Optional[bool] = True

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Merchant Cloud Image on creation
class MerchantCloudImageCreate(MerchantCloudImageBase):
    created_at: Optional[datetime] = datetime.today().now()
    updated_at: Optional[datetime] = datetime.today().now()

    pass


class MerchantCloudImageInDBBase(MerchantCloudImageBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Merchant Cloud Image on update
class MerchantCloudImageUpdate(MerchantCloudImageBase):
    updated_at: Optional[datetime] = datetime.today().now()


# Additional properties to return via Merchant Cloud Image
class MerchantCloudImage(MerchantCloudImageInDBBase):
    rule: Optional[Rule]
    pass


class MerchantCloudImageInDB(MerchantCloudImageInDBBase):
    pass


@dataclass
class MerchantFormModel:
    file_name: str = Form('IMAGE_DEFAULT_NAME')
    file: UploadFile = File(...)
    description: str = Form('THIS_IS_DESCRIPTION')
    file_size: str = Form('200x200')


@dataclass
class BulkMerchantFormModel:
    file_name: str = Form('IMAGE_DEFAULT_NAME')
    files: List[UploadFile] = File(...)
    description: str = Form('THIS_IS_DESCRIPTION')
    file_size: str = Form('200x200')
