import calendar
from datetime import datetime, timedelta
from typing import Optional, Any
from pydantic import BaseModel
from app.Utils.shared_utils import daterange

# Shared properties
from app import schemas


class ReportBase(BaseModel):
    x_axis: Optional[str]
    y_axis: Optional[float]
    min_value: Optional[Any]
    max_value: Optional[float]
    count: Optional[float]


# Properties to receive via API on creation
class ReportCreate(ReportBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


# Properties to receive via API on update
class ReportUpdate(ReportBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


class ReportInDBBase(ReportBase):
    id: int

    class Config:
        orm_mode = True


# Additional properties to return via API
class Report(ReportInDBBase):
    pass


class ReportInDB(ReportInDBBase):
    pass


class GenerateReport:
    @staticmethod
    def get_range_date(cur_month: int, year: int):
        today_range_date = [
            schemas.ReportCreate(x_axis="00:00 - 00:59", y_axis=0, min_value=float(00.00), max_value=float(00.59),
                                 count=0),
            schemas.ReportCreate(x_axis="01:00 - 01:59", y_axis=0, min_value=float(01.00), max_value=float(01.59),
                                 count=0),
            schemas.ReportCreate(x_axis="02:00 - 02:59", y_axis=0, min_value=float(02.00), max_value=float(02.59),
                                 count=0),
            schemas.ReportCreate(x_axis="03:00 - 03:59", y_axis=0, min_value=float(03.00), max_value=float(03.59),
                                 count=0),
            schemas.ReportCreate(x_axis="04:00 - 04:59", y_axis=0, min_value=float(04.00), max_value=float(04.59),
                                 count=0),
            schemas.ReportCreate(x_axis="05:00 - 05:59", y_axis=0, min_value=float(05.00), max_value=float(05.59),
                                 count=0),
            schemas.ReportCreate(x_axis="06:00 - 06:59", y_axis=0, min_value=float(06.00), max_value=float(06.59),
                                 count=0),
            schemas.ReportCreate(x_axis="07:00 - 07:59", y_axis=0, min_value=float(07.00), max_value=float(07.59),
                                 count=0),
            schemas.ReportCreate(x_axis="08:00 - 08:59", y_axis=0, min_value=float(08.00), max_value=float(08.59),
                                 count=0),
            schemas.ReportCreate(x_axis="09:00 - 09:59", y_axis=0, min_value=float(09.00), max_value=float(09.59),
                                 count=0),
            schemas.ReportCreate(x_axis="10:00 - 10:59", y_axis=0, min_value=float(10.00), max_value=float(10.59),
                                 count=0),
            schemas.ReportCreate(x_axis="11:00 - 11:59", y_axis=0, min_value=float(11.00), max_value=float(11.59),
                                 count=0),
            schemas.ReportCreate(x_axis="12:00 - 12:59", y_axis=0, min_value=float(12.00), max_value=float(12.59),
                                 count=0),
            schemas.ReportCreate(x_axis="13:00 - 13:59", y_axis=0, min_value=float(13.00), max_value=float(13.59),
                                 count=0),
            schemas.ReportCreate(x_axis="14:00 - 14:59", y_axis=0, min_value=float(14.00), max_value=float(14.59),
                                 count=0),
            schemas.ReportCreate(x_axis="15:00 - 15:59", y_axis=0, min_value=float(15.00), max_value=float(15.59),
                                 count=0),
            schemas.ReportCreate(x_axis="16:00 - 16:59", y_axis=0, min_value=float(16.00), max_value=float(16.59),
                                 count=0),
            schemas.ReportCreate(x_axis="17:00 - 17:59", y_axis=0, min_value=float(17.00), max_value=float(17.59),
                                 count=0),
            schemas.ReportCreate(x_axis="18:00 - 18:59", y_axis=0, min_value=float(18.00), max_value=float(18.59),
                                 count=0),
            schemas.ReportCreate(x_axis="19:00 - 19:59", y_axis=0, min_value=float(19.00), max_value=float(19.59),
                                 count=0),
            schemas.ReportCreate(x_axis="20:00 - 20:59", y_axis=0, min_value=float(20.00), max_value=float(20.59),
                                 count=0),
            schemas.ReportCreate(x_axis="21:00 - 21:59", y_axis=0, min_value=float(21.00), max_value=float(21.59),
                                 count=0),
            schemas.ReportCreate(x_axis="22:00 - 22:59", y_axis=0, min_value=float(22.00), max_value=float(22.59),
                                 count=0),
            schemas.ReportCreate(x_axis="23:00 - 23:59", y_axis=0, min_value=float(23.00), max_value=float(23.59),
                                 count=0),
        ]
        last_month_range_date = []
        range_month = calendar.monthrange(datetime.today().year, month=cur_month)
        for x in range(1, range_month[1] + 1):
            xdate = datetime.strptime(f"{x}-{cur_month}-{datetime.today().year}", "%d-%m-%Y")
            xdate_name = xdate.strftime("%d-%b-%Y")
            last_month_range_date.append(
                schemas.ReportCreate(x_axis=f"{xdate_name}", y_axis=0, min_value=float(x), max_value=0, count=0))

        year_range_date = []
        for _month in range(1, 12 + 1):
            xdate = datetime.strptime(f"1-{_month}-{year}", "%d-%m-%Y")
            xdate_name = xdate.strftime("%b-%Y")
            year_range_date.append(
                (schemas.ReportCreate(x_axis=f"{xdate_name}", y_axis=0, min_value=int(_month), max_value=0, count=0))
            )
        return today_range_date, last_month_range_date, year_range_date

    @staticmethod
    async def get_range_week():
        today_week_date = [
            schemas.ReportCreate(x_axis="MONDAY", y_axis=0, min_value="MONDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="TUESDAY", y_axis=0, min_value="TUESDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="WEDNESDAY ", y_axis=0, min_value="WEDNESDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="THURSDAY", y_axis=0, min_value="THURSDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="FRIDAY", y_axis=0, min_value="FRIDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="SATURDAY", y_axis=0, min_value="SATURDAY", max_value=0, count=0),
            schemas.ReportCreate(x_axis="SUNDAY", y_axis=0, min_value="SUNDAY", max_value=0, count=0)]

        today_date = datetime.today()
        seven_day_date = datetime.today() - timedelta(days=6)

        list_date = await daterange(today_date, seven_day_date)
        for x in list_date:
            week_ = x.strftime("%A").upper()
            for xy in today_week_date:
                if xy.min_value == week_:
                    xy.x_axis = x.strftime("%d-%m-%Y")

        return today_week_date

    @staticmethod
    async def get_range_week_dynamic():
        last_week = []
        today_date = datetime.today() + timedelta(days=1)
        seven_day_date = datetime.today() - timedelta(days=6)

        list_date = await daterange(today_date, seven_day_date)
        for x in list_date:
            week_ = x.strftime("%A").upper()
            date_week = x.strftime("%d-%m-%Y")
            last_week.append(schemas.ReportCreate(x_axis=date_week, y_axis=0, min_value=week_, max_value=0, count=0))

        return last_week

    @staticmethod
    async def get_range_week_based_on_date(_date: str):
        last_week = []
        _date_start = datetime.strptime(_date, "%d-%m-%Y")
        # today_date = _date_start + timedelta(days=1)
        today_date = _date_start
        seven_day_date = _date_start - timedelta(days=6)

        list_date = await daterange(today_date, seven_day_date)
        for x in list_date:
            week_ = x.strftime("%A").upper()
            date_week = x.strftime("%d-%m-%Y")
            last_week.append(schemas.ReportCreate(x_axis=date_week, y_axis=0, min_value=week_, max_value=0, count=0))

        return last_week
