from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account
from datetime import datetime


# Shared properties
class DomainBase(BaseModel):
    domain: Optional[str]
    used_link: Optional[str]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Domain on creation
class DomainCreate(DomainBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class DomainInDBBase(DomainBase):
    id: int
    account: Account
    record_status: Optional[int] = 1

    record_status: Optional[int]

    class Config:
        orm_mode = True


# Properties to receive via Domain on update
class DomainUpdate(DomainBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Domain
class Domain(DomainInDBBase):
    pass


class DomainInDB(DomainInDBBase):
    pass
