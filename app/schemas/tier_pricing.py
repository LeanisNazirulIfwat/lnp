from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class TierPricingBase(BaseModel):
    name: Optional[str]
    quantity_from: Optional[int]
    quantity_to: Optional[int]
    price: Optional[Decimal]
    product_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via TierPricing on creation
class TierPricingCreate(TierPricingBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class TierPricingInDBBase(TierPricingBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via TierPricing on update
class TierPricingUpdate(TierPricingBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via TierPricing
class TierPricing(TierPricingInDBBase):
    pass


class TierPricingInDB(TierPricingInDBBase):
    pass
