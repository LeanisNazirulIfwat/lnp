from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator
from app.schemas.shipping_rate import ShippingRate


# Shared properties
class ShippingZoneBase(BaseModel):
    name: Optional[str]
    states: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ShippingZone on creation
class ShippingZoneCreate(ShippingZoneBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ShippingZoneInDBBase(ShippingZoneBase):
    id: int
    record_status: Optional[int] = 1

    shipping_zone_shipping_rate: Optional[ShippingRate]

    class Config:
        orm_mode = True


# Properties to receive via ShippingZone on update
class ShippingZoneUpdate(ShippingZoneBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ShippingZone
class ShippingZone(ShippingZoneInDBBase):
    pass


class ShippingZoneInDB(ShippingZoneInDBBase):
    pass
