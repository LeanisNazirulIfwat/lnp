from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PaymentSettingBase(BaseModel):
    api_interface: str = 'default'
    fpx_bank_selection: str = 'grid'
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PaymentSetting on creation
class PaymentSettingCreate(PaymentSettingBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PaymentSettingInDBBase(PaymentSettingBase):
    id: int

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via PaymentSetting on update
class PaymentSettingUpdate(PaymentSettingBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via PaymentSetting
class PaymentSetting(PaymentSettingInDBBase):
    pass


class PaymentSettingInDB(PaymentSettingInDBBase):
    pass
