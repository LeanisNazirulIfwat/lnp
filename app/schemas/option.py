from typing import Optional, Any
from pydantic import BaseModel, ValidationError, validator
from datetime import datetime


# Shared properties
class SystemOptionBase(BaseModel):
    application_name: Optional[str]
    main_site: Optional[str]
    logo: Optional[Any]
    logo_alt: Optional[Any]
    qr_code_logo: Optional[Any]
    favicon: Optional[Any]
    meta_image: Optional[Any]
    meta_description: Optional[str]
    background_color: Optional[str]
    background_image: Optional[Any]
    primary_color: Optional[str]
    secondary_color: Optional[str]
    site_title: Optional[str]
    site_support: Optional[str]
    company_name: Optional[str]
    platform_version: Optional[str]
    mailer: Optional[str]
    faq_url: Optional[str]
    privacy_term_url: Optional[str]
    contact_url: Optional[str]
    facebook: Optional[str]
    twitter: Optional[str]
    instagram: Optional[str]
    linkedin: Optional[str]
    base_url_endpoint: Optional[str]
    callback_url: Optional[str]
    powered_by: Optional[str]
    account_id: Optional[int]
    api_parent_key: Optional[str]
    record_status: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class SystemOptionCreate(SystemOptionBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SystemOptionInDBBase(SystemOptionBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class SystemOptionUpdate(SystemOptionBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class SystemOption(SystemOptionInDBBase):
    pass


class SystemOptionInDB(SystemOptionInDBBase):
    pass
