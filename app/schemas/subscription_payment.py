from typing import Optional
from pydantic import BaseModel, ValidationError, validator, constr, NonNegativeInt, condecimal
from datetime import datetime

# Shared properties
class SubscriptionPaymentBase(BaseModel):
    account_id: Optional[NonNegativeInt]
    subscription_plan_id: Optional[NonNegativeInt]
    month: Optional[NonNegativeInt]
    year: Optional[NonNegativeInt]
    invoice_no: Optional[constr(strip_whitespace=True)]
    transaction_no: Optional[constr(strip_whitespace=True)]
    payment_status_id: Optional[NonNegativeInt]

    record_status: Optional[NonNegativeInt] = 1

    amount: Optional[condecimal(decimal_places=2)]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class SubscriptionPaymentCreate(SubscriptionPaymentBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SubscriptionPaymentInDBBase(SubscriptionPaymentBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class SubscriptionPaymentUpdate(SubscriptionPaymentBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class SubscriptionPayment(SubscriptionPaymentInDBBase):
    pass


class SubscriptionPaymentInDB(SubscriptionPaymentInDBBase):
    pass
