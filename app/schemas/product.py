from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator, confloat, conint
from app.schemas.product_category import ProductCategory


# Shared properties
class ProductBase(BaseModel):
    name: Optional[str]
    title: Optional[str]
    code: Optional[str]
    description: Optional[str]
    min_purchase_quantity: Optional[conint(ge=0)]
    max_purchase_quantity: Optional[conint(ge=0)]
    price: Optional[confloat(ge=0)]
    quantity: Optional[conint(ge=0)]
    account_id: Optional[int]
    product_category_list: Optional[Any]

    intro: Optional[str]
    footer: Optional[str]
    image: Optional[Any]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    @validator('min_purchase_quantity', 'max_purchase_quantity', 'price')
    def none_value(cls, v):
        if v is None:
            raise ValueError("NONE_VALUE_NOT_ALLOWED")
        return v


# Properties to receive via Product on creation
class ProductCreate(ProductBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    limited_stock_enable: Optional[bool]
    physical_product_enable: Optional[bool]
    tier_pricing_enable: Optional[bool]
    product_variant_enable: Optional[bool]

    base_url: Optional[str]
    short_url: Optional[str]
    weight: Optional[float]
    weight_condition: Optional[int]
    value: Optional[int]
    pass

    @validator('weight', 'weight_condition', 'value', pre=True)
    def create_no_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    @validator('limited_stock_enable')
    def check_stock(cls, v, values, **kwargs):
        if v is True:
            if 'quantity' in values and values['quantity'] is None:
                raise ValueError(f"NONE_VALUE_NOT_ALLOWED_FOR_QUANTITY")
        return v


class ProductInDBBase(ProductBase):
    id: int
    record_status: Optional[int] = 1

    product_category: ProductCategory

    class Config:
        orm_mode = True


# Properties to receive via Product on update
class ProductUpdate(ProductBase):
    product_category_id: Optional[int]
    updated_at: Optional[datetime] = datetime.today()
    limited_stock_enable: Optional[bool]
    physical_product_enable: Optional[bool]
    tier_pricing_enable: Optional[bool]
    product_variant_enable: Optional[bool]

    base_url: Optional[str]
    short_url: Optional[str]
    weight: Optional[float]
    weight_condition: Optional[int]
    value: Optional[int]
    pass

    @validator('weight', 'weight_condition', 'value', pre=True)
    def create_no_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    @validator('limited_stock_enable')
    def check_stock(cls, v, values, **kwargs):
        if v is True:
            if 'quantity' in values and values['quantity'] is None:
                raise ValueError(f"NONE_VALUE_NOT_ALLOWED_FOR_QUANTITY")
        return v


# Additional properties to return via Product
class Product(ProductInDBBase):
    pass


class ProductInDB(ProductInDBBase):
    pass


class ProductInView(ProductBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    limited_stock_enable: Optional[bool]
    physical_product_enable: Optional[bool]
    tier_pricing_enable: Optional[bool]
    product_variant_enable: Optional[bool]

    base_url: Optional[str]
    short_url: Optional[str]
    weight: Optional[float]
    weight_condition: Optional[int]
    value: Optional[int]

    product_category_details: Optional[Any]
    product_product_variant: Optional[Any]
    product_tier_pricing: Optional[Any]
    pass


class ProductVue(ProductInView):
    custom_pricing_exist: Optional[bool]
    record_status: Optional[int]
    id: Optional[int]
    pass
