from typing import Any, Optional
from pydantic import BaseModel


class ParcelAsiaData(BaseModel):
    id: str
    ref_code: str
    ref_by: Any
    key: str
    username: Any
    fullname: str
    email: str
    email_verify_key: Any
    email_is_verified: str
    phone: str
    phone_verify_code: str
    phone_is_verified: str
    ic: Any
    is_business: str
    company_name: Any
    company_no: Any
    company_certificate: Any
    sender_name: str
    sender_phone: str
    sender_address_line_1: str
    sender_address_line_2: str
    sender_address_line_3: str
    sender_address_line_4: str
    sender_postcode: str
    sender_city: str
    sender_state_code: str
    sender_state: str
    sender_country_code: str
    sender_country: str
    default_content_type: Any
    default_content_description: Any
    default_content_value: Any
    default_parcel_weight: Any
    default_parcel_size: Any
    default_parcel_width: Any
    default_parcel_height: Any
    default_parcel_length: Any
    default_send_sms: str
    enable_custom_brand: str
    custom_brand_type: Any
    custom_brand_img: Any
    custom_brand_company: Any
    custom_brand_text: Any
    connote_type: Any
    quick_topup_package: Any
    preferred_claimed_flyers_size: Any
    status: str
    group: str
    login_try_count: Any
    login_last_try_at: Any
    login_try_again_at: Any
    login_try_ip: str
    last_accessed_at: str
    registered_at: Any
    created_at: str
    created_by: Any
    modified_at: str
    remember_token: Any
    email_verified_at: Any
    type: Any
    roles: Any
    flex_admin_id: Any
    contract_acc_markup: Any
    terms_check: Any


class ParcelAsiaMeta(BaseModel):
    vendor: str
    endpoint: str
    api_key: str
    currency_label: str
    currency_code: str
    topup_balance: str
    timestamp: str


class ParcelAsiaModel(BaseModel):
    status: Optional[bool] = None
    message: Optional[str] = None
    data: Optional[Any] = None
    meta: Optional[Any] = None
    hash: Optional[str] = None


class ParcelAsiaHashModel(BaseModel):
    status: Optional[bool] = None
    message: Optional[str] = None
    data: Optional[Any] = None
    meta: Optional[Any] = None
    hash: Optional[str] = None
    apiKey: Optional[str] = "kea8e829a817bbe655eaa74599b930e9b"
    apiSecret: Optional[str] = "s1e54d8183dee7af971740adfcc621253"


class ShipmentResponse(BaseModel):
    shipment_key: str
    integration_order_id: Any
    provider_code: str
    provider_label: str
    provider_logo: str
    tracking_no: str
    declared_weight: str
    normal_price: str
    exclusive_price: str
    effective_price: str
    type: str
    send_method: str
    shipment_status: str
    checkout_status: str
    sender_name: str
    sender_company_name: str
    sender_address_line_1: str
    sender_address_line_2: str
    sender_address_line_3: str
    sender_address_line_4: str
    sender_city: str
    sender_state: str
    sender_country: str
    sender_email: str
    receiver_phone: str
    receiver_name: str
    receiver_company_name: Any
    receiver_address_line_1: str
    receiver_address_line_2: str
    receiver_address_line_3: str
    receiver_address_line_4: str
    receiver_city: str
    receiver_state: str
    receiver_country: Any
    receiver_email: str
