from datetime import datetime
from typing import Optional, Any
from app.schemas.subscription import Subscription
from app.schemas.user import User
from pydantic import UUID4, BaseModel
from decimal import Decimal


class SubresourceUris(BaseModel):
    media: Optional[str]


# Shared properties
class TwilioResponseBase(BaseModel):
    account_sid: Optional[str]
    api_version: Optional[str]
    body: Optional[str]
    date_created: Optional[datetime]
    date_sent: Optional[datetime]
    date_updated: Optional[datetime]
    direction: Optional[str]
    error_code: Optional[str]
    error_message: Optional[str]
    from_: Optional[str]
    messaging_service_sid: Optional[str]
    num_media: Optional[str]
    num_segments: Optional[str]
    price: Optional[str]
    price_unit: Optional[str]
    sid: Optional[str]
    status: Optional[str]
    subresource_uris: SubresourceUris
    to: Optional[str]
    uri: Optional[str]


# Properties to receive via API on creation
class TwilioResponseCreate(TwilioResponseBase):
    pass


# Properties to receive via API on update
class TwilioResponseUpdate(TwilioResponseBase):
    pass


class TwilioResponseInDBBase(TwilioResponseBase):
    class Config:
        orm_mode = True


# Additional properties to return via API
class TwilioResponse(TwilioResponseInDBBase):
    pass


class TwilioResponseInDB(TwilioResponseInDBBase):
    pass


class TwilioFeedbackResponse(BaseModel):
    pass


class TwilioSendEmail(BaseModel):
    from_: str = 'admin@leanis.com.my'
    to_: str = "syahirah.zaki@leanis.com.my"
    bcc_: Optional[str] = 'admin@leanis.com.my'
    subject: Optional[str] = 'Message from Leanx'
    body: Optional[str] = 'Messager from Leanx'
    template_id: Optional[str] = 'd-0199ac4e20bb4931aa720c553dc886b9'
    template_enable: Optional[bool] = False


class TwilioSender(BaseModel):
    from_: str = 'admin@leanis.com.my'
    to_: str = "syahirah.zaki@leanis.com.my"
    bcc_: Optional[str] = 'admin@leanis.com.my'


class TwilioPaymentReceipt(TwilioSender):
    customer: Optional[str] = 'admin@leanis.com.my'
    email: Optional[str] = 'admin@leanis.com.my'
    phone: Optional[str] = 'admin@leanis.com.my'
    invoice: Optional[str] = 'admin@leanis.com.my'
    amount: Optional[str] = 'admin@leanis.com.my'
    date_time: Optional[str] = datetime.today().now().strftime("%d-%m-%Y")


class TwilioResetPassword(TwilioSender):
    recipient_name: Optional[str] = 'John Dow Ray Me'
    reset_password_new_password: str
    leanx_support_email: Optional[str] = 'support@leanx.io'
    leanx_add_line_1: Optional[str] = 'Unit 8, Level 13A, Tower 1, '
    leanx_add_line_2: Optional[str] = '8trium, Jalan Cempaka SD12/5, '
    leanx_city: Optional[str] = 'Bandar Sri Damansara '
    leanx_postcode: Optional[str] = '52200 '
    leanx_company_name: Optional[str] = 'LeanX '
    leanx_io_name: Optional[str] = 'leanx.io '
    leanx_state: Optional[str] = 'Kuala Lumpur'
    leanx_country: Optional[str] = 'Malaysia'
    leanis_website: Optional[str] = 'https://leanis.com.my'
    leanx_website: Optional[str] = 'https://leanx.io'


class TransactionData(BaseModel):
    txn_date: Optional[str] = datetime.now().strftime("%B %d, %Y, %H:%M:%S %p")
    transaction_amount: Optional[str]
    transaction_id: Optional[str]


class TwilioPrefund(TwilioSender):
    recipient_name: Optional[str] = 'John Dow Ray Me'
    leanx_support_email: Optional[str] = 'support@leanx.io'
    leanx_add_line_1: Optional[str] = 'Unit 8, Level 13A, Tower 1, '
    leanx_add_line_2: Optional[str] = '8trium, Jalan Cempaka SD12/5, '
    leanx_city: Optional[str] = 'Bandar Sri Damansara '
    leanx_postcode: Optional[str] = '52200 '
    leanx_company_name: Optional[str] = 'LeanX '
    leanx_io_name: Optional[str] = 'leanx.io '
    leanx_state: Optional[str] = 'Kuala Lumpur'
    leanx_country: Optional[str] = 'Malaysia'
    leanis_website: Optional[str] = 'https://leanis.com.my'
    leanx_website: Optional[str] = 'https://leanx.io'
    transaction_data: TransactionData
