from datetime import datetime
from typing import Optional, Any
from app.schemas.subscription import Subscription
from app.schemas.user import User
from pydantic import UUID4, BaseModel
from decimal import Decimal


class PublicPaymentList(BaseModel):
    payment_type: str
    payment_status: str
    payment_model_reference_id: int
