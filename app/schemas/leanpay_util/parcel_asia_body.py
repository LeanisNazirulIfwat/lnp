from typing import Any, Optional, List
from pydantic import BaseModel


class ParcelBaseModel(BaseModel):
    api_key: str

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b"
            }
        }


class CheckPriceModel(BaseModel):
    api_key: Optional[str]
    sender_postcode: str
    receiver_postcode: str
    receiver_country_code: Optional[str] = "MY"
    declared_weight: str

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "sender_postcode": "68000",
                "receiver_postcode": "08000",
                "declared_weight": 2
            }
        }


class SddPriceModel(BaseModel):
    api_key: str
    pickup_address: Optional[str]
    pickup_postcode: str
    pickup_lat: Optional[str]
    pickup_lng: Optional[str]
    receiver_address: Optional[str]
    receiver_postcode: str
    receiver_lat: Optional[str]
    receiver_lng: Optional[str]
    declared_weight: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "pickup_address": "Unit 8, Level 13A, Tower 1,\n8trium, Jalan Cempaka SD12/5,\nBandar Sri Damansara,\n52200 Kuala Lumpur, Malaysia.",
                "pickup_postcode": "52200",
                "pickup_lat": "3.193210",
                "pickup_lng": "101.606720",
                "receiver_address": "Unit 8, Level 13A, Tower 1,\n8trium, Jalan Cempaka SD12/5,\nBandar Sri Damansara,\n52200 Kuala Lumpur, Malaysia.",
                "receiver_postcode": "52200",
                "receiver_lat": "3.193210",
                "receiver_lng": "101.606720",
                "declared_weight": "2"
            }
        }


class CreateShipmentModel(BaseModel):
    api_key: str
    send_method: str
    send_date: str
    type: str
    declared_weight: str
    size: str
    width: str
    length: str
    height: str
    provider_code: str
    content_type: str
    content_description: str
    content_value: float
    sender_name: str
    sender_phone: str
    sender_email: str
    sender_company_name: str
    sender_address_line_1: str
    sender_address_line_2: str
    sender_address_line_3: str
    sender_address_line_4: str
    sender_postcode: str
    receiver_name: str
    receiver_phone: str
    receiver_email: str
    receiver_address_line_1: str
    receiver_address_line_2: str
    receiver_address_line_3: str
    receiver_address_line_4: str
    receiver_postcode: str
    receiver_city: Optional[str]
    receiver_state: Optional[str]
    receiver_country_code: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "send_method": "pickup",
                "send_date": "2022-09-19",
                "type": "parcel",
                "declared_weight": 1,
                "size": "box",
                "width": 20,
                "length": 100,
                "height": 10,
                "provider_code": "citylink",
                "content_type": "general",
                "content_description": "DIY item",
                "content_value": 15.00,
                "sender_name": "Nazirul Ifwat",
                "sender_phone": "601111112434",
                "sender_email": "nazirul.ifwat@leanis.com.my",
                "sender_company_name": "",
                "sender_address_line_1": "Unit 8, Level 13A, Tower 1,",
                "sender_address_line_2": "8trium, Jalan Cempaka SD125,",
                "sender_address_line_3": "Bandar Sri Damansara,",
                "sender_address_line_4": "",
                "sender_postcode": "52200",
                "receiver_name": "Tun Mahathir",
                "receiver_phone": "0199999999",
                "receiver_email": "tun@mahathir.com",
                "receiver_address_line_1": "Jabatan Perdana Menteri",
                "receiver_address_line_2": "Bulatan Utama Putrajaya",
                "receiver_address_line_3": "",
                "receiver_address_line_4": "",
                "receiver_postcode": "62000"
            }
        }


class CheckOutModel(BaseModel):
    api_key: str
    shipment_keys: Optional[List]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "shipment_keys": ["8cff59a37c6c11f96106d055e0638cb8", "a25786029a8751cf964f86917f0be559"]
            }
        }


class ShipmentModel(CheckOutModel):
    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "shipment_keys": ["403cf1d341a89cd7b002cbef1c337c85"],
            }
        }


class ConsignmentNoteModel(CheckOutModel):
    tracking_no: Optional[List]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "tracking_no": ["ERA311010700MY", "ERA311010695MY"]
            }
        }


class CheckPriceBulkModel(BaseModel):
    api_key: str
    shipments: List[CheckPriceModel]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "shipments": [
                    {
                        "sender_postcode": "53000",
                        "receiver_postcode": "57000",
                        "receiver_country_code": "MY",
                        "declared_weight": "0.35"
                    },
                    {
                        "sender_postcode": "53000",
                        "receiver_postcode": "68000",
                        "receiver_country_code": "MY",
                        "declared_weight": "1",
                        "type": "parcel",
                        "provider_code": "poslaju"
                    },
                    {
                        "sender_postcode": "53000",
                        "receiver_postcode": "68000",
                        "receiver_country_code": "SG",
                        "declared_weight": "1"
                    }
                ]
            }
        }


class TraceModel(ParcelBaseModel):
    tracking_no: str

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "tracking_no": "ERA2918222323MY"
            }
        }


class PostcodeModel(BaseModel):
    api_key: str
    postcode: str

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "postcode": "68000"
            }
        }


class BulkCreateAwbBaseModel(BaseModel):
    integration_order_id: str
    receiver_postcode: str
    receiver_country_code: str
    declared_weight: float
    type: str
    provider_code: str
    size: str
    send_method: str
    send_date: str
    content_type: str
    content_description: str
    content_value: str
    sender_name: str
    sender_phone: str
    sender_email: str
    sender_company_name: str
    pickup_address: Optional[str]
    pickup_postcode: str
    pickup_lat: str
    pickup_lng: str
    receiver_name: str
    receiver_phone: str
    receiver_email: str
    receiver_company: Any
    receiver_address: str
    receiver_lat: str
    receiver_lng: str


class BulkCreateAwbModel(BaseModel):
    api_key: str
    shipments: List[BulkCreateAwbBaseModel]

    class Config:
        schema_extra = {
            "example": {
                "api_key": "kea8e829a817bbe655eaa74599b930e9b",
                "shipments": [
                    {
                        "integration_order_id": "MYID291822",
                        "receiver_postcode": "47301",
                        "receiver_country_code": "MY",
                        "declared_weight": 0.5,
                        "type": "parcel",
                        "provider_code": "grabx",
                        "size": "flyers_m",
                        "send_method": "pickup",
                        "send_date": "2022-09-19",
                        "content_type": "outdoors",
                        "content_description": "Hiking boots",
                        "content_value": 200,
                        "sender_name": "Nazirul Ifwat",
                        "sender_phone": "601111112434",
                        "sender_email": "nazirul.ifwat@leanis.com.my",
                        "sender_company_name": "nazirul.ifwat@leanis.com.my",
                        "pickup_address": "3349, jalan palimbayan indah 3, sungai penchala,60000 kuala lumpur",
                        "pickup_postcode": "60000",
                        "pickup_lat": "3.1687034",
                        "pickup_lng": "101.6274215",
                        "receiver_name": "Penerima Cubaan",
                        "receiver_phone": "0192837463",
                        "receiver_email": "penerima@cubaan.com",
                        "receiver_company": None,
                        "receiver_address": "Paradigm mall Petaling jaya 1, Jalan SS 7\/26a, Ss 7, 47301 Petaling Jaya, Selangor, Malaysia",
                        "receiver_lat": "3.1051171",
                        "receiver_lng": "101.5959701"
                    },
                    {
                        "integration_order_id": "REFID29382",
                        "receiver_postcode": "05150",
                        "receiver_country_code": "SG",
                        "declared_weight": 0.5,
                        "type": "document",
                        "provider_code": "aramex",
                        "size": "flyers_s",
                        "send_method": "pickup",
                        "send_date": "2022-06-16",
                        "content_type": "papers",
                        "content_description": "Agreement Document",
                        "content_value": 10,
                        "sender_name": "Izwan Wahab",
                        "sender_phone": "60182798114",
                        "sender_email": "robotys@gmail.com",
                        "sender_company_name": "MYPARCEL ASIA SDN. BHD.",
                        "sender_address_line_1": "No 28 Lorong Dagang 9",
                        "sender_address_line_2": "Taman Dagang Jaya",
                        "sender_address_line_3": "",
                        "sender_address_line_4": "",
                        "sender_postcode": "68000",
                        "sender_city": None,
                        "sender_state": None,
                        "sender_country": "Malaysia",
                        "receiver_name": "Mr Tester Xavier",
                        "receiver_phone": "650129382",
                        "receiver_email": "xavier@tester.com",
                        "receiver_company": None,
                        "receiver_address_line_1": "Unit 123-A Block B",
                        "receiver_address_line_2": "Orchard Road Square",
                        "receiver_address_line_3": None,
                        "receiver_address_line_4": None,
                        "receiver_city": "Singapore",
                        "receiver_state": "Singapore",
                        "receiver_country": "Singapore"
                    }
                ]
            }
        }
