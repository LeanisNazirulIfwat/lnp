from typing import Optional
from pydantic import BaseModel, validator
from decimal import Decimal
from datetime import datetime

# Shared properties
class CustomerBillItemBase(BaseModel):
    product_id: Optional[int]
    customer_bill_id: Optional[int]
    quantity: Optional[int]
    amount: Optional[Decimal]
    total_amount: Optional[Decimal]
    record_status: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via CustomerBillItem on creation
class CustomerBillItemCreate(CustomerBillItemBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class CustomerBillItemInDBBase(CustomerBillItemBase):
    id: int
    class Config:
        orm_mode = True


# Properties to receive via CustomerBillItem on update
class CustomerBillItemUpdate(CustomerBillItemBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via CustomerBillItem
class CustomerBillItem(CustomerBillItemInDBBase):
    pass


class CustomerBillItemInDB(CustomerBillItemInDBBase):
    pass
