from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PayoutRecordBase(BaseModel):
    switch_invoice_id: Optional[str]
    merchant_invoice_id: Optional[str]
    transaction_invoice_no: Optional[str]
    payout_invoice_no: Optional[str]
    payout_account_number: Optional[str]
    transaction_status: Optional[str]
    value: Optional[Decimal]
    account_id: Optional[int]
    data: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PayoutRecord on creation
class PayoutRecordCreate(PayoutRecordBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    api_key: Optional[str]

    pass


class PayoutRecordInDBBase(PayoutRecordBase):
    id: int
    record_status: Optional[int] = 1
    updated_at: Optional[datetime] = datetime.today()

    class Config:
        orm_mode = True


# Properties to receive via PayoutRecord on update
class PayoutRecordUpdate(PayoutRecordBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via PayoutRecord
class PayoutRecord(PayoutRecordInDBBase):
    pass


class PayoutRecordInDB(PayoutRecordInDBBase):
    pass


class IPayoutListing(PayoutRecordBase):
    payout_bank_account : Optional[str]
    recipient_reference: Optional[str]
    payment_description: Optional[str]
    bank_name: Optional[str]
    pass
