from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from decimal import Decimal

# Shared properties

class PaymentRecordBase(BaseModel):
    payment_date: Optional[datetime]
    reference_number: Optional[str]
    payment_mode: Optional[str]
    payment_method: Optional[str]
    amount: Optional[Decimal]
    transaction_fee: Optional[float]
    switch_transaction_fee: Optional[float]
    bank_code: Optional[str]
    kind: Optional[str]
    net_amount: Optional[Decimal]
    customer_id: Optional[int]
    record_status: Optional[int] = 1
    paid_at: Optional[datetime]
    data: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PaymentRecord on creation
class PaymentRecordCreate(PaymentRecordBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PaymentRecordInDBBase(PaymentRecordBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via PaymentRecord on update
class PaymentRecordUpdate(PaymentRecordBase):
    amount_fee: Optional[Decimal]
    fee_type: Optional[int]
    updated_at: Optional[datetime] = datetime.today()
    total: Optional[str]
    pass


# Additional properties to return via PaymentRecord
class PaymentRecord(PaymentRecordInDBBase):
    pass


class PaymentRecordInDB(PaymentRecordInDBBase):
    pass
