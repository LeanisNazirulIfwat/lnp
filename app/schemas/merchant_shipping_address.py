from typing import Optional, Any
from pydantic import BaseModel, validator
from decimal import Decimal
from datetime import datetime


# Shared properties
class MerchantShippingAddressBase(BaseModel):
    name: Optional[str]
    phone_number: Optional[str]
    address_line_one: Optional[str]
    address_line_two: Optional[str]
    address_line_three: Optional[str]
    address_line_four: Optional[str]
    address_postcode: Optional[str]
    address_state: Optional[str]
    address_city: Optional[str]
    address_country: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ShippingAddress on creation
class MerchantShippingAddressCreate(MerchantShippingAddressBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class MerchantShippingAddressInDBBase(MerchantShippingAddressBase):
    id: int
    record_status: Optional[int] = 1
    shipping_profile_id: Optional[int]
    shipping_address_shipping_profile: Optional[Any]

    class Config:
        orm_mode = True


# Properties to receive via MerchantShippingAddress on update
class MerchantShippingAddressUpdate(MerchantShippingAddressBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via MerchantShippingAddress
class MerchantShippingAddress(MerchantShippingAddressInDBBase):
    pass


class MerchantShippingAddressInDB(MerchantShippingAddressInDBBase):
    pass
