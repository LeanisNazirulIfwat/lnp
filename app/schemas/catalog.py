from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.collection import Collection
from app.schemas.page import Page
from app.schemas.share_setting import ShareSetting
from datetime import datetime


# Shared properties
class CatalogBase(BaseModel):
    name: Optional[str]
    slug: Optional[str]
    base_url: Optional[str]
    short_url: Optional[str]
    account_id: Optional[int]
    collection: Optional[str]
    layout: Optional[str]
    intro: Optional[str]
    footer: Optional[str]
    banner: Optional[str]
    address: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Catalog on creation
class CatalogCreate(CatalogBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class CatalogInDBBase(CatalogBase):
    id: int

    share_setting: ShareSetting
    collection: Collection
    page: Page

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Catalog on update
class CatalogUpdate(CatalogBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Catalog
class Catalog(CatalogInDBBase):
    pass


class CatalogInDB(CatalogInDBBase):
    pass
