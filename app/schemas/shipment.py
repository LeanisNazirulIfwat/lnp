from datetime import datetime
from typing import Optional, Union, Any
from pydantic import BaseModel, validator
from app.schemas.customer import Customer
from app.schemas.shipping_profile import ShippingProfile


# Shared properties
class ShipmentBase(BaseModel):
    shipment_status: Optional[str]
    customer_id: Optional[int]
    shipping_profile_id: Optional[int]
    account_id: Optional[int]
    customer_bill_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Shipment on creation
class  ShipmentCreate(ShipmentBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    shipping_infos: Optional[Union[str, Any]]
    parcel_shipment_key: Optional[str]
    parcel_tracking_no: Optional[str]
    pass


class ShipmentInDBBase(ShipmentBase):
    id: int
    record_status: Optional[int] = 1

    customer: Customer
    shipping_profile: ShippingProfile

    class Config:
        orm_mode = True


# Properties to receive via Shipment on update
class ShipmentUpdate(ShipmentBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Shipment
class Shipment(ShipmentInDBBase):
    pass


class ShipmentInDB(ShipmentInDBBase):
    pass
