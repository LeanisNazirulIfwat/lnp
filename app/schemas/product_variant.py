from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class ProductVariantBase(BaseModel):
    options: Optional[str]
    code: Optional[str]
    quantity: Optional[int]
    min_quantity: Optional[int]
    max_quantity: Optional[int]
    price: Optional[Decimal]
    custom_pricing_enable: Optional[bool]
    custom_pricing_price: Optional[Decimal]
    product_variant_image: Optional[str]

    product_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ProductVariant on creation
class ProductVariantCreate(ProductVariantBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ProductVariantInDBBase(ProductVariantBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via ProductVariant on update
class ProductVariantUpdate(ProductVariantBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ProductVariant
class ProductVariant(ProductVariantInDBBase):
    pass


class ProductVariantInDB(ProductVariantInDBBase):
    pass
