from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.customer_bill import CustomerBill


# Shared properties
class CustomerBase(BaseModel):
    full_name: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]
    address_line_one: Optional[str]
    address_line_two: Optional[str]
    address_postcode: Optional[int]
    address_state: Optional[str]
    address_city: Optional[str]
    address_country: Optional[str] = "Malaysia"
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    class Config:
        schema_extra = {
            "example": {
                "full_name": "TOMATOMAN",
                "email": "TOMATOMAN@GMAIL.COM",
                "phone_number": "0126598594",
                "address": "SELANGOR",
            }
        }


# Properties to receive via Customer on creation
class CustomerCreate(CustomerBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


class CustomerInDBBase(CustomerBase):
    id: int
    customer_bill: CustomerBill
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Customer on update
class CustomerUpdate(CustomerBase):
    Customer_bill_id: Optional[int]

    updated_at: Optional[datetime] = datetime.today()


# Additional properties to return via Customer
class Customer(CustomerInDBBase):
    pass


class CustomerInDB(CustomerInDBBase):
    pass
