from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class ProductCategoryBase(BaseModel):
    name: Optional[str]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ProductCategory on creation
class ProductCategoryCreate(ProductCategoryBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ProductCategoryInDBBase(ProductCategoryBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via ProductCategory on update
class ProductCategoryUpdate(ProductCategoryBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ProductCategory
class ProductCategory(ProductCategoryInDBBase):
    pass


class ProductCategoryInDB(ProductCategoryInDBBase):
    pass
