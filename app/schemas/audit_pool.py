from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class AuditPoolBase(BaseModel):
    pool_id: Optional[int]
    pool_type: Optional[int]
    previous_value: Optional[Decimal]
    current_value: Optional[Decimal]
    charges: Optional[Decimal]
    account_id: Optional[int]
    api_key: Optional[str]
    invoice_no: Optional[str]
    invoice_status: Optional[str]
    data: Optional[str]
    transaction_type: Optional[str]
    description: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via AuditPool on creation
class AuditPoolCreate(AuditPoolBase):
    created_at: Optional[datetime] = datetime.today()
    precision_created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class AuditPoolInDBBase(AuditPoolBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via AuditPool on update
class AuditPoolUpdate(AuditPoolBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via AuditPool
class AuditPool(AuditPoolInDBBase):
    pass


class AuditPoolInDB(AuditPoolInDBBase):
    pass
