from typing import Optional
from pydantic import BaseModel, validator
from decimal import Decimal
from datetime import datetime


# Shared properties
class CollectionItemBase(BaseModel):
    product_id: Optional[int]
    collection_id: Optional[int]
    quantity: Optional[int]
    amount: Optional[Decimal]
    total_amount: Optional[Decimal]
    record_status: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v

    # @validator('total_amount', 'amount')
    # def passwords_match(cls, v):
    #     if v == 0:
    #         raise ValueError('ZERO_VALUE_NOT_ALLOWED')
    #     return v

# Properties to receive via CollectionItem on creation
class CollectionItemCreate(CollectionItemBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class CollectionItemInDBBase(CollectionItemBase):
    id: int

    class Config:
        orm_mode = True


# Properties to receive via CollectionItem on update
class CollectionItemUpdate(CollectionItemBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via CollectionItem
class CollectionItem(CollectionItemInDBBase):
    pass


class CollectionItemInDB(CollectionItemInDBBase):
    pass
