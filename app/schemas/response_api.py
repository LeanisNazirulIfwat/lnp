from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, Json


# Shared properties
class ResponseApiBase(BaseModel):
    response_code: Optional[int]
    description: Optional[str]
    app_version: Optional[str]
    talk_to_server_before: Optional[datetime]
    data: Optional[Any]
    breakdown_errors: Optional[str]
    token: Optional[str]


class ResponseListApiBase(BaseModel):
    draw: Optional[int]
    record_total: Optional[int]
    record_filtered: Optional[int]
    data: Optional[Any]
    next_page_start: Optional[int]
    next_page_length: Optional[int]
    previous_page_start: Optional[int]
    previous_page_length: Optional[int]


class Sort(BaseModel):
    parameter_name: Optional[str] = "created_at"
    sort_type: Optional[str] = "desc"


class Search(BaseModel):
    search_enable: Optional[bool] = False
    search_key: Optional[str]
    search_column: Optional[str]
    search_replace_word_enable: Optional[bool] = False
    search_word_replace: Optional[str]
    search_word_replace_to: Optional[str]


class PayloadListQuery(BaseModel):
    start_date: Optional[str] = "01-01-1991"
    end_date: Optional[str] = "01-01-2099"
    record_status: Optional[int] = 1
    invoice_status: Optional[Any]
    search: Search
    sort: Sort
