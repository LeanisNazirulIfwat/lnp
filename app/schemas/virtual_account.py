from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class VirtualAccountBase(BaseModel):
    account_id: Optional[int]
    pool_name: Optional[str]
    balance: Optional[Decimal]
    switch_payout_pool_id: Optional[str]
    merchant_payout_pool_id: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via VirtualAccount on creation
class VirtualAccountCreate(VirtualAccountBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class VirtualAccountInDBBase(VirtualAccountBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via VirtualAccount on update
class VirtualAccountUpdate(VirtualAccountBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via VirtualAccount
class VirtualAccount(VirtualAccountInDBBase):
    pass


class VirtualAccountInDB(VirtualAccountInDBBase):
    pass
