import decimal
from typing import Optional, Any
from pydantic import BaseModel, validator
from decimal import Decimal
from app.schemas.collection import Collection
from app.schemas.collection_item import CollectionItem
from app.schemas.payment_record import PaymentRecord
from datetime import datetime


# Shared properties
class CustomerBillBase(BaseModel):
    full_name: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]
    status: Optional[bool]
    total: Optional[Decimal]
    collection_id: Optional[int]
    payment_record_id: Optional[int]
    total_amount_with_fee: Optional[Decimal]
    transaction_fee: Optional[Decimal]
    quantity: Optional[int] = 1
    account_id: Optional[int]
    invoice_no: Optional[str]
    customer_id: Optional[int]
    transaction_invoice_no: Optional[str]
    invoice_status_id: Optional[int]
    shipping_address_id: Optional[int]
    invoice_status: Optional[str]
    base_url: Optional[str]
    checking_counter: Optional[int]
    api_key: Optional[str]
    payment_date: Optional[datetime]
    payment_provider: Optional[str]

    sms_enable: Optional[bool] = False
    whatsapp_enable: Optional[bool] = False
    email_enable: Optional[bool] = False

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via CustomerBill on creation
class CustomerBillCreate(CustomerBillBase):
    price_per_quantity: Optional[Decimal]
    bill_transaction_type_id: Optional[int]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


class CustomerBillInDBBase(CustomerBillBase):
    id: int

    collection: Optional[Collection]
    payment_record: Optional[PaymentRecord]
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via CustomerBill on update
class CustomerBillUpdate(CustomerBillBase):
    amount_fee: Optional[Decimal]
    fee_type: Optional[int]
    updated_at: Optional[datetime] = datetime.today()
    record_status: Optional[int] = 1
    pass


# Additional properties to return via CustomerBill
class CustomerBill(CustomerBillInDBBase):
    pass


class CustomerBillInDB(CustomerBillInDBBase):
    pass


class CustomerCollectionBill(CustomerBillBase):
    id: Optional[int]
    record_status: Optional[int] = 1
    product_listing: Optional[Any]
    updated_at: Optional[datetime] = datetime.today()
    pass


class CustomerBillMethodView(CustomerBillInDBBase):
    product_listing: Optional[Any]
    customer_bill_account: Optional[Any]
    customer_shipping_address: Optional[Any]
    created_at: Optional[datetime]
    account_options: Optional[Any]

    pass


class CustomerBillPublicCreate(CustomerBillCreate):
    collection_uuid: str
    pass
