from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PoolBase(BaseModel):
    pool_name: Optional[str]
    value: Optional[Decimal]
    account_id: Optional[int]
    pool_type: Optional[int]
    api_key: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Pool on creation
class PoolCreate(PoolBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PoolInDBBase(PoolBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Pool on update
class PoolUpdate(PoolBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Pool
class Pool(PoolInDBBase):
    pass


class PoolInDB(PoolInDBBase):
    pass
