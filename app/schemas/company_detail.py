import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator
from app.schemas.support_detail import SupportDetail
from app.schemas.business_owner_detail import BusinessOwnerDetail
from datetime import datetime


# Shared properties
class CompanyDetailBase(BaseModel):
    account_name: Optional[str]
    company_name: Optional[str]
    company_number: Optional[str]
    business_website_url: Optional[str]
    nature_of_business: Optional[str]
    support_detail_id: Optional[int]
    business_owner_detail_id: Optional[int]
    logo: Optional[str]
    fav_icon: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via CompanyDetail on creation
class CompanyDetailCreate(CompanyDetailBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class CompanyDetailInDBBase(CompanyDetailBase):
    id: int

    support_detail: SupportDetail
    business_owner_detail: BusinessOwnerDetail
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via CompanyDetail on update
class CompanyDetailUpdate(CompanyDetailBase):
    updated_at: Optional[datetime] = datetime.today()


# Additional properties to return via CompanyDetail
class CompanyDetail(CompanyDetailInDBBase):
    pass


class CompanyDetailInDB(CompanyDetailInDBBase):
    pass


# Properties to receive via Info details on update
class InfoDetailUpdate(CompanyDetailBase):
    updated_at: Optional[datetime] = datetime.today()
    company_detail_business_owner_detail: BusinessOwnerDetail
