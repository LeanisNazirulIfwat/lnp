from typing import Optional
from pydantic import BaseModel, validator
from datetime import datetime
from decimal import Decimal

# Shared properties
class SubscriptionBase(BaseModel):
    plan_name: Optional[str]
    plan_charges: Optional[int]
    fpx_charges: Optional[float]
    threshold_charges: Optional[float]
    credit_card_charges: Optional[int]
    payment_form: Optional[int]
    bill_form: Optional[int]
    domain: Optional[int]
    short_url: Optional[str]
    store: Optional[int]
    catalog: Optional[int]
    product: Optional[int]
    virtual_account: Optional[int]
    api: Optional[str]

    description: Optional[str]
    ewallet_charges: Optional[float]
    bnpl_charges: Optional[float]
    payout_charges: Optional[float]
    paypal_charges: Optional[float]
    crypto_charges: Optional[float]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class SubscriptionCreate(SubscriptionBase):
    credit_card_enable: Optional[bool]
    fpx_enable: Optional[bool]
    ewallet_enable: Optional[bool]
    bnpl_enable: Optional[bool]
    paypal_enable: Optional[bool]
    crypto_enable: Optional[bool]

    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SubscriptionInDBBase(SubscriptionBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode: True


# Properties to receive via API on update
class SubscriptionUpdate(SubscriptionBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class Subscription(SubscriptionInDBBase):
    pass


class SubscriptionInDB(SubscriptionInDBBase):
    pass
