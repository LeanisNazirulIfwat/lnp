from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PageBase(BaseModel):
    title: Optional[str]
    slug: Optional[str]
    cover: Optional[str]
    content: Optional[str]
    account_id: Optional[int]
    published: Optional[bool]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Page on creation
class PageCreate(PageBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PageInDBBase(PageBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Page on update
class PageUpdate(PageBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Page
class Page(PageInDBBase):
    pass


class PageInDB(PageInDBBase):
    pass
