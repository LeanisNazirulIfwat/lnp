from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.rule import Rule


# Shared properties
class DiscountBase(BaseModel):
    name: Optional[str]
    start_date: Optional[datetime]
    end_date: Optional[datetime]
    account_usage_limit: Optional[str]
    account_id: Optional[int]
    coupon_uuid: Optional[str]
    rule_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Discount on creation
class DiscountCreate(DiscountBase):
    created_at: Optional[datetime] = datetime.today().now()
    updated_at: Optional[datetime] = datetime.today()

    pass


class DiscountInDBBase(DiscountBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Discount on update
class DiscountUpdate(DiscountBase):
    updated_at: Optional[datetime] = datetime.today().now()


# Additional properties to return via Discount
class Discount(DiscountInDBBase):
    rule: Optional[Rule]
    pass


class DiscountInDB(DiscountInDBBase):
    pass
