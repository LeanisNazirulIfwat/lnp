from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class SubscriptionHistoryBase(BaseModel):
    subscribed_at: Optional[datetime]
    activated_at: Optional[datetime]
    plan_name: Optional[int]
    interval: Optional[str]
    start_date: Optional[datetime]
    end_date: Optional[datetime]
    account_id: Optional[int]
    upgrade_complete: Optional[bool]
    current_plan_name: Optional[int]
    bill: Optional[int]
    bill_status: Optional[str]


# Properties to receive via SubscriptionHistory on creation
class SubscriptionHistoryCreate(SubscriptionHistoryBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


class SubscriptionHistoryInDBBase(SubscriptionHistoryBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via SubscriptionHistory on update
class SubscriptionHistoryUpdate(SubscriptionHistoryBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via SubscriptionHistory
class SubscriptionHistory(SubscriptionHistoryInDBBase):
    pass


class SubscriptionHistoryInDB(SubscriptionHistoryInDBBase):
    pass


class SubscriptionPlanUpdateCreate(SubscriptionHistoryBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
