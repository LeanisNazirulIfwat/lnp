from datetime import datetime
from typing import Optional, Text
from pydantic import BaseModel,validator
from app.schemas.account import Account


# Shared properties
class ShareSettingBase(BaseModel):
    title: Optional[str]
    description: Optional[str]
    image_of_media_social: Optional[Text]
    image_for_whatsapp: Optional[Text]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ShareSetting on creation
class ShareSettingCreate(ShareSettingBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ShareSettingInDBBase(ShareSettingBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via ShareSetting on update
class ShareSettingUpdate(ShareSettingBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ShareSetting
class ShareSetting(ShareSettingInDBBase):
    pass


class ShareSettingInDB(ShareSettingInDBBase):
    pass
