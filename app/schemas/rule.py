from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from decimal import Decimal

# Shared properties
class RuleBase(BaseModel):
    discount_for: Optional[str]
    discount_type: Optional[str]
    discount_amount: Optional[Decimal]
    condition: Optional[str]
    group: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via Rule on creation
class RuleCreate(RuleBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class RuleInDBBase(RuleBase):
    id: int

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via Rule on update
class RuleUpdate(RuleBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via Rule
class Rule(RuleInDBBase):
    pass


class RuleInDB(RuleInDBBase):
    pass
