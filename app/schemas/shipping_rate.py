from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class ShippingRateBase(BaseModel):
    courier: Optional[int]
    price: Optional[float]
    condition: Optional[str]
    weight_condition: Optional[int]
    min_weight: Optional[Decimal]
    max_weight: Optional[Decimal]
    shipping_zone_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ShippingRate on creation
class ShippingRateCreate(ShippingRateBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ShippingRateInDBBase(ShippingRateBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via ShippingRate on update
class ShippingRateUpdate(ShippingRateBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ShippingRate
class ShippingRate(ShippingRateInDBBase):
    pass


class ShippingRateInDB(ShippingRateInDBBase):
    pass
