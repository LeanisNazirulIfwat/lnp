from typing import Optional
from pydantic import BaseModel, validator
from datetime import datetime

# Shared properties
class SystemPreferenceBase(BaseModel):
    name: Optional[str]
    description: Optional[str]
    record_status: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class SystemPreferenceCreate(SystemPreferenceBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SystemPreferenceInDBBase(SystemPreferenceBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class SystemPreferenceUpdate(SystemPreferenceBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class SystemPreference(SystemPreferenceInDBBase):
    pass


class SystemPreferenceInDB(SystemPreferenceInDBBase):
    pass
