from datetime import datetime
from decimal import Decimal
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class TransactionTopupRecordBase(BaseModel):
    invoice_no: Optional[str]
    transaction_invoice_no: Optional[str]
    transaction_status: Optional[str]
    amount: Optional[Decimal]
    account_id: Optional[int]
    payment_date: Optional[datetime]
    data: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via TransactionTopupRecord on creation
class TransactionTopupRecordCreate(TransactionTopupRecordBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class TransactionTopupRecordInDBBase(TransactionTopupRecordBase):
    id: int
    record_status: Optional[int] = 1
    updated_at: Optional[datetime] = datetime.today()

    class Config:
        orm_mode = True


# Properties to receive via TransactionTopupRecord on update
class TransactionTopupRecordUpdate(TransactionTopupRecordBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via TransactionTopupRecord
class TransactionTopupRecord(TransactionTopupRecordInDBBase):
    pass


class TransactionTopupRecordInDB(TransactionTopupRecordInDBBase):
    pass
