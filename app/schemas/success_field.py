from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class SuccessFieldBase(BaseModel):
    additional_message: Optional[str]
    redirect_url: Optional[str]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via SuccessField on creation
class SuccessFieldCreate(SuccessFieldBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SuccessFieldInDBBase(SuccessFieldBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via SuccessField on update
class SuccessFieldUpdate(SuccessFieldBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via SuccessField
class SuccessField(SuccessFieldInDBBase):
    pass


class SuccessFieldInDB(SuccessFieldInDBBase):
    pass
