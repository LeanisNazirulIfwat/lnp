from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class SupportDetailBase(BaseModel):
    support_contact_person: Optional[str]
    support_email:  Optional[str]
    support_phone_number:  Optional[str]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via SupportDetail on creation
class SupportDetailCreate(SupportDetailBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class SupportDetailInDBBase(SupportDetailBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via SupportDetail on update
class SupportDetailUpdate(SupportDetailBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via SupportDetail
class SupportDetail(SupportDetailInDBBase):
    pass


class SupportDetailInDB(SupportDetailInDBBase):
    pass
