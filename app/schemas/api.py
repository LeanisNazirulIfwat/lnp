from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator


# Shared properties
class ApiBase(BaseModel):
    interface_name: Optional[str]
    uuid: Optional[str]
    callback_url: Optional[str]
    redirect_url: Optional[str]
    cancel_url: Optional[str]
    time_out_url: Optional[str]
    payment_mode: Optional[str]
    fpx_bank_selection: Optional[str]
    hash_key: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class ApiCreate(ApiBase):
    account_id: Optional[int]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ApiInDBBase(ApiBase):
    id: int
    payment_model: Optional[str]
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class ApiUpdate(ApiBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class Api(ApiInDBBase):
    pass


class ApiInDB(ApiInDBBase):
    pass


class ApiDisplay(ApiInDBBase):
    access_id: Optional[str]
    pass
