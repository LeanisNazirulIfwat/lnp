from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator


# Shared properties
class BusinessOwnerDetailBase(BaseModel):
    legal_name: Optional[str]
    individual_nric: Optional[str]
    address_line_one: Optional[str]
    address_line_two: Optional[str]
    address_postcode: Optional[int]
    address_state: Optional[str]
    address_city: Optional[str]
    address_country: Optional[str]
    bank_name: Optional[str]
    business_bank_account_number: Optional[str]
    bank_account_statement_header: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via BusinessOwnerDetail on creation
class BusinessOwnerDetailCreate(BusinessOwnerDetailBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class BusinessOwnerDetailInDBBase(BusinessOwnerDetailBase):
    id: int
    external_account: Optional[str]

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via BusinessOwnerDetail on update
class BusinessOwnerDetailUpdate(BusinessOwnerDetailBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via BusinessOwnerDetail
class BusinessOwnerDetail(BusinessOwnerDetailInDBBase):
    pass


class BusinessOwnerDetailInDB(BusinessOwnerDetailInDBBase):
    pass
