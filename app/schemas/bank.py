from datetime import datetime
from typing import Optional
from pydantic import BaseModel, ValidationError, validator
from decimal import Decimal


# Shared properties
class BankBase(BaseModel):
    name: str
    code: Optional[str]
    bank_branch: Optional[str]
    account_number: Optional[str]
    description: Optional[str]
    bank_account_statement: Optional[str]
    record_status: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class BankCreate(BankBase):
    account_id: Optional[int]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


class BankInDBBase(BankBase):
    id: int

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class BankUpdate(BankBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via API
class Bank(BankInDBBase):
    pass


class BankInDB(BankInDBBase):
    pass
