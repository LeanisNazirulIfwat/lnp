from typing import Optional, Any, List
from pydantic import BaseModel, Field, validator, condecimal, conint
from app.schemas.share_setting import ShareSetting
from app.schemas.support_detail import SupportDetail
from app.schemas.success_field import SuccessField
from app.schemas.payment_setting import PaymentSetting
from app.schemas.account import Account
from decimal import Decimal
from datetime import datetime


# Shared properties
class CollectionBase(BaseModel):
    title: Optional[str]
    base_url: Optional[str] = Field(None)
    short_url: Optional[str]
    min_amount: Optional[condecimal(decimal_places=2, gt=0)]
    tax: Optional[Decimal]
    discount: Optional[int]
    custom_field: Optional[str]
    description: Optional[str]
    secondary_description: Optional[str]
    footer_description: Optional[str]
    collection_method: Optional[int] = 1
    record_status: Optional[int]

    fixed_amount: Optional[bool]
    allow_customers_to_enter_quantity: Optional[bool]
    enable_quantity_limit: Optional[bool]
    quantity_limit: Optional[condecimal(ge=0.00)]
    enable_total_amount_tracking: Optional[bool]
    enable_billing_address: Optional[bool]
    total_target_amount: Optional[float]
    disable_payment_when_target_reached: Optional[bool]
    show_seller_information_enable: Optional[bool] = False
    cod_enable: Optional[bool] = False
    sms_enable: Optional[bool] = False
    whatsapp_enable: Optional[bool] = False
    email_enable: Optional[bool] = False
    uuid: Optional[str]

    share_setting_id: Optional[int]
    success_field_id: Optional[int]
    payment_setting_id: Optional[int]
    support_detail_id: Optional[int]
    shipping_profile_id: Optional[int]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class CollectionCreate(CollectionBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class CollectionInDBBase(CollectionBase):
    id: int
    collection_share_setting: Optional[ShareSetting]
    collection_success_field: Optional[SuccessField]
    collection_payment_setting: Optional[PaymentSetting]
    collection_account: Optional[Account]

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via API on update
class CollectionUpdate(CollectionBase):
    updated_at: Optional[datetime] = datetime.today()
    record_status: Optional[int] = 1
    pass


# Additional properties to return via API
class Collection(CollectionInDBBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


class CollectionInDB(CollectionInDBBase):
    pass


class CollectionView(CollectionBase):
    collection_share_setting: Optional[ShareSetting]
    collection_support_detail: Optional[SupportDetail]
    collection_success_field: Optional[SuccessField]
    collection_payment_setting: Optional[PaymentSetting]
    collection_account: Optional[Account]
    # account_company_details: Optional[Any]
    bill_invoice_suggestion: Optional[str]
    account_options: Optional[Any]


class CollectionMethodView(CollectionView):
    product_listing: Optional[Any]
    pass


class ProductListing(BaseModel):
    product_id: int
    name: str
    amount: Decimal
    quantity: int
    id: Optional[Any]
    total_amount: Decimal

    @validator('*', pre=True)
    def passwords_match(cls, v):
        if v == 0:
            raise ValueError('ZERO_VALUE_NOT_ALLOWED')
        return v


# Properties to receive via API on creation
class CollectionCreateBill(CollectionBase):
    record_status: Optional[int] = 1
    product_listing: Optional[List[ProductListing]]
    pass


class CollectionUpdateBill(CollectionBase):
    record_status: Optional[int] = 1
    product_listing: Optional[Any]
    pass
