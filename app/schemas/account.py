from datetime import datetime
from typing import Optional, Any
from app.schemas.subscription import Subscription
from app.schemas.user import User
from pydantic import UUID4, BaseModel, validator


# Shared properties
class AccountBase(BaseModel):
    name: Optional[str]
    description: Optional[str] = "MERCHANT"
    current_subscription_ends: Optional[datetime]
    is_active: Optional[bool] = True
    user_id: Optional[int] = None
    subscription_plan_id: Optional[int] = None
    system_preference_id: Optional[int] = None
    merchant_id: Optional[str]
    account_type: Optional[str]
    bank_type: Optional[int] = 1
    enable_customer_pay: Optional[bool] = False
    current_subscription_time_period: Optional[int] = 1

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via API on creation
class AccountCreate(AccountBase):
    url: Optional[str]
    business_location: Optional[str]
    business_proof: Optional[str]
    account_name: Optional[str]
    parent_key: Optional[str]
    company_details_id: Optional[int]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


# Properties to receive via API on update
class AccountUpdate(AccountBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


class AccountInDBBase(AccountBase):
    id: int
    record_status: Optional[int] = 1
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    account_type: Optional[str]
    record_status: Optional[int]
    users: Optional[User]
    subscription_plan: Optional[Any]
    # business_owner_details_id: Optional[int]
    # account_business_owner_details: Optional[Any]
    account_company_details: Optional[Any]

    class Config:
        orm_mode = True


# Additional properties to return via API
class Account(AccountInDBBase):
    pass


class AccountInDB(AccountInDBBase):
    pass
