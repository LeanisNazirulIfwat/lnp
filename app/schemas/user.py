from datetime import datetime
from typing import Optional, Any

from app.schemas.user_role import UserRole
from pydantic import BaseModel, EmailStr, validator, ValidationError, constr


# Shared properties
class UserBase(BaseModel):
    email: Optional[EmailStr] = None
    is_active: Optional[bool] = True
    full_name: constr(max_length=63, strip_whitespace=True) = None
    phone_number: constr(max_length=63, strip_whitespace=True) = None

    # regex = "^(\+?6?01)[02-46-9]-*[0-9]{7}$|^(\+?6?01)[1]-*[0-9]{8}$"
    # account_id: Optional[int] = None

    @validator('phone_number')
    def check_phone_number(cls, v):
        if v == "":
            raise ValueError("EMPTY_PHONE_NUMBER")
        elif ' ' in v:
            raise ValueError('NO_SPACE_IS_ALLOWED')
        return v


# Properties to receive via API on creation
class UserCreate(UserBase):
    password: str
    system_id: Optional[int]
    api_key: Optional[str]
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()


# Properties to receive via API on update
class UserUpdate(UserBase):
    system_id: Optional[int]
    password: Optional[str]
    updated_at: Optional[datetime] = datetime.today()
    record_status: Optional[int]
    pass


class UserInDBBase(UserBase):
    id: int
    user_role: Optional[UserRole]
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


# Additional properties to return via API
class User(UserInDBBase):
    role_name: Optional[str]
    pass


# Additional properties stored in DB
class UserInDB(UserInDBBase):
    hashed_password: str


# Additional properties for creating merchant
class UserMerchant(BaseModel):
    email: Optional[EmailStr] = None
    is_active: Optional[bool] = True
    full_name: constr(max_length=63, strip_whitespace=True) = None
    phone_number: constr(max_length=63, strip_whitespace=True) = None
    password: str
    response_additional_message: str
    redirect_url: str
    response_title: str
    description: str
    image_of_media_social: str
    image_for_whatsapp: str
    api_interface: str
    fpx_bank_selection: str


# Additional properties for creating merchant
class UserAdditionalSetting(BaseModel):
    email_verified: Optional[bool] = False
    name: constr(max_length=63, strip_whitespace=True) = None
    first_name: constr(max_length=63, strip_whitespace=True) = None
    last_name: constr(max_length=63, strip_whitespace=True) = None
    OTP: constr(max_length=63, strip_whitespace=True) = None
    phone_number: constr(max_length=63, strip_whitespace=True) = None
    mobile_pin: constr(max_length=63, strip_whitespace=True) = None
    kyc_information: constr(max_length=63, strip_whitespace=True) = None
    secure_word: constr(max_length=63, strip_whitespace=True) = None
    api_key: constr(max_length=63, strip_whitespace=True) = None


# Additional properties for creating white label user
class UserWhiteLabel(UserMerchant):
    application_name: Optional[str]
    main_site: Optional[str]
    logo: Optional[str]
    logo_alt: Optional[str]
    qr_code_logo: Optional[str]
    favicon: Optional[str]
    meta_image: Optional[str]
    meta_description: Optional[str]
    background_color: Optional[str]
    background_image: Optional[str]
    primary_color: Optional[str]
    secondary_color: Optional[str]
    company_name: Optional[str]
    platform_version: Optional[str]
