from datetime import datetime
from decimal import Decimal
from typing import Optional, List
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PrefundRecordBase(BaseModel):
    switch_pool_id: Optional[str]
    merchant_pool_id: Optional[str]
    transaction_invoice_no: Optional[str]
    transaction_status: Optional[str]
    value: Optional[Decimal]
    account_id: Optional[int]
    data: Optional[str]
    image_path: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        # if v == 0:
        #     raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PrefundRecord on creation
class PrefundRecordCreate(PrefundRecordBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PrefundRecordInDBBase(PrefundRecordBase):
    id: int
    record_status: Optional[int] = 1
    updated_at: Optional[datetime] = datetime.today()

    class Config:
        orm_mode = True


# Properties to receive via PrefundRecord on update
class PrefundRecordUpdate(PrefundRecordBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via PrefundRecord
class PrefundRecord(PrefundRecordInDBBase):
    pass


class PrefundRecordInDB(PrefundRecordInDBBase):
    pass


class PrefundBulkUpdateBase(BaseModel):
    updated_at: Optional[datetime] = datetime.today()
    id: List[Optional[int]]
    transaction_status: List[Optional[str]]
    image_path: List[Optional[str]]
