from typing import Any, List, Optional

from pydantic import BaseModel


class FullPayoutServiceObj(BaseModel):
    id: int
    payout_service_id: int
    duitnow_provider_id: int
    name: str
    category_code: str
    group_code: str
    channel_code: str
    agent_code: str
    agent_channel_code: str
    rate: str
    rate_type_reference_id: int
    country_code: str
    staging_bic_code: str
    production_bic_code: str
    payment_model_reference_id: str
    max_charge: str
    min_charge: str
    max_charge_applicable_id: int
    min_charge_applicable_id: int
    record_status_id: str
    created_at: str
    updated_at: str
    client_access_token_id: int
    seller_id_payment_switch: str
    bank_code: str
    bank_name: str
    seller_id: str
    seller_name: str
    client_id: str
    exchange_id: str
    exchange_name: str
    company_registration_no: str
    business_category_code: str
    msic_code: str
    ssl_trusted_root_cert: Any
    ssl_key_cert: Any
    ssl_full_chain_cert: Any
    code_signing_private_key: Any
    code_signing_public_key: Any
    payout_from_account_no: str


class Selected(BaseModel):
    paymentSwitchChargeAmount: float
    providerTypeReferenceId: int
    providerTypeReference: str
    duitnowServiceId: int
    selectedPayoutServiceId: int
    fullPayoutServiceObj: FullPayoutServiceObj


class Option(BaseModel):
    selectedPaymentServiceId: int
    currentPayoutSwitchCharge: float
    providerTypeReferenceId: int
    providerTypeReference: str
    duitnowServiceId: int
    fullPayoutServiceObj: FullPayoutServiceObj


class BestRate(BaseModel):
    selected: Selected
    options: List[Option]
    response_code: int


class BestRateTwo(BaseModel):
    selected: Optional[Selected]
    response_code: int


class SwitchPayOutResponse(BaseModel):
    payout_service_id: int
    payment_model_reference_id: int
    payment_model_reference: str
    unique_reference: str
    preferred_switch_payout_integration_provider_code: str
    name: str
    category_code: str
    group_code: str
    channel_code: str
    record_status_id: int
    record_status: str
    best_rate: Optional[BestRateTwo]
