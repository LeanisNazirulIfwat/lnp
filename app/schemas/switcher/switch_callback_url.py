import decimal
from datetime import datetime
from typing import Optional, Any
from app.schemas.subscription import Subscription
from app.schemas.user import User
from pydantic import UUID4, BaseModel, ValidationError, validator, EmailStr
from app.core.config import settings


# Shared properties
class SwitchCallbackUrlBase(BaseModel):
    invoice_no: Optional[str]
    invoice_status_id: Optional[int]
    invoice_status: Optional[str]
    amount: Optional[float]
    payment_service_id: Optional[int]
    collection_uuid: Optional[str]
    # client_redirect_url: Optional[str]
    # client_callback_url: Optional[str]
    # client_transaction_details: Optional[str]
    transaction_details: Optional[str]
    description: Optional[str]
    card_token: Optional[str]

    @validator('amount')
    def name_x_space_zero(cls, v):
        min_value = decimal.Decimal(str(v)).as_tuple().exponent
        if min_value < -2:
            raise ValueError('must be only 2 decimal places')
        return v

    class Config:
        schema_extra = {
            "example": {
                "collection_uuid": "B9CAB38C4F",
                "amount": 20156.15,
                "payment_service_id": 55,
                "redirect_url": "https://leanis.com.my",
                "callback_url": f"{settings.API_UAT_URL}api/v1/callback-url/call-encrypt",
                "full_name": "MAMOMAN",
                "email": "MAMOMAN@GMAIL.COM",
                "phone_number": "0112459861",
                "address": "SERI KEMBANGAN",
                "quantity": 5
            }
        }


# Properties to receive via API on creation
class SwitchCallbackUrlCreate(SwitchCallbackUrlBase):
    secure_pay_token: Optional[str] = settings.SECURE_PAY_TOKEN
    redirect_url: Optional[str] = settings.REDIRECT_URL_LEANPAY
    callback_url: Optional[str] = settings.CALLBACK_URL_LEANPAY

    full_name: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]
    address_line_one: Optional[str]
    address_state: Optional[str]
    address_line_two: Optional[str]
    address_postcode: Optional[int]
    address_city: Optional[str]
    address_country: Optional[str]
    shipping_address_id: Optional[int]
    product_listing: Optional[Any]
    quantity: Optional[int] = 1


class SwitchPublicBillCreation(SwitchCallbackUrlBase):
    secure_pay_token: Optional[str] = settings.SECURE_PAY_TOKEN
    redirect_url: Optional[str] = settings.REDIRECT_URL_LEANPAY
    callback_url: Optional[str] = settings.CALLBACK_URL_LEANPAY

    full_name: Optional[str]
    email: EmailStr
    phone_number: Optional[str]
    address_line_one: Optional[str]
    address_state: Optional[str]
    address_line_two: Optional[str]
    address_postcode: Optional[int]
    address_city: Optional[str]
    address_country: Optional[str]
    shipping_address_id: Optional[int]
    product_listing: Optional[Any]
    quantity: Optional[int] = 1


# Properties to receive via API on update
class SwitchCallbackUrlUpdate(SwitchCallbackUrlBase):
    pass


class SwitchCallbackUrlInDBBase(SwitchCallbackUrlBase):
    class Config:
        orm_mode = True


# Additional properties to return via API
class SwitchCallbackUrl(SwitchCallbackUrlInDBBase):
    pass


class SwitchCallbackUrlInDB(SwitchCallbackUrlInDBBase):
    pass


class SwitchCallbackPrefundPool(SwitchCallbackUrlCreate):
    virtual_pool_reference: Optional[str]
    virtual_pool_callback: Optional[str] = f"{settings.API_UAT_URL}api/v1/callback-url/call-prefund-encrypt"
    client_data: Optional[str]
    invoice_type_id: Optional[int] = 1
    fee: Optional[float]
    fee_type_id: Optional[int]
    pass


class SwitchCallbackTopupPool(SwitchCallbackUrlCreate):
    callback_url: Optional[str] = settings.CALLBACK_URL_LEANPAY
    invoice_type_id: Optional[int] = 3
    client_data: Optional[str]
    fee: Optional[float]
    fee_type_id: Optional[int]
    pass


class SwitchCallbackPayoutResponse(BaseModel):
    client_identifier: Optional[str] = "LEANPAY"
    virtual_pool_reference: Optional[str]
    payout_service_id: Optional[str]
    amount: Optional[str]
    client_redirect_url: Optional[str]
    client_callback_url: Optional[str] = f"{settings.API_UAT_URL}api/v1/callback-url/call-invoice-payout-encrypt"
    third_party_account_no: Optional[str]
    recipient_reference: Optional[str]
    payment_description: Optional[str]
    client_data: Optional[Any]
    skip_encryption: Optional[str] = False
