from datetime import datetime
from typing import Optional, Any
from app.schemas.subscription import Subscription
from app.schemas.user import User
from pydantic import UUID4, BaseModel
from decimal import Decimal


# Shared properties
class SwitchPaymentBase(BaseModel):
    payment_service_id: int
    amount: Decimal
    redirect_url: Optional[str]
    callback_url: Optional[str]
    secure_pay_token: Optional[str]
    installment_period: Optional[int] = None
    card_token: Optional[int]
    payload: Optional[str]
    client_identifier: Optional[str]
    skip_encryption: Optional[bool] = True


# Properties to receive via API on creation
class SwitchPaymentCreate(SwitchPaymentBase):
    pass


# Properties to receive via API on update
class SwitchPaymentUpdate(SwitchPaymentBase):
    pass


class SwitchPaymentInDBBase(SwitchPaymentBase):
    class Config:
        orm_mode = True


# Additional properties to return via API
class SwitchPayment(SwitchPaymentInDBBase):
    pass


class SwitchPaymentInDB(SwitchPaymentInDBBase):
    pass


class SwitchC2PServices(BaseModel):
    id: int
    payment_service_id: int
    name: str
    category_code: str
    group_code: str
    channel_code: str
    agent_code: Optional[str]
    agent_channel_code: Optional[str]
    switch_integration_provider_code: str
    country_code: str
    record_status_id: str
    rate: float


class FullSelectedService(BaseModel):
    id: int
    payment_service_id: Optional[int]
    name: Optional[str]
    category_code: Optional[str]
    group_code: Optional[str]
    channel_code: Optional[str]
    agent_code: Optional[str]
    agent_channel_code: Optional[str]
    switch_integration_provider_code: Optional[str]
    country_code: Optional[str]
    record_status_id: Optional[int]
    rate: Optional[Decimal]
    created_at: Optional[str]
    updated_at: Optional[str]
    rate_type_reference_id: Optional[int]
    c2p_provider_id: Optional[int]
    max_charge: Optional[Decimal]
    min_charge: Optional[Decimal]
    max_charge_applicable_id: Optional[int]
    min_charge_applicable_id: Optional[int]
    client_access_token_id: Optional[int]
    seller_id_payment_switch: Optional[str]
    bank_code: Optional[str]
    bank_name: Optional[str]
    seller_id: Optional[str]
    seller_name: Optional[str]
    exchange_id: Optional[str]
    exchange_name: Optional[str]
    company_registration_no: Optional[str]
    business_category_code: Optional[str]
    msic_code: Optional[str]


class Selected(BaseModel):
    paymentSwitchChargeAmount: Decimal
    providerTypeReferenceId: int
    providerTypeReference: str
    c2pOrFpxServiceId: int
    selectedPaymentServiceId: int
    fullPaymentServiceObj: FullSelectedService


class BestRate(BaseModel):
    selected: Optional[Selected]
    options: Optional[Any]
    response_code: int


class SwitchPaymentResponses(BaseModel):
    payment_service_id: int
    payment_model_reference_id: int
    payment_model_reference: str
    unique_reference: str
    switch_payment_service_id: int
    switch_payment_service: str
    name: str
    category_code: str
    group_code: str
    channel_code: str
    record_status_id: int
    record_status: str
    best_rate: BestRate


class SwitchPublicPaymentResponses(BaseModel):
    payment_service_id: int
    payment_model_reference_id: int
    payment_model_reference: str
    unique_reference: str
    payment_service: str
    name: str
    rate: float
    record_status_id: int
    record_status: str


class SwitchPublicPayoutResponses(BaseModel):
    payout_service_id: int
    payment_model_reference_id: int
    payment_model_reference: str
    unique_reference: str
    payment_service: str
    name: str
    rate: float
    record_status_id: int
    record_status: str


class ManualFullPaymentService(BaseModel):
    id: Optional[int]
    payment_service_id: Optional[int]
    fpx_provider_id: Optional[int]
    name: Optional[str]
    category_code: Optional[str]


class ManualSelected(BaseModel):
    paymentSwitchChargeAmount: Optional[float]
    providerTypeReferenceId: Optional[int]
    providerTypeReference: Optional[str]
    c2pOrFpxServiceId: Optional[int]
    selectedPaymentServiceId: Optional[int]
    fullPaymentServiceObj: Optional[ManualFullPaymentService]


class SwitchManualPaymentResponse(BaseModel):
    invoice_no: Optional[str]
    client_data: Optional[Any]
    invoice_status_id: Optional[int]
    invoice_type_id: Optional[int]
    invoice_type: Optional[str]
    invoice_status: Optional[str]
    amount: Optional[str]
    payment_switch_charge_rate: Optional[str]
    payment_model_reference_id: Optional[int]
    selected: Optional[ManualSelected]
    profit: Optional[str]
    payment_service_id: Optional[int]
    client_redirect_url: Optional[str]
    client_callback_url: Optional[str]
    client_transaction_details: Optional[Any]
    fpx_direct_response: Optional[str]
    description: Optional[Any]
    card_token: Optional[Any]
    transaction_response_time: Optional[str]
    original_transaction_response_time: Optional[str]
