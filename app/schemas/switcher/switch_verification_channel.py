from typing import Any, List, Optional

from pydantic import BaseModel


class DuitnowServiceModel(BaseModel):
    staging_bic_code: Optional[str]
    production_bic_code: Optional[str]
    record_status_id: Optional[int]
    id: Optional[int]


class VerificationChannelBaseModel(BaseModel):
    payout_service_id: Optional[int]
    unique_reference: Optional[str]
    name: Optional[str]
    category_code: Optional[str]
    group_code: Optional[str]
    channel_code: Optional[str]
    record_status_id: Optional[int]
    record_status: Optional[str]
    duitnowServices: Optional[List[DuitnowServiceModel]]


class VerificationCheckBaseModel(BaseModel):
    client_identifier: Optional[str]
    payout_service_id: Optional[int]
    verification_channel_id: Optional[int]
    from_account_no: Optional[str]
    third_party_account_no: Optional[str]
