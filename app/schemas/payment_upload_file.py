from datetime import datetime
from typing import Optional
from pydantic import BaseModel, validator
from app.schemas.account import Account


# Shared properties
class PaymentUploadFileBase(BaseModel):
    title: Optional[str]
    status: Optional[str] = "PENDING"
    status_id: Optional[int] = 2
    collection_id: Optional[int]
    customer_bills: Optional[str]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PaymentUploadFile on creation
class PaymentUploadFileCreate(PaymentUploadFileBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class PaymentUploadFileInDBBase(PaymentUploadFileBase):
    id: int

    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via PaymentUploadFile on update
class PaymentUploadFileUpdate(PaymentUploadFileBase):
    completed_at: Optional[datetime]
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via PaymentUploadFile
class PaymentUploadFile(PaymentUploadFileInDBBase):
    pass


class PaymentUploadFileInDB(PaymentUploadFileInDBBase):
    pass
