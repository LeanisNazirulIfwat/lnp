from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator
from app.schemas.shipping_zone import ShippingZone


# Shared properties
class ShippingProfileBase(BaseModel):
    name: Optional[str]
    account_id: Optional[int]

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        if v == 0:
            raise ValueError("ZERO_VALUE_NOT_ALLOWED")
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via ShippingProfile on creation
class ShippingProfileCreate(ShippingProfileBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()

    pass


class ShippingProfileInDBBase(ShippingProfileBase):
    id: int
    record_status: Optional[int] = 1

    shipping_profile_shipping_zone: Optional[ShippingZone]
    shipping_profile_shipment: Any

    class Config:
        orm_mode = True


# Properties to receive via ShippingProfile on update
class ShippingProfileUpdate(ShippingProfileBase):
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via ShippingProfile
class ShippingProfile(ShippingProfileInDBBase):
    pass


class ShippingProfileInDB(ShippingProfileInDBBase):
    pass
