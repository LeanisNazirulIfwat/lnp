from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel, validator
from app.schemas.domain import Domain
from app.schemas.share_setting import ShareSetting
from app.schemas.support_detail import SupportDetail
from app.schemas.account import Account
from app.schemas.payment_setting import PaymentSetting
from app.schemas.collection import Collection


# Shared properties
class PaymentCollectionChannelBase(BaseModel):
    collection_id: Optional[int]
    name: Optional[str]
    description: Optional[str]
    data: Optional[str]

    fpx_enable: Optional[bool] = True
    credit_card_enable: Optional[bool] = False
    ewallet_enable: Optional[bool] = False
    bnpl_enable: Optional[bool] = False
    paypal_enable: Optional[bool] = False
    crypto_enable: Optional[bool] = False

    @validator('*', pre=True)
    def name_x_space_zero(cls, v):
        assert v != '', 'EMPTY_STRING_NOT_ALLOWED'
        return v


# Properties to receive via PaymentCollectionChannel on creation
class PaymentCollectionChannelCreate(PaymentCollectionChannelBase):
    account_id: Optional[int]
    pass


class PaymentCollectionChannelInDBBase(PaymentCollectionChannelBase):
    id: int
    record_status: Optional[int] = 1

    class Config:
        orm_mode = True


# Properties to receive via PaymentCollectionChannel on update
class PaymentCollectionChannelUpdate(PaymentCollectionChannelBase):
    created_at: Optional[datetime] = datetime.today()
    updated_at: Optional[datetime] = datetime.today()
    pass


# Additional properties to return via PaymentCollectionChannel
class PaymentCollectionChannel(PaymentCollectionChannelInDBBase):
    pass


class PaymentCollectionChannelInDB(PaymentCollectionChannelInDBBase):
    pass
