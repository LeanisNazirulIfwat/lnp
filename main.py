from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.responses import ORJSONResponse
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from app.api.api_v1.api import api_router
from app.core.config import settings
from starlette.responses import RedirectResponse
from app.db.init_db import init_db
from app.db.session import SessionLocal
import logging
from fastapi.encoders import jsonable_encoder
from fastapi import Request, status
from fastapi.exceptions import RequestValidationError
import datetime
from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi.responses import PlainTextResponse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    default_response_class=ORJSONResponse,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/assets", StaticFiles(directory="assets"), name="assets")
app.mount("/Settlement", StaticFiles(directory="Settlement/daily"), name="daily")

app.include_router(api_router, prefix=settings.API_V1_STR)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return ORJSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({"response_code": 6112,
                                  "description": "FAILED",
                                  "app_version": settings.API_V1_STR,
                                  "talk_to_server_before": datetime.datetime.today(),
                                  "data": exc.body,
                                  "location": exc.errors()[0]["loc"],
                                  "breakdown_errors": exc.errors()[0]["msg"],
                                  "token": request.headers})
    )


@app.exception_handler(StarletteHTTPException)
async def my_exception_handler(request, exception):
    return ORJSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder({"response_code": exception.status_code,
                                  "description": "FAILED",
                                  "app_version": settings.API_V1_STR,
                                  "talk_to_server_before": datetime.datetime.today(),
                                  "data": request.headers["host"],
                                  "breakdown_errors": exception.detail,
                                  "token": request.headers})
    )


@app.middleware("http")
async def request_middleware(request: Request, call_next):
    logger.info("Request started")
    try:
        return await call_next(request)

    except Exception as ex:
        logger.error(ex)


def init() -> None:
    db = SessionLocal()
    init_db(db)


def create() -> None:
    logger.info("Creating initial data")
    init()
    logger.info("Initial data created")


@app.get("/")
def main():
    return RedirectResponse(url="/docs/")


@app.get("/loaderio-d7ad65e97fd36e72fbf87d8755af1e5a/", response_class=PlainTextResponse)
def main_loader():
    # return RedirectResponse(url="/docs/")
    return "loaderio-d7ad65e97fd36e72fbf87d8755af1e5a"


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    create()
    uvicorn.run(app, host="127.0.0.1", port=5049)
