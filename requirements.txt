python-jose[cryptography]
passlib[bcrypt]~=1.7.4
fastapi~=0.81.0
alembic~=1.8.1
SQLAlchemy~=1.4.26
inflect~=5.3.0
PyMySQL
pydantic[email]~=1.8.2
uvicorn~=0.15.0
starlette~=0.19.1
python-multipart
python-dateutil~=2.8.2
python-dotenv~=0.19.1
requests~=2.26.0
pandas~=1.3.5
openpyxl
XlsxWriter~=3.0.2
xlrd~=2.0.1
Pillow~=9.0.0
schedule
twilio~=7.11.0
sendgrid~=6.9.7
aiohttp~=3.8.1
boto3~=1.26.11
orjson
gunicorn