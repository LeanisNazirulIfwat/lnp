import datetime
import xlsxwriter
from fastapi import HTTPException
from io import BytesIO
from app.Utils.shared_utils import SharedUtils


def set_column_width(self):
    length_list = [len(x) for x in self.columns]
    for i, width in enumerate(length_list):
        self.worksheet.set_column(i, i, width)


def creating_settlement(settlement_name, list_data: list, cur_account):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, cur_account.name)

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'LEANIS SDN BHD')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'INVOICE_NO', bold)
    worksheet.write(row + 13, col + 2, 'TRANSACTION_INVOICE_NO', bold)
    worksheet.write(row + 13, col + 3, 'FULL_NAME', bold)
    worksheet.write(row + 13, col + 4, 'EMAIL', bold)
    worksheet.write(row + 13, col + 5, 'PHONE_NUMBER', bold)
    worksheet.write(row + 13, col + 6, 'PAYMENT_MODE', bold)
    worksheet.write(row + 13, col + 7, 'PAYMENT_METHOD', bold)
    worksheet.write(row + 13, col + 8, 'TRANSACTION_AMOUNT', bold)
    worksheet.write(row + 13, col + 9, 'TRANSACTION_CREATE_DATE', bold)
    worksheet.write(row + 13, col + 10, 'COLLECTION_ID', bold)
    worksheet.write(row + 13, col + 11, 'CUSTOMER_ID', bold)

    # End of Excel template writing

    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                if list_data[x - 1].payment_record_id is not None:
                    worksheet.write(row + 13 + x, col, x)
                    worksheet.write(row + 13 + x, col + 1, list_data[x - 1].invoice_no)
                    worksheet.write(row + 13 + x, col + 2, list_data[x - 1].transaction_invoice_no)
                    worksheet.write(row + 13 + x, col + 3, list_data[x - 1].full_name)
                    worksheet.write(row + 13 + x, col + 4, list_data[x - 1].email)
                    worksheet.write(row + 13 + x, col + 5, list_data[x - 1].phone_number)
                    worksheet.write(row + 13 + x, col + 6, list_data[x - 1].customer_bill_payment_record.payment_mode)
                    worksheet.write(row + 13 + x, col + 7, list_data[x - 1].customer_bill_payment_record.payment_method)
                    worksheet.write(row + 13 + x, col + 8, list_data[x - 1].customer_bill_payment_record.amount)
                    worksheet.write(row + 13 + x, col + 9, list_data[x - 1].created_at.date().isoformat())
                    worksheet.write(row + 13 + x, col + 10, list_data[x - 1].collection_id)
                    worksheet.write(row + 13 + x, col + 11, list_data[x - 1].customer_id)
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write_formula(row + 13 + len(list_data) + 6, col + 8, f'=SUM(I15:I{15 + len(list_data)})')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )


def creating_settlement_monthly(settlement_name, list_data: list, cur_account, report_date):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, cur_account.name)

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'LEANIS SDN BHD')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, report_date)

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'INVOICE_NO', bold)
    worksheet.write(row + 13, col + 2, 'TRANSACTION_INVOICE_NO', bold)
    worksheet.write(row + 13, col + 3, 'FULL_NAME', bold)
    worksheet.write(row + 13, col + 4, 'EMAIL', bold)
    worksheet.write(row + 13, col + 5, 'PHONE_NUMBER', bold)
    worksheet.write(row + 13, col + 6, 'PAYMENT_MODE', bold)
    worksheet.write(row + 13, col + 7, 'PAYMENT_METHOD', bold)
    worksheet.write(row + 13, col + 8, 'TRANSACTION_AMOUNT', bold)
    worksheet.write(row + 13, col + 9, 'TRANSACTION_CREATE_DATE', bold)
    worksheet.write(row + 13, col + 10, 'COLLECTION_ID', bold)
    worksheet.write(row + 13, col + 11, 'CUSTOMER_ID', bold)

    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                if list_data[x - 1].payment_record_id is not None:
                    total = total + list_data[x - 1].customer_bill_payment_record.amount
                    worksheet.write(row + 13 + x, col, x)
                    worksheet.write(row + 13 + x, col + 1, list_data[x - 1].invoice_no)
                    worksheet.write(row + 13 + x, col + 2, list_data[x - 1].transaction_invoice_no)
                    worksheet.write(row + 13 + x, col + 3, list_data[x - 1].full_name)
                    worksheet.write(row + 13 + x, col + 4, list_data[x - 1].email)
                    worksheet.write(row + 13 + x, col + 5, list_data[x - 1].phone_number)
                    worksheet.write(row + 13 + x, col + 6, list_data[x - 1].customer_bill_payment_record.payment_mode)
                    worksheet.write(row + 13 + x, col + 7, list_data[x - 1].customer_bill_payment_record.payment_method)
                    worksheet.write(row + 13 + x, col + 8, list_data[x - 1].customer_bill_payment_record.amount)
                    worksheet.write(row + 13 + x, col + 9, list_data[x - 1].created_at.date().isoformat())
                    worksheet.write(row + 13 + x, col + 10, list_data[x - 1].collection_id)
                    worksheet.write(row + 13 + x, col + 11, list_data[x - 1].customer_id)
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 8, total)
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )


def creating_admin_payment_settlement(settlement_name, list_data: list):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'MERCHANT_ID', bold)
    worksheet.write(row + 13, col + 2, 'MERCHANT_NAME', bold)
    worksheet.write(row + 13, col + 3, 'BANK_ACCOUNT_NUMBER', bold)
    worksheet.write(row + 13, col + 4, 'TOTAL_SALE', bold)
    worksheet.write(row + 13, col + 5, 'OUTSTANDING_AMOUNT', bold)
    worksheet.write(row + 13, col + 6, 'TOTAL_REVENUE', bold)
    worksheet.write(row + 13, col + 7, 'STATUS', bold)

    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                total = total + list_data[x - 1]["TOTAL_SALE"]
                worksheet.write(row + 13 + x, col, x)
                worksheet.write(row + 13 + x, col + 1, list_data[x - 1]["MERCHANT_ID"])
                worksheet.write(row + 13 + x, col + 2, list_data[x - 1]["MERCHANT_NAME"])
                worksheet.write(row + 13 + x, col + 3, list_data[x - 1]["BANK_ACCOUNT_NUMBER"])
                worksheet.write(row + 13 + x, col + 4, list_data[x - 1]["TOTAL_SALE"])
                worksheet.write(row + 13 + x, col + 5, list_data[x - 1]["OUTSTANDING_AMOUNT"])
                worksheet.write(row + 13 + x, col + 6, list_data[x - 1]["TOTAL_REVENUE"])
                worksheet.write(row + 13 + x, col + 7, "PENDING_SETTLEMENT")
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 9, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 10, f'{total}')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )


def creating_pay_out_report(settlement_name, list_data: list, cur_name):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, cur_name)

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'SWITCH_INVOICE_ID', bold)
    worksheet.write(row + 13, col + 2, 'MERCHANT_INVOICE_ID', bold)
    worksheet.write(row + 13, col + 3, 'INVOICE_NO', bold)
    worksheet.write(row + 13, col + 4, 'AMOUNT', bold)
    worksheet.write(row + 13, col + 5, 'TRANSACTION_STATUS', bold)

    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                total = total + list_data[x - 1]["AMOUNT"]
                worksheet.write(row + 13 + x, col, x)
                worksheet.write(row + 13 + x, col + 1, list_data[x - 1]["SWITCH_INVOICE_ID"])
                worksheet.write(row + 13 + x, col + 2, list_data[x - 1]["MERCHANT_INVOICE_ID"])
                worksheet.write(row + 13 + x, col + 3, list_data[x - 1]["INVOICE_NO"])
                worksheet.write(row + 13 + x, col + 4, list_data[x - 1]["AMOUNT"])
                worksheet.write(row + 13 + x, col + 5, "TRANSACTION_STATUS")
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 8, f'{total}')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )


def creating_admin_pay_out_report(settlement_name, list_data: list):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT ADMIN PREFUND REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'MERCHANT_NAME', bold)
    worksheet.write(row + 13, col + 2, 'MERCHANT_ID', bold)
    worksheet.write(row + 13, col + 3, 'TOTAL_PAYOUT_SALE', bold)
    worksheet.write(row + 13, col + 4, 'VIRTUAL_ACCOUNT_NUMBER', bold)

    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                total = total + list_data[x - 1]["TOTAL_PAYOUT_SALE"]
                worksheet.write(row + 13 + x, col, x)
                worksheet.write(row + 13 + x, col + 1, list_data[x - 1]["MERCHANT_NAME"])
                worksheet.write(row + 13 + x, col + 2, list_data[x - 1]["MERCHANT_ID"])
                worksheet.write(row + 13 + x, col + 3, list_data[x - 1]["TOTAL_PAYOUT_SALE"])
                worksheet.write(row + 13 + x, col + 4, list_data[x - 1]["VIRTUAL_ACCOUNT_NUMBER"])
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 8, f'{total}')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )


def creating_prefund_report(settlement_name, list_data: list, cur_name):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, cur_name)

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT PREFUND REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Prefund Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Prefund Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'MERCHANT_POOL_ID', bold)
    worksheet.write(row + 13, col + 2, 'INVOICE_NO', bold)
    worksheet.write(row + 13, col + 3, 'AMOUNT', bold)
    worksheet.write(row + 13, col + 4, 'TRANSACTION_STATUS', bold)

    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                total = total + list_data[x - 1]["AMOUNT"]
                worksheet.write(row + 13 + x, col, x)
                worksheet.write(row + 13 + x, col + 1, list_data[x - 1]["MERCHANT_POOL_ID"])
                worksheet.write(row + 13 + x, col + 2, list_data[x - 1]["INVOICE_NO"])
                worksheet.write(row + 13 + x, col + 3, list_data[x - 1]["AMOUNT"])
                worksheet.write(row + 13 + x, col + 4, "TRANSACTION_STATUS")
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 8, f'{total}')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )

def creating_admin_prefund_report(settlement_name, list_data: list):
    # Excel template creation using xls writer

    output = BytesIO()
    # workbook = xlsxwriter.Workbook(f'./Settlement/{settlement_name}.xlsx')
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet(f'{settlement_name} Report')
    bold = workbook.add_format({'bold': True})
    center = workbook.add_format({'align': 'center'})

    row = 0
    col = 0
    worksheet.insert_image('A2', f'./Settlement/leanpay_logo_only.png')
    worksheet.write(row + 4, col + 3, 'Report ID', bold)
    worksheet.write(row + 4, col + 4, f'LP-{SharedUtils.my_random_string(10)}-MM')

    worksheet.write(row + 4, col + 6, 'Report Requester', bold)
    worksheet.write(row + 4, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 1, col + 3, 'Report Title', bold)
    worksheet.write(row + 4 + 1, col + 4, 'SETTLEMENT REPORT')

    worksheet.write(row + 4 + 1, col + 6, 'Report Author', bold)
    worksheet.write(row + 4 + 1, col + 7, 'Leanis Solution Sdn. Bhd.')

    worksheet.write(row + 4 + 2, col + 3, 'Settlement Report Date', bold)
    worksheet.write(row + 4 + 2, col + 4, datetime.datetime.now().date().isoformat())

    worksheet.write(row + 4 + 3, col + 3, 'Settlement Generated Date', bold)
    worksheet.write(row + 4 + 3, col + 4, datetime.datetime.now().isoformat())

    worksheet.write(row + 13, col, 'RECORD_SEQUENCE', bold)
    worksheet.write(row + 13, col + 1, 'MERCHANT_NAME', bold)
    worksheet.write(row + 13, col + 2, 'MERCHANT_ID', bold)
    worksheet.write(row + 13, col + 3, 'TOTAL_PREFUND_SALE', bold)
    # End of Excel template writing
    total = 0
    try:
        if list_data:
            for x in range(1, len(list_data) + 1):
                total = total + list_data[x - 1]["TOTAL_PREFUND_SALE"]
                worksheet.write(row + 13 + x, col, x)
                worksheet.write(row + 13 + x, col + 1, list_data[x - 1]["MERCHANT_NAME"])
                worksheet.write(row + 13 + x, col + 2, list_data[x - 1]["MERCHANT_ID"])
                worksheet.write(row + 13 + x, col + 3, list_data[x - 1]["TOTAL_PREFUND_SALE"])
        worksheet.write(row + 13 + len(list_data) + 8, col, 'END_OF_REPORT', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 7, 'GRAND_TOTAL', bold)
        worksheet.write(row + 13 + len(list_data) + 6, col + 8, f'{total}')
        workbook.close()
        output.seek(0)
        return output

    except Exception as e:
        raise HTTPException(
            status_code=400, detail=e
        )
